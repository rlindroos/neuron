COMMENT
Spike detection mechanism for vizualization of synaptic input onto the morphology

ENDCOMMENT


NEURON {
	THREADSAFE
    SUFFIX spikeMonitor
    RANGE spike, fake
    POINTER I
}

PARAMETER {
    fake    = 0
}


ASSIGNED { 
    I
    spike
}



INITIAL {
	I       = 0
	spike   = 0
}


BREAKPOINT {
    spike = I
}



