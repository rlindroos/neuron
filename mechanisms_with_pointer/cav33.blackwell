TITLE T-type calcium channel (Cav3.2)

UNITS {
    (mV) = (millivolt)
    (mA) = (milliamp)
    (S) = (siemens)
    (molar) = (1/liter)
    (mM) = (millimolar)
    FARADAY = (faraday) (coulomb)
    R = (k-mole) (joule/degC)
}

NEURON {
    SUFFIX cav31
    USEION cal READ cali, calo WRITE ical VALENCE 2
    RANGE pbar, ical, gca
}

PARAMETER {
    pbar   =   0.0  (cm/s)
    mvhalf = -63.0  (mV)
    mslope =  -8    (mV)
    hvhalf = -86    (mV)
    hslope =   5    (mV)  
    q      =   2        : Jedrzejewska-Szmek et al., 2017 (globals.g)
}

ASSIGNED { 
    v (mV)
    ical (mA/cm2)
    gca
    ecal (mV)
    celsius (degC)
    cali (mM)
    calo (mM)
    minf
    hinf
    theta
    beta
    mA
    mB
    mtau (ms)
    htau (ms)
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    gca = pbar*m*m*m*h
    ical = ghk(v, cali, calo)*gca
}

INITIAL {
    rates(v)
    m = minf
    h = hinf
}

DERIVATIVE states { 
    rates(v)
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}

PROCEDURE rates(v (mV)) {
    minf        = 1/(1+exp((v-mvhalf)/mslope))
    hinf        = 1/(1+exp((v-hvhalf)/hslope)) 
     
    theta       = 14552*( (v)+84.5 )
	beta        = exp(  ( (v)+84.5 )/7.12 ) -1.0
	mA          = theta/beta
    mB          = 4984.2 * exp((v) / 13.0)
    mtau        = 1.0 / (mA + mB) + 2.2  
    
    theta       = 2652*( (v)+94.5 )
	beta        = exp( ( (v)+94.5 )/5.12 ) -1.0
	mA          = theta/beta
    mB          = 4984.2 * exp((v) / 13.0)
    htau        = 1.0 / (mA + mB) + 100
}

FUNCTION ghk(v (mV), ci (mM), co (mM)) (.001 coul/cm3) {
    LOCAL z, eci, eco
    z = (1e-3)*2*FARADAY*v/(R*(celsius+273.15))
    if(z == 0) {
        z = z+1e-6
    }
    eco = co*(z)/(exp(z)-1)
    eci = ci*(-z)/(exp(-z)-1)
    ghk = (1e-3)*2*FARADAY*(eci-eco)
}

COMMENT 

Original data by Iftinca (2006) , rat, 37 C.

Genesis implementation by Kai Du (21 C) <kaidu828@gmail.com> m^2*h.

Revised Genesis model by Robert Lindroos <robert.lindroos@ki.se>, m*h, 37 C.

NEURON implementation by Alexander Kozlov <akozlov@nada.kth.se>, smooth
fit of mtau and htau.

Revised NEURON model by Robert Lindroos -> 37 C 

ENDCOMMENT
