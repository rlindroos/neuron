TITLE random factor generator

COMMENT

draws a random factor from a uniform distribution on initialization.
The factor is also mapped onto a modulation range.

ENDCOMMENT

NEURON {
    THREADSAFE
    POINT_PROCESS random_factor
    RANGE null_pointer, minMod, maxMod, factor    
}

PARAMETER {
    null_pointer    = 0        : switch to create factor as zero
    minMod          = 0.99
    maxMod          = 1.01
    factor          = 0.0
} 


BREAKPOINT { }


INITIAL {
    LOCAL rand, diff
    
    if (null_pointer == 0) { 
        
        diff   = maxMod - minMod
        
        : draw random uniform value from 0:1
        rand = scop_random()
        
        : map to modulation range from literature
        factor = (minMod + diff*rand) - 1
    }
    
}


