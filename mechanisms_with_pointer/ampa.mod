: this model is built-in to neuron with suffix ampa
: Schaefer et al. 2003

COMMENT
modified from epsp Hay et al (2006)

ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS ampa
	RANGE onset, tau, gmax, Erev, i
	NONSPECIFIC_CURRENT i
}
UNITS {
	(nA)   = (nanoamp)
	(mV)   = (millivolt)
	(umho) = (micromho)
}

PARAMETER {
	onset = 200  (ms)    : activation time
	tau1  = 1.9  (ms)    : rise time constant
	tau2  = 4.8  (ms)    : decay time constant
	gmax  = 5e-3 (uS)    : maximum conductance        
	Erev  = 0    (mV)    : reversal potential
	v	         (mV)
}

ASSIGNED { 
    i (nA)
    SF 
}

INITIAL {
    LOCAL Tpk
    
	Tpk = tau1*tau2 / (tau2 - tau1) * log(tau2/tau1)
	SF  = 1 / ( exp(-Tpk/tau2) - exp(-Tpk/tau1) )
}


BREAKPOINT {
    i = gmax * g(t) * (v - Erev)
}


FUNCTION g(x) {	
    
    LOCAL a1, a2, e1, e2
    
    : returns conductance			
	
	if (x < onset) {
		g = 0
	}else{
	
	    a1 = -(x - onset) / tau1
        e1 =  exp(a1)
        
        a2 = -(x - onset) / tau2
        e2 =  exp(a2)
        
        : SF = scale factor
        g  =  SF * (e2 - e1) 
        
	}
}
