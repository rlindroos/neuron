
import pickle
import matplotlib.pyplot as plt
import numpy as np


# load data
def load_obj(name ):
    '''
    functions used to load data in the pickle format. 
    Used to un-pickle data from dictionaries
    
    name    = file name of the pickled data, including ending
    '''
    
    with open(name, 'rb') as f:
        return pickle.load(f)
        
data = load_obj('platau_data_pickled.pkl')

# check and plot

time = data['time']


count  = 0
thresh = 13
AMP  = {'success':[], 'fail':[], 'all':[]}
DIST = {'success':[], 'fail':[], 'all':[]}
DUR  = {'success':[], 'fail':[], 'all':[]}
Ca   = {'success':[], 'fail':[], 'all':[]}
Cal  = {'success':[], 'fail':[], 'all':[]}
PEAK = {'cai':[], 'cali':[]}
fig,ax = plt.subplots(3,1, figsize=(4,8) )

for secID in range(58):
    
    amp  = data[secID]['amp']
    dur  = data[secID]['dur'] 
    vm   = data[secID]['voltage']
    i    = data[secID]['peak_index'][0]
    dist = data[secID]['dist']
    cai  = data[secID]['cai']
    cali = data[secID]['cali']
    
    # plot
    #ax[0].plot(time, cai)
    #ax[1].plot(time, cali)
    
    # get peak ca
    PEAK['cai' ].append( max(cai)  )
    PEAK['cali'].append( max(cali) )
     
    
    DIST['all'].append(dist)
    AMP['all'].append(amp)
    DUR['all'].append(dur)
    
    base = vm[0]
    half_vm = base + amp /2.0
    
    above_half_max_index = [j for j,v in enumerate(vm) if v >= half_vm]
    
    if amp >= thresh and dur >= 50:
        if len(DIST['success']) == 0:
            # initiate array
            array = np.array(vm)
        else:
            array = np.vstack((array, np.array(vm))) 
        c = 'r'
        count += 1
        DIST['success'].append( dist )
        AMP['success'].append(amp)
        DUR['success'].append(dur)
        #plt.plot(time, vm, lw=2, c='r')
        if dur > 80 and amp > 13:
            index = count-1
            print dur, index
    else:
        c = 'k'
        DIST['fail'].append( dist)
        AMP[ 'fail'].append( amp )
        DUR[ 'fail'].append( dur )
    
    '''
    if thresh == 19:
        plt.plot([time[i],time[i]], [base, base+amp], '--k')
        plt.plot([time[above_half_max_index[0]], time[above_half_max_index[-1]]], [half_vm, half_vm], '--k')'''

added   = np.add(PEAK['cali'],PEAK['cai'])
average = np.divide(added, 2)
print len(average), len(DIST['all'])
ax[0].plot( DIST['all'], average,      'o', ms=15, mew=1, c='m', mec='w' )
ax[1].plot( DIST['all'], PEAK['cai'],  'o', ms=15, mew=1, c='b', mec='w' )
ax[2].plot( DIST['all'], PEAK['cali'], 'o', ms=15, mew=1, c='r', mec='w' )
ax[0].set_title('Average Ca conc')
ax[1].set_title('NPQR pool')
ax[2].set_title('LT, nmda & ampa pool')

fig.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/plateaus_Ca_vs_dist%d.png' % (thresh), transparent=True)
#plt.show()

print 100 * count / 58, vm[0]

plt.figure(figsize=(6,3))
shift = -0.4
plt.fill_between([200,220], [-80.0+shift,-80.0+shift], [-68,-68], color='grey', alpha=0.6)     # stim range
plt.plot([200,500], [-80.0+shift,-80.0+shift], lw=3, ls='--', c='k')

# calc mean and std and plot
mean = np.mean(array,0)
std  = np.std( array,0)
#plt.fill_between(time, mean-std, mean+std, color='r', alpha=0.3)
plt.plot(time, array[index,:], color='k', lw=3)

# experimental data
[x2,y2] = np.loadtxt('../../results/plateau/du_plateau.csv', unpack=True)
time = [t*(-1)+218         for t in x2]
vm   = [v*(-1)-143.2+shift for v in y2]
plt.plot(time, vm, c='r', lw=3)
plt.plot([400,500], [-75,-75], lw=3, c='k')
plt.plot([400,400], [-75,-70], lw=3, c='k')

plt.xlim([150,500])
plt.axis('off')

plt.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/plateaus_thresh%d.png' % (thresh), transparent=True)

plt.figure()
plt.hist( DIST['success'], color='r', histtype='stepfilled', normed=True, alpha=0.8)
plt.hist( DIST['fail'], color='k', histtype='stepfilled', normed=True, alpha=0.8 )
plt.hist( DIST['success'], color='r', histtype='step', normed=True, lw=3)
plt.hist( DIST['fail'], color='k', histtype='step', normed=True, lw=3 )
plt.yticks([])

#plt.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/plateaus_dist_thresh%d.png' % (thresh), transparent=True)


plt.figure()
plt.plot(DIST['all'], AMP['all'], 'o', color='k', ms=25, alpha=0.5, mew=4, mec='w')
plt.plot([0, 300], [thresh, thresh], '--r', lw=3)
v = plt.axis()
plt.ylim([0, v[3]])
plt.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/plateaus_amp_dist.png', transparent=True)
plt.figure()
plt.plot(DIST['all'], DUR['all'], 'o', color='k', ms=25, alpha=0.5, mew=4, mec='w')
plt.plot([0, 300], [50, 50], '--r', lw=3)
v = plt.axis()
plt.ylim([0, v[3]])
plt.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/plateaus_dur_dist.png', transparent=True)

plt.close('all')





