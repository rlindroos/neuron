import json
import pandas
import efel
import pickle
import glob
import numpy as np
import bluepyopt as bpopt
import bluepyopt.ephys as ephys

from utils import *

import matplotlib.pyplot as plt

import cell_model, cell_evaluator
morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
parameters = cell_model.define_parameters('parameters.json')
ca_params  = cell_model.define_parameters('parameters_ca.json')
mechanisms = cell_model.define_mechanisms('mechanisms.json')
ca_mech    = cell_model.define_mechanisms('mechanisms_ca.json')
parameters = parameters + ca_params
mechanisms = mechanisms + ca_mech
protocols  = cell_evaluator.define_protocols('protocols-val.json')
calculator = cell_evaluator.define_fitness_calculator(protocols, 'features-val.json')


''' plotting paramters '''
fs      = (6,4)
color   = {'exp':'red',  'sim':'black', 'exp_pop':'grey'}

''' ------------------ '''


expdata = {'IDthresh-sub_547.soma.v': 'expdata/ECBL_IDthresh_ch3_547.dat',}
for p in protocols:
    key = '%s.soma.v' % (p)
    t, i = p.split('_')
    t = t.replace('-sub', '')
    val = 'expdata/ECBL_%s_ch3_%s.dat' % (t,i)
    expdata[key] = val
    #print key, '\t', val

morphology = ephys.morphologies.NrnFileMorphology(
    morphofile, do_replace_axon=True)

### this code is (slighlty updated) from the BluePyOpt/examples/expsyn/ExpSyn.ipynb notebook:
# https://github.com/BlueBrain/BluePyOpt/blob/master/examples/expsyn/ExpSyn.ipynb

# NEURON simulator
nrn_sim = ephys.simulators.NrnSimulator()

# Object that points to sectionlist somatic
somatic_loc = ephys.locations.NrnSeclistLocation('somatic',seclist_name='somatic')

# Object that points to the center of the soma
somacenter_loc = ephys.locations.NrnSeclistCompLocation(
    name='somacenter',
    seclist_name='somatic',
    sec_index=0,
    comp_x=0.5)


# Add ExpSyn synapse pointprocess at the center of the soma
expsyn_mech = ephys.mechanisms.NrnMODPointProcessMechanism(                     
    name='expsyn',                                                              
    suffix='ExpSyn',                                                            
    locations=[somacenter_loc])

expsyn_mech.tau1 = 2.2
expsyn_mech.tau2 = 12.0
expsyn_mech.e    = 0

# Once we have defined a point process, we can create a Location object that points to it
expsyn_loc = ephys.locations.NrnPointProcessLocation(                           
    'expsyn_loc',                                                               
    pprocess_mech=expsyn_mech)


cell = ephys.models.CellModel(
    'dspn', 
    morph=morphology, 
    mechs=mechanisms+[expsyn_mech], 
    params=parameters)
#print cell

# Now we need to define the fitness function. 
# (The idea is to stimulate the synapse 5 times, and let the resulting train of EPSPs reach exactly -50 mV) 
# no optimization used.

#We first create a stimulus that injects the presynaptic events:
stim_start = 20
number = 5
interval = 5

netstim = ephys.stimuli.NrnNetStimStimulus(                                  
    total_duration=200,                                                      
    number=5,                                                                
    interval=5,                                                              
    start=stim_start,                                                        
    weight=5e-4,                                                             
    locations=[expsyn_loc])

stim_end = stim_start + interval * number

rec = ephys.recordings.CompRecording(
    name='soma.v', 
    location=somacenter_loc,
    variable='v')

protocol = ephys.protocols.SweepProtocol('netstim_protocol', [netstim], [rec])

opt_params = [p.name for p in cell.params.values() if not p.frozen]
#for x in sorted(opt_params): print x

simulator = ephys.simulators.NrnSimulator()

evaluator = ephys.evaluators.CellEvaluator(
    cell_model=cell,
    param_names=opt_params,
    fitness_protocols=protocols,
    fitness_calculator=calculator,
    sim=simulator)

best_models = json.load(open('best_models.json'))
ind = 3 # 3, 1, 2, 5, 6
test_parameters = best_models[ind]

run_evaluator = False
if run_evaluator:
    test_responses = evaluator.run_protocols(
        protocols=protocols.values(), 
        param_values=test_parameters)
    # pickle test_responses
    with open('test_responses.pkl', 'wb') as f:
        pickle.dump(test_responses, f, pickle.HIGHEST_PROTOCOL)
else:
    with open('test_responses.pkl', 'rb') as f:
        test_responses = pickle.load(f)

def extract_features(Tlist,Ylist, start=100, end=1100, markers=[]):
    # extracts electrical features from trace using bbp efel library
    
    
    # set interpolation frequency
    #efel.setDoubleSetting('interp_step', 0.05)
    #efel.setDoubleSetting('DerivativeThreshold', 30.0)
    
    traces = []
    
    for i,y in enumerate(Ylist):
        # code based on example example code from: https://github.com/BlueBrain/eFEL --------------------

        # A 'trace' is a dictionary
        trace1 = {}

        # Set the 'T' (=time) key of the trace
        trace1['T'] = Tlist[i]

        # Set the 'V' (=voltage) key of the trace
        trace1['V'] = y

        # Set the 'stim_start' (time at which a stimulus starts, in ms)
        # key of the trace
        # Warning: this need to be a list (with one element)
        trace1['stim_start'] = [start]

        # Set the 'stim_end' (time at which a stimulus end) key of the trace
        # Warning: this need to be a list (with one element)
        trace1['stim_end'] = [end]

        # Multiple traces can be passed to the eFEL at the same time, so the
        # argument should be a list
        traces.append(trace1)
        
        print markers[i]

    # Now we pass 'traces' to the efel and ask it to calculate the feature
    # values
    #print markers[i]
    if 'IV' in markers[i]:
        return efel.getFeatureValues(  traces,
                                    ['steady_state_voltage_stimend'])
    else:
        return efel.getFeatureValues(  traces,
                                    ['mean_frequency', 'inv_first_ISI'])
    
        
        
def getSpikedata_x_y(x,y, threshold=0.0):
    ''' 
    There's probably a Neuron function for this (e.g. netcon)--use instead? 
    
    getSpikedata_x_y(x,y) -> return spike times as a list
    
    Extracts and returns the spikes from spike trace data.
    
    extraction algorithm:
    -extract list containing index for all points in y larger than threshold.
    -sorts out the index(es) that are the first one(s) crossing the threshold, i.e. the 
        first index of each spike, and stores the timing of the event (x[index]). 
    This is done by looping over all index and check if the index is equal to the previous 
        index + 1. 
    If not it is the first index of a spike.
        
    If no point is above threshold in the trace the function returns an empty list [].
    
    x           = time vector
    y           = vm vector
    threshold   = min amplitude used for spike detection.
                  If the amplitude of the spike is lower than threshold spikes won't be
                    detected.
                  If the Threshold is not crossed in the repolarizing phase, no new spike 
                    can be detected.
    '''
    
    #count = 0
    spikes = []
    
    # pick out index for all points above zero potential for potential trace
    spikeData = [i for i,v in enumerate(y) if v > threshold]

    # if no point above 0
    if len(spikeData) == 0:
        
        return spikes

    else:
        # pick first point of each individual transient (spike)...
        for j in range(0, len(spikeData)-1):
            if j==0:
                
                #count += 1
                spikes.append(x[spikeData[j]])

            # ...by checking above stated criteria
            elif not spikeData[j] == spikeData[j-1]+1:
                #count += 1
                spikes.append(x[spikeData[j]])
            
    return spikes 



def get_voltage_deflection(t, y, t_startstim=100, t_endstim=1100, dur=200):
    
    # get base value index
    init = next(i for i,x in enumerate(t) if x >= t_startstim) -1
    
    # get index of time trace (default stim ends = 1100 ms and stimulation length is last 200 ms)
    iend = next(i for i,x in enumerate(t) if x >= t_endstim) - 1
    istart = next(i for i,x in enumerate(t) if x >= t_endstim-dur)
    
    # return mean over interval def by [istart, iend]
    return [y[init], np.mean(y[istart:iend])]
    


    
def def_axis(ax, axis=['left'], tpw=2, tpl=2):
    
    ax.tick_params(width=tpw, length=tpl) 
    
    for side in ['left', 'right']:
        if side in axis:
            ax.yaxis.set_ticks_position(side)
    for side in ['top', 'bottom']:
        if side in axis:
            ax.xaxis.set_ticks_position(side)
    for side in ['left', 'right', 'top', 'bottom']:
        if side in axis:
            ax.spines[side].set_linewidth(tpl)
        else:
            ax.spines[side].set_visible(False)
    
    
    if not 'top' in axis and not 'bottom' in axis:
        ax.set_xticks([])
    if not 'left' in axis and not 'right' in axis:
        ax.set_yticks([])
    
     
        

def plot_function(responses, expdata=[], junction_potential=0):
    
    
    f_IV_exp, a_IV_exp = plt.subplots(1,1)
    f_IV_sim, a_IV_sim = plt.subplots(1,1)
    f_IV_both, a_IV_both = plt.subplots(2,1, figsize=fs, gridspec_kw = {'height_ratios':[5, 2]})
    f_Th, a_Th         = plt.subplots(2,1, figsize=fs, gridspec_kw = {'height_ratios':[9, 1]})
    f_FI, a_FI         = plt.subplots(1,1, figsize=(6,9))
    f_IV, a_IV         = plt.subplots(1,1)
    
    # stimulation paradigm
    f_IV_stim, a_IV_stim = plt.subplots(1,1, figsize=(6,3))
    f_Th_stim, a_Th_stim = plt.subplots(1,1, figsize=(6,3))
    
    ax_IV      = {'exp':a_IV_exp, 'sim':a_IV_sim}
    SPIKES     = {'exp':[], 'sim':[], 'I':[]}
    POTENTIAL  = {'exp':[], 'sim':[], 'I':[]}
    DEFLECTION = {'exp':[], 'sim':[], 'I':[]}      
    
    lw         = 2
    
    first_spiking = {'exp':True,  'sim':True}
    
    TRACES =  {  'IV':{'exp':[],  'sim':[], 'name':[], 'time':{}},
                 'th':{'exp':[],  'sim':[], 'name':[], 'time':{}} }
    
    for index, (name, response) in enumerate(sorted(responses.items())):
        
        print index, name
        
        for tech in ['exp','sim']:    
            if tech == 'exp':
                time,voltage = np.loadtxt(expdata[name], unpack=True)
                voltage = voltage - junction_potential
                n = name.split('.')[0]
                I = float(protocols[n].stimuli[0].step_amplitude)*1000
                if 'IV' in name:
                    POTENTIAL[ 'I'].append( I )
                    DEFLECTION['I'].append( I )
                else:
                    SPIKES['I'].append( I )
            else:
                time = response['time']
                voltage = response['voltage']
            
            if 'IV' in name:
                ax_IV[tech].plot( time, voltage, color=color[tech], lw=lw)
                a_IV_both[0].plot(time, voltage, color=color[tech], lw=lw)
                # get voltage deflection for IV curve
                vinit,vend = get_voltage_deflection(time, voltage) 
                POTENTIAL[tech].append(vend)
                DEFLECTION[tech].append(vend-vinit)
                IVth = 'IV'
                
            else:
                IVth = 'th'
                # extract spikes
                spikes = getSpikedata_x_y(time, voltage)
                if len(spikes) == 0:
                    print '---zero spikes--', name, tech
                    SPIKES[tech].append( 0 )
                else: 
                    SPIKES[tech].append( 1000.0*(len(spikes)-1)/(spikes[-1]-spikes[0]) )
                    if first_spiking[tech]:
                        #ax_Th[tech].plot(time, voltage, color=color[tech], lw=lw)
                        a_Th[0].plot(       time, voltage, color=color[tech], lw=lw)
                        first_spiking[tech] = False
                        print '*****', name
            
            # add traces to list ---------------------------------------------------
            
            if tech not in TRACES[IVth]['time']:
                TRACES[IVth]['time'][tech] = [time]
            else:
                TRACES[IVth]['time'][tech].append(time)
            
            if tech == 'exp':
                TRACES[IVth]['name'].append(name)
                   
            TRACES[IVth][tech].append(voltage)
            
            
            
    
    VALUES = {}
    for i,t in enumerate(['IV', 'th']):
        
        VALUES[t] = {}
        
        if t == 'IV':
            start = 100
            end  = 1000
        else:
            start = 700
            end  = 2700
            
        for tech in ['exp','sim']:
            #print t, tech
            # call ephys here --------------------------------------------
            VALUES[t][tech] = extract_features( TRACES[t]['time'][tech], 
                                                TRACES[t][tech], 
                                                start=start, 
                                                end=end, 
                                                markers=TRACES[t]['name'])
                
                        
    # import planert and plot in grey
    for i,f in enumerate(glob.glob('../D1-MSN_wip/Exp_data/FI/*D1*') ):
        [x_i,y_i] = np.loadtxt(f, unpack=True)
        a_FI.plot(x_i, y_i, color='lightgrey', lw=3)
    '''
    for i,f in enumerate(glob.glob('../D1-MSN_wip/Exp_data/FI/*D2*') ):
        [x_i,y_i] = np.loadtxt(f, unpack=True)
        a4.plot(x_i, y_i, color='lightgrey', lw=3)'''
    
    # get current for these stim
    a_FI.plot(SPIKES['I'], SPIKES['exp'], color=color['exp'], lw=3)
    a_FI.plot(SPIKES['I'], SPIKES['sim'], color=color['sim'], lw=3)
    a_IV.plot(DEFLECTION['I'], DEFLECTION['exp'], color=color['exp'], lw=3)
    a_IV.plot(DEFLECTION['I'], DEFLECTION['sim'], color=color['sim'], lw=3)
    
    ya=-30
    xa=700
    shift=100
    xl=500
    yl=50
    for ax in [a_Th[0]]:
        ax.plot([xa-xl+shift,xa+shift],[ya,ya], color='black')
        ax.plot([xa+shift,xa+shift],[ya,ya+yl],  color='black')
        ax.axis('off')
        ax.set_ylim([-90,50])
        ax.set_xlim([xa-xl+shift,xa+2000+300])
        
    # plot stimulation
    lwstim=2
    for ax in [a_Th_stim, a_Th[1]]:
        ax.plot([xa-xl, xa], [0, 0], 'grey', lw=lwstim)
        ax.plot([xa,  xa], [0, SPIKES['I'][1]], 'grey', lw=lwstim)
        ax.plot([xa, xa+2000], [SPIKES['I'][1],SPIKES['I'][1]], 'grey', lw=lwstim)
        ax.plot([xa+2000,xa+2000], [SPIKES['I'][1], 0], 'grey', lw=lwstim)
        ax.plot([xa+2000,xa+2000+300], [0, 0], 'grey', lw=lwstim)
        ax.axis('off')
        ax.set_xlim([xa-xl+shift,xa+2000+300])
        
    for ax in [a_IV_stim, a_IV_both[1]]:
        for I in DEFLECTION['I']:
            ax.plot([100, 1100], [I,I], 'grey', lw=lwstim)
        ax.plot([0, 1300], [0,0], 'grey', lw=lwstim)
        ax.plot([100, 100],   [min(DEFLECTION['I']),max(DEFLECTION['I'])], 'grey', lw=lwstim)
        ax.plot([1100, 1100], [min(DEFLECTION['I']),max(DEFLECTION['I'])], 'grey', lw=lwstim)
        ax.axis('off')
        
    for ax in ax_IV:
        #ax.plot([100, 550], [0, 0], 'k', lw=6)
        #ax.plot([100, 100], [25, 30], 'k', lw=3)
        #ax.plot([100, 200], [30, 30], 'k', lw=3)
        ax_IV[ax].set_xlim([50,1300])
        ax_IV[ax].set_ylim([-110,-65])
        #def_axis(ax_IV[ax], axis=['left'], tpw=2, tpl=4)
        #ax.axis('off')
    for ax in a_IV_both:
        ax.set_xlim([50,1300])
    a_IV_both[0].set_ylim([-110,-65])
    def_axis(a_IV_both[0], axis=['left'], tpw=2, tpl=2)
    
    f_IV_exp.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IV_exp_curve_dspn.pdf', transparent=True)
    f_IV_sim.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IV_sim_curve_dspn.pdf', transparent=True)
    f_Th.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/Th_curve_dspn.pdf', transparent=True)
    f_FI.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IF_curve_dspn.pdf', transparent=True)
    f_IV.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IV_extracted_curve_dspn.pdf', transparent=True)
    f_IV_stim.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IV_stim_dspn.pdf', transparent=True)
    f_IV_both.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IV_both_curve_dspn.pdf', transparent=True)
    plt.close('all')
    
    return VALUES, {'IV':DEFLECTION['I'], 'th':SPIKES['I']}


                      
VALUES, CURRENT = plot_function(test_responses, expdata=expdata, junction_potential=9.5)    

print VALUES['th']['exp']

cell_ids = ['150602_c1_D1', '150602_c9_D1', '150603_c7_D1', 
            '150908_c5_D1', '150908_c6_D1', '150917_c10_D1', 
            '150917_c6_D1', '150917_c9_D1', '151123_c2_D1', 
            '151123_c4_D1', '160118_c7_D1', '160118_c8_D1', 
            '160118_c9_D1']
path_to_expdata = 'jsons/'

fig1, ax1 = plt.subplots(1,1, figsize=fs)
fig2, ax2 = plt.subplots(1,1, figsize=fs)
fig3, ax3 = plt.subplots(1,1, figsize=fs)
'''
fig.suptitle('Cell variability, D1 MSN (n={})'.format(len(cell_ids)))
ax1.set_title('Subthreshold response')
ax2.set_title('Steady-state rate')
ax3.set_title('Initial rate')
ax1.set_ylabel('Voltage (mV)')
ax2.set_ylabel('Frequency (Hz)')
ax3.set_ylabel('Frequency (Hz)')
ax1.set_xlabel('Current (pA)')
ax2.set_xlabel('Current (pA)')
ax3.set_xlabel('Current (pA)')'''
for cell in cell_ids:
    spec = json.load(open(path_to_expdata+cell+'-spec.json'))
    x = [s[0]['stimulus_amp'] for s in spec['cell_features']['IV']]
    y = [s[1]['steady_state_voltage_stimend'][0] for s in spec['cell_features']['IV']]
    ax1.plot(x, y, color=color['exp_pop'])
    x = [s[0]['stimulus_amp'] for s in spec['cell_features']['IDthresh']]
    y = [s[1]['mean_frequency'][0] for s in spec['cell_features']['IDthresh']]
    ax2.plot(x, y, color=color['exp_pop'])
    y = [s[1]['inv_first_ISI'][0] for s in spec['cell_features']['IDthresh']]
    ax3.plot(x, y, color=color['exp_pop'])

# import planert and plot in grey
for i,f in enumerate(glob.glob('../D1-MSN_wip/Exp_data/FI/*D1*') ):
    [x_i,y_i] = np.loadtxt(f, unpack=True)
    #ax2.plot(x_i, y_i, color='lightgrey', ls='--')
    ax3.plot(x_i, y_i, color='lightgrey', ls='--')


for t,tech in enumerate(['exp','sim']):
    
    # IV 
    y1 = []
    for res in VALUES['IV'][tech]:
        y1.append( res['steady_state_voltage_stimend'] )
    ax1.plot(CURRENT['IV'], y1, color[tech], lw=3)
    
    # IF mean
    y2 = []
    for res in VALUES['th'][tech]:
        if not res['mean_frequency']:
            y2.append( 0 )
        else:
            y2.append( res['mean_frequency'] )
    ax2.plot(CURRENT['th'], y2, color[tech], lw=3)
    
    # IF inv. first ISI
    y3 = []
    for res in VALUES['th'][tech]:
        y3.append( res['inv_first_ISI'] )
    ax3.plot(CURRENT['th'], y3, color[tech], lw=3)

def_axis(ax1, axis=['left', 'bottom'], tpw=2, tpl=2)
def_axis(ax2, axis=['left', 'bottom'], tpw=2, tpl=2)
def_axis(ax3, axis=['left', 'bottom'], tpw=2, tpl=2)
fig1.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IV_tot.png', transparent=True)
fig2.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IF_mean_tot.png', transparent=True)
fig3.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/IF_first_tot.png', transparent=True)
plt.show()
    

