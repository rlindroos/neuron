#!/usr/bin/env python

"Model variability by combination of optimized sets of intrinsic parameters and morphological reconstructions"

from __future__ import division, print_function
import argparse

def parse():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("parameters", type=str, help="optimized free parameters (JSON)")
    parser.add_argument("morphology", type=str, help="reconstructed morphology file (SWC)", nargs='+')
    return parser.parse_args()

args = parse()
var_models = []
cell_id = '150917_c10_D1'
count = 0

for morphofile in args.morphology:
    import cell_model, cell_evaluator

    parameters = cell_model.define_parameters('parameters.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    protocols = cell_evaluator.define_protocols('protocols-val.json')
    calculator = cell_evaluator.define_fitness_calculator(protocols, 'features-val.json')

    import json
    import efel
    import numpy as np
    import bluepyopt as bpopt
    import bluepyopt.ephys as ephys

    morphology = ephys.morphologies.NrnFileMorphology(morphofile, do_replace_axon=True)

    cell = ephys.models.CellModel(
        'dspn',
        morph=morphology,
        mechs=mechanisms,
        params=parameters)
    opt_params = [p.name for p in cell.params.values() if not p.frozen]
    simulator = ephys.simulators.NrnSimulator()
    evaluator = ephys.evaluators.CellEvaluator(
        cell_model=cell,
        param_names=opt_params,
        fitness_protocols=protocols,
        fitness_calculator=calculator,
        sim=simulator)

    test_models = json.load(open(args.parameters))

    ind = 0

    for test_parameters in test_models:
        ind += 1
        count += 1
        test_responses = evaluator.run_protocols(
            protocols=protocols.values(),
            param_values=test_parameters)

        iv_responses = {k:test_responses[k] for k in test_responses if k.startswith('IV_')}
        id_responses = {k:test_responses[k] for k in test_responses if k.startswith('IDthresh_')}
        protocols_val=json.load(open('config/protocols-val.json'))

        iv_protocols = {k:protocols_val[k] for k in protocols_val if k.startswith('IV_')}

        iv_stimuli = []
        for p in sorted(iv_protocols):
            amp0 = iv_protocols[p]['stimuli'][0]['amp']
            amp1 = iv_protocols[p]['stimuli'][1]['amp']
            stim = (amp0 + amp1)*1e3
            iv_stimuli.append(stim)

        traces = []
        features = ['steady_state_voltage_stimend']
        for resp in sorted(iv_responses):
            trace = {}
            trace['T'] = iv_responses[resp]['time']
            trace['V'] = iv_responses[resp]['voltage']
            trace['stim_start'] = [100]
            trace['stim_end'] = [1100]
            traces.append(trace)
        feature_values = efel.getFeatureValues(traces, features)

        iv_volts = [f['steady_state_voltage_stimend'][0] for f in feature_values]

        from scipy.optimize import curve_fit

        def fit(x, a, b, c, d):
            return a + b*x + np.exp((x-c)/d)

        def dfit(x, a, b, c, d):
            return b + np.exp((x-c)/d)/d

        xx = np.array(iv_stimuli)
        yy = np.array(iv_volts)
        popt, pcov = curve_fit(fit, xx, yy, bounds=([-10000,0,-10000,1], [0,0.1,1000,1000]))

        cell_features = {}

        cell_features['vm_rest'] = fit(0, *popt)

        slope = dfit(xx, *popt)
        cell_features['ir_fitmin'] = min(slope)*1e3
        cell_features['ir_fitmax'] = max(slope)*1e3
        cell_features['ir_fitrest'] = dfit(0, *popt)*1e3

        id_protocols = {k:protocols_val[k] for k in protocols_val if k.startswith('IDthresh_')}

        id_stimuli = []
        for p in sorted(id_protocols):
            amp0 = id_protocols[p]['stimuli'][0]['amp']
            amp1 = id_protocols[p]['stimuli'][1]['amp']
            stim = (amp0 + amp1)*1e3
            id_stimuli.append(stim)

        traces = []
        features = ['AHP_depth', 'AHP_depth_abs', 'AHP_depth_abs_slow',
                    'AHP_slow_time', 'AHP_time_from_peak', 'AP_amplitude',
                    'AP_begin_voltage', 'AP_fall_rate', 'AP_rise_rate',
                    'ISI_CV', 'Spikecount', 'adaptation_index2',
                    'inv_first_ISI', 'mean_frequency',
                    'min_voltage_between_spikes', 'peak_voltage',
                    'spike_half_width', 'steady_state_voltage',
                    'time_to_first_spike', 'voltage_base']
        for resp in sorted(id_responses):
            trace = {}
            trace['T'] = id_responses[resp]['time']
            trace['V'] = id_responses[resp]['voltage']
            trace['stim_start'] = [700]
            trace['stim_end'] = [2700]
            traces.append(trace)
        spikes = efel.getFeatureValues(traces, ['Spikecount'])
        if not np.all(np.array([x['Spikecount'][0] for x in spikes]) >= 4):
            continue
        feature_values = efel.getFeatureValues(traces, features)

        f_i = []
        f_i1 = []
        for feat in feature_values:
            f_i.append(feat['mean_frequency'][0])
            f_i1.append(feat['inv_first_ISI'][0])

        xx = np.array(id_stimuli)
        yy = np.array(f_i)
        p = np.polyfit(xx, yy, 1)
        x1 = np.array(id_stimuli)
        y1 = np.array(f_i1)
        p1 = np.polyfit(x1, y1, 1)

        cell_features['fi_avg_minstim'] = id_stimuli[0]
        cell_features['fi_avg_fitslope'] = p[0]
        cell_features['fi_avg_fitbaserate'] = p[1]+p[0]*xx[0]

        cell_features['fi_1st_minstim'] = id_stimuli[0]
        cell_features['fi_1st_fitslope'] = p1[0]
        cell_features['fi_1st_fitbaserate'] = p1[1]+p1[0]*x1[0]

        cell_features['fi_avg_rate_idlow'] = f_i[0]
        cell_features['fi_avg_rate_idmid'] = f_i[4]
        cell_features['fi_avg_rate_idhigh'] = f_i[-1]

        cell_features['fi_1st_rate_idlow'] = f_i1[0]
        cell_features['fi_1st_rate_idmid'] = f_i1[4]
        cell_features['fi_1st_rate_idhigh'] = f_i1[-1]

        ap_peak = []
        ap_amplitude = []
        ap_fall_rate = []
        ap_rise_rate = []
        ap_avg_thresh = []
        spike_half_width = []
        ahp_depth = []
        ahp_depth_abs = []
        ahp_depth_abs_slow = []
        ahp_min_voltage = []
        ahp_time_from_peak = []
        ahp_slow_time = []
        for feat in feature_values:
            ap_peak.extend(feat['peak_voltage'])
            ap_amplitude.extend(feat['AP_amplitude'])
            ap_fall_rate.extend(feat['AP_fall_rate'])
            ap_rise_rate.extend(feat['AP_rise_rate'])
            ap_avg_thresh.extend(feat['AP_begin_voltage'])
            spike_half_width.extend(feat['spike_half_width'])
            ahp_depth.extend(feat['AHP_depth'])
            ahp_depth_abs.extend(feat['AHP_depth_abs'])
            ahp_depth_abs_slow.extend(feat['AHP_depth_abs_slow'])
            ahp_min_voltage.extend(feat['min_voltage_between_spikes'])
            ahp_time_from_peak.extend(feat['AHP_time_from_peak'])
            ahp_slow_time.extend(feat['AHP_slow_time'])

        ad_idlow = feature_values[0]['adaptation_index2']
        ad_idmid = feature_values[4]['adaptation_index2']
        ad_idhigh = feature_values[-1]['adaptation_index2']
        ap_delay_idlow = feature_values[0]['time_to_first_spike']
        ap_delay_idmid = feature_values[4]['time_to_first_spike']
        ap_delay_idhigh = feature_values[-1]['time_to_first_spike']

        cell_features['ap_peak'] = np.mean(ap_peak)
        cell_features['ap_amplitude'] = np.mean(ap_amplitude)
        cell_features['ap_fall_rate'] = np.mean(ap_fall_rate)
        cell_features['ap_rise_rate'] = np.mean(ap_rise_rate)
        cell_features['ap_avg_thresh'] = np.mean(ap_avg_thresh)
        cell_features['ap_half_width'] = np.mean(spike_half_width)
        cell_features['ahp_depth'] = np.mean(ahp_depth)
        cell_features['ahp_depth_abs'] = np.mean(ahp_depth_abs)
        cell_features['ahp_depth_abs_slow'] = np.mean(ahp_depth_abs_slow)
        cell_features['ahp_min_voltage'] = np.mean(ahp_min_voltage)
        cell_features['ahp_time_from_peak'] = np.mean(ahp_time_from_peak)
        cell_features['ahp_slow_time'] = np.mean(ahp_slow_time)
        cell_features['ad_idlow'] = np.mean(ad_idlow)
        cell_features['ad_idmid'] = np.mean(ad_idmid)
        cell_features['ad_idhigh'] = np.mean(ad_idhigh)
        cell_features['ap_delay_idlow'] = np.mean(ap_delay_idlow)
        cell_features['ap_delay_idmid'] = np.mean(ap_delay_idmid)
        cell_features['ap_delay_idhigh'] = np.mean(ap_delay_idhigh)

        import os

        test_model = {}
        test_model['cell_id'] = cell_id
        test_model['morphology'] = os.path.basename(morphofile)
        test_model['parameters'] = test_parameters
        test_model['v_i'] = {'stim': iv_stimuli, 'voltage': iv_volts}
        test_model['f_i'] = {'stim': id_stimuli, 'frequency': f_i, 'inv_first_ISI': f_i1}
        test_model['features'] = cell_features
        var_models.append(test_model)

        print('%5.2f%% ' % (100*count/(len(args.morphology)*len(test_models))),
            cell_id, morphofile, 'par:%d' % ind)


with open('var_models.json', 'w') as fp:
    json.dump(var_models, fp, indent=4, sort_keys=True)
