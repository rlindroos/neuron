"""Expsyn synapse parameter fitting"""

# pylint: disable=R0914


import os

import bluepyopt as bpopt
import bluepyopt.ephys as ephys

import json
import numpy as np
import cell_model, cell_evaluator
import matplotlib.pyplot as plt
from utils import *

from scipy.optimize import curve_fit


# curve_fit function
def func(x, a, b, c):
    return a * np.exp( (x-b) / c )
    

def save_vector(x, y, outfile):
    '''
    save vectors to file.
    
    x       = x-vector
    y       = y-vector
    outfile = file name to be used
    '''
    
    with open(outfile, "w") as out:
        for time, y in zip(x, y):
            out.write("%g %g\n" % (time, y))


def save_obj(obj, name ):
    '''
    functions used to save data in the pickle format. 
    Used to pickle dictionaries
    
    obj     = dictionary with data
    name    = name to be used, without file ending (.pkl will be added)
    '''
    
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def get_distance():
    ''' return distance of sections '''
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
    #protocols  = cell_evaluator.define_protocols('protocols_dSPN.json')
    #calculator = cell_evaluator.define_fitness_calculator(protocols, 'features.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    #parameters = cell_model.define_parameters('parameters.json')
    
    # def cell
    cell = ephys.models.CellModel(  name='dspn', 
                                    morph=morphology,
                                    mechs=mechanisms )
    
    cell.instantiate(sim=nrn_sim)
    
    h = nrn_sim.neuron.h
    
    secId2dist = {}
    count = 0
    for sec in h.allsec():
        if sec.name().find('dend') >= 0:
            secId2dist[ int(sec.name().split('[')[2].split(']')[0]) ] = h.distance(0.5, sec=sec)
            
            for seg in sec:
                count += 1
    
    return secId2dist
    

def def_axis(ax, axis=['left'], tpw=2, tpl=2):
    
    ax.tick_params(width=tpw, length=tpl) 
    
    for side in ['left', 'right']:
        if side in axis:
            ax.yaxis.set_ticks_position(side)
    for side in ['top', 'bottom']:
        if side in axis:
            ax.xaxis.set_ticks_position(side)
    for side in ['left', 'right', 'top', 'bottom']:
        if side in axis:
            ax.spines[side].set_linewidth(tpl)
        else:
            ax.spines[side].set_visible(False)
    
    
    if not 'top' in axis and not 'bottom' in axis:
        ax.set_xticks([])
    if not 'left' in axis and not 'right' in axis:
        ax.set_yticks([])


def main(id2dist):
    """Main"""
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    parameters = cell_model.define_parameters('parameters.json')
    ca_params  = cell_model.define_parameters('parameters_ca.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    ca_mech    = cell_model.define_mechanisms('mechanisms_ca.json')
    parameters = parameters + ca_params
    mechanisms = mechanisms + ca_mech
    protocol_definitions  = json.load(open('config/protocols-val.json'))
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # def morph
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
    
    cell = ephys.models.CellModel(
        'dspn', 
        morph=morphology, 
        mechs=mechanisms, 
        params=parameters)
    
    # def soma location   
    somatic_loc = ephys.locations.NrnSeclistLocation(
        'somatic',
        seclist_name='somatic')

    somacenter_loc = ephys.locations.NrnSeclistCompLocation(
        name='somacenter',
        seclist_name='somatic',
        sec_index=0,
        comp_x=0.5)
    
    fig,ax      = plt.subplots(3,1, figsize=(6,12))
    N_sec2loop  = len(id2dist)
    
    
    sec_loc = []
    REC     = [ ephys.recordings.CompRecording(
                    name='soma.v',
                    location=somacenter_loc,
                    variable='v') ]
    
    # record (ca conc) in all middle of all sections
    for secID in range(N_sec2loop):
        
        sec_loc.append( ephys.locations.NrnSeclistCompLocation(
            name='secCenter%d'%(secID),
            seclist_name='basal',
            sec_index=secID,
            comp_x=0.5))
        
        REC.append( ephys.recordings.CompRecording(
            name='sec%d.v' % (secID),
            location=sec_loc[secID],
            variable='v')   )
        
        REC.append( ephys.recordings.CompRecording(
            name='sec%d.cai' % (secID),
            location=sec_loc[secID],
            variable='cai')   )
        
        REC.append( ephys.recordings.CompRecording(
            name='sec%d.cali' % (secID),
            location=sec_loc[secID],
            variable='cali')   )

    # set constant istim
    delay       = 150
    relax_time  = delay - 20
    duration    = 250
    distance    = 30
    
    protocol_definition = protocol_definitions.items()[0] # select first prot (same in all)
    stimuli = [] 
    stimulus_definition = protocol_definition[1]['stimuli'][1]
    stimuli.append(ephys.stimuli.NrnSquarePulse(
        step_amplitude=stimulus_definition['amp'],
        step_delay=stimulus_definition['delay'],
        step_duration=stimulus_definition['duration'],
        location=somacenter_loc,
        total_duration=duration))

    # spike inducing stimuli
    stimuli.append(ephys.stimuli.NrnSquarePulse(
        step_amplitude=2.5,
        step_delay=delay,
        step_duration=2,
        location=somacenter_loc,
        total_duration=duration))

    protocol = ephys.protocols.SweepProtocol(
        'netstim_protocol',
        stimuli,
        REC)
    
    best_models = json.load(open('best_models.json'))
    default_param_values = best_models[3]
        
    responses = protocol.run(                                                    
        cell_model=cell,                                                         
        param_values=default_param_values,                                              
        sim=nrn_sim)   
    
    RES     = {'vm':[], 'dist':[], 'cai':[], 'cali':[], 'sum':[]}
    time    = [ responses['soma.v']['time'][index] - delay for index, tt in enumerate(responses['soma.v']['time']) if tt >= relax_time]
    voltage = [ responses['soma.v']['voltage'][index]      for index, tt in enumerate(responses['soma.v']['time']) if tt >= relax_time]
    for secID in range(N_sec2loop):    
        Vl = [ responses['sec%d.v'%(secID)]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt >= relax_time]
        ax[0].plot(time, Vl, 'k', alpha=0.3)
        cali = [ responses['sec%d.cali'%(secID)]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt >= relax_time]
        ax[1].plot(time, cali, '#006d2c', alpha=1.0)
        cai = [ responses['sec%d.cai'%(secID)]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt >= relax_time]
        ax[1].plot(time, cai, '#993404', alpha=1.0) 
        RES['dist'].append( id2dist[secID] )
        RES['vm'].append( max( Vl ) - Vl[0] )
        RES['cai'].append( max( cai ) - cai[0])
        RES['cali'].append( max( cali ) - cali[0])
        RES['sum'].append( (max(cai)+max(cali)) - (cai[0]+cali[0]) )
    ax[0].plot(time, voltage, 'k', alpha=1.0)
    
    # sort, normalize and plot peak values
    index =  np.argsort( RES['dist'] )
    norm_ind = next(i[0] for i in enumerate(RES['dist']) if i[1] < 40)
    dist = [RES['dist'][i] for i in index if RES['dist'][i] > distance]
    distAll = [RES['dist'][i] for i in index]
    vm   = [RES['vm'][i]  / RES['vm'][norm_ind]   for i in index]
    cai  = [RES['cai'][i] / RES['cai'][norm_ind]  for i in index]
    cali = [RES['cali'][i]/ RES['cali'][norm_ind] for i in index]
    Csum = [RES['sum'][i] / RES['sum'][norm_ind]  for i in index if RES['dist'][i] > distance]
    ax[2].plot(distAll, cai,  'o', ms=10, c='#993404', mew=2, mec='w', alpha=1.0)
    ax[2].plot(distAll, cali, 'o', ms=10, c='#006d2c', mew=2, mec='w', alpha=1.0)
    ax[2].plot(distAll, vm,   'o', ms=10, c='k',       mew=2, mec='w', alpha=1.0)
    #ax[2].plot(dist,    Csum, 'o', ms=20, c='red',     mew=1, mec='w', alpha=1)
    
    # get regression lines
    popt, pcov = curve_fit(func, dist, Csum, p0=[1,40,-15])
    
    # plot
    ax[2].plot(dist, func(dist, *popt), 'brown', lw=4, label='sci-opt')
    [x1,y1] = np.loadtxt('../../results/bAP/bAP-DayEtAl2006-D1.csv', unpack=True)
    ax[2].plot(x1,y1, color='k', lw=4, ls='--', label='Day')
    
    f1, a1 = plt.subplots(2,1,figsize=(3,4), gridspec_kw = {'height_ratios':[3, 1]})
    a1[0].plot(time, voltage, 'grey', lw=2)
    a1[0].set_xlim([-10, 30])
    a1[0].axis('off')
    a1[1].plot([-10,0], [0,0],   'grey', lw=2.0)
    a1[1].plot([0,0], [0,2.5],   'grey', lw=2.0)
    a1[1].plot([0,2], [2.5,2.5], 'grey', lw=2.0)
    a1[1].plot([2,2], [2.5,0],   'grey', lw=2.0)
    a1[1].plot([2,30], [0,0],    'grey', lw=2.0)
    a1[1].set_xlim([-10, 30])
    a1[1].set_ylim([-0.5, 3])
    a1[1].axis('off')
    f2, a2 = plt.subplots(figsize=(3,4))
    a2.plot(dist, Csum, 'o', ms=6, c='w', mew=2, mec='k', alpha=0.5)
    a2.plot(dist, func(dist, *popt), color='k', lw=4)
    a2.plot(x1,y1,                   color='r', lw=4, ls='-')
    
    def_axis(a2, axis=['left', 'bottom'], tpw=2, tpl=2)
    
    fig.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/ca_all.png', transparent=True)
    f1.savefig( '../../../Dropbox/Manuscripts/network/Figures_raw/ca_sim_prot.png', transparent=True)
    f2.savefig( '../../../Dropbox/Manuscripts/network/Figures_raw/ca_bAP-dCa.png', transparent=True)
    
    plt.show()    

if __name__ == '__main__':
    id2dist = get_distance()
    main(id2dist)
