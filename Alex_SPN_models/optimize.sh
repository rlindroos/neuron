#!/bin/bash -e

ipcontroller --ip='*' --quiet  \
  --HeartMonitor.period=10000 \
  --HubFactory.registration_timeout=600 \
  --HeartMonitor.max_heartmonitor_misses=2500 &
sleep 60
srun ipengine &
sleep 60

rm -rf x86_64
nrnivmodl ./mechanisms >/dev/null

START=$(date +%s)
python optimize.py
END=$(date +%s)

rm -rf std.err __pycache__ x86_64

echo Time elapsed: $(($END - $START)) seconds.
