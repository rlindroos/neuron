#!/usr/bin/env python

"Model validation using features of cell population"

from __future__ import division, print_function

import numpy as np
import argparse
import json

def parse():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("population", type=str, help="features of cell population (JSON)")
    parser.add_argument("models", type=str, help="features of model variations (JSON)")
    return parser.parse_args()

cell_id = '150917_c10_D1'
morphology = 'WT-P270-20'
threshold = 3

args = parse()
pop_summary = json.load(open(args.population))
var_models = json.load(open(args.models))
pop_features = {f:{'mean':np.mean(pop_summary[f]), 'std':np.std(pop_summary[f]), 'min':np.min(pop_summary[f]), 'max':np.min(pop_summary[f])} for f in pop_summary}

val_scores = []
val_models = []
for model in var_models:
    cell_features = model['features']
    scores = [abs(cell_features[f]-pop_features[f]['mean'])/pop_features[f]['std'] for f in sorted(cell_features)]
    if max(scores) < threshold:
        #s1 = np.array(scores)
        #d = min([np.linalg.norm(s1-np.array(s))/np.linalg.norm(s1) for s in val_scores]+[1e6])
        #if d > 0.3:
        #    val_scores.append(scores)
        #    val_models.append(model)
        val_scores.append(scores)
        val_models.append(model)

with open('val_models.json', 'w') as fp:
    json.dump(val_models, fp, indent=4, sort_keys=True)

spec = json.load(open(cell_id + '-spec.json'))
vxx = [s[0]['stimulus_total_amp'] for s in spec['cell_features']['IV']]
vyy = [s[1]['steady_state_voltage_stimend'][0] for s in spec['cell_features']['IV']]
fxx = [s[0]['stimulus_total_amp'] for s in spec['cell_features']['IDthresh']]
fyy = [s[1]['mean_frequency'][0] for s in spec['cell_features']['IDthresh']]
f1y = [s[1]['inv_first_ISI'][0] for s in spec['cell_features']['IDthresh']]

from matplotlib import pyplot as plt

fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(12,3))
fig.suptitle('Model variability, cell {}, morph {} et al (n={})'.format(cell_id, morphology, len(val_models)))
for v in val_models:
    xx = v['v_i']['stim']
    yy = v['v_i']['voltage']
    ax1.plot(xx, yy, color='lightgrey')
ax1.plot(vxx, vyy, color='blue', linewidth=3)
for v in val_models:
    xx = v['f_i']['stim']
    yy = v['f_i']['frequency']
    ax2.plot(xx, yy, color='lightgrey')
ax2.plot(fxx, fyy, color='blue', linewidth=3)
for v in val_models:
    xx = v['f_i']['stim']
    yy = v['f_i']['inv_first_ISI']
    ax3.plot(xx, yy, color='lightgrey')
ax3.plot(fxx, f1y, color='blue', linewidth=3)
plt.savefig('val.pdf')
