# Original file l5pc_model.py by Werner Van Geit at EPFL/Blue Brain Project
# Modified by Alexander Kozlov <akozlov@kth.se>

import os
import json

import bluepyopt.ephys as ephys

script_dir = os.path.dirname(__file__)
config_dir = os.path.join(script_dir, 'config')

from distance_scaler import *


def define_mechanisms(filename):
    mech_definitions = json.load(open(os.path.join(config_dir, filename)))
    mechanisms = []
    for sectionlist, channels in mech_definitions.items():
        seclist_loc = ephys.locations.NrnSeclistLocation(
            sectionlist,
            seclist_name=sectionlist)
        for channel in channels:
            mechanisms.append(ephys.mechanisms.NrnMODMechanism(
                name='%s.%s' % (channel, sectionlist),
                mod_path=None,
                prefix=channel,
                locations=[seclist_loc],
                preloaded=True))
    return mechanisms


def define_parameters(filename):
    param_configs = json.load(open(os.path.join(config_dir, filename)))
    parameters = []
    dist_param_bounds = {'naf': [[0.001,0.2], [0.8,1.0], [20.0,40.0],  [0.0,20.0]],
                         'kaf': [[0.99,1.01], [0.0,1.0], [60.0,140.0], [-50.0,-20.0]]
                         }
    for param_config in param_configs:
        if 'value' in param_config:
            frozen = True
            value = param_config['value']
            bounds = None
        elif 'bounds':
            frozen = False
            bounds = param_config['bounds']
            value = None
        else:
            raise Exception(
                'Parameter config has to have bounds or value: %s'
                % param_config)
        if param_config['type'] == 'global':
            parameters.append(
                ephys.parameters.NrnGlobalParameter(
                    name=param_config['param_name'],
                    param_name=param_config['param_name'],
                    frozen=frozen,
                    bounds=bounds,
                    value=value))
        elif param_config['type'] in ['section', 'range']:
            if 'param_list' in param_config:
                dist_param_names = []
                for p in range(4):
                    dist_param_names.append( '%s_v%d' % (param_config['mech'], p+1) )
                scaler = NrnSegmentSomaDistanceScaler(
                    distribution=param_config['dist'],
                    dist_param_names=dist_param_names)
                for n,name in enumerate(dist_param_names):
                    vx = ephys.parameters.NrnSectionParameter(
                                        name=name,
                                        param_name=name,
                                        locations=[],
                                        frozen=False,
                                        bounds=dist_param_bounds[param_config['mech']][n]
                                        )
                    parameters.append( vx )
                    # set parameter in scaler
                    setattr(scaler, name, vx)
            else:
                scaler = ephys.parameterscalers.NrnSegmentLinearScaler()
            seclist_loc = ephys.locations.NrnSeclistLocation(
                param_config['sectionlist'],
                seclist_name=param_config['sectionlist'])
            name = '%s.%s' % (param_config['param_name'],
                              param_config['sectionlist'])
            if param_config['type'] == 'section':
                parameters.append(
                    ephys.parameters.NrnSectionParameter(
                        name=name,
                        param_name=param_config['param_name'],
                        value_scaler=scaler,
                        value=value,
                        frozen=frozen,
                        bounds=bounds,
                        locations=[seclist_loc]))
            elif param_config['type'] == 'range':
                parameters.append(
                    ephys.parameters.NrnRangeParameter(
                        name=name,
                        param_name=param_config['param_name'],
                        value_scaler=scaler,
                        value=value,
                        frozen=frozen,
                        bounds=bounds,
                        locations=[seclist_loc]))
        else:
            raise Exception(
                'Param config type has to be global, section or range: %s' %
                param_config)
    return parameters
