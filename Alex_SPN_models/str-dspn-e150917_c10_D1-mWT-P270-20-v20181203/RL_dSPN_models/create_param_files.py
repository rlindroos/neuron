import json
import pickle
import numpy as np

parameters  = json.load(open('config/parameters_dSPN.json'))
par         = json.load(open('params_dMSN.json'))
with open('D1_71bestFit.pkl', 'rb') as f:
    data    = pickle.load(f)



def define_parameter(   section_list,
                        param_name,
                        distribution,
                        v1, v2, v3, v4,
                        gbar
                        ):
    '''define parameter in dict format for running as ephys object'''
    
    mech_param,mech = param_name.split('_')
    param_type      = "range"
    
    if   distribution == 0: 
        return {'sectionlist':section_list,
                'param_name': param_name, 
                'mech': mech,
                'value': gbar,
                'dist_type': 'uniform',
                'type': param_type,
                'mech_param':mech_param
                }   
    elif distribution == 1:
        if mech in ['kaf', 'naf'] and section_list == 'basal':
            
            dist  = "(1-{%s_v1} + {%s_v1}/(1+math.exp(({distance}-{%s_v2}+6.1)/{%s_v3}) ))*{value}" % (mech, mech, mech, mech)
            
            half_range = 0.001
            if gbar - half_range < 0:
                gbar = half_range
            
            return {'sectionlist':section_list,
                    'param_name': param_name, 
                    'mech': mech,
                    'bounds': [gbar-half_range, gbar+half_range],
                    'param_list': [v1, v2, v3, v4],
                    'dist': dist,
                    'dist_type': 'exp',
                    'type': param_type,
                    'mech_param':mech_param,
                    }
        else:
            
            dist  = "(%.12g + %.12g/(1+math.exp(({distance}-%.12g+6.1)/%.12g) ))*{value}" % (v1, v2, v3, v4)
            
            return {'sectionlist':section_list,
                    'param_name': param_name, 
                    'mech': mech,
                    'value': gbar,
                    'dist': dist,
                    'dist_type': 'exp',
                    'type': param_type,
                    'mech_param':mech_param,
                    }
            
    elif distribution == 2: 
        dist_type   = 'exp' 
        #dist        = "({v1} + {v2}*math.exp(({distance}-{v3})/{v4}) )*{value}"



for v in range(3):
    
    variables       = data[v]['variables']
    params          = []
    
    # passive parameters
    
    
    params.append( {
        "param_name": "celsius",
        "type": "global",
        "value": 35
    })
    params.append( {
        "param_name": "v_init",
        "type": "global",
        "value": -80
    })
    params.append( {
        "param_name": "g_pas",
        "sectionlist": "all",
        "type": "section",
        "dist_type": "uniform",
        "value": 1.25e-5
    })
    params.append( {
        "param_name": "e_pas",
        "sectionlist": "all",
        "type": "section",
        "dist_type": "uniform",
        "value": -70
    })
    params.append( {
        "param_name": "cm",
        "sectionlist": "all",
        "type": "section",
        "dist_type": "uniform",
        "value": 1
    })
    params.append( {
        "param_name": "Ra",
        "sectionlist": "all",
        "type": "section",
        "dist_type": "uniform",
        "value": 150
    })
    params.append( {
        "param_name": "ek",
        "sectionlist": "all",
        "type": "section",
        "dist_type": "uniform",
        "value": -85
    })
    
    # Active
    #   soma
    params.append(define_parameter("somatic", "gbar_naf",     0, 1, 0, 0, 0, float(par['gbar_naf_somatic']['Value'])))
    params.append(define_parameter("somatic", "gbar_kaf",     0, 1, 0, 0, 0, float(par['gbar_kaf_somatic']['Value'])))
    params.append(define_parameter("somatic", "gbar_kas",     0, 1, 0, 0, 0, float(par['gbar_kas_somatic']['Value'])))
    params.append(define_parameter("somatic", "gbar_kdr",     0, 1, 0, 0, 0, float(par['gbar_kdr_somatic']['Value'])))
    params.append(define_parameter("somatic", "gbar_bk",      0, 1, 0, 0, 0, float(par['gbar_bk_somatic' ]['Value'])))
    params.append(define_parameter("somatic", "pbar_cal12",   0, 1, 0, 0, 0, 1.34e-5))
    params.append(define_parameter("somatic", "pbar_cal13",   0, 1, 0, 0, 0, 1.34e-6))
    params.append(define_parameter("somatic", "pbar_car",     0, 1, 0, 0, 0, 1.34e-4))
    params.append(define_parameter("somatic", "pbar_can",     0, 1, 0, 0, 0,    4e-5))
    params.append(define_parameter("somatic", "gbar_kir",     0, 1, 0, 0, 0,    
                                                float(par['gbar_kir_somatic']['Value'])*np.power(10,variables['kir'][0])
                                                ))
    params.append(define_parameter("somatic", "gbar_sk",      0, 1, 0, 0, 0,    
                                                float(par['gbar_sk_somatic' ]['Value'])*np.power(10,variables['sk' ][0])
                                                ))
    # dendrite
    params.append(define_parameter("basal", "gbar_kdr",   0,  1, 0, 0, 0, float(par['gbar_kdr_dend']['Value'])))
    params.append(define_parameter("basal", "gbar_bk",    0,  1, 0, 0, 0, float(par['gbar_bk_dend' ]['Value'])))
    params.append(define_parameter("basal", "pbar_cal12", 0,  1, 0, 0, 0, 1e-5))
    params.append(define_parameter("basal", "pbar_cal13", 0,  1, 0, 0, 0, 1e-6))
    params.append(define_parameter("basal", "pbar_car",   0,  1, 0, 0, 0, 1e-4))
    params.append(define_parameter("basal", "gbar_sk",    0,  1, 0, 0, 0,    
                                                float(par['gbar_sk_dend'   ]['Value'])*np.power(10,variables['sk' ][0])
                                                ))
    params.append(define_parameter("basal", "gbar_naf",   1,  
                                                1.0-variables['naf'][1],
                                                variables['naf'][1],
                                                variables['naf'][2],
                                                variables['naf'][3],
                                                np.power(10,variables['naf'][0])*float(par['gbar_naf_dend']['Value'])
                                                ))
    params.append(define_parameter("basal", "gbar_kaf",   1,  
                                                1.0,
                                                variables['kaf'][1],
                                                variables['kaf'][2],
                                                variables['kaf'][3],
                                                np.power(10,variables['kaf'][0])*float(par['gbar_kaf_dend']['Value'])
                                                ))
    params.append(define_parameter("basal", "gbar_kas",   1,  0.1,
                                                0.9,
                                                variables['kas'][1],
                                                variables['kas'][2],
                                                np.power(10,variables['kas'][0])*float(par['gbar_kas_dend']['Value'])
                                                ))
    params.append(define_parameter("basal", "gbar_kir",   0,  1, 
                                                0, 0, 0,    
                                                float(par['gbar_kir_dend'  ]['Value'])*np.power(10,variables['kir'][0])
                                                ))
    params.append(define_parameter("basal", "pbar_can",   1,  1.0-variables['can'][1],  
                                                variables['can'][1],     
                                                variables['can'][2],     
                                                variables['can'][3],     
                                                np.power(10,variables['can'][0])
                                                ))
    params.append(define_parameter("basal", "pbar_cav32", 1,  0,                        
                                                1,                        
                                                variables['c32'][1],      
                                                variables['c32'][2],      
                                                np.power(10,variables['c32'][0])
                                                ))
    params.append(define_parameter("basal", "pbar_cav33", 1,  0,                        
                                                1,                       
                                                variables['c33'][1],      
                                                variables['c33'][2],      
                                                np.power(10,variables['c33'][0])
                                                ))
    #axon
    params.append(define_parameter("axonal", "gbar_kas",  0,  1, 0, 0, 0,  float(par['gbar_kas_axonal']['Value'])))
    params.append(define_parameter("axonal", "gbar_naf",  1,  1, 
                                                0.1, 30,  1,  
                                                float(par['gbar_naf_axonal']['Value'])
                                                ))

    #(1 + 0.9/(1 + math.exp(({distance}-30.0)/-1.0) )))



    with open('config/parameters_optim_%02d_dSPN.json' % (v), 'w') as fp:
        json.dump(params, fp, indent=4, sort_keys=True)





