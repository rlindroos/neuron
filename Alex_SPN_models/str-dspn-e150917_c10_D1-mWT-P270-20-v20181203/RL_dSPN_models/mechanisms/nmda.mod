: this model is built-in to neuron with suffix nmda

COMMENT
Mg blocked NMDA current.

The Mg block and NMDA current follow kinetics from Rhodes (2006)
"The Properties and Implications of NMDA Spikes in Neocortical Pyramidal Cells"

modified from epsp in Hay et al (2006)

ENDCOMMENT
					       
INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	POINT_PROCESS nmda
	RANGE onset, tau1, tau2, gmax, Erev, mg, block, alpha, i
	NONSPECIFIC_CURRENT i
}
UNITS {
	(nA)   = (nanoamp)
	(mV)   = (millivolt)
	(umho) = (micromho)
}

PARAMETER {
	onset = 200  (ms)    : activation time
	tau1  = 7.10 (ms)    : rise time constant
	tau2  = 165.0 (ms)    : decay time constant
	gmax  = 5e-2 (uS)    : maximum conductance        
	Erev  = 0    (mV)    : reversal potential
	mg    = 2    (mM)    : external magnesium concentration
	alpha = 0.08         : voltage dependency of Mg block
	v	         (mV)
}

ASSIGNED { 
    i (nA)
    SF
}

INITIAL {
    LOCAL Tpk
    
	Tpk = tau1*tau2 / (tau2 - tau1) * log(tau2/tau1)
	SF  = 1 / ( exp(-Tpk/tau2) - exp(-Tpk/tau1) )
}


BREAKPOINT {

    i = gmax * g(t) * (v - Erev)
}

FUNCTION MgBlock(vm) {
    
    MgBlock = 1 / (1 + mg/3.57*exp(-alpha * vm) )
    
}


FUNCTION g(x) {	
    
    LOCAL a1, a2, e1, e2, block
    
    : returns conductance scaled to have a maximal value of gmax in mg free conditions			
	
	if (x < onset) {
		g = 0
	}else{
	
	    a1 = -(x - onset) / tau1
        e1 =  exp(a1)
        
        a2 = -(x - onset) / tau2
        e2 =  exp(a2)
        
        block = MgBlock(v)
        
        : SF = scale factor
        g  =  SF * (e2 - e1) * block 
        
	}
}
