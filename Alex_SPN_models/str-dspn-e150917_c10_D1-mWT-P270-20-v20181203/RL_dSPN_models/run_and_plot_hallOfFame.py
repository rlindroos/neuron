import matplotlib.pyplot as plt
import json
import pickle
import pandas
import efel
import numpy as np
import bluepyopt as bpopt
import bluepyopt.ephys as ephys

# _________________________________________________________________________________________

nrn = ephys.simulators.NrnSimulator()



# _________________________________________________________________

def plot_responses(responses, ax, c='b'):
    
    if c == 'b': lw=1; a=0.6
    else: lw=3; a=1
        
        
    ax[0].plot(responses['step1.soma.v']['time'], responses['step1.soma.v']['voltage'], c=c, alpha=a, lw=lw)
    ax[1].plot(responses['step2.soma.v']['time'], responses['step2.soma.v']['voltage'], c=c, alpha=a, lw=lw)
    ax[2].plot(responses['step3.soma.v']['time'], responses['step3.soma.v']['voltage'], c=c, alpha=a, lw=lw)
    
    
    
    

# _________________________________________________________________
        
        
sweep_protocols = []
soma_loc = ephys.locations.NrnSeclistCompLocation(
        name='soma',
        seclist_name='somatic',
        sec_index=0,
        comp_x=0.5)
for protocol_name, amplitude in [('step1', 311e-3), ('step2', 350e-3), ('step3', 375e-3)]:
    stim = ephys.stimuli.NrnSquarePulse(
                step_amplitude=amplitude,
                step_delay=700,
                step_duration=2000,
                location=soma_loc,
                total_duration=3000)
    rec = ephys.recordings.CompRecording(
            name='%s.soma.v' % protocol_name,
            location=soma_loc,
            variable='v')
    protocol = ephys.protocols.SweepProtocol(protocol_name, [stim], [rec], cvode_active=True)
    sweep_protocols.append(protocol)
twostep_protocol = ephys.protocols.SequenceProtocol('twostep', protocols=sweep_protocols)



# _________________________________________________________________________________________

morphology = ephys.morphologies.NrnFileMorphology(
    'morphology/WT-dMSN_P270-20_1.02_SGA1-m24.swc', 
    do_replace_axon=True )
    
    
# _________________________________________________________________________________________    
    
    
mech_definitions = json.load(open('config/mechanisms_dSPN.json'))

def add_mechanism(sectionlist, channel):

    seclist_loc = ephys.locations.NrnSeclistLocation(
        sectionlist,
        seclist_name=sectionlist)
        
    return ephys.mechanisms.NrnMODMechanism(
                        name='%s.%s' % (channel, sectionlist),
                        mod_path=None,
                        prefix=channel,
                        locations=[seclist_loc],
                        preloaded=True)




seclist_loc = ephys.locations.NrnSeclistLocation(
    'all',
    seclist_name='all')
    
mechanisms = [ ephys.mechanisms.NrnMODMechanism(
                    name='pas.all',
                    mod_path=None,
                    prefix='pas',
                    locations=[seclist_loc],
                    preloaded=True)
                    ]

# ______________________________________________________________________________________



def set_param(param_config):

    dist_param_bounds = {'naf': [[0.001,0.2], [0.8,1.0], [20.0,40.0],  [0.0,10.0]],
                         'kaf': [[0.99,1.01], [0.0,1.0], [90.0,140.0], [-50.0,-20.0]]
                         }
        
    if 'value' in param_config:
        frozen = True
        value = param_config['value']
        bounds = None
    elif 'bounds':
        frozen = False
        bounds = param_config['bounds']
        value = None
    else:
        raise Exception(
            'Parameter config has to have bounds or value: %s'
            % param_config)
    if param_config['type'] == 'global':
        parameter = ephys.parameters.NrnGlobalParameter(
                name=param_config['param_name'],
                param_name=param_config['param_name'],
                frozen=frozen,
                bounds=bounds,
                value=value)
    elif param_config['type'] in ['section', 'range']:
        
        # def scaler and scaler params: UPDATED! --------------------------------------
        if 'param_list' in param_config:
            dist_param_names = []
            for p in range(4):
                # add location to name? E.g. "kaf_v1.basal". this would enable different distributions over location
                dist_param_names.append( '%s_v%d' % (param_config['mech'], p+1) )
            scaler = NrnSegmentSomaDistanceScaler(
                    name=param_config['mech'],
                    distribution=param_config['dist'],
                    dist_param_names=dist_param_names
                    )
            # def distribution parameters 
            for n,name in enumerate(dist_param_names):
                vx = ephys.parameters.MetaParameter(
                                    name=name,
                                    obj=scaler,
                                    attr_name=name,
                                    frozen=False,
                                    bounds=dist_param_bounds[param_config['mech']][n]
                                    )

                parameters.insert(0, vx )

                # set parameter in scaler
                setattr(scaler, name, vx)

                # default distribution values
                scaler_default_values[name] = param_config['param_list'][n]
        elif param_config['dist_type'] == 'exp':
            scaler = NrnSegmentSomaDistanceScaler(
                    distribution=param_config['dist'])
        else:
            scaler = NrnSegmentLinearScaler()
        
        # END UPDATE -----------------------------------------------------------------------
            
        seclist_loc = ephys.locations.NrnSeclistLocation(
            param_config['sectionlist'],
            seclist_name=param_config['sectionlist'])
        
        name = '%s.%s' % (param_config['param_name'],
                          param_config['sectionlist'])
        
        if param_config['type'] == 'section':
            parameter = ephys.parameters.NrnSectionParameter(
                    name=name,
                    param_name=param_config['param_name'],
                    value_scaler=scaler,
                    value=value,
                    frozen=frozen,
                    bounds=bounds,
                    locations=[seclist_loc])
        elif param_config['type'] == 'range':
            parameter = ephys.parameters.NrnRangeParameter(
                    name=name,
                    param_name=param_config['param_name'],
                    value_scaler=scaler,
                    value=value,
                    frozen=frozen,
                    bounds=bounds,
                    locations=[seclist_loc])
    else:
        raise Exception(
            'Param config type has to be global, section or range: %s' %
            param_config)
    
    return parameter



# _________________________________________________________________________________________


from distance_scaler import *

param_configs = json.load(open('config/parameters_optim_01_dSPN.json'))


scaler_default_values = {}
parameters            = []

for param_config in param_configs:
    
    if 'sectionlist' in param_config and 'mech' in param_config:
        mechanisms.append( add_mechanism( param_config['sectionlist'], param_config['mech'] ) )
    
    parameters.append( set_param(param_config) )

    

cell = ephys.models.CellModel(
        name='cell',
        morph=morphology,
        mechs=mechanisms,
        params=parameters)        



# _________________________________________________________________________________________


fig, ax = plt.subplots( 3,1, figsize=(6,12) )

# plot original
original_values = {u'kaf_v4': -34.54377732608168, u'kaf_v1': 1.0, u'kaf_v3': 116.81627421555731, u'kaf_v2': 0.3220156107697027, u'naf_v4': 3.5417136952409995, u'naf_v2': 0.993597426760602, u'naf_v3': 28.14064261173643, u'naf_v1': 0.006402573239397968, "gbar_naf.basal":0.42320566848780294, 'gbar_kaf.basal':0.10326444612227474}

plot_responses(twostep_protocol.run(cell_model=cell, param_values=original_values, sim=nrn), ax, c='lightgrey')


free_params = [u'kaf_v4', u'kaf_v3', u'kaf_v2', u'kaf_v1', u'naf_v4', u'naf_v3', u'naf_v2', u'naf_v1', u'gbar_naf.basal', u'gbar_kaf.basal']

best_list = [[-20.907914984186036, 115.18129186940908, 0.5174512108269131, 0.9988824802576519, 8.04412430970686, 27.20416004986117, 0.9919221115485051, 0.18331525644862412, 0.4230296210527136, 0.10253454334140477],
[-20.907914984186036, 115.33218036658818, 0.6253376480688833, 0.9950229349561467, 5.353935327255914, 21.114089919319674, 0.9871590144533917, 0.19725905377509417, 0.4231758625649493, 0.10246439761216504],
[-21.60715918223737, 129.4449337399998, 0.671766548384656, 0.9945426875704535, 4.292622578041264, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.4232344095536781, 0.10234313414687808],
[-21.60715918223737, 119.59184061696031, 0.6733275096845892, 0.99471467219119, 4.266909771242586, 27.03035884131673, 0.9835383160564386, 0.1777057076213464, 0.42319610411408, 0.10311637519628214],
[-23.51299984573534, 105.39490113512407, 0.5650397034911421, 0.9950229349561467, 5.872766044444876, 21.114089919319674, 0.9888734187015437, 0.19097563197687956, 0.42337232783734996, 0.10246439761216504],
[-21.031350213424243, 124.77758451366536, 0.6446761642846257, 0.9914478206027224, 6.459540298891451, 26.981778674931334, 0.9702618672904026, 0.1887584333315147, 0.42351203777207685, 0.10328273122344153],
[-20.81007269480532, 124.82323514429677, 0.6446761642846257, 0.9980208604225876, 5.850845489912347, 25.084731747504808, 0.9756238838589893, 0.1978842379805409, 0.4235486026128482, 0.10336967430393561],
[-21.60715918223737, 129.544549487455, 0.6733275096845892, 0.99471467219119, 4.266909771242586, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.42337208803046916, 0.10275097043081091],
[-21.60715918223737, 129.544549487455, 0.6733275096845892, 0.99471467219119, 4.266909771242586, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.42337208803046916, 0.10275097043081091],
[-21.60715918223737, 129.544549487455, 0.6733275096845892, 0.99471467219119, 4.266909771242586, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.42337208803046916, 0.10275097043081091],
[-21.60715918223737, 129.544549487455, 0.6733275096845892, 0.99471467219119, 4.266909771242586, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.42337208803046916, 0.10275097043081091],
[-21.60715918223737, 129.544549487455, 0.6733275096845892, 0.99471467219119, 4.266909771242586, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.42337208803046916, 0.10275097043081091],
[-25.079522216444, 129.544549487455, 0.6733275096845892, 0.99471467219119, 5.607970425728988, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.42337208803046916, 0.10269057644004893],
[-21.60715918223737, 129.544549487455, 0.6733275096845892, 0.9960617674547229, 5.954553293117205, 27.03035884131673, 0.9835383160564386, 0.1788165639042291, 0.4233116672420253, 0.10294043170938186],
[-21.134922092338243, 128.27302598498872, 0.48131261517616564, 0.9939830153711406, 1.7506697141558956, 25.277581591602967, 0.9699875311985732, 0.18625913905975447, 0.423425142422281, 0.10275097043081091],
[-20.907914984186036, 115.13564119902288, 0.3621675044263396, 0.9912850716499937, 5.815644197958907, 26.75526562641396, 0.9886922034244098, 0.1836106031979275, 0.4229930561845162, 0.10244859147976372],
[-20.384369309252214, 130.21235823319043, 0.6301914684821571, 0.9988087542805681, 4.964214264668332, 27.115429093832205, 0.9454611748749708, 0.16998063997328233, 0.42330830104168227, 0.10276802236359368],
[-20.743566234089847, 133.61491732216655, 0.6901719012956226, 0.9948608430494836, 5.8779890345988965, 27.8598455174143, 0.9919222745944375, 0.17878565196599938, 0.42301151897592376, 0.10229582240924451],
[-20.907914984186036, 129.2599151842197, 0.6300792643833515, 0.9958080110694156, 5.294844092541149, 27.03035884131673, 0.9839893205637922, 0.1788165639042291, 0.42304122427629914, 0.10275097043081091],
[-21.11226401957396, 120.50595542839747, 0.5611996728045862, 0.9993512002576582, 4.938571849484244, 36.196252172306956, 0.9666279745545844, 0.15843712595733142, 0.4237725180618825, 0.10281444259867861],
[-21.11226401957396, 120.50595542839747, 0.5611996728045862, 0.9985301798203003, 4.938571849484244, 36.196252172306956, 0.9750110210094645, 0.15843712595733142, 0.4237725180618825, 0.10281444259867861],
[-20.907914984186036, 115.18129186940908, 0.5174512108269131, 0.9988824802576519, 8.04412430970686, 27.20416004986117, 0.9919221115485051, 0.18331525644862412, 0.4230296210527136, 0.10253454334140477],
[-20.907914984186036, 134.91992427905097, 0.6695192370885874, 0.9966427462277896, 5.294844092541149, 27.03035884131673, 0.9952590389164636, 0.16586173376769495, 0.42324950784073423, 0.10267504431610207],
[-21.85312637663663, 130.10297733362214, 0.6459638390991658, 0.9940602823761384, 7.0213965564266045, 26.245928325317358, 0.9834743082138843, 0.1918649015160965, 0.4235120421076291, 0.10337650783693723],
[-20.907914984186036, 130.21235823319043, 0.6695192370885874, 0.996969382647745, 5.294844092541149, 27.115429093832205, 0.9919222745944375, 0.16998063997328233, 0.42324950784073423, 0.10229582240924451],
[-33.84722150385259, 115.35137933685508, 0.5283215835771896, 0.9994731717314195, 4.523824074629441, 36.44643699459829, 0.9904849167366364, 0.15025169128099988, 0.42323693712948424, 0.10244419616027976]]

best_resp = []

for bl in best_list:
    best_ind_dict = {}
    for par,value in zip(free_params,bl):
        best_ind_dict[par] = value
     
    best_resp.append( twostep_protocol.run(cell_model=cell, param_values=best_ind_dict, sim=nrn) )



for resp in best_resp:
    plot_responses(resp, ax)

plt.show()

