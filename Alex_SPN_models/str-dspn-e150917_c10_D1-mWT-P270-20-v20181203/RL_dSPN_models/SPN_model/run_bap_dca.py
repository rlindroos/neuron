
from __future__ import print_function, division
from   neuron           import h
import numpy                as np
import MSN_builder          as build
import matplotlib.pyplot    as plt
import json
from scipy.optimize import curve_fit

# Load mechanisms
import neuron               as nrn
nrn.load_mechanisms('../mechanisms/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

# curve_fit function
def func(x, a, b, c):
    return a * np.exp( (x-b) / c )

# load parameters and protocols
with open('../best_parameters.json') as f:
    best_params = json.load(f)
with open('../config/parameters.json') as f:
    params      = json.load(f)
# load stim prot
with open('../config/protocols.json') as f:
    protocols = json.load(f)

fig,ax = plt.subplots( 4,2, figsize=(10,18) )

for i,min_dist in enumerate([30,40]):
 
    # build cell
    cell = build.MSN(params, best_params, -105.9, extended=True)

    # set stimulation
    istim0       = h.IClamp(0.5, sec=cell.soma)
    istim0.delay = protocols['IV_505']['stimuli'][1]['delay']
    istim0.amp   = protocols['IV_505']['stimuli'][1]['amp']
    istim0.dur   = protocols['IV_505']['stimuli'][1]['duration']
    istim = h.IClamp(0.5, sec=cell.soma)
    istim.delay = 100
    istim.amp   = 2.3
    istim.dur   = 2.0

    tm = h.Vector()
    tm.record(h._ref_t)
    vm = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)


    CaLT = {}
    CaNPQR = {}
    for sec in cell.dendlist:
        CaNPQR[sec] = h.Vector()    
        CaNPQR[sec].record(sec(0.5)._ref_cai)
        CaLT[sec] = h.Vector()
        CaLT[sec].record(sec(0.5)._ref_cali)

        
    h.finitialize(-85)
    while h.t < 200:
        h.fadvance() 

    ax[0,i].plot(tm, vm, color='blue')

    base_index = 3600
    for key in CaLT:
        ax[1,i].plot(tm,CaLT[key], 'r', alpha=0.5)
        ax[1,i].plot(tm,CaNPQR[key], 'b', alpha=0.5)
        
        ax[1,i].plot(tm[base_index],CaLT[key][base_index], 'ok', ms=15)


    # get peak [Ca] and sort based on somatic distance
    sorted_on_dist  = {}
    distances       = np.arange(min_dist,170, 10)  
    R = []
    D = []  
    NAME = [] 
    for key in CaLT:
        
        L   = CaLT[  key].to_python()
        N   = CaNPQR[key].to_python()
        
        # this is were concentrations are summed 
        # (instead of averaged, doesn't matter since relative concen is used)
        V           = np.add( L, N )
        
        dca         = max(V[base_index:-1]) - V[base_index]
        dca_R       = max(N[base_index:-1]) - N[base_index]
        dca_L       = max(L[base_index:-1]) - L[base_index]
        
        dist        = int(np.round( h.distance(0.5, sec=key) ))
        
        for d in distances:

            if dist > d-5 and dist < d+5:
                
                if d not in sorted_on_dist:
                    sorted_on_dist[d] = {'sum':[], 'L':[], 'R':[]}
                
        
                sorted_on_dist[d]['sum'].append(dca)
                sorted_on_dist[d]['L'].append(dca_L)
                sorted_on_dist[d]['R'].append(dca_R)
                
                R.append(dca)
                D.append(dist)
                NAME.append(key.name())
                break
                
    # calc mean and return list as function of distance
    y       = []
    y1      = []
    norm    = np.mean(sorted_on_dist[distances[0]]['sum'])

    for d in distances:
        y1.append( np.divide(np.mean(sorted_on_dist[d]['sum']), norm) )
        y. append( np.mean( sorted_on_dist[d]['sum']) )
        
    ax[2,i].plot(distances,  y, 'brown', lw=6)
    ax[3,i].plot(D, np.divide(R,norm), 'o', c='brown', ms=10, mew=1, mec='w')
    
    for dd,d in enumerate(D):
        
        rnorm = np.divide(R[dd],norm)
        if rnorm > 0.8:
            print( NAME[dd], rnorm, d )


    # REGRESSION

    # sort lists
    I = [(v,j) for j,v in enumerate(D)]
    X = [x for _,x in sorted(zip(I,D))]
    Y = np.divide( [x for _,x in sorted(zip(I,R))], norm )

    # get regression lines
    popt, pcov = curve_fit(func, X, Y, p0=[1,40,-27])

    print(popt)


    # plot
    ax[3,i].plot(X, func(X, *popt), 'brown', lw=6, label='sci-opt')
    [x1,y1] = np.loadtxt('../../../results/bAP/bAP-DayEtAl2006-D1.csv', unpack=True)
    ax[3,i].plot(x1,y1, color='k', lw=2, ls='--', label='Day')
    
    ax[0,i].set_title('Included = '+str(min_dist)+' um', fontsize=26)
    
    # delete new axon
    cell.axon = None

ax[3,0].legend(loc=0)
plt.show()

