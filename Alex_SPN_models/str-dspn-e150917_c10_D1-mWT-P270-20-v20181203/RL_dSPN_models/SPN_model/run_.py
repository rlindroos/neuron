
from __future__ import print_function, division
from neuron import h
import numpy                as np
import MSN_builder          as build
import matplotlib.pyplot    as plt
import json

# Load mechanisms
import neuron               as nrn
nrn.load_mechanisms('../mechanisms/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

# load parameters and protocols
with open('../best_parameters.json') as f:
    best_params = json.load(f)
with open('../config/parameters.json') as f:
    params      = json.load(f)
# load stim prot
with open('../config/protocols.json') as f:
    protocols = json.load(f)

# expdata and simulation keys
expdata = {
    'IDthresh-sub_547.soma.v': 'expdata/ECBL_IDthresh_ch3_547.dat',
    'IDthresh_548.soma.v'    : 'expdata/ECBL_IDthresh_ch3_548.dat',
    'IDthresh_549.soma.v'    : 'expdata/ECBL_IDthresh_ch3_549.dat',
    'IDthresh_550.soma.v'    : 'expdata/ECBL_IDthresh_ch3_550.dat',
    'IV_505.soma.v'          : 'expdata/ECBL_IV_ch3_505.dat',
    'IV_509.soma.v'          : 'expdata/ECBL_IV_ch3_509.dat'
}

fig,ax = plt.subplots(len(expdata), 1, figsize=(6,14))

for e,ek in enumerate([-105.9]): 
    # -90, -92.6, -105.9
    # build cell
    cell = build.MSN(params, best_params, ek)

    '''
    h.topology(cell)

    for sec in cell.allseclist:
        h.psection(sec=sec)
    '''
    
    # set stimulation
    istim = h.IClamp(0.5, sec=cell.soma)
    istim0 = h.IClamp(0.5, sec=cell.soma)

    
    # simulate and plot
    for k,key in enumerate(['IDthresh-sub_547.soma.v',
                            'IDthresh_548.soma.v',
                            'IDthresh_549.soma.v',
                            'IDthresh_550.soma.v',
                            'IV_505.soma.v',
                            'IV_509.soma.v'         ]):
        
        if e == 0:
            x,y         = np.loadtxt('../'+expdata[key], unpack=True)
            y           = [i-9.5 for i in y]
            for l in range(1):
                ax[k].plot(x,y, color='grey')  
            ax[k].set_ylabel(key) 
        if k == 0:
            ax[0].set_title('ek = '+str(ek), fontsize=28) 
        
        prot        = key.split('.')[0]
        p           = protocols[prot]
        
        print( ek, p['stimuli'][0]['delay'], p['stimuli'][0]['amp'], p['stimuli'][0]['duration'] )
        
        istim0.delay = p['stimuli'][1]['delay']
        istim0.amp   = p['stimuli'][1]['amp']
        istim0.dur   = p['stimuli'][1]['duration']
        
        istim.delay = p['stimuli'][0]['delay']
        istim.amp   = p['stimuli'][0]['amp']
        istim.dur   = p['stimuli'][0]['duration']
        
        tm = h.Vector()
        tm.record(h._ref_t)
        vm = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        h.finitialize(-85)
        #h.topology(cell)

        
        while h.t < p['stimuli'][0]['totduration']:
            h.fadvance() 
        
        ax[k].plot(tm, vm, color='blue')
    
        for sec in cell.allseclist:
            h.psection(sec=sec)
    # delete new axon
    cell.axon = None
        
        
plt.show()

