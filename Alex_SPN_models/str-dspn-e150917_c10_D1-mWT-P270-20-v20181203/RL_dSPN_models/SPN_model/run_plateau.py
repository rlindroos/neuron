
from __future__ import print_function, division
from   neuron           import h
import numpy                as np
import MSN_builder          as build
import matplotlib.pyplot    as plt
import json
from scipy.optimize import curve_fit

# Load mechanisms
import neuron               as nrn
nrn.load_mechanisms('../mechanisms/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

# curve_fit function
def func(x, a, b, c):
    return a * np.exp( (x-b) / c )

# load parameters and protocols
with open('../best_parameters.json') as f:
    best_params = json.load(f)
with open('../config/parameters.json') as f:
    params      = json.load(f)
# load stim prot
with open('../config/protocols.json') as f:
    protocols = json.load(f)

fig,ax = plt.subplots( 1,2, figsize=(12,6) )

for i,compartment in enumerate(['31','46']):
 
    # build cell
    cell = build.MSN(params, best_params, -105.9, extended=True)
    
    # set stimulation
    istim0       = h.IClamp(0.5, sec=cell.soma)
    istim0.delay = protocols['IV_505']['stimuli'][1]['delay']
    istim0.amp   = protocols['IV_505']['stimuli'][1]['amp']
    istim0.dur   = protocols['IV_505']['stimuli'][1]['duration']

    ## ---------------------------------------------------------
    # create the synapse and set in soma; to be moved.    
    
    for sec in cell.dendlist:
        if sec.name().split('[')[1].split(']')[0] == compartment:
            break
    
    glutamate       = h.tmGlut(0.5, sec=sec)
    stim            = h.NetStim()
    stim.number     = 20
    stim.start      = 100
    stim.interval   = 0.2 # mean interval between two spikes in ms (default 1 ms)
    nc              = h.NetCon(stim, glutamate)
    
    # specify synapse
    glutamate.nmda_ratio= 3
    glutamate.q         = 1
    glutamate.tau1_ampa = 1.9
    glutamate.tau2_ampa = 4.8
    glutamate.tau1_nmda = 6
    glutamate.tau2_nmda = 116
    # turn off short term plasticity
    glutamate.stp       = 0
    
    # def NetCon
    nc.weight[0]    = 0.333e-3
    nc.delay        = 0

    tm = h.Vector()
    tm.record(h._ref_t)
    vm = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    vml = h.Vector()
    vml.record(sec(0.5)._ref_v)

        
    h.finitialize(-85)
    while h.t < 300:
        h.fadvance() 

    ax[i].plot(tm, vm, color='blue', label='soma')
    ax[i].plot(tm, vml, color='red', label='local')
    ax[i].set_title(sec.name()+' %.1f' %(h.distance(0.5, sec=sec))+' um', fontsize=24)
    ax[i].set_ylim([-90,0])
    
    # delete new axon
    cell.axon = None

ax[0].legend(loc=0)
plt.show()

