import matplotlib.pyplot as plt
import json
import pickle
import pandas
import efel
import numpy as np
import bluepyopt as bpopt
import bluepyopt.ephys as ephys

# _________________________________________________________________________________________

nrn = ephys.simulators.NrnSimulator()

# _________________________________________________________________________________________

morphology = ephys.morphologies.NrnFileMorphology(
    'morphology/WT-dMSN_P270-20_1.02_SGA1-m24.swc', 
    do_replace_axon=True )
    
    
# _________________________________________________________________________________________    
    
    
mech_definitions = json.load(open('config/mechanisms_dSPN.json'))

def add_mechanism(sectionlist, channel):

    seclist_loc = ephys.locations.NrnSeclistLocation(
        sectionlist,
        seclist_name=sectionlist)
        
    return ephys.mechanisms.NrnMODMechanism(
                        name='%s.%s' % (channel, sectionlist),
                        mod_path=None,
                        prefix=channel,
                        locations=[seclist_loc],
                        preloaded=True)




seclist_loc = ephys.locations.NrnSeclistLocation(
    'all',
    seclist_name='all')
    
mechanisms = [ ephys.mechanisms.NrnMODMechanism(
                    name='pas.all',
                    mod_path=None,
                    prefix='pas',
                    locations=[seclist_loc],
                    preloaded=True)
                    ]


# _________________________________________________________________________________________


from distance_scaler import *

param_configs = json.load(open('config/parameters_optim_01_dSPN.json'))

dist_param_bounds = {'naf': [[0.001,0.2], [0.8,1.0], [20.0,40.0],  [0.0,10.0]],
                     'kaf': [[0.99,1.01], [0.0,1.0], [90.0,140.0], [-50.0,-20.0]]
                    }

scaler_default_values = {}
parameters            = []

for param_config in param_configs:
    
    if 'sectionlist' in param_config and 'mech' in param_config:
        mechanisms.append( add_mechanism( param_config['sectionlist'], param_config['mech'] ) )
    
    if 'value' in param_config:
        frozen = True
        value = param_config['value']
        bounds = None
    elif 'bounds':
        frozen = False
        bounds = param_config['bounds']
        value = None
    else:
        raise Exception(
            'Parameter config has to have bounds or value: %s'
            % param_config)
    if param_config['type'] == 'global':
        parameters.append(
            ephys.parameters.NrnGlobalParameter(
                name=param_config['param_name'],
                param_name=param_config['param_name'],
                frozen=frozen,
                bounds=bounds,
                value=value))
    elif param_config['type'] in ['section', 'range']:
        
        # def scaler and scaler params: UPDATED! --------------------------------------
        if 'param_list' in param_config:
            dist_param_names = []
            for p in range(4):
                # add location to name? E.g. "kaf_v1.basal". this would enable different distributions over location
                dist_param_names.append( '%s_v%d' % (param_config['mech'], p+1) )
            scaler = NrnSegmentSomaDistanceScaler(
                    name=param_config['mech'],
                    distribution=param_config['dist'],
                    dist_param_names=dist_param_names
                    )
            # def distribution parameters 
            for n,name in enumerate(dist_param_names):
                vx = ephys.parameters.NrnRangeParameter(
                                    name=name,
                                    param_name=name,
                                    locations=[],
                                    frozen=False,
                                    bounds=dist_param_bounds[param_config['mech']][n]
                                    )

                parameters.insert(0, vx )

                # set parameter in scaler
                setattr(scaler, name, vx)

                # default distribution values
                scaler_default_values[name] = param_config['param_list'][n]
        elif param_config['dist_type'] == 'exp':
            scaler = NrnSegmentSomaDistanceScaler(
                    distribution=param_config['dist'])
        else:
            scaler = NrnSegmentLinearScaler()
        
        # END UPDATE -----------------------------------------------------------------------
            
        seclist_loc = ephys.locations.NrnSeclistLocation(
            param_config['sectionlist'],
            seclist_name=param_config['sectionlist'])
        
        name = '%s.%s' % (param_config['param_name'],
                          param_config['sectionlist'])
        
        if param_config['type'] == 'section':
            parameters.append(
                ephys.parameters.NrnSectionParameter(
                    name=name,
                    param_name=param_config['param_name'],
                    value_scaler=scaler,
                    value=value,
                    frozen=frozen,
                    bounds=bounds,
                    locations=[seclist_loc]))
        elif param_config['type'] == 'range':
            parameters.append(
                ephys.parameters.NrnRangeParameter(
                    name=name,
                    param_name=param_config['param_name'],
                    value_scaler=scaler,
                    value=value,
                    frozen=frozen,
                    bounds=bounds,
                    locations=[seclist_loc]))
    else:
        raise Exception(
            'Param config type has to be global, section or range: %s' %
            param_config)
    

# _________________________________________________________________

cell = ephys.models.CellModel(
        name='cell',
        morph=morphology,
        mechs=mechanisms,
        params=parameters)        
        
# _________________________________________________________________
        
        
sweep_protocols = []
soma_loc = ephys.locations.NrnSeclistCompLocation(
        name='soma',
        seclist_name='somatic',
        sec_index=0,
        comp_x=0.5)
for protocol_name, amplitude in [('step1', 311e-3), ('step2', 350e-3), ('step3', 375e-3)]:
    stim = ephys.stimuli.NrnSquarePulse(
                step_amplitude=amplitude,
                step_delay=700,
                step_duration=2000,
                location=soma_loc,
                total_duration=3000)
    rec = ephys.recordings.CompRecording(
            name='%s.soma.v' % protocol_name,
            location=soma_loc,
            variable='v')
    protocol = ephys.protocols.SweepProtocol(protocol_name, [stim], [rec], cvode_active=True)
    sweep_protocols.append(protocol)
twostep_protocol = ephys.protocols.SequenceProtocol('twostep', protocols=sweep_protocols)





# _________________________________________________________________

def plot_responses(responses):
    plt.subplot(3,1,1)
    plt.plot(responses['step1.soma.v']['time'], responses['step1.soma.v']['voltage'])
    plt.legend(loc=2)
    plt.subplot(3,1,2)
    plt.plot(responses['step2.soma.v']['time'], responses['step2.soma.v']['voltage'])
    plt.legend(loc=2)
    plt.subplot(3,1,3)
    plt.plot(responses['step3.soma.v']['time'], responses['step3.soma.v']['voltage'])
    plt.legend(loc=2)
    plt.tight_layout()
    plt.show()

# _________________________________________________________________



efel_feature_means = {'step1': {'Spikecount': 0}, 
                      'step2': {'Spikecount': 4}, 
                      'step3': {'Spikecount': 18}}

objectives = []

for protocol in sweep_protocols:
    stim_start = protocol.stimuli[0].step_delay
    stim_end = stim_start + protocol.stimuli[0].step_duration
    for efel_feature_name, mean in efel_feature_means[protocol.name].items():
        feature_name = '%s.%s' % (protocol.name, efel_feature_name)
        feature = ephys.efeatures.eFELFeature(
                    feature_name,
                    efel_feature_name=efel_feature_name,
                    recording_names={'': '%s.soma.v' % protocol.name},
                    stim_start=stim_start,
                    stim_end=stim_end,
                    exp_mean=mean,
                    exp_std=0.05 * mean)
        objective = ephys.objectives.SingletonObjective(
            feature_name,
            feature)
        objectives.append(objective)


# _________________________________________________________________




score_calc = ephys.objectivescalculators.ObjectivesCalculator(objectives)
opt_params = [p.name for p in cell.params.values() if not p.frozen]
print opt_params


# _________________________________________________________________



cell_evaluator = ephys.evaluators.CellEvaluator(
        cell_model=cell,
        param_names=opt_params,
        fitness_protocols={twostep_protocol.name: twostep_protocol},
        fitness_calculator=score_calc,
        sim=nrn)



# _________________________________________________________________



import bluepyopt as bpop
optimisation = bpop.optimisations.DEAPOptimisation(
        evaluator=cell_evaluator,
        offspring_size = 8)
        

# _____________________________________________________________________

final_pop, hall_of_fame, logs, hist = optimisation.run(max_ngen=20)



#------------

for pop in final_pop:
    print pop
    
# ------------------

for fame in hall_of_fame:
    print fame

with open('hall_of_fame.json', 'w') as fp:
    json.dump(hall_of_fame, fp, indent=4, sort_keys=True)


# -------------------

best_ind_dict = cell_evaluator.param_dict(hall_of_fame[0])
print(cell_evaluator.evaluate_with_dicts(best_ind_dict))


# -----------------------


resp_best = twostep_protocol.run(cell_model=cell, param_values=best_ind_dict, sim=nrn)

plot_responses(resp_best)



