class Neuron:
    def __init__(self, name="MSN_D1", x=0, y=0, z=0):
        from loadswc import load_swc
        use_axon = False
        load_swc('morphology/WT-P270-20-14ak.swc', self,
                 use_axon=use_axon, xshift=x, yshift=y, zshift=z)
        if not use_axon:
            from neuron import h
            self.axon = [h.Section(cell=self, name='axon[0]'),
                     h.Section(cell=self, name='axon[1]')]
            for sec in self.axon:
                sec.L = 30
                sec.diam = 1
                sec.nseg = 1
            self.axon[0].connect(self.soma[0](0.5))
            self.axon[1].connect(self.axon[0](1))
            self.all += self.axon
        self._name = name
        self._insert_mechanisms()
        self._discretize_model()
        self._set_mechanism_parameters()

    def __str__(self):
        return str(self._name) + "_instance"

    def _discretize_model(self):
        for sec in self.all:
            sec.nseg = 1 + 2 * int(sec.L / 40)

    def _insert_mechanisms(self):
        for sec in self.all:
            sec.insert("pas")
        for sec in self.soma:
            for mech in [
                    "naf",
                    "kaf",
                    "kas",
                    "kdrf",
                    "kir23",
                    "cal12",
                    "cal13",
                    "can",
                    "car",
                    "cadyn",
                    "caldyn",
                    "sk",
                    "bk"
                ]:
                sec.insert(mech)
        for sec in self.axon:
            for mech in [
                    "naf",
                    "nap",
                    "kas",
                    "kdrs",
                    "kmf"
                ]:
                sec.insert(mech)
        for sec in self.dend:
            for mech in [
                    "naf",
                    "kaf",
                    "kas",
                    "kdrf",
                    "kir23",
                    "cal12",
                    "cal13",
                    "car",
                    "cat32",
                    "cat33",
                    "cadyn",
                    "caldyn",
                    "sk",
                    "bk"
                ]:
                sec.insert(mech)

    def _set_mechanism_parameters(self):
	from math import exp
	from neuron import h
	h.distance(sec=self.soma[0])
        for sec in self.all:
            sec.cm = 1
            sec.e_pas = -70
        for sec in self.axon:
            sec.g_pas = 0.0005252946510365526
            sec.Ra = 260.9720185624328
            sec.ek = -105.94
            sec.ena = 53.34
            sec.gbar_naf = 2.425636111778545
            sec.q_naf = 2.2165031972047555
            sec.shift_naf = -9.102268745227411
            sec.gbar_nap = 0.0009665228245154416
            sec.gbar_kas = 0.053441855670630714
            sec.gbar_kdrs = 0.006263082896939075
            sec.gbar_kmf = 0.0005459162156831379
        for sec in self.dend:
            sec.cm = 2.44
            sec.g_pas = 0.00016603463532606754
            sec.Ra = 396.76169312342176
            sec.ek = -105.94
            sec.ena = 53.34
	    for seg in sec:
		dist = h.distance(seg.x, sec=sec)
		seg.gbar_naf = (0.0 + 1.0*exp(dist*(-0.02))) * 0.06318466189912314
            sec.q_naf = 2.2165031972047555
            sec.shift_naf = -9.102268745227411
            sec.gbar_kaf = 0.000792298975637375
            sec.q_kaf = 1.7865586891571699
            sec.shift_kaf = 13.541498798552855
            sec.gbar_kas = 0.0064689266504902145
            sec.gbar_kdrf = 0.00017609598998262534
            sec.gbar_kir23 = 0.0003538551899103817
            sec.pbar_cal12 = 0.8e-7
            sec.pbar_cal13 = 0.1e-7
            sec.pbar_car = 10e-7
            sec.pbar_cat32 = 0.2e-8
            sec.pbar_cat33 = 0.2e-8
            sec.gbar_bk = 0.0005169282495766693
            sec.gbar_sk = 0.00465877424623525
        for sec in self.soma:
            sec.g_pas = 9.162161686884578e-05
            sec.Ra = 296.47179480870136
            sec.ek = -105.94
            sec.ena = 53.34
            sec.gbar_naf = 17.219524990038877
            sec.q_naf = 2.2165031972047555
            sec.shift_naf = -9.102268745227411
            sec.gbar_kaf = 0.00029361520024639987
            sec.q_kaf = 1.7865586891571699
            sec.shift_kaf = 13.541498798552855
            sec.gbar_kas = 0.03204274570451835
            sec.gbar_kdrf = 0.000832830676034385
            sec.gbar_kir23 = 0.0002533100011552162
            sec.pbar_cal12 = 0.8e-7
            sec.pbar_cal13 = 0.1e-7
            sec.pbar_can = 12e-7
            sec.pbar_car = 8e-7
            sec.gbar_bk = 0.000859206853059506
            sec.gbar_sk = 0.0029807083326063373

