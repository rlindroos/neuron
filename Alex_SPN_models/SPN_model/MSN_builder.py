#
'''
The MSN class defining the cell
'''

from neuron import h
import numpy as np
import json


# ======================= the MSN class ==================================================


class MSN:
    def __init__(self, params, best_params, ek, name="MSN_D1", x=0, y=0, z=0, extended=False):
        
        Import = h.Import3d_SWC_read()
        Import.input('../morphology/WT-P270-20-15ak-cor.swc')
        imprt = h.Import3d_GUI(Import, 0)
        imprt.instantiate(None)
        h.define_shape()
        
        self._name  = name
        h.celsius   = 35
        
        # create somalist and dendlist
        self._create_sectionlists(['soma', 'dend', 'axon'])
        
        # recreate axon
        diams = []
        for s,sec in enumerate(self.axonlist):
            diams.append(sec.diam)
            if s == 1:
                break
        for section in h.allsec():
            if section.name().find('axon') >= 0:
                h.delete_section(sec=section)
        
        self.axon = [   h.Section(name='axon[0]'),
                        h.Section(name='axon[1]')]
                        
        for s,sec in enumerate(self.axon):
            sec.L       = 30
            sec.diam    = diams[s]
            sec.nseg    = 1
        self.axon[0].connect(self.soma(0.5))
        self.axon[1].connect(self.axon[0](1))
        
        # create axonlist and allsec list
        self._create_sectionlists(['axon', 'all'])
        
        # insert mechanism etc
        self._insert_mechanisms()
        self._discretize_model()
        self._set_mechanism_parameters(best_params,ek)
        
        if extended:
            self._extend_Ca()
        
        
    def __str__(self):
        return str(self._name) + "_instance"

    def _discretize_model(self):
        for sec in h.allsec():
            sec.nseg = 1 + 2 * int(sec.L / 40)

    def _insert_mechanisms(self):
        for sec in h.allsec():
            sec.insert("pas")
        for sec in self.somalist:
            for mech in [
                    "naf",
                    "kaf",
                    "kas",
                    "kir",
                    "can",
                    "car",
                    "cadyn",
                    "sk",
                    "bk"
                ]:
                sec.insert(mech)
        for sec in self.dendlist:
            for mech in [
                    "naf",
                    "kaf",
                    "kas",
                    "kir"
                ]:
                sec.insert(mech)
    
    
    def _create_sectionlists(self, compartments):
        for comp in compartments:
            if comp == 'soma':
                self.somalist = h.SectionList()
                for sec in h.allsec():
                    nsomasec = 0
                    if sec.name().find('soma') >= 0:
                        self.somalist.append(sec=sec)
                        if nsomasec == 0:
                            self.soma = sec
                        else:
                            nsomasec = 1
            if comp == 'axon':
                self.axonlist = h.SectionList()
                for sec in h.allsec():
                    if sec.name().find('axon') >= 0:
                        self.axonlist.append(sec=sec)
            if comp == 'dend':
                self.dendlist = h.SectionList()
                for sec in h.allsec():
                    if sec.name().find('dend') >= 0:
                        self.dendlist.append(sec=sec)
            if comp == 'all':
                self.allseclist  = h.SectionList()
                for sec in h.allsec():
                    self.allseclist.append(sec=sec)
                
                
    def _set_mechanism_parameters(self, best_params, ek):
        h.distance(sec=self.soma)
        for sec in h.allsec():
            sec.cm      = 1
            sec.e_pas   = best_params["e_pas.all"]
            sec.Ra      = best_params["Ra.all"]
            sec.g_pas   = best_params["g_pas.all"]
        for sec in self.dendlist:
            sec.cm  =  2.44
            sec.ek  =   ek
            for seg in sec:
                dist = h.distance(seg.x, sec=sec)
                seg.gbar_naf = (0.0 + 1.0*np.exp(-0.02*dist) ) * best_params["gbar_naf.basal"]
                seg.gbar_kaf = best_params["gbar_kaf.basal"]
                seg.gbar_kas = best_params["gbar_kas.basal"]
                seg.gbar_kir = best_params["gbar_kir.basal"]
        for sec in self.somalist:
            sec.ek  =   ek
            for seg in sec:
                seg.gbar_naf = best_params["gbar_naf.somatic"]
                seg.gbar_kaf = best_params["gbar_kaf.somatic"]
                seg.gbar_kas = best_params["gbar_kas.somatic"]
                seg.gbar_kir = best_params["gbar_kir.somatic"]
                seg.pbar_can = 12e-7
                seg.pbar_car = 8e-7
                seg.gbar_bk  = best_params["gbar_bk.somatic"]
                seg.gbar_sk  = best_params["gbar_sk.somatic"]  
                
    def _extend_Ca(self):
        
        # insert additional Ca mechansims
        for sec in h.allsec():
            if sec.name().find('axon') < 0:
                
                if sec.name().find('dend') >= 0:
                    # T-type only in dendrites
                    for mech in [
                            "cav32",
                            "cav33"
                        ]:
                        sec.insert(mech)
                    # set parameters
                    for seg in sec:
                        dist = h.distance(seg.x, sec=sec)
                        seg.pbar_cav32 = 1.0e-7 / (1 + np.exp((dist-130)/(-20.0)) ) 
                        seg.pbar_cav33 = 1.0e-9 / (1 + np.exp((dist-60 )/(-60.0)) )
                
                for mech in [
                        "cal12",
                        "cal13",
                        "caldyn",
                        "car",
                        "cadyn",
                    ]:
                    sec.insert(mech)
                # set parameters
                for seg in sec:
                    seg.pbar_cal12 = 1e-6
                    seg.pbar_cal13 = 1e-7
                    seg.pbar_car   = 8e-7
                
                    

