"""Expsyn synapse parameter fitting"""

# pylint: disable=R0914


import os

import bluepyopt as bpopt
import bluepyopt.ephys as ephys

import json
import numpy as np
import pickle
import cell_model, cell_evaluator
import matplotlib.pyplot as plt
from utils import *

import local_ehpys_morphologies as local_morphologies


def save_vector(x, y, outfile):
    '''
    save vectors to file.
    
    x       = x-vector
    y       = y-vector
    outfile = file name to be used
    '''
    
    with open(outfile, "w") as out:
        for time, y in zip(x, y):
            out.write("%g %g\n" % (time, y))

def save_obj(obj, name ):
    '''
    functions used to save data in the pickle format. 
    Used to pickle dictionaries
    
    obj     = dictionary with data
    name    = name to be used, without file ending (.pkl will be added)
    '''
    
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def get_distance():
    ''' return distance of sections '''
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
        
    #protocols  = cell_evaluator.define_protocols('protocols_dSPN.json')
    #calculator = cell_evaluator.define_fitness_calculator(protocols, 'features.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    #parameters = cell_model.define_parameters('parameters.json')
    
    # def cell
    cell = ephys.models.CellModel(  name='dspn', 
                                    morph=morphology,
                                    mechs=mechanisms )
    
    cell.instantiate(sim=nrn_sim)
    
    h = nrn_sim.neuron.h
    
    secId2dist = {}
    count = 0
    for sec in h.allsec():
        if sec.name().find('dend') >= 0:
            secId2dist[ int(sec.name().split('[')[2].split(']')[0]) ] = h.distance(0.5, sec=sec)
            
            for seg in sec:
                count += 1
    
    print count
    
    return secId2dist

def main(id2dist):
    """Main"""
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    parameters = cell_model.define_parameters('parameters.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    protocol_definitions  = json.load(open('config/protocols-val.json'))
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # def morph
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
    
    # def soma location   
    somatic_loc = ephys.locations.NrnSeclistLocation(
        'somatic',
        seclist_name='somatic')

    somacenter_loc = ephys.locations.NrnSeclistCompLocation(
        name='somacenter',
        seclist_name='somatic',
        sec_index=0,
        comp_x=0.5)
    
    id2res = {}
    
    fig,ax = plt.subplots(3,1, figsize=(6,12))
    N_sec2loop = 20
    N_sec = len( id2dist )
    sec2loop = np.random.choice(N_sec, N_sec2loop, replace=False)
    for secID in range(N_sec):
        sec_loc  = []
        expsyn_mech = []
        expsyn_loc = []
        for x in range(len(sec2loop)):
            sec_loc.append( ephys.locations.NrnSeclistCompLocation(
                name='secCenter%d'%(secID),
                seclist_name='basal',
                sec_index=secID,
                comp_x=0.5))

            expsyn_mech.append( ephys.mechanisms.NrnMODPointProcessMechanism(                     
                name='tmglut',                                                              
                suffix='tmGlut',                                                            
                locations=[sec_loc[x]]))

            expsyn_loc.append( ephys.locations.NrnPointProcessLocation(
                'expsyn_loc',
                pprocess_mech=expsyn_mech[x]) )
        
        tau_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_tau',                                                          
            param_name='tau',                                                           
            value=3,                                                                       
            locations=expsyn_loc)
        tauR_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_tauR',                                                          
            param_name='tauR',                                                           
            value=200,                                                                    
            bounds=[1,1000],                                                             
            locations=expsyn_loc)
        tauF_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_tauF',                                                          
            param_name='tauF',                                                           
            value=200,                                                                    
            bounds=[0,1000],                                                             
            locations=expsyn_loc)
        U_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_U',                                                          
            param_name='U',                                                           
            value=0.3,                                                                    
            bounds=[0,1],                                                             
            locations=expsyn_loc)
        q_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_q',                                                          
            param_name='q',                                                           
            value=3,                                                                   
            locations=expsyn_loc)
        
        # def synapse
        ratio  = 5.0
        weight = 1880e-6*10.0/(3*ratio)
        ratio_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_ratio',                                                          
            param_name='nmda_ratio',                                                           
            value=ratio,                                                                  
            locations=expsyn_loc)
        t1ampa_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_t1ampa',                                                          
            param_name='tau1_ampa',                                                           
            value=1.9,                                                                  
            locations=expsyn_loc)
        t2ampa_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_t2ampa',                                                          
            param_name='tau2_ampa',                                                           
            value=4.8,                                                                  
            locations=expsyn_loc)
        t1nmda_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_t1nmda',                                                          
            param_name='tau1_nmda',                                                           
            value=12.13,                                                                  
            locations=expsyn_loc)
        t2nmda_param = ephys.parameters.NrnPointProcessParameter(                   
            name='tmglut_t2nmda',                                                          
            param_name='tau2_nmda',                                                           
            value=231,                                                                  
            locations=expsyn_loc)
        

        parameters.append(tau_param)
        parameters.append(tauR_param)
        parameters.append(tauF_param)
        parameters.append(U_param)
        parameters.append(q_param)
        
        parameters.append(ratio_param)
        parameters.append(t1ampa_param)
        parameters.append(t2ampa_param)
        parameters.append(t1nmda_param)
        parameters.append(t2nmda_param)

        #We first create a stimulus that injects the presynaptic events:
        stim_start = 200
        number     = 1
        interval   = 1    # (ms)
        stimuli    = []
        for i in range(len(expsyn_loc)):
            stim_start_individual = stim_start + i
            stimuli.append( ephys.stimuli.NrnNetStimStimulus(                                  
                total_duration=600,                                                      
                number=number,                                                                
                interval=interval,                                                              
                start=stim_start_individual,                                                        
                weight=weight,                                                           
                locations=[expsyn_loc[i]]) )

        stim_end = stim_start*2 + interval*number

        cell = ephys.models.CellModel(
            'dspn', 
            morph=morphology, 
            mechs=mechanisms+expsyn_mech, 
            params=parameters)
        
        # recordings -------------------------------------------------------
        rec = []
        rec.append( ephys.recordings.CompRecording(
            name='soma.v',
            location=somacenter_loc,
            variable='v'))
        for i,secID2 in enumerate(sec2loop): 
            rec.append( ephys.recordings.CompRecording(
                name='sec%d.v'%(secID2),
                location=sec_loc[i],
                variable='v'))

        
        # set constant istim ------------------------------------------------
        protocol_definition = protocol_definitions.items()[0] # select first prot (same in all)
        stimulus_definition = protocol_definition[1]['stimuli'][1]
        stimuli.append(ephys.stimuli.NrnSquarePulse(
            step_amplitude=stimulus_definition['amp'],
            step_delay=stimulus_definition['delay'],
            step_duration=stimulus_definition['duration'],
            location=somacenter_loc,
            total_duration=600))

        protocol = ephys.protocols.SweepProtocol(
            'netstim_protocol',
            stimuli,
            rec)

        max_volt_feature = ephys.efeatures.eFELFeature(
            'maximum_voltage',
            efel_feature_name='maximum_voltage',
            recording_names={'': 'soma.v'},
            stim_start=stim_start,
            stim_end=stim_end,
            exp_mean=-50,
            exp_std=.1)
        max_volt_objective = ephys.objectives.SingletonObjective(
            max_volt_feature.name,
            max_volt_feature)

        score_calc = ephys.objectivescalculators.ObjectivesCalculator(
            [max_volt_objective])
        
        opt_params = [p.name for p in cell.params.values() if not p.frozen]
        
        cell_evaluator = ephys.evaluators.CellEvaluator(
            cell_model=cell,
            param_names=opt_params,
            fitness_protocols={protocol.name: protocol},
            fitness_calculator=score_calc,
            sim=nrn_sim)

        best_models = json.load(open('best_models.json'))
        default_param_values = best_models[3]
        
        #default_param_values['tmglut_tau'] = 3
        default_param_values['tmglut_tauR'] = 40
        default_param_values['tmglut_tauF'] = 50
        default_param_values['tmglut_U'] = 0.32
        #default_param_values['tmglut_q'] = 2.5
        
        responses = protocol.run(                                                    
            cell_model=cell,                                                         
            param_values=default_param_values,                                              
            sim=nrn_sim)  
             
        time    = [ responses['soma.v']['time'][index]    for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]          
        voltage = [ responses['soma.v']['voltage'][index] for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
        ax[0].plot(time, voltage, 'k', alpha=0.5, lw=2)
        VM = np.zeros((N_sec2loop, len(voltage)))
        for i,secID2 in enumerate(sec2loop):
            Vl = [ responses['sec%d.v'%(secID2)]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
            ax[1].plot(time, Vl, 'r', alpha=0.3)
            VM[i,:] = Vl
        
        
        # get peak indexes
        c = (np.diff(np.sign(np.diff(voltage))) < 0).nonzero()[0] +1
        
        #if secID == 0:
         #   C = np.zeros((N_sec2loop, len(c)))
        #
        #C[secID,:] = np.array(voltage)[c]
        
        ax[0].plot(np.array(time)[c], np.array(voltage)[c], '*', ms=10 )
        ax[0].set_ylim([-88, -68])
        #plt.savefig('../../../Desktop/sss_multipli_cortical.png')
                                                              
        ax[0].set_xlabel('Time (ms)')                                                      
        ax[0].set_ylabel('Voltage (ms)')                                                   
        
        vm_mean = np.mean( VM, axis=0 )
        
        
        # normalize to amp of first epsp
        #C = C - voltage[0]
        #CT = C.transpose()
        #CN = CT / C[:,0]
        #cnorm = CN.transpose()*100
        # calc mean and std
        #cmean = np.mean(cnorm, axis=0)
        #cstd  = np.std(cnorm, axis=0)
        
        ax[1].plot(time, vm_mean, 'k', lw=3, alpha=0.5)
        #ax[2].errorbar(np.arange(len(cmean)), cmean, cstd, marker='.', ms=25, lw=2, c='r', markerfacecolor='w', markeredgecolor='r', markeredgewidth=2) 
        #ax[2].set_ylim([20,180])
        
        print( secID )
        
        amp = np.array(voltage)[c] - voltage[0]
        
        # get duration
        half_max_value       = voltage[0] + amp / 2.0
        above_half_max_index = [i for i,v in enumerate(voltage) if v >= half_max_value]
        start_index          = above_half_max_index[ 0]
        end_index            = above_half_max_index[-1]
        duration             = time[end_index] - time[start_index]
        
        id2res[secID] = {'amp':amp, 'dist':id2dist[secID], 'dur':duration, 'voltage':voltage, 'peak_index':c}
    
    # save extracted values:
    id2res['params'] = {'weight': weight, 'ratio':ratio, 'time_constants': 'time constats from tmglut?'}
    id2res['time']   = time
    save_obj(id2res, 'platau_data_pickled' )    
    
    [x1,y1] = np.loadtxt('../../results/plateau/Plotkin_plateau_D1.csv', unpack=True)
    time = [t*1000 for t in x1]
    vm   = [v*1000-5 for v in y1]
    ax[0].plot(time, vm, c='red', lw=3, ls='--')
    ax[2].plot(time, vm, c='red', lw=3, ls='--')
    
    [x2,y2] = np.loadtxt('../../results/plateau/du_plateau.csv', unpack=True)
    time = [t*(-1)+220 for t in x2]
    vm   = [v*(-1)-150 for v in y2]
    ax[0].plot(time, vm, c='red', lw=3, ls='-.')
    ax[2].plot(time, vm, c='red', lw=3, ls='-.')
    
    
    
    plt.show()    
    

if __name__ == '__main__':
    id2dist = get_distance()
    #main(id2dist)
