TITLE Persistent sodium current (Nav1.6)

NEURON {
    SUFFIX nap_ms
    USEION na READ ena WRITE ina
    RANGE gbar, gna, ina
}

UNITS {
    (S) = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

PARAMETER {
    gbar = 0.0 (S/cm2) 
    :q = 1	: room temperature 22 C
    q = 3	: body temperature 35 C
}

ASSIGNED {
    v (mV)
    ena (mV)
    ina (mA/cm2)
    gna (S/cm2)
    minf
    mtau (ms)
    hinf
    htau (ms)
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gbar*m*h
    ina = gna*(v-ena)
}

DERIVATIVE states {
    rates()
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}

INITIAL {
    rates()
    m = minf
    h = hinf
}

PROCEDURE rates() {
    UNITSOFF
    minf = 1/(1+exp((v-(-52.6))/(-4.6)))
    mtau = 0.07+0.4*exp(-((v-(-40))/13)^2)
    hinf = 1/(1+exp((v-(-48.8))/10))
    htau = 2800+3600*exp(-((v-(-66))/28)^2)
    UNITSON
}

COMMENT

Original data  and model by Magistretti and Alonso (1999) from principal
neurons of entorhinal cortex of young-adult Long-Evans rats, age P25-P35
[1].  Voltage-clamp recordings were performed at room temperature
22 C. Kinetics of m1 h1 type was used. Instantaneous activation was
replaced by fast variable [2, 3].

Smooth fit of mtau and htau by Alexander Kozlov <akozlov@kth.se>.

[1] Magistretti J, Alonso A (1999) Biophysical properties and slow
voltage-dependent inactivation of a sustained sodium current in entorhinal
cortex layer-II principal neurons: a whole-cell and single-channel
study. J Gen Physiol 114(4):491-509.

[2] Traub RD, Buhl EH, Gloveli T, Whittington MA (2003) Fast rhythmic
bursting can be induced in layer 2/3 cortical neurons by enhancing
persistent Na+ conductance or by blocking BK channels. J Neurophysiol
89(2):909-21.

[3] Wolf JA, Moyer JT, Lazarewicz MT, Contreras D, Benoit-Marand M,
O'Donnell P, Finkel LH (2005) NMDA/AMPA ratio impacts state transitions
and entrainment to oscillations in a computational model of the nucleus
accumbens medium spiny projection neuron. J Neurosci 25(40):9080-95.
ENDCOMMENT
