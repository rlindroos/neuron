COMMENT

attempt to implement striatum specific kcnq channel. 
but not working very well -> do not capture all feature of experiments?!

kcnq, non inactivating potassium current (recorded in striatal SPN)
based on Shen et al., 2005
"Cholinergic Suppression of KCNQ Channel Currents Enhances Excitability of Striatal Medium Spiny Neurons"

mod implementation by Robert Lindroos
robert. lindroos at ki. se

ENDCOMMENT

NEURON	{
	SUFFIX Im_str
	USEION k READ ek WRITE ik
	RANGE gbar, g, ik, mtau, m2tau, rtau
	RANGE mVhalf, mSlope, r
}

UNITS	{
	(S) = (siemens)
	(mV) = (millivolt)
	(mA) = (milliamp)
}

PARAMETER	{
	gbar    = 0.00001 (S/cm2) 
	mVhalf  = -42.4 (mV)
	mSlope  = -8.1 (mV)
	rtau    = 500
}

ASSIGNED	{
	v	(mV)
	ek	(mV)
	ik	(mA/cm2)
	g	(S/cm2)
	minf
	mtau
	m2inf
	m2tau
	rinf
}

STATE	{ 
	m
	m2
	r
}

BREAKPOINT	{
	SOLVE states METHOD cnexp
	ratio()
	g  = gbar*m      :( r*m + (1-r)*m2 )
	ik = g*(v-ek)
}

DERIVATIVE states	{
    ratio()
    r'  = (rinf -r )/rtau
	rates()
	m'  = (minf -m )/mtau
	m2' = (m2inf-m2)/m2tau
}

INITIAL{
    ratio()
	rates()
	m = minf
	m2 = minf
	r = rinf
}

PROCEDURE rates(){
    LOCAL fast, slow
    
    slow    = 120+4000/(1+exp((v+20.0)/-25.0))
    fast    = 62 +80/(1+exp((v+65.0)/-9)) -80/(1+exp((v+26.0)/-5.0))
    
    : time constant
    :mtau    = r * fast + (1-r) * slow 
    mtau    = fast
    m2tau   = slow
    
    minf    = 1 / (1 + exp( (v-mVhalf) / mSlope ) )
    m2inf   = 1 / (1 + exp( (v-mVhalf-40) / 100 ) )
    
}

PROCEDURE ratio(){
    : ratio of fast to slow m-gate
    rinf       = (80-(v+100)/1.5)/100
    if (rinf < 0) { rinf = 0 }
    if (rinf > 1) { rinf = 1 }    
}

