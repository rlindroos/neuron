:[$URL: https://bbpteam.epfl.ch/svn/analysis/trunk/IonChannel/xmlTomod/CreateMOD.c $]
:[$Revision: 1367 $]
:[$Date: 2010-03-26 15:17:59 +0200 (Fri, 26 Mar 2010) $]
:[$Author: rajnish $]
:Comment :
:Reference :Functional analysis of the mouse Scn8a sodium channel. J. Neurosci., 1998, 18, 6093-102

NEURON	{
	SUFFIX nav16
	USEION na READ ena WRITE ina
	RANGE gbar, gNav1_6, ina, BBiD, q
}

UNITS	{
	(S) = (siemens)
	(mV) = (millivolt)
	(mA) = (milliamp)
}

PARAMETER	{
	gbar = 0.00001 (S/cm2) 
	BBiD = 125 
	q    = 3
}

ASSIGNED	{
	v	(mV)
	ena	(mV)
	ina	(mA/cm2)
	gNav1_6	(S/cm2)
	mInf
	mTau
}

STATE	{ 
	m
}

BREAKPOINT	{
	SOLVE states METHOD cnexp
	gNav1_6 = gbar*m
	ina = gNav1_6*(v-ena)
}

DERIVATIVE states	{
	rates()
	m' = (mInf-m)/(mTau/q)
}

INITIAL{
	rates()
	m = mInf
}

PROCEDURE rates(){
	UNITSOFF 
		mInf = 1.0000/(1+ exp(-0.03937*4.2*(v - -17.000))) 
		mTau = 1
	UNITSON
}
