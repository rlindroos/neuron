
# used to test how complex spiking can be turned off in models
# -> what mechanisms are missing?
#
# This folder contains a local version of the model builder where changes are implemented
#
# common functions are imported from the D1... folder
#
# TODO:
# add nap to AIS
# add kdr to AIS
# add Na/K-ATPase (pump)
# add M-current to AIS (modulated by ACh?)
# test increased gaba in soma (FS like inhibition)
#
# can multiple neuromodulators combine to open "window" where these spikes can be created?
# e.g. serotonin excites FS and ChINs
#
# model version used: 2, pattern 33, bg 0, pattern size 32, DA 50%
#
# run from terminal using special


from __future__ import print_function, division
from neuron import h

import numpy                as np
import MSN_builder          as build        # need to be imported before D1-MSN... is added to path 
import glob
import pickle

import sys
sys.path.insert(0, '../D1-MSN_wip')
import common_functions     as use

pc = h.ParallelContext()
id = int(pc.id())

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

from scipy.signal import butter, filtfilt, freqz

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    tstop           =   1200
    cvode           =   h.CVode()
    
    
    cell_type   =   'D1'
    par         =   '../D1-MSN_wip/params_dMSN.json'
    morphology  =   '../D1-MSN_wip/WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    model_version = int(h.start_value)
    
    
    
    # load random sets
    path_to_lib         = '../D1-MSN_wip/Libraries/'
    random_variables  = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    
    
       
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    use.set_pointers(cell, pointer, mod_list)
                    
    
    ns      = {}
    nc      = {}
    Syn     = {}
    n_gaba  = 90
    ratio   = 5.0
    s_gaba = {}
    c_gaba ={}
    syn_gaba = {}
    for s,sec in enumerate(cell.dendlist):
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 15.0e-3
        else:
            gbase = 0.2e-3
        
        delay = 0
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,           \
                                NS_interval=1000.0/30.0,    \
                                NC_conductance=gbase,       \
                                NS_start=delay )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/n_gaba,       \
                                NC_conductance=gbase*300,     \
                                NS_start=delay      )
        
        Syn[sec.name()+'_glut'].ratio = 1/ratio
        
        # turn of gaba at certain time
        ns[sec.name()+'_gaba'].number = n_gaba / 2.0
        
        
        # create delayed gaba synapse
        use.random_synapse(s_gaba, c_gaba, syn_gaba, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/n_gaba,       \
                                NC_conductance=gbase*300,     \
                                NS_start=650+np.random.uniform(150)      )
    
    
    
    
    # turn off naf
    '''
    for sec in cell.axonlist:
        for seg in sec:
            for mech in seg:
                if mech.name() == 'naf':
                    print( 'naf == 0 in sec %s, seg %0.1f' % (sec.name(), seg.x) )
                    mech.gbar = 0.0
    
    
    for sec in cell.somalist:
        for seg in sec:
            for mech in seg:
                if mech.name() == 'naf':
                    print( 'naf == 0 in sec %s, seg %0.1f' % (sec.name(), seg.x) )
                    mech.gbar = 0.0
    '''
    N = 5
    for iter in range(N):
    
        # record vectors
        # -global
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        
        # finalize and run --------------------------------------------------------------------------------------------------------------------------------------------
        h.finitialize(-70)
        while h.t < tstop:
            h.fadvance()  
        
        plt.plot(tm,vm)
    
    plt.xlim([200, 1200])
    plt.savefig('../../../Dropbox/replicate_Wills_complex_spike_N=%d_NA-ratio=%d.png' % (N, ratio))
    plt.show()
   
    
