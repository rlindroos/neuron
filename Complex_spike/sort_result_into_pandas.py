
# based on plot_inVivo_upd.py in Main folder (D1_MSN-wip/InVivo/Mixed...)

import  numpy               as np
import  glob
import  multiprocessing
from    scipy               import stats
from    joblib              import Parallel, delayed

import  sys
sys.path.append('../D1-MSN_wip')

import common_functions     as use


# setup ----------------------------------------------------------------------------------
ISI             =       '1ms'
V               =       'V[0-8]'
path            =       'Simulations/2019-04-17_WithIm-lowACh/'
tag             =       'Restricted'
N               =       3

L               =       {'16':5}

time            =       np.arange(-500,500.001,50e-3)
max_time        =       200         # ms, following activation pattern
sort_index      =       range(10) #[3,1,8,10,5,6,9,7,2,4]


# functions ------------------------------------------------------------------------------





def transform_to_pandas(array, file_name, D=5, mode='a', pattern_len=16):
    
    # loop over all entries in the result array, extract fetures and writes to csv file 
    # this file can then be read into a pandas frame from a graphical tool for further instection
    
    columns = [ 'Model',
                'DA',
                'dt',
                'pattern',
                'sub_pattern',
                'pattern_len',
                'ratio',
                'count',
                'latency',
                'broken?']
    
    file = open(file_name, mode)
    
    if mode == 'w':
        file.write(",".join(columns))
    
    # create master array with all parameters
    m   =   len(array)*D
    n   =   len(columns)
    A   =   np.zeros((m,n))
    
    # loop array & pattern
    counter = 0
    for entry in array:
        model_version   = entry['V']
        dopamine_mod    = entry['mod']
        pattern         = entry['P']
        for d in range(D):
            
            # fill array
            line          = [   model_version,
                                dopamine_mod,
                                0,
                                pattern,
                                d,
                                pattern_len,
                                entry[d][1],
                                entry[d][2],
                                entry[d][3],
                                entry[d][4]     ]
            
            file.write('\n')
            file.write(",".join([str(l) for l in line]))
    
    file.close()
            
    



def check_sliding_average(trace):
    
    N = len(trace)
    n = N/40
    
    x = []
    y = []
    
    over = {'x':[],'y':[]}
    
    for s in range(N-n,0,-n):
        e = s+n
        y.append(np.mean(trace[s:e]))
        x.append(s)
        if np.mean(trace[s:e]) > -40:
               
            over['x'].append(s)
            over['y'].append(np.mean(trace[s:e]))
            
            if len(over['x']) > 4:
                return True
                
    return False
        


def get_all_data(f, time, D=5):
    
    # standardize data structure and check if complex spikes are present
    
    data    =   use.load_obj(f)
    
    V       =   int(f.split('_V')[ 1].split('_')[0])
    da      =   int(f.split('mod')[1].split('.')[0])
    P       =   int(f.split('pattern')[1].split('_')[0])
    
    M       =   {'mod':da, 'V':V, 'P':P}
    
    for key in range(D):
        
        flag    =   0
        
        for bg in range(10):    
            if check_sliding_average(data[key]['vm'][bg]):
                flag = 1   
                break
        
        M[key] = [data[key]['spikes'], data[key]['values'][0], data[key]['values'][1], data[key]['values'][3], flag, data[key]['values'][4] ]
            
            
    return M


# do the deed

for pattern_length  in ['16']:  # , '32'
    try:
        RES = use.load_obj(path+tag+pattern_length+'_removethisifwhanttouseold_gmax1000.pkl')
        print '--loading repickled data' 
    except:   
        files       =   glob.glob(path+'Im2InVivoDAfixed'+tag+'*'+ISI+'_mixed'+pattern_length+'_D1_'+V+'*') 
        num_cores   =   multiprocessing.cpu_count() 
        #for f in files:
        #    get_all_data( f, time, D=L[pattern_length] )
        RES         =   Parallel(n_jobs=num_cores)(delayed(get_all_data)( f, time, D=L[pattern_length] ) for f in files)
        use.save_obj(RES, path+tag+pattern_length+'_gmax1000' )



RES16 = use.load_obj(path+tag+'16_gmax1000.pkl')
#RES32 = use.load_obj(path+tag+'32_gmax1000.pkl')
transform_to_pandas(RES16, path+'array_large.csv', D=L['16'], mode='w', pattern_len=16)
#transform_to_pandas(RES32, path+'array_large.csv', D=L['32'], mode='a', pattern_len=32)




        
    
        
        
    
    
