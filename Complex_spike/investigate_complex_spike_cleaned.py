
# used to test how complex spiking can be turned off in models; 
#   cleaned version from test...py


# -> what mechanisms are missing?
#
# This folder contains a local version of the model builder where changes are implemented
#
# common functions are imported from the D1... folder
#
# TODO:
# add nap to AIS
# add kdr to AIS
# add Na/K-ATPase (pump)
# add M-current to AIS (modulated by ACh?)
# test increased gaba in soma (FS like inhibition)
#
# can multiple neuromodulators combine to open "window" where these spikes can be created?
# e.g. serotonin excites FS and ChINs
#
# model version used: 2, pattern 33, bg 0, pattern size 32, DA 50%
#
# run from terminal using special


from __future__ import print_function, division
from neuron import h

import numpy                as np
import MSN_builder          as build        # need to be imported before D1-MSN... is added to path 
import glob
import pickle

import sys
sys.path.insert(0, '../D1-MSN_wip')
import common_functions     as use

pc = h.ParallelContext()
id = int(pc.id())

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

from scipy.signal import butter, filtfilt, freqz

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # random_gabaergic_activation_times.pkl 
    # holds two lists of lists with activation times. 
    # Each individual list holds 10 activation times. 
    # the keys are 100 and 200 (int) and referes to the length of the intervals where spikes are chosen from (min act time is 1000)
    with open('random_gabaergic_activation_times.pkl', 'rb') as handle:
        gaba_trains = pickle.load(handle)
    
    
    DAmod           =   True
    tag             =   'Restricted'
    
    ISI             =   1
    tstop           =   1500
    cvode           =   h.CVode()
    pattern_length  =   str(int(h.pattern_length))
    
    if DAmod:
        modulate_synapse    =   True
        modulate_gaba       =   True
        modulate_axon       =   False
    else:
        modulate_synapse    =   False
        modulate_gaba       =   False
        modulate_axon       =   False
    
    cell_type   =   'D1'
    par         =   '../D1-MSN_wip/params_dMSN.json'
    morphology  =   '../D1-MSN_wip/WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    model_version = int(h.start_value)
    
    
    print(cell_type, morphology, par, 'DAmod', DAmod, h.modulation_percentage)
    
    use_randomized_naf = False
    
    # load random sets
    path_to_lib         = '../D1-MSN_wip/Libraries/'
    if use_randomized_naf: random_variables    = use.load_obj(cell_type+'_rNaf_14bestFit.pkl')[model_version]['variables']
    else: random_variables  = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    mixActPat           = use.load_obj(path_to_lib+'mixActPat'+tag+'32.pkl')
    
    # map index to unique final section
    index_map = mixActPat['map' ][1]
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
        
    modulation_factors = use.get_fixed_modulation_factors(  cell_type,                          \
                                                            mod_list+['ampa','namda','gaba'],   \
                                                            h.modulation_percentage             )
    
    
    if modulate_synapse:
        syn_fact = modulation_factors[-3:-1]
    else:
        syn_fact = False
    
    if modulate_gaba:
        gabaMod = modulation_factors[-1]
    else:
        gabaMod = False
    
       
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    if use_randomized_naf:
        # set naf shift
        for sec in h.allsec():
            if sec.name().find('axon') >= 0:
                for seg in sec:
                    seg.naf.shiftm = random_variables['naf_shift'][0]
                    seg.naf.shifth = random_variables['naf_shift'][1]
                    seg.naf.taum   = random_variables['naf_shift'][2]
                    seg.naf.tauh   = random_variables['naf_shift'][3]
                break
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
                    
                    
    # load modulation factors and scale conductances  
    if DAmod:
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )
    
    
    # set stimuli (including modulation)
    rand, ncon, stim, spike_list = use.set_mixed_stimuli(   cell,               \
                                                            mixActPat,          \
                                                            index_map,          \
                                                            syn_fact=syn_fact,  \
                                                            ISI=ISI             )
    
    VAL = {'par':    {  'DAmod':DAmod,                          \
                        'modulate_synapse':modulate_synapse,    \
                        'modulate_gaba':modulate_gaba,          \
                        'modulate_axon':modulate_axon,          \
                        'synaptic_strength':int(h.synaptic_strength)  }}
    
    tvec_local_vm   = [600,800] + range(1000, 1100, 10) + [1100, 1120, 1150, 1200, 1300]
    tvec_index_vm   = [ int(x/0.025) for x in tvec_local_vm]
    tvec_local_i    = [950]     + range(1000,1040)      + range(1040,1101,10)
    tvec_index_i    = [ int(x/0.025) for x in tvec_local_i]
    
    if int(pattern_length) < 30:
        n_iter  = len(mixActPat['steps'])
        shift   = 0
    else:
        n_iter  = len(mixActPat['steps'])+1
        shift   = 1
        
    inj    = 50
    timing = 55
    dur    = 1
    
    # set current injection
    '''
    Istim           = h.IClamp(0.5, sec=cell.soma)
    Istim.delay     =   1000 + timing
    Istim.dur       =   dur
    Istim.amp       =   inj *1e-3
    '''
        
    for i in range( n_iter ): 
        
        VAL[i] = {'vm':{}, 'local_vm':{}, 'local_I':{}, 'actSpikes':{}}
        
        # (re-)set bg and shift annealing
        
        if i > 0 or int(pattern_length) < 30:
            spike_list, stim, ncon  = use.mixed_stimuli_annealing(mixActPat, index_map, i-shift, stim, ncon, rand, spike_list)
            
            if i == 0:
                # know pattern length < 30 (since i == 0)
                # -> reset spike time 1000:1015
                rand, ncon, stim, spike_list = use.reset_mixed_stimuli(spike_list, ISI=ISI)
        
        # set bg noise (including modulation)
        Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                                syn_fact=syn_fact,              \
                                                gabaMod=gabaMod                 )
        
        
        if not i == n_iter -1: continue
        
        # change mg-block parameters
        alpha = 0.07
        for sec in cell.dendlist:
            #Syn[sec.name()+'_glut'].mg = 1.1
            Syn[sec.name()+'_glut'].alpha = alpha
        
        '''
        for key in Syn:
            if key.find('axon') >= 0 or key.find('soma') >= 0:        # key.find('soma') >= 0 
                if key.find('glut') >= 0:
                    nc[key].weight[0] = 1e-12   # turn of by setting to low value
        '''
        
         
        for iter in range(14):
        
            for sec in h.allsec():
                if sec.name().find('axon') >= 0:
                    break
        
        
            end = 200
            # set high somatic inhibition:
            INH = {'nc':[], 'ns':[], 'syn':[], 'rec':[], 'vec':[]}
            nn = 0
            train_shift = 0
            for iba in range(nn):
                INH['syn'].append( h.Exp2Syn(0.5, sec=cell.soma) )
                INH['syn'][iba].tau1       = 0.25
                INH['syn'][iba].tau2       = 3.75
                INH['syn'][iba].e          = -60
                
                # create vecstim obj
                # random spike times from list
                
                c = gaba_trains[end][iba+train_shift] # list within a list within a dict
                INH['vec'].append( h.Vector(c) )
                INH['ns'].append( h.VecStim() )
                INH['ns'][iba].play(INH['vec'][iba])
                
                '''
                # create NetStim object
                INH['ns'].append( h.NetStim() )
                INH['ns'][iba].start       = 0
                INH['ns'][iba].interval    = 100 # mean interval between two spikes in ms
                INH['ns'][iba].noise       = 1
                INH['ns'][iba].number      = 1000
                '''
                
                # create NetCon object
                INH['nc'].append( h.NetCon(INH['ns'][iba],INH['syn'][iba]) )
                INH['nc'][iba].delay       = 0
                INH['nc'][iba].weight[0]   = 1.5e-3
                INH['nc'][iba].threshold   = 0.0
                
                INH['rec'].append( h.Vector() )
                INH['rec'][iba].record( INH['syn'][iba]._ref_i )
                
            
            # simulation loop ----------------------------------------------------------------
            #for j in range(10): # only use first bg
                
            # record vectors
            # -global
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            
            
            #sec(0.8).nav16.q = 5
            # axon
            naf = h.Vector()
            naf.record( sec(0.8).naf._ref_ina)
            nam = h.Vector()
            nam.record( sec(0.8).naf._ref_m)
            nah = h.Vector()
            nah.record( sec(0.8).naf._ref_h)
            naf_mtau = h.Vector()
            naf_mtau.record( sec(0.8).naf._ref_mtau)
            naf_htau = h.Vector()
            naf_htau.record( sec(0.8).naf._ref_htau)
            # soma
            nafs = h.Vector()
            nafs.record( cell.soma(0.5).naf._ref_ina)
            nams = h.Vector()
            nams.record( cell.soma(0.5).naf._ref_m)
            nahs = h.Vector()
            nahs.record( cell.soma(0.5).naf._ref_h)
            nafs_mtau = h.Vector()
            nafs_mtau.record( cell.soma(0.5).naf._ref_mtau)
            nafs_htau = h.Vector()
            nafs_htau.record( cell.soma(0.5).naf._ref_htau)
            # other
            vma  = h.Vector()
            vma.record(sec(0.8)._ref_v)
            im = h.Vector()
            im.record( sec(0.8).Im._ref_ik)
            kas = h.Vector()
            kas.record( sec(0.8).kas._ref_ik)
            
            naf_mtau = h.Vector()
            naf_mtau.record( sec(0.8).naf._ref_mtau)
            naf_htau = h.Vector()
            naf_htau.record( sec(0.8).naf._ref_htau)
            
            
            # -local
            local_I         = {}
            for section in rand:
                secL = spike_list['name_to_sec'][section]
                
                local_I[section] = h.Vector()
                local_I[section].record(rand[section]._ref_I)
            
            bg_gaba_axon = h.Vector()
            bg_gaba_axon.record( Syn['axon[0]_gaba']._ref_i )
            bg_gaba_soma = h.Vector()
            bg_gaba_soma.record( Syn['soma[0]_gaba']._ref_i )
            bg_glut_axon = h.Vector()
            bg_glut_axon.record( Syn['axon[0]_glut']._ref_i )
            bg_glut_soma = h.Vector()
            bg_glut_soma.record( Syn['soma[0]_glut']._ref_i )
            
            
            #potential in all dendritic sections as well as conductance of bg noise (skipping nmda since slow)
            dendritc_vm = {'E':{} }
            bg_g        = {'A':{}, 'G':{}}
            nmda        = {'block':{}, 'I':{}, 'ampa':{}, 'gaba':{}}
            for dend_sec in cell.dendlist:
                # vm
                dendritc_vm['E'][dend_sec.name()] = h.Vector()   
                dendritc_vm['E'][dend_sec.name()].record( dend_sec(0.5)._ref_v )
                # ampa conductance
                bg_g['A'][dend_sec.name()] = h.Vector()
                bg_g['A'][dend_sec.name()].record( Syn[dend_sec.name()+'_glut']._ref_g_ampa )
                # gaba conductance
                bg_g['G'][dend_sec.name()] = h.Vector()
                bg_g['G'][dend_sec.name()].record( Syn[dend_sec.name()+'_gaba']._ref_g )
                # nmda block
                nmda['block'][dend_sec.name()] = h.Vector()
                nmda['block'][dend_sec.name()].record( Syn[dend_sec.name()+'_glut']._ref_block )
                # nmda current
                nmda['I'][dend_sec.name()] = h.Vector()
                nmda['I'][dend_sec.name()].record( Syn[dend_sec.name()+'_glut']._ref_i_nmda )
                # ampa current
                nmda['ampa'][dend_sec.name()] = h.Vector()
                nmda['ampa'][dend_sec.name()].record( Syn[dend_sec.name()+'_glut']._ref_i_ampa )
                # gaba current
                nmda['gaba'][dend_sec.name()] = h.Vector()
                nmda['gaba'][dend_sec.name()].record( Syn[dend_sec.name()+'_gaba']._ref_i )
            
                
            # finalize and run --------------------------------------------------------------------------------------------------------------------------------------------
            h.finitialize(-70)
            while h.t < tstop:
                h.fadvance()  
            
            # shift spike traces 
            start_index = 22000
            t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= start_index and x[0]%2==0]
            # vm
            v   = [x[1]      for x in enumerate( vm) if x[0] >= start_index and x[0]%2==0]
            va  = [x[1]      for x in enumerate(vma) if x[0] >= start_index and x[0]%2==0]
            # synaptic current
            il  = [x[1]      for x in enumerate(local_I[section] ) if x[0] >= start_index and x[0]%2==0]
            # naf axon
            nf      = [x[1] for x in enumerate(naf)         if x[0] >= start_index and x[0]%2==0]
            nm      = [x[1] for x in enumerate(nam)         if x[0] >= start_index and x[0]%2==0]
            nh      = [x[1] for x in enumerate(nah)         if x[0] >= start_index and x[0]%2==0]
            mtau    = [x[1] for x in enumerate(naf_mtau)    if x[0] >= start_index and x[0]%2==0]
            htau    = [x[1] for x in enumerate(naf_htau)    if x[0] >= start_index and x[0]%2==0]
            # soma
            nfs     = [x[1] for x in enumerate(nafs)        if x[0] >= start_index and x[0]%2==0]
            nms     = [x[1] for x in enumerate(nams)        if x[0] >= start_index and x[0]%2==0]
            nhs     = [x[1] for x in enumerate(nahs)        if x[0] >= start_index and x[0]%2==0]
            mtaus   = [x[1] for x in enumerate(nafs_mtau)   if x[0] >= start_index and x[0]%2==0]
            htaus   = [x[1] for x in enumerate(nafs_htau)   if x[0] >= start_index and x[0]%2==0]
            # other currents
            m   = [x[1]      for x in enumerate( im) if x[0] >= start_index and x[0]%2==0]
            ks  = [x[1]      for x in enumerate(kas) if x[0] >= start_index and x[0]%2==0]
            # bg
            bg_i_a = [x[1] for x in enumerate(bg_gaba_axon) if x[0] >= start_index and x[0]%2==0]
            bg_i_s = [x[1] for x in enumerate(bg_gaba_soma) if x[0] >= start_index and x[0]%2==0]
            bg_e_a = [x[1] for x in enumerate(bg_glut_axon) if x[0] >= start_index and x[0]%2==0]
            bg_e_s = [x[1] for x in enumerate(bg_glut_soma) if x[0] >= start_index and x[0]%2==0]
            # gaba
            spikes = []
            for train in INH['vec']:
                spikes = spikes + train.to_python()
            spikes = [s-1000 for s in spikes]
            for i,rec in enumerate(INH['rec']):
                r   = [x[1]  for x in enumerate( rec) if x[0] >= start_index and x[0]%2==0]
                if i == 0:  gaba_current = r
                else:       gaba_current = np.add(gaba_current, r)  
            if len(INH['rec']) == 0:
                gaba_current = np.zeros(len(t))     
                
            ALLDEND = {}
            G_BG = {'A':{},'G':{}}
            NMDA = {'block':{}, 'I':{}}
            AMPA = {}
            GABA = {}
            for key in dendritc_vm['E']:
                # vm
                ALLDEND[key] = [x[1] for x in enumerate(dendritc_vm['E'][key] ) if x[0] >= start_index and x[0]%2==0]
                # ampa
                G_BG['A'][key]    = [x[1] for x in enumerate(bg_g['A'][key] ) if x[0] >= start_index and x[0]%2==0]
                # gaba
                G_BG['G'][key]    = [x[1] for x in enumerate(bg_g['G'][key] ) if x[0] >= start_index and x[0]%2==0]
                # nmda block
                NMDA['block'][key]= [x[1] for x in enumerate(nmda['block'][key] ) if x[0] >= start_index and x[0]%2==0]
                # nmda current
                NMDA['I'][key]    = [x[1] for x in enumerate(nmda['I'][key] ) if x[0] >= start_index and x[0]%2==0]
                # ampa current
                AMPA[key]         = [x[1] for x in enumerate(nmda['ampa'][key] ) if x[0] >= start_index and x[0]%2==0]
                # ampa current
                GABA[key]         = [x[1] for x in enumerate(nmda['gaba'][key] ) if x[0] >= start_index and x[0]%2==0]
            
            # record structure
            all_traces = {  'time':t,
                            'Vm': { 'soma':v, 'axon':va, 'dendrites':ALLDEND},
                            'na': { 'axon':{ 'I':nf,  'm':nm,  'h':nh,  'mtau':mtau,  'htau':htau},
                                    'soma':{ 'I':nfs, 'm':nms, 'h':nhs, 'mtau':mtaus, 'htau':htaus}},
                            'k':  { 'M':m, 'kas':ks},
                            'bg': { 'soma_i':bg_i_s, 'soma_e':bg_e_s, 'axon_i':bg_i_a, 'axon_e':bg_e_a},
                            'gaba':{'spikes':spikes, 'sum':gaba_current, 'bg':GABA},
                            'I_syn':il,
                            'gbg':G_BG,
                            'nmda':NMDA,
                            'ampa':AMPA}
                            
            #fname_base = 'Pickled_recordings/model%d_run%d_I%d_Iinj%dms%dnAdur%dms_noKCNQ' % (model_version, iter, nn, timing, inj, dur)
            fname_base = 'Pickled_recordings/model%d_run%d_I%d_MgBlock%0.2fincreasedGmax_noKCNQ' % (model_version, iter, nn, alpha)
             
            with open(fname_base + '.pkl', 'wb') as f:
                pickle.dump(all_traces, f, pickle.HIGHEST_PROTOCOL)
            
   
    
