
# Folder testing how complex spiking can be turned off in models
# -> what mechanisms are missing?
#
# run from terminal using:
#
#       ../D1-MSN_wip/Mech/$CPU/special -c "{start_value=2 synaptic_strength=1000 modulation_percentage=50 pattern_length=32}" test_investigate_complex_spike.py
#
# This folder contains a local version of the model builder where changes are implemented
#
# common functions are imported from the D1... folder
#
# TODO:
# add nap to AIS
# add kdr to AIS
# add Na⁺/K⁺-ATPase (pump)
# add M-current to AIS (modulated by ACh?)
# test increased gaba in soma (FS like inhibition)
#
# can multiple neuromodulators combine to open "window" where these spikes can be created?
# e.g. serotonin excites FS and ChINs
#
# model version used: 2, pattern 33, bg 0, pattern size 32, DA 50%
