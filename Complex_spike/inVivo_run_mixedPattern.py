
# script used for running in vivo simultion of clustered synaptic activation
# This script is based on plateaus_in_vivo_saveFeatures.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_clustered_D<>_section<>.pkl

from __future__ import print_function, division
from neuron import h
import numpy                as np
import MSN_builder          as build
import glob
import pickle

import sys

path = '../Neuron' # '../D1-MSN_wip' #       
sys.path.insert(0, path)
import common_functions     as use

pc = h.ParallelContext()
id = int(pc.id())


h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


# import matplotlib.pyplot as plt



# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    DAmod           =   True
    tag             =   'Restricted'
    
    ISI             =   1
    tstop           =   1500
    cvode           =   h.CVode()
    pattern_length  =   str(int(h.pattern_length))
    
    if DAmod:
        modulate_synapse    =   True
        modulate_gaba       =   True
        modulate_axon       =   False
    else:
        modulate_synapse    =   False
        modulate_gaba       =   False
        modulate_axon       =   False
    
    cell_type   =   'D1'
    par         =   path+'/params_dMSN.json'
    morphology  =   path+'/WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    40
    
    norm          = id - int(np.floor(id/N))*N
    model_version = int(np.floor(id/N)) + int(h.start_value)
    '''
    norm = 32
    model_version = 5'''
    
    print(cell_type, morphology, par, 'DAmod', DAmod)
    
    
    # load random sets
    random_variables    = use.load_obj(path+'/Libraries/'+cell_type+'_71bestFit.pkl')[model_version]['variables']
    mixActPat           = use.load_obj(path+'/Libraries/mixActPat'+tag+pattern_length+'.pkl')
    
    # map index to unique final section
    index_map = mixActPat['map' ][norm]
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
        
    modulation_factors = use.get_fixed_modulation_factors(  cell_type,                          \
                                                            mod_list+['ampa','namda','gaba'],   \
                                                            h.modulation_percentage             )
    
    
    if modulate_synapse:
        syn_fact = modulation_factors[-3:-1]
    else:
        syn_fact = False
    
    if modulate_gaba:
        gabaMod = modulation_factors[-1]
    else:
        gabaMod = False
    
       
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
                    
                    
    # load modulation factors and scale conductances  
    if DAmod:
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )
    
    
    # set stimuli (including modulation)
    rand, ncon, stim, spike_list = use.set_mixed_stimuli(   cell,               \
                                                            mixActPat,          \
                                                            index_map,          \
                                                            syn_fact=syn_fact,  \
                                                            ISI=ISI             )
    
    VAL = {'par':    {  'DAmod':DAmod,                          \
                        'modulate_synapse':modulate_synapse,    \
                        'modulate_gaba':modulate_gaba,          \
                        'modulate_axon':modulate_axon,          \
                        'synaptic_strength':int(h.synaptic_strength)  }}
    
    tvec_local_vm   = [600,800] + range(1000, 1100, 10) + [1100, 1120, 1150, 1200, 1300]
    tvec_index_vm   = [ int(x/0.025) for x in tvec_local_vm]
    tvec_local_i    = [950]     + range(1000,1040)      + range(1040,1101,10)
    tvec_index_i    = [ int(x/0.025) for x in tvec_local_i]
        
    for i in range( 1+len(mixActPat['steps']) ): 
        
        VAL[i] = {'vm':{}, 'local_vm':{}, 'local_I':{}, 'actSpikes':{}}
        
        # (re-)set bg and shift annealing
        
        if i > 0:
            spike_list, stim, ncon  = use.mixed_stimuli_annealing(mixActPat, index_map, i-1, stim, ncon, rand, spike_list)
        
        print( i, len(rand) )
        
        # set bg noise (including modulation)
        Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                                syn_fact=syn_fact,              \
                                                gabaMod=gabaMod                 )
                
        # spike array
        SPIKES      = np.zeros((10,20)) 
        BROKEN      = np.zeros((10))
        
        # simulation loop ----------------------------------------------------------------
        for j in range(10):
            
            # record vectors
            # -global
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            
            # -local
            local_vm        = {}
            local_I         = {}
            for section in stim:
                sec = spike_list['name_to_sec'][section]
                
                local_vm[section] = h.Vector()
                local_vm[section].record(sec(0.5)._ref_v)
                
                local_I[section] = h.Vector()
                local_I[section].record(rand[section]._ref_I)
                
            
            # finalize and run
            h.finitialize(-80)
            while h.t < tstop:
                h.fadvance()  
                
            
            # shift spike traces 
            t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= 20000 and x[0]%2==0]
            v   = [x[1]      for x in enumerate( vm) if x[0] >= 20000 and x[0]%2==0]
            
            # get spikes (crop at 20 spikes)
            spikes = use.getSpikedata_x_y( t, v )[:20]
            SPIKES[j,:len(spikes)]  = spikes
            
            # get sliding average and check if broken
            broken, t_slide, v_slide = use.check_sliding_average(v, t) 
            n   = int(len(v      )/2) 
            n2  = int(len(v_slide)/2)
            
            # combine slide with full intensity during a 200 ms window
            V = v_slide[:n2-1] + v[n:n+int(n/2)] + v_slide[n2+int(n2/2):]
            
            # update BROKEN?
            if broken:
                BROKEN[j] = 1
                
            # save only the local time points from tvec
            for section in local_vm:
                local_vm[section] = [local_vm[section][x] for x in tvec_index_vm]
                local_I[ section] = [local_I[ section][x] for x in tvec_index_i ]
                
            VAL[i]['vm'][j]         = V
            VAL[i]['local_vm' ][j]  = local_vm
            VAL[i]['local_I'  ][j]  = local_I
            VAL[i]['actSpikes'][j]  = spike_list
            
            
            if 'time' not in VAL:
                VAL['time']         = {}
                VAL['time']['t']    = t_slide[:n2-1] + t[n:n+int(n/2)] + t_slide[n2+int(n2/2):]
                VAL['time']['tvec_local_vm']   = [x-1000 for x in tvec_local_vm]
                VAL['time']['tvec_local_i' ]   = [x-1000 for x in tvec_local_i ]
            
            
            
            # TODO  ------------------------------------
            #       -save to file instead of dict
            '''
            plt.plot(VAL['time']['t'], VAL[i]['vm'][j], 'k')
            plt.show()
            ggg'''
            
            
               
        ratio, c1, c2, t2s, t2s_mean = use.analyse_spike_data(SPIKES, max_time=200, t2s_cutoff=2)
            
        VAL[i]['values'] = [   ratio, c1, c2, t2s, t2s_mean   ]
        VAL[i]['spikes'] = SPIKES 
        VAL[i]['broken'] = BROKEN
     
        use.save_obj(VAL, 'Im2InVivoDAfixed'+tag+str(ISI)+'ms_mixed'+pattern_length+'_'+cell_type+'_V'+str(model_version)+'_pattern'+str(index_map)+'_mod'+str(int(h.modulation_percentage)) )  
        
        
   
    
