
# categorize and analyze complex spiking from pkl data
#
# 

from __future__ import print_function, division
from neuron import h

import numpy                as np
import pandas               as pd
import glob
import pickle
import sys

from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes




'''
            all_traces = {  'time':t,
                            'Vm': { 'soma':v, 'axon':va, 'dendrites':ALLDEND},
                            'na': { 'axon':{ 'I':nf,  'm':nm,  'h':nh,  'mtau':mtau,  'htau':htau},
                                    'soma':{ 'I':nfs, 'm':nms, 'h':nhs, 'mtau':mtaus, 'htau':htaus}},
                            'k':  { 'M':m, 'kas':ks},
                            'bg': { 'soma_i':bg_i_s, 'soma_e':bg_e_s, 'axon_i':bg_i_a, 'axon_e':bg_e_a},
                            'gaba':{'spikes':spikes, 'sum':gaba_current, 'bg':I},
                            'I_syn':il,
                            'gbg':G_BG,
                            'nmda':{ 'block':mg_block, 'I':current},
                            'ampa': I }
'''

# load model parameters
with open('../D1-MSN_wip/Libraries/D1_71bestFit.pkl', 'rb') as handle:
    model_params = pickle.load(handle)
    
# load a file
with open(sys.argv[1], 'rb') as handle:
    result_dict = pickle.load(handle)
    
    
# extract features

def extract_features_efel(t,y):
    # extracts electrical features from trace using bbp efel library
    
    import efel
    
    # set interpolation frequency
    #efel.setDoubleSetting('interp_step', 0.05)
    #efel.setDoubleSetting('DerivativeThreshold', 30.0)
    
    # code based on example example code from: https://github.com/BlueBrain/eFEL --------------------
    
    # A 'trace' is a dictionary
    trace1 = {}

    # Set the 'T' (=time) key of the trace
    trace1['T'] = t

    # Set the 'V' (=voltage) key of the trace
    trace1['V'] = y

    # Set the 'stim_start' (time at which a stimulus starts, in ms)
    # key of the trace
    # Warning: this need to be a list (with one element)
    trace1['stim_start'] = [0]

    # Set the 'stim_end' (time at which a stimulus end) key of the trace
    # Warning: this need to be a list (with one element)
    trace1['stim_end'] = [500]

    # Multiple traces can be passed to the eFEL at the same time, so the
    # argument should be a list
    traces = [trace1]

    # Now we pass 'traces' to the efel and ask it to calculate the feature
    # values
    features      =    [    'AP_amplitude',
                            'peak_voltage',
                            'peak_indices'  ]
    traces_results = efel.getFeatureValues(traces,
                                           features
                                          )
    
    # don't run data on solutions where features could not be extracted (due to late spike).
    '''
    if traces_results[0]['AP_amplitude'] == None:
        return None'''
    
    return traces_results



def extract_features_local(t,v, threshold=-20):
    
    # get index of values larger than threshold (-) ....---....---....--- 
    index_above_threshold = [x[0] for x in enumerate(v) if x[1] > threshold]
    
    if len(index_above_threshold) < 1:
        return -1
    
    
    # get first index of each spike sequence
    index = []
    for i in range(1,len(index_above_threshold)):
        
        if not index_above_threshold[i] == index_above_threshold[i-1]+1:
            index.append( index_above_threshold[i] )
            #print(i, index_above_threshold[i], index_above_threshold[i-1])
    
    
    # loop over all spike sequences and extract max values
    extreme_values  = { 'v' :{  'max':np.zeros(len(index)+1,dtype=int), 
                                'min':np.zeros(len(index)+1,dtype=int)},
                        'i' :{  'max':np.zeros(len(index)+1,dtype=int),
                                'min':np.zeros(len(index)+1,dtype=int)}}
    start_index     = index_above_threshold[0]
    for ii,i in enumerate(index):
        end_index   = i
        spike_seq   = v[start_index:end_index]
        extreme_values['v']['max'][ii] = max(spike_seq)
        extreme_values['v']['min'][ii] = min(spike_seq)
        extreme_values['i']['max'][ii] = np.argmax(spike_seq) + start_index
        extreme_values['i']['min'][ii] = np.argmin(spike_seq) + start_index
        start_index = i
    extreme_values['v']['max'][-1] = max(v[start_index:])
    extreme_values['v']['min'][-1] = min(v[start_index:])  
    extreme_values['i']['max'][-1] = np.argmax(v[start_index:start_index+400]) + start_index
    extreme_values['i']['min'][-1] = np.argmin(v[start_index:start_index+400]) + start_index 
    return extreme_values
     

def get_spike_times(t,curve):
    
    # differentiate
    dtrace = np.diff(curve)
        
    # take sign
    sign = np.sign(dtrace)
    
    # diff sign change
    dsign = np.diff(sign)
    
    # find pos/neg values
    index = np.add(np.where(dsign > 0.0), 1)   # np.argwhere(dsign > 0.0) + 1
    
    # get time points
    spike_times = {'t':t[index].tolist()[0], 'y':curve[index]}
    
    return spike_times


def get_current_direction_from_soma(vsoma, vaxon, vdend_dict):
    '''
    Calculates the direction of current flow between soma and neurites by comparing
        the somatic membrane potential against the initial section of each neurite
    -vsoma and vaxon are list with membrane potential over time 
    -vdend is a dict containing lists of all dendritic potentials
    * returns a dict with differences over time 
    '''
    
    # initial sections number of each dendritic stem
    initial_sections = [0, 19, 30, 31, 42, 49, 54, 55]
    
    # loop over inital sections
    DF = {'axon':np.subtract(vsoma,vaxon)} 
    for seck in vdend_dict:
        secID = int(seck.split('[')[1].split(']')[0])
        if secID in initial_sections:
            # calc diff and add to list
            i = initial_sections.index(secID)
            DF['dend%d'%(i)] = np.subtract(vsoma,vdend_dict[seck])
            
    return DF
    
    


def center_yaxis(ax):
    """center yaxis so that zero is in the middle"""
    '''
    _, y1 = ax1.transData.transform((0, v1))
    _, y2 = ax2.transData.transform((0, v2))
    inv = ax2.transData.inverted()
    _, dy = inv.transform((0, 0)) - inv.transform((0, y1-y2))
    miny, maxy = ax2.get_ylim()
    ax2.set_ylim(miny+dy, maxy+dy)'''
    
    # center plots so symetric around zero
    # TODO change ymin value so less wasted area in fig.
    xmin, xmax, ymin, ymax = ax.axis()
    y = max(ymax, -ymin)
    ax.set_ylim([-y,y])


def plot_mgblock(ax):
    
    v = np.arange(-80,40)
    mgblock =  1.0 / (1 + 1.0 * np.exp(-0.062 * v) / 3.57 )
    diff = np.diff(mgblock)
    ac = np.diff(diff)
    ax.fill_between([0,1], [-42,-42], [-47,-47], color='k', alpha=0.5)
    ax.plot(mgblock, v)
    ax.plot(np.divide(diff,max(diff)), v[:-1])
    ax.plot(np.divide(ac,max(ac)),    v[1:-1])
    ax.set_xlim([1,0])
    
    
def plot_window_current(ax):
    
    v           = np.arange(-80,40)
    mVhalf      = -25.0 
    hVhalf      = -62.0 
    mSlope      =  -9.2 
    hSlope      =   6.0     
    minf        = 1 / (1 + np.exp( (v-mVhalf) / mSlope ) )
    hinf        = 1 / (1 + np.exp( (v-hVhalf) / hSlope ) )
    
    window      = np.multiply( np.power(minf,3), hinf )
    ax.plot( np.divide(window, max(window)), v, ls='--', color='k' )
    
    #window      = np.multiply( minf, hinf )
    #ax.plot( np.divide(window, max(window)), v, ls='--', color='k' )
    


# load files
import matplotlib.pyplot as plt

fig,ax  = plt.subplots(8,1, figsize=(8,18), gridspec_kw={'height_ratios':[1,1,1,1,1,1,1,1]})
fna,ana = plt.subplots(5,1, figsize=(8,12), gridspec_kw={'height_ratios':[1,1,1,1,1]})
ff,fa   = plt.subplots(4,2, figsize=(10,10), gridspec_kw={'width_ratios':[1,2], 'height_ratios':[1,1,1,1]})
ax2 = ax[6].twinx()
fa2 = fa[2,1].twinx()

fa[0,1].plot([-50,250], [-70,-70], '--k', lw=2)

plot_window_current(fa[0,0])
plot_mgblock(fa[0,0])

print(sys.argv[1:])

colors = ['b', 'r', 'g', 'brown', 'orange']

# scale factors for sodium current (to compare with nmda; distributed vs point process)
na_seg_are = [467.59463594, 94.2477796077]  # soma and distal seg of AIS (um2)
um2_to_cm2 = 1e-8   
mA_to_nA   = 1e6
scale_factor = 1e-2
na_area_sf = np.multiply(na_seg_are,scale_factor)
	 
for i,f in enumerate(sys.argv[1:]):

    with open(f, 'rb') as handle:
        result_dict = pickle.load(handle)
    
    t = np.array( result_dict['time'] )
    v = np.array( result_dict['Vm'  ]['soma'] )
    
    # approximate current direction from soma to axon and dendrites (not scaled for axial resistance) 
    flow = get_current_direction_from_soma( result_dict['Vm']['soma'],
                                            result_dict['Vm']['axon'],
                                            result_dict['Vm']['dendrites'])
    total_flow_dend = np.zeros(len(t))
    for neurite in flow:
        if neurite == 'axon': 
            ana[3].plot(t, flow[neurite], color=colors[i])
            fa[1,1].plot(t, flow[neurite], color=colors[i])
        elif '5' in neurite:
            fa[1,1].plot(t, flow[neurite], color=colors[i], ls='--')
        else: 
            total_flow_dend = np.add(total_flow_dend,flow[neurite])
            #fa[1,1].plot(t, flow[neurite], color=colors[i])
    
    ana[4].plot(t,total_flow_dend, color=colors[i])
    fa[1,1].plot(t,total_flow_dend, color=colors[i])
    
    #res = extract_features_efel(t,v)
    extreme_values = extract_features_local(t,v,threshold=-25)
    
    # somatic potential
    ax[0].plot(t,v, color=colors[i], label=f.split('I')[1].split('_')[0])
    # peak of spikes
    if extreme_values == -1: pass #continue
    else:
        max_index = extreme_values['i']['max']
        min_index = extreme_values['i']['min']
        ax[0].plot(t[max_index], v[max_index], '.', ms=12, mew=1, mec='w', color='k')
        #fa[0,1].plot(t[max_index], v[max_index], '.', ms=12, mew=1, mec='w', color='k')
        # min voltage AHP
        #ax[0].plot(t[min_index], v[min_index], '.', ms=12, mew=1, mec='w', color='k')
    # axonal potential
    ax[0].plot(  t,result_dict['Vm']['axon'], color=colors[i])
    fa[0,1].plot(t,result_dict['Vm']['axon'], color=colors[i])
    fa[0,1].plot(t,result_dict['Vm']['soma'], color=colors[i])
    
    for k,key in enumerate(result_dict['Vm']['dendrites']):
        secID = int(key.split('[')[1].split(']')[0])
        if secID in [0, 19, 30, 31, 42, 49, 53, 54, 55]:
            alpha = 1
        else: alpha = 0.1
        ax[2].plot(t,result_dict['Vm']['dendrites'][key], color=colors[i], alpha=alpha)
        
        if k == 0:
            nmdai = result_dict['nmda']['I'][key]
            ampai = result_dict['ampa'][key]
            gabai = result_dict['gaba']['bg'][key]
            vmdend= result_dict['Vm']['dendrites'][key]
            block = result_dict['nmda']['block'][key]
            nb_nmda=np.divide(result_dict['nmda']['I'][key],result_dict['nmda']['block'][key])
        else:
            if secID == 53:
                fa[0,1].plot(t,result_dict['Vm']['dendrites'][key], color=colors[i], ls=':')  
            else:  
                nmdai = np.add(nmdai, result_dict['nmda']['I'][key])
                ampai = np.add(ampai, result_dict['ampa'][key])
                gabai = np.add(gabai, result_dict['gaba']['bg'][key])
                vmdend= np.add(vmdend,result_dict['Vm']['dendrites'][key])
                block = np.add(block, result_dict['nmda']['block'][key])
                nb_nmda=np.add(nb_nmda,np.divide(result_dict['nmda']['I'][key],result_dict['nmda']['block'][key]))
        ax[3].plot(t,result_dict['nmda']['block'][key], color=colors[i], alpha=alpha)
        ax[4].plot(t,result_dict['nmda']['I'][key], color=colors[i], alpha=alpha)
    fa[0,1].plot(t,np.divide(vmdend, len(result_dict['Vm']['dendrites'])-1), color=colors[i], lw=2)
    #fa[3,1].plot(t,np.divide(np.divide(nmdai,len(result_dict['Vm']['dendrites'])-1),np.divide(block,len(result_dict['Vm']['dendrites'])-1)), color=colors[i], lw=1, ls=':')
    fa[3,1].plot(t,nmdai, color=colors[i], lw=2)
    ax[4].plot(t,np.divide(nb_nmda,10), color=colors[i], lw=1, ls=':')
    fa[3,1].plot(t,np.divide(nb_nmda,10), color=colors[i], lw=1, ls=':')
    ax[4].plot(t,nmdai, color=colors[i], lw=2)
    ax[5].plot(t,ampai, color=colors[i], lw=2)
    ax[5].plot(t,gabai, color=colors[i], lw=2)
    ax2.hist(result_dict['gaba']['spikes'], bins=40, color=colors[i], histtype='step', alpha=0.5)
    ax[6].plot(t,result_dict['gaba']['sum'], color=colors[i], lw=2)
    ax[6].plot(t,result_dict['bg']['soma_e'], color=colors[i], lw=1)
    ax[6].plot(t,result_dict['bg']['axon_e'], color=colors[i], lw=1, ls='--')
    # sodium current
    ana[0].plot(t,result_dict['na']['soma']['I'], color=colors[i])
    ana[0].plot(t,result_dict['na']['axon']['I'], color=colors[i])
    #ana[0].plot(t,np.add(np.multiply(result_dict['na']['soma']['I'],na_area_sf[0]),np.multiply(result_dict['na']['axon']['I'],na_area_sf[1])), color=colors[i])
    #fa2.plot(t,np.multiply(result_dict['na']['soma']['I'],na_area_sf[0]), color=colors[i], ls=':')
    #fa2.plot(t,np.multiply(result_dict['na']['axon']['I'],na_area_sf[1]), color=colors[i], ls='-')
    fa[2,1].plot(t,result_dict['na']['axon']['I'], color=colors[i])
    fa[2,1].plot(t,result_dict['na']['soma']['I'], color=colors[i], ls=':')
    
    
    ana[1].plot(t,result_dict['na']['axon']['m'], color=colors[i])
    ana[1].plot(t,result_dict['na']['axon']['h'], color=colors[i])
    ana[2].plot(t,result_dict['na']['soma']['m'], color=colors[i])
    ana[2].plot(t,result_dict['na']['soma']['h'], color=colors[i])
    if i == 0:
        spike_times_glut = []
        spike_times_gaba = []
        for key in result_dict['gbg']['A']:
            spike_times_glut = spike_times_glut + list(get_spike_times(t,np.array(result_dict['gbg' ]['A'][key]))['t'])
            spike_times_gaba = spike_times_gaba + list(get_spike_times(t,np.array(result_dict['gbg' ]['G'][key]))['t'])
        #print( spike_times_glut )
        ax[7].hist(spike_times_glut, bins=400, color='orange', histtype='step', alpha=1)
        ax[7].hist(spike_times_gaba, bins=400, color='k', histtype='step', alpha=1)
    

ana[3].plot([-50,250],[0,0], '--k', lw=1)
ana[4].plot([-50,250],[0,0], '--k', lw=1)
ax2.plot([0, 300], [0,0], '--k', alpha=0.5)
ax2.set_yticks([0,2,4,6])
ylabels = ['vm soma', 'vm axon', 'vm dend', 'nmda mg-block', 'nmda I', 'ampa/gaba', 'GABA soma', 'bg E/I']
for i,a in enumerate(ax):
    a.set_xlim([-50,250])
    a.set_ylabel(ylabels[i])
    if i < 2:
        a.plot([200,200], [-80,0], 'k--')
        a.plot([0,200], [-60,-60], 'k--')
ax[0].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=3, mode="expand", borderaxespad=0.)
ylna = ['na s+a', 'gates a', 'gates s', 'axial s-a', 'axial s-d']
for i,a in enumerate(ana):
    a.set_xlim([-50,250])
    a.set_ylabel(ylna[i])
center_yaxis(ax2)

for a in fa[1:,1]:
    a.plot([-50,250], [0,0], '--k', lw=2)

for a in fa[0,:]:
    a.set_ylim([-80,40])
for a in fa[:,1]:
    a.set_xlim([-50,200])

for a in fa[1:,0]:
    a.axis('off')
for a in fa[:-2,1]:
    a.axis('off')
#fa[3,1].axis('off')

for a in fa[-2:,1]:
    a.xaxis.set_ticks_position('bottom')
    #a.spines['right'].set_visible(False)
    a.spines['top'].set_visible(False)
    a.spines['left'].set_visible(False)
    a.yaxis.set_ticks_position('right')
    #a.set_yticks([])

fa[2,1].spines['bottom'].set_visible(False)
fa[2,1].set_xticks([])
fa[2,1].set_ylim([-2.5,0.01])
fa[2,1].set_yticks([-2.0,-0.5])
fa[3,1].set_ylim([-0.45,0.01])
fa[3,1].set_yticks([-0.45,-0.3,-0.15])
fa2.set_ylim([-1.0,0.01])
fa2.axis('off')


fa[0,0].xaxis.set_ticks_position('top')
fa[0,0].spines['right'].set_visible(False)
fa[0,0].spines['bottom'].set_visible(False)
fa[0,0].yaxis.set_ticks_position('left')
fa[0,0].xaxis.set_ticks_position('top')
fig.savefig('analysing.png', bbox_inches='tight')
fna.savefig('analysing_Na.png', bbox_inches='tight')
ff.savefig( 'analysing_mgblock.png', bbox_inches='tight', transparent=True)
#plt.close('all')
plt.show()



'''
# load model parameters
with open('D1_rNaf_axon_19bestFit.pkl', 'rb') as handle:
    params_axon_only = pickle.load(handle)
with open('D1_rNaf_14bestFit.pkl', 'rb') as handle:
    params_all = pickle.load(handle)

columns     = ['vrest', 'cs']
parameters  = ['naf', 'naf_shift', 'kaf', 'kas', 'kir', 'sk', 'c32', 'c33', 'can']
for chan in parameters:
    for p in range(len(params_all[0]['variables'][chan])):
        if chan.find('_') >= 0:
            chan = chan.split('_')[1]
        columns.append( '%s_%d' % (chan,p) )

halflen = int( (len(columns)-2)/2 )

array_all   = np.zeros( (14,len(columns)) )
array_axon  = np.zeros( (19,len(columns)) )
count       = {'all':0, 'axon':0}

gs      = GridSpec( 3,len(columns)-2, height_ratios=[2, 4, 1], hspace = .1)

f2      = plt.figure(figsize=(24,18))
ax1     = plt.subplot(gs[0,:halflen])
ax2     = plt.subplot(gs[0, halflen+1:])
a2      = plt.subplot(gs[1, :])
axes2   = []
for i in range(len(columns)-2):
    axes2.append( plt.subplot(gs[2,i]) )
    
f3      = plt.figure(figsize=(24,18))
ax3     = plt.subplot(gs[0,:halflen])
ax4     = plt.subplot(gs[0, halflen+1:])
a3      = plt.subplot(gs[1, :])
axes3   = []
for i in range(len(columns)-2):
    axes3.append( plt.subplot(gs[2,i]) )
    
ax      = [[ax1,ax2],[ax3,ax4]]
'''


def best_fit_parameter_distribution(array, parameters, ax=None):
    '''
    plot distribution range of best fit solutions (normalized to parameter range)
    '''
    
    ax = ax if ax is not None else plt.gca()
    
    colors = ['k', 'orange']
    alphas = [0.3, 1.0]
    
    # parameter ranges
    param_range      = [    [-0.5,0.5], \
                            [0.8,1.0], \
                            [10.0,60.0],   \
                            [1.0,30.0],  \
                            [-5,5], \
                            [-5,5], \
                            [0.1, 0.58], \
                            [-0.5,0.5], \
                            [-0.5,0.5], \
                            [0.0,0.9], \
                            [1.0,130.0],   \
                            [-70.0,-3.0],  \
                            [-0.5,0.5],    \
                            [-5.0,60.0],  \
                            [1.0,70.0], \
                            [-0.5,0.5], \
                            [-0.5,0.5],    \
                            [-9.0,-6.0], \
                            [1.0,130.0],   \
                            [-70.0,-3.0], \
                            [-9.0,-6.0], \
                            [1.0,130.0],   \
                            [-70.0,-3.0], \
                            [-7.0,-5.0], \
                            [0.8,1.0], \
                            [10.0,60.0],   \
                            [1.0,30.0] ]
    
    #fd,ad   =   plt.subplots(1,1, figsize=(16,8))
    
    x       =   range( len(parameters)-2 )
    
    # loop rows in array
    for index in range(array.shape[0]):
        
        color = colors[int(array[index,1])]
        alpha = alphas[int(array[index,1])]
        
        # create list
        y = []
        for j in x:
            val     =   array[index,j+2]
            A       =   param_range[j][0]
            B       =   param_range[j][1]
            factor  =   (val-A) / (B-A)
            y.append(   factor  )
            
            if val > B or val < A:
                print( j, parameters[j+2], A, val, B ) 
                print()
            
        # plot
        ax.plot(x, y, '-o', ms=20, color=color, alpha=alpha)
             
    ax.set_xticks(x)
    ax.set_xticklabels(parameters[2:], fontsize=20, rotation=90)


def hinton(matrix, max_weight=None, ax=None, col=None, row=None):
    """Draw Hinton diagram for visualizing a weight matrix.
    from:
    http://python-for-multivariate-analysis.readthedocs.io/a_little_book_of_python_for_multivariate_analysis.html
    """
    ax = ax if ax is not None else plt.gca()

    if not max_weight:
        max_weight = 2**np.ceil(np.log(np.abs(matrix).max())/np.log(2))

    ax.patch.set_facecolor('lightgray')
    ax.set_aspect('equal', 'box')
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())
    
    if col and row:
        
        w       = matrix[col][row]
        color   = 'orange' if w > 0 else 'black'
        print( w, color )
        size    = np.sqrt(np.abs(w))
        rect = plt.Rectangle([0.5 - size / 2, 0.5 - size / 2], size, size,
                                 facecolor=color, edgecolor=color)
        ax.add_patch(rect)
        
    else:
        for (x, y), w in np.ndenumerate(matrix):
            color   = 'orange' if w > 0 else 'black'
            size = np.sqrt(np.abs(w))
            rect = plt.Rectangle([x - size / 2, y - size / 2], size, size,
                                 facecolor=color, edgecolor=color)
            ax.add_patch(rect)
    
    
        nticks = matrix.shape[0]
        #ax.xaxis.tick_top()
        ax.set_xticks(range(nticks))
        ax.set_xticklabels(list(matrix.columns), rotation=45, fontsize=30)
        ax.set_yticks(range(nticks))
        ax.set_yticklabels(matrix.columns, fontsize=30)
        ax.grid(False)

        ax.autoscale_view()
        ax.invert_yaxis()

def check_sliding_average(trace):
    
    N = len(trace)
    n = int(N/40)
    
    x = []
    y = []
    
    over = {'x':[],'y':[]}
    
    #print len(trace), n
    for s in range(N-n,0,-n):
        e = s+n
        y.append(np.mean(trace[s:e]))
        x.append(s)
        if np.mean(trace[s:e]) > -40:
               
            over['x'].append(s)
            over['y'].append(np.mean(trace[s:e]))
            
            if len(over['x']) > 3:
                return True
                
    return False

def check_tract_data(time, trace):

    # find index for t > 0 (clustered activation time) 
    index = next(x[0] for x in enumerate(time) if x[1] >= 0)   
    
    # ---- check if complex spikes following clustered activation
    if check_sliding_average(trace[index:]):
        cs = 1
    else: cs = 0
    
    # ---- average vm before clustered synaptic activation
    
    # clip potential spikes (to not affect average)
    clipped_vm = [-50 if x > -50 else x for x in trace[:index]]
    
    mean_vm = np.mean( clipped_vm ) 
    
    
    return mean_vm, cs

def loop_over_pickled_files(files):

    # loop over pickle files
    for f in files:
        
        # get data from file name
        if f.find('Axon') >= 0:
            axon    = True
            c       = count['axon']
        else: 
            axon = False
            c       = count['all']
        
        if c > 18: continue
        
        run     = f.split('run')[  1].split('_')[0]
        model   = f.split('model')[1].split('_')[0]
        
        # load file and extract information
        with open(f, 'rb') as handle:
            data = pickle.load(handle)
        
        # -holds complex spikes?
        vrest, cs = check_tract_data(data['time'], data['Vm']['soma'])
        
        # -get naf parameter data
        if axon:    random_variables = params_axon_only
        else:       random_variables = params_all
        
        
        shiftm = random_variables[c]['variables']['naf_shift'][0]
        shifth = random_variables[c]['variables']['naf_shift'][1]
        taum   = random_variables[c]['variables']['naf_shift'][2]
        tauh   = random_variables[c]['variables']['naf_shift'][3]
        
        chan_params = [vrest, cs]
        for chan in parameters:
            for p in range(len(random_variables[c]['variables'][chan])):
                chan_params.append( random_variables[c]['variables'][chan][p] )
        
        # sort into pandas frame
        if axon:
            array_axon[count['axon'],:] = chan_params
            count['axon'] += 1
            i = 0
        else:
            array_all[ count[ 'all'],:] = chan_params
            count[ 'all'] += 1
            i = 1
        
        # plot
        ax[i][cs].plot( data['time'], data['Vm']['soma'], 'k' )
        

# this function will probably not work. must uncomment code above 
# and maybe return something for code below to work
#loop_over_pickled_files(sys.argv[1:])

 
# analyze
''' 
df_axon = pd.DataFrame(array_axon, columns=columns)
corr1   = df_axon.corr()
print(corr1)

for i in range(len(columns)-2):
    hinton(corr1, ax=axes2[i], col='cs', row=i+2)

a2.set_ylabel('axon only', fontsize=34)
a2.set_xlim([-0.5, len(columns)-2.5])
best_fit_parameter_distribution(array_axon, columns, ax=a2)


df_all  = pd.DataFrame(array_all,  columns=columns)
corr2   = df_all.corr()
for i in range(len(columns)-2):
    hinton(corr2, ax=axes3[i], col='cs', row=i+2)
#hinton(corr2, ax=a3)
a3.set_ylabel('all', fontsize=34)
a3.set_xlim([-0.5, len(columns)-2.5])
best_fit_parameter_distribution(array_all, columns, ax=a3)


for i in [0,1]:
    for j in [0,1]:
        ax[i][j].set_ylim([-100,40])


#print(df_axon)
#print()
#print(df_all)

f3.savefig( '../../../Desktop/all_run1.png')

#plt.show() 

'''    
    
