
# used to test how complex spiking can be turned off in models
# -> what mechanisms are missing?
#
# This folder contains a local version of the model builder where changes are implemented
#
# common functions are imported from the D1... folder
#
# TODO:
# add nap to AIS
# add kdr to AIS
# add Na/K-ATPase (pump)
# add M-current to AIS (modulated by ACh?)
# test increased gaba in soma (FS like inhibition)
#
# can multiple neuromodulators combine to open "window" where these spikes can be created?
# e.g. serotonin excites FS and ChINs
#
# model version used: 2, pattern 33, bg 0, pattern size 32, DA 50%
#
# run from terminal using special


from __future__ import print_function, division
from neuron import h

import numpy                as np
import MSN_builder          as build        # need to be imported before D1-MSN... is added to path 
import glob
import pickle

import sys
sys.path.insert(0, '../D1-MSN_wip')
import common_functions     as use

pc = h.ParallelContext()
id = int(pc.id())

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

from scipy.signal import butter, filtfilt, freqz

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    tstop           =   1500
    cvode           =   h.CVode()
    
    
    cell_type   =   'D1'
    par         =   '../D1-MSN_wip/params_dMSN.json'
    morphology  =   '../D1-MSN_wip/WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    model_version = int(h.start_value)
    
    
    
    # load random sets
    path_to_lib         = '../D1-MSN_wip/Libraries/'
    random_variables  = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    
    
       
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    use.set_pointers(cell, pointer, mod_list)
                    
    
    ns      = {}
    nc      = {}
    Syn     = {}
    drivers = {'syn':{}, 'ns':{}, 'nc':{} }
    ratio   = 3.0
    minDist = 800
    gmax    = 0.18e-3
    alpha   = 0.062
    for s,sec in enumerate(cell.dendlist):
        
        # set bg noise----------------------------------
        
        key                 = sec.name() + '_glut'
        Syn[key]            = h.glutamate2(0.5, sec=sec)
        
        # create NetStim object
        ns[key]             = h.NetStim()
        ns[key].start       = 0
        ns[key].interval    = 1000.0/50.0 # mean interval between two spikes in ms
        ns[key].noise       = 0.2
        ns[key].number      = 100
        ns[key].seed( len(Syn) )
        
        # create NetCon object
        nc[key]             = h.NetCon(ns[key],Syn[key]) 
        nc[key].delay       = np.random.uniform(50)
        nc[key].weight[0]   = gmax
        nc[key].threshold   = 0.1
                                
        
        Syn[key].ratio      = 1.0/ratio
        
        Syn[key].mg         = 1
        Syn[key].alpha      = alpha
        
        # set drivers----------------------------------
        
        if 'dend' in sec.name():
            drivers['syn'][key] = h.glutamate2(0.5, sec=sec)
            
            # create NetStim object
            drivers['ns'][key]             = h.NetStim()
            drivers['ns'][key].start       = 0
            drivers['ns'][key].interval    = 2 # mean interval between two spikes in ms
            drivers['ns'][key].noise       = 1.0
            drivers['ns'][key].number      = 1
            drivers['ns'][key].seed( len(Syn) )
            
            # create NetCon object
            extra = 50
            drivers['nc'][key]             = h.NetCon(drivers['ns'][key],drivers['syn'][key]) 
            drivers['nc'][key].delay       = 1000+np.random.uniform(30)
            drivers['nc'][key].weight[0]   = gmax*extra
            drivers['nc'][key].threshold   = 0.1
            
            drivers['syn'][key].mg    = 1
            drivers['syn'][key].alpha = alpha
            drivers['syn'][key].ratio = 1.0/extra
         
        if h.distance(0.1, sec=sec) > minDist:
            key2                 = sec.name() + '_gaba'
            Syn[key2]            = h.Exp2Syn(0.1, sec=sec)
            
            # create NetStim object
            ns[key2]             = h.NetStim()
            ns[key2].start       = 0
            ns[key2].interval    = 1000.0/3.0 # mean interval between two spikes in ms
            ns[key2].noise       = 1.0
            ns[key2].number      = 20
            ns[key2].seed( len(Syn) )
            
            # create NetCon object
            nc[key2]             = h.NetCon(ns[key2],Syn[key2]) 
            nc[key2].delay       = 0
            nc[key2].weight[0]   = 1.5e-3
            nc[key2].threshold   = 0.1
        
        
    # turn off naf
    '''
    for sec in cell.axonlist:
        for seg in sec:
            for mech in seg:
                if mech.name() == 'naf':
                    print( 'naf == 0 in sec %s, seg %0.1f' % (sec.name(), seg.x) )
                    mech.gbar = 0.0
    
    
    for sec in cell.somalist:
        for seg in sec:
            for mech in seg:
                if mech.name() == 'naf':
                    print( 'naf == 0 in sec %s, seg %0.1f' % (sec.name(), seg.x) )
                    mech.gbar = 0.0
    '''
    
    N = 1
    for iter in range(N):
    
        # record vectors
        # -global
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        
        # finalize and run --------------------------------------------------------------------------------------------------------------------------------------------
        h.finitialize(-70)
        while h.t < tstop:
            h.fadvance()  
        
        plt.plot(tm,vm)
    
    plt.plot([200, 1500], [-25,-25], '--k', lw=2)
    plt.xlim([200, 1500])
    plt.savefig('../../../Dropbox/replicate_Wills_complex_spike2_N=%d_NA-ratio=%d_alpha=%0.2f_strongActivation%d.png' % (N, ratio, alpha, extra))
    plt.show()
   
    
