
# used to test how complex spiking can be turned off in models
# -> what mechanisms are missing?
#
# This folder contains a local version of the model builder where changes are implemented
#
# common functions are imported from the D1... folder
#
# TODO:
# add nap to AIS
# add kdr to AIS
# add Na/K-ATPase (pump)
# add M-current to AIS (modulated by ACh?)
# test increased gaba in soma (FS like inhibition)
#
# can multiple neuromodulators combine to open "window" where these spikes can be created?
# e.g. serotonin excites FS and ChINs
#
# model version used: 2, pattern 33, bg 0, pattern size 32, DA 50%
#
# run from terminal using special


from __future__ import print_function, division
from neuron import h

import numpy                as np
import MSN_builder          as build        # need to be imported before D1-MSN... is added to path 
import glob
import pickle

import sys
sys.path.insert(0, '../D1-MSN_wip')
import common_functions     as use

pc = h.ParallelContext()
id = int(pc.id())

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

from scipy.signal import butter, filtfilt, freqz

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # random_gabaergic_activation_times.pkl 
    # holds two lists of lists with activation times. 
    # Each individual list holds 10 activation times. 
    # the keys are 100 and 200 (int) and referes to the length of the intervals where spikes are chosen from (min act time is 1000)
    with open('random_gabaergic_activation_times.pkl', 'rb') as handle:
        gaba_trains = pickle.load(handle)
    
    
    DAmod           =   True
    tag             =   'Restricted'
    
    ISI             =   1
    tstop           =   1500
    cvode           =   h.CVode()
    pattern_length  =   str(int(h.pattern_length))
    
    if DAmod:
        modulate_synapse    =   True
        modulate_gaba       =   True
        modulate_axon       =   False
    else:
        modulate_synapse    =   False
        modulate_gaba       =   False
        modulate_axon       =   False
    
    cell_type   =   'D1'
    par         =   '../D1-MSN_wip/params_dMSN.json'
    morphology  =   '../D1-MSN_wip/WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    model_version = int(h.start_value)
    
    
    print(cell_type, morphology, par, 'DAmod', DAmod, h.modulation_percentage)
    
    use_randomized_naf = False
    
    # load random sets
    path_to_lib         = '../D1-MSN_wip/Libraries/'
    if use_randomized_naf: random_variables    = use.load_obj(cell_type+'_rNaf_14bestFit.pkl')[model_version]['variables']
    else: random_variables  = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    mixActPat           = use.load_obj(path_to_lib+'mixActPat'+tag+'32.pkl')
    
    # map index to unique final section
    index_map = mixActPat['map' ][1]
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
        
    modulation_factors = use.get_fixed_modulation_factors(  cell_type,                          \
                                                            mod_list+['ampa','namda','gaba'],   \
                                                            h.modulation_percentage             )
    
    
    if modulate_synapse:
        syn_fact = modulation_factors[-3:-1]
    else:
        syn_fact = False
    
    if modulate_gaba:
        gabaMod = modulation_factors[-1]
    else:
        gabaMod = False
    
       
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    if use_randomized_naf:
        # set naf shift
        for sec in h.allsec():
            if sec.name().find('axon') >= 0:
                for seg in sec:
                    seg.naf.shiftm = random_variables['naf_shift'][0]
                    seg.naf.shifth = random_variables['naf_shift'][1]
                    seg.naf.taum   = random_variables['naf_shift'][2]
                    seg.naf.tauh   = random_variables['naf_shift'][3]
                break
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
                    
                    
    # load modulation factors and scale conductances  
    if DAmod:
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )
    
    
    # set stimuli (including modulation)
    rand, ncon, stim, spike_list = use.set_mixed_stimuli(   cell,               \
                                                            mixActPat,          \
                                                            index_map,          \
                                                            syn_fact=syn_fact,  \
                                                            ISI=ISI             )
    
    VAL = {'par':    {  'DAmod':DAmod,                          \
                        'modulate_synapse':modulate_synapse,    \
                        'modulate_gaba':modulate_gaba,          \
                        'modulate_axon':modulate_axon,          \
                        'synaptic_strength':int(h.synaptic_strength)  }}
    
    tvec_local_vm   = [600,800] + range(1000, 1100, 10) + [1100, 1120, 1150, 1200, 1300]
    tvec_index_vm   = [ int(x/0.025) for x in tvec_local_vm]
    tvec_local_i    = [950]     + range(1000,1040)      + range(1040,1101,10)
    tvec_index_i    = [ int(x/0.025) for x in tvec_local_i]
    
    if int(pattern_length) < 30:
        n_iter  = len(mixActPat['steps'])
        shift   = 0
    else:
        n_iter  = len(mixActPat['steps'])+1
        shift   = 1
        
    for i in range( n_iter ): 
        
        VAL[i] = {'vm':{}, 'local_vm':{}, 'local_I':{}, 'actSpikes':{}}
        
        # (re-)set bg and shift annealing
        
        if i > 0 or int(pattern_length) < 30:
            spike_list, stim, ncon  = use.mixed_stimuli_annealing(mixActPat, index_map, i-shift, stim, ncon, rand, spike_list)
            
            if i == 0:
                # know pattern length < 30 (since i == 0)
                # -> reset spike time 1000:1015
                rand, ncon, stim, spike_list = use.reset_mixed_stimuli(spike_list, ISI=ISI)
        
        # set bg noise (including modulation)
        Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                                syn_fact=syn_fact,              \
                                                gabaMod=gabaMod                 )
        
        if not i == n_iter -1: continue
        
        '''
        for key in Syn:
            if key.find('axon') >= 0 or key.find('soma') >= 0:        # key.find('soma') >= 0 
                if key.find('glut') >= 0:
                    nc[key].weight[0] = 1e-12   # turn of by setting to low value'''
        
         
        for iter in range(3):
        
            for sec in h.allsec():
                if sec.name().find('axon') >= 0:
                    break
        
        
            end = 200
            # set high somatic inhibition:
            INH = {'nc':[], 'ns':[], 'syn':[], 'rec':[], 'vec':[]}
            nn = 10
            for iba in range(nn):
                INH['syn'].append( h.Exp2Syn(0.5, sec=cell.soma) )
                INH['syn'][iba].tau1       = 0.25
                INH['syn'][iba].tau2       = 3.75
                INH['syn'][iba].e          = -60
                
                # create vecstim obj
                # random spike times from list
                
                c = gaba_trains[end][iba] # list within a list within a dict
                INH['vec'].append( h.Vector(c) )
                INH['ns'].append( h.VecStim() )
                INH['ns'][iba].play(INH['vec'][iba])
                
                '''
                # create NetStim object
                INH['ns'].append( h.NetStim() )
                INH['ns'][iba].start       = 0
                INH['ns'][iba].interval    = 100 # mean interval between two spikes in ms
                INH['ns'][iba].noise       = 1
                INH['ns'][iba].number      = 1000
                '''
                
                # create NetCon object
                INH['nc'].append( h.NetCon(INH['ns'][iba],INH['syn'][iba]) )
                INH['nc'][iba].delay       = 0
                INH['nc'][iba].weight[0]   = 1.5e-3
                INH['nc'][iba].threshold   = 0.0
                
                INH['rec'].append( h.Vector() )
                INH['rec'][iba].record( INH['syn'][iba]._ref_i )
                
            
            # simulation loop ----------------------------------------------------------------
            #for j in range(10): # only use first bg
                
            # record vectors
            # -global
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            
            
            #sec(0.8).nav16.q = 5
            # axon
            naf = h.Vector()
            naf.record( sec(0.8).naf._ref_ina)
            nam = h.Vector()
            nam.record( sec(0.8).naf._ref_m)
            nah = h.Vector()
            nah.record( sec(0.8).naf._ref_h)
            naf_mtau = h.Vector()
            naf_mtau.record( sec(0.8).naf._ref_mtau)
            naf_htau = h.Vector()
            naf_htau.record( sec(0.8).naf._ref_htau)
            # soma
            nafs = h.Vector()
            nafs.record( cell.soma(0.5).naf._ref_ina)
            nams = h.Vector()
            nams.record( cell.soma(0.5).naf._ref_m)
            nahs = h.Vector()
            nahs.record( cell.soma(0.5).naf._ref_h)
            nafs_mtau = h.Vector()
            nafs_mtau.record( cell.soma(0.5).naf._ref_mtau)
            nafs_htau = h.Vector()
            nafs_htau.record( cell.soma(0.5).naf._ref_htau)
            # other
            '''
            nap = h.Vector()
            nap.record( sec(0.8).na16._ref_ina)'''
            vma  = h.Vector()
            vma.record(sec(0.8)._ref_v)
            im = h.Vector()
            im.record( sec(0.8).Im._ref_ik)
            '''
            kdr = h.Vector()
            kdr.record( sec(0.8).kdr._ref_ik)'''
            kas = h.Vector()
            kas.record( sec(0.8).kas._ref_ik)
            
            naf_mtau = h.Vector()
            naf_mtau.record( sec(0.8).naf._ref_mtau)
            naf_htau = h.Vector()
            naf_htau.record( sec(0.8).naf._ref_htau)
            
            
            # -local
            local_vm        = {}
            local_I         = {}
            for section in rand:
                secL = spike_list['name_to_sec'][section]
                
                local_vm[section] = h.Vector()
                local_vm[section].record(secL(0.5)._ref_v)
                
                local_I[section] = h.Vector()
                local_I[section].record(rand[section]._ref_I)
            
            # sec connecting stem zero (where sec 18 is attached) to soma
            for parent_stem in h.allsec():
                if parent_stem.name().find('dend[0]') >= 0:
                    break
            local_stem0 = h.Vector()
            local_stem0.record(parent_stem(0.5)._ref_v)
            
            bg_gaba_axon = h.Vector()
            bg_gaba_axon.record( Syn['axon[0]_gaba']._ref_i )
            bg_gaba_soma = h.Vector()
            bg_gaba_soma.record( Syn['soma[0]_gaba']._ref_i )
            bg_glut_axon = h.Vector()
            bg_glut_axon.record( Syn['axon[0]_glut']._ref_i )
            bg_glut_soma = h.Vector()
            bg_glut_soma.record( Syn['soma[0]_glut']._ref_i )
            
                
            # finalize and run
            h.finitialize(-70)
            while h.t < tstop:
                h.fadvance()  
            
            
            # shift spike traces 
            start_index = 22000
            t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= start_index and x[0]%2==0]
            # vm
            v   = [x[1]      for x in enumerate( vm) if x[0] >= start_index and x[0]%2==0]
            va  = [x[1]      for x in enumerate(vma) if x[0] >= start_index and x[0]%2==0]
            vl  = [x[1]      for x in enumerate(local_vm[section]) if x[0] >= start_index and x[0]%2==0]
            vl0 = [x[1]      for x in enumerate(local_stem0      ) if x[0] >= start_index and x[0]%2==0]
            # synaptic current
            il  = [x[1]      for x in enumerate(local_I[section] ) if x[0] >= start_index and x[0]%2==0]
            # naf axon
            nf      = [x[1] for x in enumerate(naf)         if x[0] >= start_index and x[0]%2==0]
            nm      = [x[1] for x in enumerate(nam)         if x[0] >= start_index and x[0]%2==0]
            nh      = [x[1] for x in enumerate(nah)         if x[0] >= start_index and x[0]%2==0]
            mtau    = [x[1] for x in enumerate(naf_mtau)    if x[0] >= start_index and x[0]%2==0]
            htau    = [x[1] for x in enumerate(naf_htau)    if x[0] >= start_index and x[0]%2==0]
            # soma
            nfs     = [x[1] for x in enumerate(nafs)        if x[0] >= start_index and x[0]%2==0]
            nms     = [x[1] for x in enumerate(nams)        if x[0] >= start_index and x[0]%2==0]
            nhs     = [x[1] for x in enumerate(nahs)        if x[0] >= start_index and x[0]%2==0]
            mtaus   = [x[1] for x in enumerate(nafs_mtau)   if x[0] >= start_index and x[0]%2==0]
            htaus   = [x[1] for x in enumerate(nafs_htau)   if x[0] >= start_index and x[0]%2==0]
            # other currents
            #pna = [x[1]      for x in enumerate(nap) if x[0] >= start_index and x[0]%2==0]
            m   = [x[1]      for x in enumerate( im) if x[0] >= start_index and x[0]%2==0]
            #kd  = [x[1]      for x in enumerate(kdr) if x[0] >= start_index and x[0]%2==0]
            ks  = [x[1]      for x in enumerate(kas) if x[0] >= start_index and x[0]%2==0]
            # bg
            bg_i_a = [x[1] for x in enumerate(bg_gaba_axon) if x[0] >= start_index and x[0]%2==0]
            bg_i_s = [x[1] for x in enumerate(bg_gaba_soma) if x[0] >= start_index and x[0]%2==0]
            bg_e_a = [x[1] for x in enumerate(bg_glut_axon) if x[0] >= start_index and x[0]%2==0]
            bg_e_s = [x[1] for x in enumerate(bg_glut_soma) if x[0] >= start_index and x[0]%2==0]
            # gaba
            spikes = []
            for train in INH['vec']:
                spikes = spikes + train.to_python()
            spikes = [s-1000 for s in spikes]
            for i,rec in enumerate(INH['rec']):
                r   = [x[1]  for x in enumerate( rec) if x[0] >= start_index and x[0]%2==0]
                if i == 0:  gaba_current = r
                else:       gaba_current = np.add(gaba_current, r)  
            if len(INH['rec']) == 0:
                gaba_current = np.zeros(len(t))           
            
            # record structure
            all_traces = {  'time':t,
                            'Vm': { 'soma':v, 'axon':va, 'dend18':vl, 'dend0':vl0},
                            'na': { 'axon':{ 'I':nf,  'm':nm,  'h':nh,  'mtau':mtau,  'htau':htau},
                                    'soma':{ 'I':nfs, 'm':nms, 'h':nhs, 'mtau':mtaus, 'htau':htaus}},
                            'k':  { 'M':m, 'kas':ks},
                            'bg': { 'soma_i':bg_i_s, 'soma_e':bg_e_s, 'axon_i':bg_i_a, 'axon_e':bg_e_a},
                            'gaba':{'spikes':spikes, 'sum':gaba_current},
                            'I_syn':il }
                            
            axI_axon = np.divide( np.subtract(va, v), cell.soma(1).ri())
            axI_dend = np.divide( np.subtract(vl0,v), cell.soma(1).ri())
            
            f,a = plt.subplots(6,1, figsize=(8,18), gridspec_kw={'height_ratios':[1,1,1,1,1,2]})
            a[0].plot(t,vl, 'b', label='dend18')
            a[0].plot(t,vl0,  'g', label='dend0')
            a[0].plot(t,va, 'k', label='axon')
            a[0].plot(t,v,  'r', label='soma')
            a[0].plot([end,end], [-80,0], 'k--')
            a[0].plot([0,200], [-60,-60], 'k--')
            # create an inset axe in the current axe:
            a[0].set_ylim([-80,50])
            a[1].hist(spikes, label='spike dist')
            a[1].set_ylim([0,15])
            a[2].plot(t,bg_i_a, 'k', label='bg_i_a')
            a[2].plot(t,bg_i_s, 'b', label='bg_i_s')
            a[2].plot(t,bg_e_a, 'g', label='bg_e_a')
            a[2].plot(t,bg_e_s, 'r', label='bg_e_s')
            a[2].set_ylim([-0.05,0.02])
            #a[3].plot(t,il, 'r', label='syn I')
            #a[3].plot(t,axI_axon, 'r', label='Ia axon')
            #a[3].plot(t,axI_dend, 'b', label='Ia dend')
            a[3].plot(t,nh, 'r', label='h-gate')
            a[3].plot(t,nm, 'g', label='m-gate')
            '''
            conductance = [np.power(x[1],3)*nh[x[0]]/0.006 for x in enumerate(nm)]
            driving_force = [(50-x)/130 for x in va]
            a[3].plot(t,conductance, 'g', label='m3h')
            a[3].plot(t,driving_force, 'k', label='df')
            dc_dt  = np.gradient(conductance,0.025)
            d2c_dt = np.gradient(dc_dt,0.025)
            ddf_dt  = np.gradient(driving_force,0.025)
            d2df_dt = np.gradient(ddf_dt,0.025)
            a[4].plot(t,d2c_dt, 'g', label='m3h')
            a[4].plot(t,np.multiply(d2df_dt,-1), 'k', label='df')
            '''
            a[4].plot(t,m, 'b', label='KCNQ')
            #a[4].plot(t,kd,'g', label='kdr')
            a[4].plot(t,ks, c='r', label='kas')
            a[4].set_ylim([-0.2,0.2])
            for i,rec in enumerate(INH['rec']):
                r   = [x[1]  for x in enumerate( rec) if x[0] >= start_index and x[0]%2==0]
                if i == 0: 
                    gaba_current = r
                else: 
                    gaba_current = np.add(gaba_current, r)
            if len(INH['rec']) == 0:
                gaba_current = np.zeros(len(t))            
            a[4].plot(t,gaba_current, 'k', label='tot gaba', lw=1) 
            
            a[5].plot(t,mtau, 'g', label='mtau')
            a[5].plot(t,htau, 'r', label='htau')
            labels = ['Vm', 'Inh dist', 'bg', 'naf parts', '2nd derivative',  'currents'] 
            for i,ax in enumerate(a):
                ax.set_xlim([-300,500])
                ax.set_title(labels[i])
                ax.legend(loc=2)
            
            fname_base = 'simulation_model%d_run%d_controlKCNQ_I%d' % (model_version, iter, nn)
            f.savefig( fname_base + '.png' ) 
            
            plt.close('all')
            
            # low pass filter
            
            # Filter requirements.
            order   = 2
            fs      = 40.0      # sample rate, Hz
            cutoff  = 0.01      # desired cutoff frequency of the filter, Hz

            # Get the filter coefficients so we can check its frequency response.
            b, a = butter_lowpass(cutoff, fs, order)
            
            colors = ['r', 'k', 'r']
            '''
            y = butter_lowpass_filter(vm, cutoff, fs, order)
            
            plt.figure()
            if iter == 1:
                plt.fill_between([1000,1200], [40,40], y2=[-70,-70], color='lightgrey')
                plt.plot([1000,1200], [-30,-30], '-w')
                plt.plot([1000,1200], [-50,-50], '-w')
                plt.plot([1100,1100], [-70,40],  '-w')
            
            plt.plot([800,1400], [-70,-70], '--k', lw=2)
            plt.plot(tm, vm, color=colors[iter], lw=1)
            plt.plot(tm, y,  color=colors[iter], lw=4)
            
            plt.ylim([-80,40])
            plt.xlim([900,1300])
            plt.axis('off')
            
            if iter == 1:
                plt.savefig( 'sim_low_passed_model%d_bg%d.png' % (model_version, iter))
            else: plt.savefig( 'sim_low_passed_model%d_bg%d.png' % (model_version, iter), transparent=True )
            
            plt.close()
            '''
            
            # persistent and transient sodium
            '''
            plt.figure()
            if iter == 1:
                plt.fill_between([1000,1200], [0,0], y2=[-3,-3], color='lightgrey')
                plt.plot([1000,1200], [-1,-1], '-w')
                plt.plot([1000,1200], [-2,-2], '-w')
                plt.plot([1100,1100], [-3,0],  '-w')
            
            y = butter_lowpass_filter(naf, cutoff, fs, order)
            
            #plt.plot(tm, naf, color=colors[iter])
            plt.plot(tm,nafs, color=colors[iter])
            plt.plot(tm, y,   color=colors[iter], lw=4)
            plt.xlim([900,1300])
            plt.ylim([-3,0.1])
            plt.axis('off')
            
            if iter == 1:
                plt.savefig( 'sim_sodium_model%d_bg%d.png' % (model_version, iter))
            else: plt.savefig( 'sim_sodium_model%d_bg%d.png' % (model_version, iter), transparent=True )
            
            #plt.show()
            plt.close()
            
            if iter == 2:
                plt.figure()
                plt.fill_between([1000,1200], [0,0], y2=[-3,-3], color='lightgrey')
                plt.plot([1000,1200], [-1,-1], '-w')
                plt.plot([1000,1200], [-2,-2], '-w')
                plt.plot([1100,1100], [-3,0],  '-w')
                
                plt.plot(tm,nafs, color='m', lw=1) 
                plt.plot(tm, naf, 'k', lw=2)
                plt.xlim([900,1300])
                plt.ylim([-3,0.1])
                plt.axis('off')
                
                i_ax = inset_axes(plt.gca(),
                                  height="30%", # set height
                                  width ="30%", # and width
                                  loc=7)
                i_ax.fill_between([1020,1170], [0,0], y2=[-1,-1], color='w')
                i_ax.plot(tm,nafs, color='m', lw=1) 
                i_ax.plot(tm, naf, 'k', lw=2)
                i_ax.set_xlim([1020,1170])
                i_ax.set_ylim([-0.3,0])
                i_ax.set_xticks([])
                i_ax.set_yticks([])
                
                plt.savefig( 'sim_sodium_AIS_model%d_bg%d.png' % (model_version, iter))
                plt.show()
            '''
             
            '''
            with open(fname_base + '.pkl', 'wb') as f:
                pickle.dump(all_traces, f, pickle.HIGHEST_PROTOCOL)'''
                
            
            # inhibition
            
            plt.figure()
            
            plt.fill_between([0,200], [40,40], y2=[-70,-70], color='lightgrey')
            plt.plot([0,200], [-30,-30], '-w')
            plt.plot([0,200], [-50,-50], '-w')
            plt.plot([100,100], [-70,40],  '-w')
            plt.plot(t,v, color='k', lw=2)
            plt.ylim([-80,40])
            plt.xlim([-100,300])
            plt.axis('off')
            plt.savefig( 'sim_inh_vm_model%d_bg%d.png' % (model_version, iter))
            
            
            plt.figure(figsize=(6,2))
            
            plt.hist(spikes)
            plt.ylim([0,15])
            plt.xlim([-100,300])
            plt.axis('off')
            plt.savefig( 'sim_inh_hist_model%d_bg%d.png' % (model_version, iter), transparent=True)
            
            
            plt.figure(figsize=(6,3))
            
            plt.fill_between([0,200], [0.2,0.2], y2=[-0.1,-0.1], color='lightgrey')
            plt.plot([0,200], [0,0], '-w')
            plt.plot([0,200], [0.1,0.1], '-w')
            plt.plot([100,100], [-0.1,0.2],'-w')
            
            for i,rec in enumerate(INH['rec']):
                r   = [x[1]  for x in enumerate( rec) if x[0] >= start_index and x[0]%2==0]
                plt.plot(t,r, 'w', lw=1, alpha=0.5)
                if i == 0: 
                    gaba_current = r
                else: 
                    gaba_current = np.add(gaba_current, r)
            if len(INH['rec']) == 0:
                gaba_current = np.zeros(len(t))            
            plt.plot(t,gaba_current, 'k', label='tot gaba', lw=2)
            
            plt.ylim([-0.1,0.2])
            plt.xlim([-100,300])
            plt.axis('off')
            plt.savefig( 'sim_inh_I_model%d_bg%d.png' % (model_version, iter), transparent=True)
            
            print('iter %d done' % (iter))
            
        
        
        
        
   
    
