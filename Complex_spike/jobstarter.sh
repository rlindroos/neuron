#!/bin/bash -l


#sbash pgenesis_Ri.job <nodes> <pfile> <TES/DOP> <rcr> <freq>

ARG1=0
while [  $ARG1 -lt 7 ]; do

        ARG2=0
        while [  $ARG2 -lt 110 ]; do
            
            # start simulations
            sbatch inVivo_mixed.job $ARG1 $ARG2
            sleep 1s
            
            # increase ARG2
            let ARG2=ARG2+50 
            
        done

    
    
    let ARG1=ARG1+3

done

