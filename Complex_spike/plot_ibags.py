
# plot things for IBAGS poster



from __future__ import print_function, division
from neuron import h

import numpy                as np
import pandas               as pd
import pickle
import glob

from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes

import  multiprocessing
from    joblib              import Parallel, delayed

plt.style.use('ggplot')


V   = 5
P   = 32
DA  = 0
# (version, pattern, DAmod%)
'''
f   =   '../D1-MSN_wip/Results/InVivo_sim_res/Oct2018/MixedPattern/Restricted/' \
        'InVivoDAfixedRestricted1ms_mixed16_D1_V%d_pattern%d_mod%d.pkl' % (V,P,DA) '''
#f   =   'Im3InVivoDAfixedRestricted1ms_mixed16_D1_V%d_pattern%d_mod%d.pkl' % (V,P,DA) 
f = 'Simulations/2019-04-17_WithIm-lowACh/Im2InVivoDAfixedRestricted1ms_mixed16_D1_V%d_pattern%d_mod%d.pkl' % (V,P,DA)


def check_sliding_average(trace):
    
    N = len(trace)
    n = int(N/40)
    
    x = []
    y = []
    
    over = {'x':[],'y':[]}
    
    for s in range(N-n,0,-n):
        e = s+n
        y.append(np.mean(trace[s:e]))
        x.append(s)
        if np.mean(trace[s:e]) > -40:
               
            over['x'].append(s)
            over['y'].append(np.mean(trace[s:e]))
            
            if len(over['x']) > 4:
                return True
                
    return False
    

def plot_individual_traces(f):
    
    fig, ax = plt.subplots(2,1, figsize=(6,6), gridspec_kw = {'height_ratios':[4, 1]})
    
    with open(f, 'rb') as handle:
        data = pickle.load(handle)

    time    = data['time']['t']
    key     = 4 # full cluster; (16 activations)

    broken = 0
    for bg in range(10):    
        trace = data[key]['vm'][bg]
        
        tt = [trace[i] for i,t in enumerate(time) if t >=0]
        if check_sliding_average(tt):
            c = 'r'
            broken += 1
        else:
            c = 'k'
            
            
        ax[0].plot(time, trace, color=c)

    ax[0].set_xlim([-200, 400]) 
    ax[0].set_ylim([-90, 50])  
    print( broken)
    ax[1].barh([1], [10-broken], color='k')
    ax[1].barh([1], [broken], color='r', left=10-broken)
    ax[1].axis('off')
    #fig.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/example_DA%d_P%d_V%d_ACh0_2' % (DA,P,V) )
    plt.show()  


#plot_individual_traces(f)


# ----------------------------------------------------------------------------------------
# Population analysis (spike prob and complex spike prob)

def analyse_population_from_pandas(df):

    # extract data
    filtered_df = df[df.pattern_len == 16]

    print( list(filtered_df) )

    for i in filtered_df.Model.unique():
        print('-----------------------')
        print('\tmodel',i)
        for da in [0, 100]:
            df_by_model = filtered_df[  (filtered_df['Model']==i)   &
                                        (filtered_df['DA']==da)     ]
            # get y-value (last in list)
            #print( df_by_model['ratio'].value_counts() )
            
            print( df_by_model.groupby('sub_pattern')['broken?'].value_counts() )
            

    # plot 

# import pandas frame
#df = pd.read_csv( '../../Dash/Restricted.csv' )
#df = pd.read_csv( 'Simulations/2019-04-17_WithIm-lowACh/array_large.csv' )
#analyse_population_from_pandas(df)

# ----------------------------------------------------------------------------------------
# use raw data

# stage 1: from raw data to single pickle ----------

def read_raw_pickle_into_dict(file_name_base, key=0):
    RES     = {}
    for mv in range(9):
        RES[mv] = {}
        for da in [0,100]: 
            
            RES[mv][da] = { 'cs': [], 'ratios':[] }
            
            broken  = 0
            count   = 0
            
            files = glob.glob('%s*mixed16_D1_V%d_pattern*_mod%d.pkl' % (file_name_base,mv,da) )
            
            for f in files:
                with open(f, 'rb') as handle:
                    data = pickle.load(handle)  
                
                time = data['time']['t']
                
                RES[mv][da]['ratios'].append( data[key]['values'][0] )
                  
                for bg in range(10):    
                    trace = data[key]['vm'][bg]
                    tt    = [trace[i] for i,t in enumerate(time) if t >=0]
                    if check_sliding_average(tt):
                        broken += 1
                        RES[mv][da]['cs'].append( 1 )
                    else:
                        RES[mv][da]['cs'].append( 0 )
                    count += 1
            
            RES[mv][da]['cs_ratio'] = 1.0 * broken / count
            
            
    return RES

'''
with_M = Parallel(n_jobs=5)(delayed(read_raw_pickle_into_dict)( 
                        'Simulations/2019-04-17_WithIm-lowACh/Im2InVivoDAfixedRestricted1ms', key=key ) for key in range(0,5,2))  
no_M   = Parallel(n_jobs=5)(delayed(read_raw_pickle_into_dict)( 
                        '../D1-MSN_wip/Results/InVivo_sim_res/Oct2018/MixedPattern/Restricted/InVivoDAfixedRestricted1ms', key=key ) for key in range(0,5,2))      
               
with open('analyse_stage1_from_raw_pickle_with_M-current.pkl', 'wr') as handle:
    pickle.dump(with_M, handle, pickle.HIGHEST_PROTOCOL)
with open('analyse_stage1_from_raw_pickle_no_M-current.pkl', 'wr') as handle:
    pickle.dump(no_M, handle, pickle.HIGHEST_PROTOCOL) 
'''
        
# stage 2: import stage 1 pickle and plot ----------   


with open('analyse_stage1_from_raw_pickle_with_M-current.pkl', 'rb') as handle:
    with_M = pickle.load(handle) 
with open('analyse_stage1_from_raw_pickle_no_M-current.pkl', 'rb') as handle:
    no_M = pickle.load(handle)  


def sort_into_array( no_M, with_M, feature='ratios', ramping=False ):
    
    array = np.zeros((2,3,9,2,40))

    # degree of clustering
    for i in range(3):
        
        # model version
        for mv in range(9):   
            
            # da level
            for da,DA in enumerate([0,100]):
                #print(i,ms,da)
                array[0,i,mv,da] =   no_M[i][mv][DA][feature]
                array[1,i,mv,da] = with_M[i][mv][DA][feature]
    
    return array


array1 = sort_into_array( no_M, with_M, feature='ratios' )
         
P = [ np.mean( array1[1,:,:,0,:] ), np.mean( array1[0,:,:,0,:] ), np.mean( array1[1,:,:,1,:] ), np.mean( array1[0,:,:,1,:] ) ]
x  = range(len(P))
plt.bar(x,P, color='k')
plt.xticks( np.arange(0.4,4, 1), ['control', 'high ACh', 'high DA', 'both high'], fontsize=20, rotation=45)
plt.ylim([0,1])

plt.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/spike_prob_Full_population.png', bbox_inches='tight')

# complex spikes
plt.figure()

array2 = sort_into_array( no_M, with_M, feature='cs_ratio' )
'''
array = np.zeros((2,3,9,2,1))

# degree of clustering
for i in range(3):
    
    # model version
    for mv in range(9):   
        
        # da level
        for da,DA in enumerate([0,100]):
            #print(i,ms,da)
            array[0,i,mv,da] =   no_M[i][mv][DA]['cs_ratio']
            array[1,i,mv,da] = with_M[i][mv][DA]['cs_ratio']'''
            
CS = [ np.mean( array2[1,:,:,0,:] ), np.mean( array2[0,:,:,0,:] ), np.mean( array2[1,:,:,1,:] ), np.mean( array2[0,:,:,1,:] ) ]
print(CS)
x  = range(len(CS))
plt.bar(x,CS, color='r')
plt.xticks( np.arange(0.4,4, 1), ['control', 'high ACh', 'high DA', 'both high'], fontsize=20, rotation=45)
plt.ylim([0,1])

plt.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/complex_spike_prob_Full_population.png', bbox_inches='tight')

plt.close('all')  

# resort array based on complex spike occurance
'''
A1 = np.zeros(array1.shape)
A2 = np.zeros(array2.shape)

for i, index in enumerate([2,5,7,1,6,0,8,3,4]):
    
    A1[:,:,i,:,:] = array1[:,:,index,:,:]
    A2[:,:,i,:,:] = array2[:,:,index,:,:]
    

# separate based on pattern

pind = []
cind = []
for i in range(3):
    P   = [ np.mean( A1[1,i,0:,0,:] ), np.mean( A1[0,i,0:,0,:] ), np.mean( A1[1,i,0:,1,:] ), np.mean( A1[0,i,0:,1,:] ) ] 
    CS  = [ np.mean( A2[1,i,0:,0,:] ), np.mean( A2[0,i,0:,0,:] ), np.mean( A2[1,i,0:,1,:] ), np.mean( A2[0,i,0:,1,:] ) ]   
    
    pind = pind + P
    cind = cind + CS
    
x  = range(0+0, 0+1*3) + range(1+3,1+2*3) + range(2+2*3,2+3*3) + range(3+3*3,3+4*3)

print (x)
sorter = range(0,len(cind),4) + range(1,len(cind),4) + range(2,len(cind),4) + range(3,len(cind),4)

print (sorter)

plt.figure()
plt.bar(x,np.array(cind)[sorter], color='r')
plt.ylim([0,1])
plt.xticks([])
plt.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/complex_spike_prob_Full_population_div.png', bbox_inches='tight')
plt.figure()
plt.bar(x,np.array(pind)[sorter], color='k')
plt.ylim([0,1])
plt.xticks([])
plt.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/spike_prob_Full_population_div.png', bbox_inches='tight')

plt.close('all')

# separate based on model

fig,ax = plt.subplots(2,9, figsize=(20,6) )

x = range(4)
for i in range(9):
    
    P   = [ np.mean( A1[1,:,i,0,:] ), np.mean( A1[0,:,i,0,:] ), np.mean( A1[1,:,i,1,:] ), np.mean( A1[0,:,i,1,:] ) ] 
    CS  = [ np.mean( A2[1,:,i,0,:] ), np.mean( A2[0,:,i,0,:] ), np.mean( A2[1,:,i,1,:] ), np.mean( A2[0,:,i,1,:] ) ]   
    
    ax[0,i].bar(x,P, color='k')
    ax[1,i].bar(x,CS, color='r')
    
    ax[0,i].set_ylim([0,1])
    ax[1,i].set_ylim([0,1])
    
    ax[0,i].set_xticks([])
    ax[1,i].set_xticks([])
    
fig.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/complex_spike_prob_over_model.png', bbox_inches='tight')    

plt.close('all')
    
'''
plt.figure(figsize=(4,4))
f   =   '../D1-MSN_wip/Results/InVivo_sim_res/Oct2018/MixedPattern/Restricted/' \
        'InVivoDAfixedRestricted1ms_mixed16_D1_V%d_pattern%d_mod%d.pkl' % (1,1,100)
        
with open(f, 'rb') as handle:
    data = pickle.load(handle)
time    = data['time']['t']
trace   = data[4]['vm'][3]

plt.fill_between([0,200], [40,40], y2=[-70,-70], color='lightgrey')
plt.plot([-80,300], [-70,-70], '--k')
plt.plot([0,200], [-30,-30], '-w')
plt.plot([0,200], [-50,-50], '-w')
plt.plot([100,100], [-70,40], '-w')
plt.plot(time, trace, color='r')
plt.fill_between([-100,-80], [40,40], y2=[-70,-70], color='w')
plt.xlim([-100,300])
plt.axis('off')
plt.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/complex_spike_example.png', bbox_inches='tight')
plt.close('all')
# low pass filter
from scipy.signal import butter, filtfilt, freqz

plt.figure()
plt.plot(time, trace, color='r')

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# Filter requirements.
order   = 2
fs      = 40.0      # sample rate, Hz
cutoff  = 0.04      # desired cutoff frequency of the filter, Hz

# Get the filter coefficients so we can check its frequency response.
b, a = butter_lowpass(cutoff, fs, order)

t = [x[1] for x in enumerate(time) if x[1] >=0 and x[1] <= 200]
d = [trace[x[0]] for x in enumerate(time) if x[1] >=0 and x[1] <= 200]
y = butter_lowpass_filter(d, cutoff, fs, order)
plt.plot(t, y, '-b', lw=2)

plt.show()

gggh

# ----------------------------------------------------------------------------------------
# ramping data

# stage 1: from raw data to single pickle ----------

def read_ramping_pickle_into_dict(file_name_base, key=0):
    RES     = {}
    for mv in range(9):
        RES[mv] = {}
        for da in [0,100]: 
            
            RES[mv][da] = { 'cs': [], 'ratios':[], 'broken':{} }
            
            broken  = 0
            count   = 0
            
            files = glob.glob('%s_D1_V%d_mod%d_pattern%d.pkl' % (file_name_base,mv,da,key) )
            
            for f in files:
                with open(f, 'rb') as handle:
                    data = pickle.load(handle)  
                
                time = data['time']['t']
                
                RES[mv][da]['ratios'].append( data[key]['values'][0] )
                  
                for bg in range(10):    
                    trace = data[key]['vm'][bg]
                    tt    = [trace[i] for i,t in enumerate(time) if t >=0]
                    if check_sliding_average(tt):
                        RES[mv][da]['broken'][broken] = trace
                        broken += 1
                        RES[mv][da]['cs'].append( 1 )
                    else:
                        RES[mv][da]['cs'].append( 0 )
                    count += 1
            
            RES[mv][da]['cs_ratio'] = 1.0 * broken / count
            
    RES['time'] = time        
    return RES

'''
with_M = Parallel(n_jobs=5)(delayed(read_ramping_pickle_into_dict)( 
                        'Simulations/2019-04-23_ramping_BG/RampingIm', key=key ) for key in range(5))  
no_M   = Parallel(n_jobs=5)(delayed(read_ramping_pickle_into_dict)( 
                        'Simulations/2019-04-23_ramping_BG/Ramping',   key=key ) for key in range(5))      
               
with open('analyse_stage1_from_ramping_pickle_with_M-current.pkl', 'wr') as handle:
    pickle.dump(with_M, handle, pickle.HIGHEST_PROTOCOL)
with open('analyse_stage1_from_ramping_pickle_no_M-current.pkl', 'wr') as handle:
    pickle.dump(no_M, handle, pickle.HIGHEST_PROTOCOL) 
'''
        
# stage 2: import stage 1 pickle and plot ----------   


with open('analyse_stage1_from_ramping_pickle_with_M-current.pkl', 'rb') as handle:
    with_M = pickle.load(handle) 
with open('analyse_stage1_from_ramping_pickle_no_M-current.pkl', 'rb') as handle:
    no_M = pickle.load(handle)  


fig, ax = plt.subplots(2,1, figsize=(6,12) )
for results in with_M:
    for mv in results:
        if mv == 'time': continue
        for da in results[mv]:
            for key in results[mv][da]['broken']:
                ax[0].plot(results['time'], results[mv][da]['broken'][key], c='k') 
for results in no_M:
    for mv in results:
        if mv == 'time': continue
        for da in results[mv]:
            for key in results[mv][da]['broken']:
                ax[1].plot(results['time'], results[mv][da]['broken'][key], c='r')


def sort_into_ramping_array( no_M, with_M, feature='ratios' ):
    
    array = np.zeros((2,9,2,5))
    
    # model version
    for mv in range(9):   
        
        # da level
        for da,DA in enumerate([0,100]):
            
            # pattern version
            for i in range(5):
                array[0,mv,da,i] =   no_M[i][mv][DA][feature]
                array[1,mv,da,i] = with_M[i][mv][DA][feature]
    
    return array


array1 = sort_into_ramping_array( no_M, with_M, feature='cs_ratio' )
         
P = [ np.mean( array1[1,:,0,:] ), np.mean( array1[0,:,0,:] ), np.mean( array1[1,:,1,:] ), np.mean( array1[0,:,1,:] ) ]
x  = range(len(P))
plt.figure()
plt.bar(x,P, color='r')
plt.xticks( np.arange(0.4,4, 1), ['control', 'high ACh', 'high DA', 'both high'], fontsize=20, rotation=45)
plt.ylim([0,1])

plt.savefig('../../../Dropbox/Karolinska/Konferenser/ibags2019/Poster/ramping_cs_prob_full_pop.png', bbox_inches='tight')
plt.show()
