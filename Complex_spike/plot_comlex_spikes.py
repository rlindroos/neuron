
# used to test how complex spiking can be turned off in models
# -> what mechanisms are missing?
#
# This folder contains a local version of the model builder where changes are implemented
#
# common functions are imported from the D1... folder
#
# TODO:
# add nap to AIS
# add kdr to AIS
# add Na/K-ATPase (pump)
# add M-current to AIS (modulated by ACh?)
# test increased gaba in soma (FS like inhibition)
#
# can multiple neuromodulators combine to open "window" where these spikes can be created?
# e.g. serotonin excites FS and ChINs
#
# model version used: 2, pattern 33, bg 0, pattern size 32, DA 50%
#
# run from terminal using special


from __future__ import print_function, division
from neuron import h

import numpy                as np
import glob
import pickle

from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes



# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # random_gabaergic_activation_times.pkl 
    # holds two lists of lists with activation times. 
    # Each individual list holds 10 activation times. 
    # the keys are 100 and 200 (int) and referes to the length of the intervals where spikes are chosen from (min act time is 1000)
    
    files = glob.glob('Figures/*.pkl')
    
    for f in files:
        
        with open(f, 'rb') as handle:
            data = pickle.load(handle)
        
            
        T   = data['time']
        Vm  = data['Vm']  
        Na  = data['na']
        K   = data['k']
        Bg  = data['bg']  
            
        # record structure
        '''
        all_traces = {  'time':t,
                        'Vm': { 'soma':v, 'axon':va, 'dend18':vl, 'dend0':vl0},
                        'na': { 'axon':{ 'I':nf,  'm':nm,  'h':nh,  'mtau':mtau,  'htau':htau},
                                'soma':{ 'I':nfs, 'm':nms, 'h':nhs, 'mtau':mtaus, 'htau':htaus},
                                'nap':pna},
                        'k':  { 'M':m, 'kdr':kd, 'kas':ks},
                        'bg': { 'soma_i':bg_i_s, 'soma_e':bg_e_s, 'axon_i':bg_i_a, 'axon_e':bg_e_a},
                        'I_syn':il }
        '''
        
        specs   =   {'c':['k','r','g','b'], 'ls':['-','--','-','--'], } 
        fig     =   plt.figure(figsize=(30,16))

        gs=GridSpec(5,4) # 4 rows, 4 columns

        a00=fig.add_subplot(gs[0,0]) # First row, first column
        a01=fig.add_subplot(gs[1,0]) # First row, second column
        a02=fig.add_subplot(gs[2,0]) # First row, third column
        a03=fig.add_subplot(gs[3,0]) # First row, fourth column
        a04=fig.add_subplot(gs[4,0])
        a10=fig.add_subplot(gs[0,1])
        a11=fig.add_subplot(gs[1,1]) 
        a12=fig.add_subplot(gs[2,1])
        a13=fig.add_subplot(gs[3,1])
        a14=fig.add_subplot(gs[4,1])
        
        ax4=fig.add_subplot(gs[:,2:]) # span all rows and last two columns
        
        axes = [a00, a01, a02, a03, a04, a10, a11, a12, a13, a14, ax4]
        
        # Vm
        a00.plot(T,Vm['dend18'], 'b', label='dend18')
        for i,comp in enumerate(['axon', 'soma']):
            a00.plot( T, Vm[comp], c=specs['c'][i], label=comp ) 
            
            # sodium
            a02.plot(T, Na[comp]['I'], c=specs['c'][i], label=comp)
            
            m3h     = [np.power(x[1],3)*Na[comp]['h'][x[0]] for x in enumerate(Na[comp]['m'])]
            df      = [ (50-x)/130 for x in Vm[comp] ]
            # 1st derivative
            dm3h    = np.gradient(m3h,0.025)
            ddf     = np.gradient(df,0.025)
            # 2nd derivative
            d2m3h   = np.gradient(dm3h,0.025)
            d2df    = np.gradient(ddf,0.025)
            
            a03.plot(T, d2m3h,  c=specs['c'][i], ls=specs['ls'][i], label=comp)
            a04.plot(T, d2df,   c=specs['c'][i], ls=specs['ls'][i], label=comp )
            
            a12.plot(T, m3h,    c=specs['c'][i], ls=specs['ls'][i], label=comp)
            
            
        a00.plot([200,200], [-80,0], 'k--')
        a00.plot([0,200], [-60,-60], 'k--')
        a00.set_ylim([-80,50])
        
        # synaptic current (of clustered activation; dend 18)
        a10.plot(T, data['I_syn'])
        
        # bg
        for i,key in enumerate(Bg):
            a01.plot(T,Bg[key], c=specs['c'][i], label=key)
        a01.set_ylim([-0.05,0.02])
        
        # Potassium
        for i,key in enumerate(K):
            a11.plot(T,K[key], c=specs['c'][i], label=key) 
        
        
        '''
        a11.plot(t,m, 'b', label='KCNQ')
        a11.plot(t,kd,'g', label='kdr')
        a11.plot(t,nf, c='r', label='naf')
        a11.plot(t,pna, c='y', label='nap')
        a11.set_ylim([-0.2,0.2])
        inset_ax = inset_axes(a11,
                              height="40%", # set height
                              width ="25%", # and width
                              loc=1)
        inset_ax.plot(t,m, c='b')  
        for i,rec in enumerate(INH['rec']):
            r   = [x[1]  for x in enumerate( rec) if x[0] >= start_index and x[0]%2==0]
            if i == 0: 
                label = 'gaba'
                gaba_current = r
            else: 
                label = ''
                gaba_current = np.add(gaba_current, r)
            a11.plot(t,r, 'k', label=label, alpha=0.3) 
            inset_ax.plot(t,r, 'k', alpha=0.3)  
        if len(INH['rec']) == 0:
            gaba_current = np.zeros(len(t))            
        a11.plot(t,gaba_current, 'k', label='tot gaba', lw=1) 
        inset_ax.plot(t,gaba_current, 'k', lw=1)    
        inset_ax.set_xlim([0,  200])
        inset_ax.set_ylim([0,0.02])
        inset_ax.axis('off')
        inset_2 = inset_axes(a11,
                              height="40%", # set height
                              width ="25%", # and width
                              loc=4)
        inset_2.plot(t,nm, c='r') 
        inset_2.plot(t,nh, c='b')      
        inset_2.set_xlim([0,  200])
        inset_2.set_ylim([0,1])
        inset_2.axis('off')
        '''
        a13.plot(T,Na['axon']['mtau'], 'k', label='mtau')
        a13.plot(T,Na['axon']['htau'], 'r', label='htau')
        labels = ['Vm', 'bg', 'Ina', 'd2dm3h', 'd2df', 'Isyn', 'Ik', 'm3h', 'Natau', 'jao', 'jao', 'jao', 'jao', 'jao' ] 
        for i,ax in enumerate(axes):
            ax.set_xlim([-100,250])
            ax.set_title(labels[i])
            ax.legend(loc=2)
        
        #fname_base = 'Figures/specific_noise_run%d_0_control_%d_controlKCNQ' % (iter, nn)
        #fig.savefig( fname_base + '.png' ) 
        
        plt.figure(figsize=(6,3))
        plt.plot(T, Vm['dend18'])
        plt.xlim([-100,250])
        plt.ylim([-80,  50])
        fig.savefig( 'more.png',  bbox_inches='tight' )
        plt.savefig( 'local.png', bbox_inches='tight' )
        
        plt.show()
        plt.close('all')    
        
        
        
        
        
   
    
