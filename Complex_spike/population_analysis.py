
# categorize and analyze complex spiking from pkl data
'''
Compares multiple traces with and without complex spikes and tries to see trends in 
peak spike amplitudes and min vm of ahp before the spike. 

general alg:
-cathagorize as cs/non-cs
-extract peak and dipps between spikes
-plot the two populations and save data to file
'''

from __future__ import print_function, division
from neuron import h

import matplotlib.pyplot    as plt
import numpy                as np
#import glob
import pickle
import sys

import functions4analysis   as f4a

from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import inset_axes

from joblib import Parallel, delayed
import multiprocessing




'''
            all_traces = {  'time':t,
                            'Vm': { 'soma':v, 'axon':va, 'dendrites':ALLDEND},
                            'na': { 'axon':{ 'I':nf,  'm':nm,  'h':nh,  'mtau':mtau,  'htau':htau},
                                    'soma':{ 'I':nfs, 'm':nms, 'h':nhs, 'mtau':mtaus, 'htau':htaus}},
                            'k':  { 'M':m, 'kas':ks},
                            'bg': { 'soma_i':bg_i_s, 'soma_e':bg_e_s, 'axon_i':bg_i_a, 'axon_e':bg_e_a},
                            'gaba':{'spikes':spikes, 'sum':gaba_current, 'bg':I},
                            'I_syn':il,
                            'gbg':G_BG,
                            'nmda':{ 'block':mg_block, 'I':current},
                            'ampa': I }
'''

# load model parameters
with open('../D1-MSN_wip/Libraries/D1_71bestFit.pkl', 'rb') as handle:
    model_params = pickle.load(handle)
    


print(sys.argv[1:], '\n')

print(len(sys.argv[1:]), '\n')

fig, ax = plt.subplots(2,2, figsize=(16,12))
f2, a2  = plt.subplots(2,1, figsize=(16,24))
#fig, ax = plt.subplots(2,2, figsize=(6,6), gridspec_kw = {'width_ratios':[3, 1]})
plt.subplots_adjust(wspace=0.01)

colors  = ['k', 'r']
endC    = ['g', 'b']

regions     = ['soma', 'axon', 'dendrites']



# extract and save data

def allocate_to_workers(files, load=False, regions=['soma', 'axon', 'dendrites']):
    
    if not load:
        num_cores = 6
        R = Parallel(n_jobs=num_cores)(delayed(f4a.extract_extreme_values)( i,f, regions=regions ) for i,f in enumerate(files) )
        with open('extreme_values_extracted_withTraces.pkl', 'wb') as handle:   
            pickle.dump(R, handle, pickle.HIGHEST_PROTOCOL)
    else:
        with open('extreme_values_extracted_withTraces.pkl', 'rb') as handle:   
            R = pickle.load(handle)
    
    all_data  = {}
    
    # plot --------
    t = R[0]['time']
    
    for res in R:
        
        if not res or res['soma']['extreme_values'] == -1: continue
        
        all_data[res['file']] = {}
        
        for r,region in enumerate(regions):  
            if region == 'dendrites':
                all_data[res['file']][region] = {}
                for dend in res[region]:
                    extreme_values  = res[region][dend]['extreme_values']
                    cs              = res[region][dend]['cs']
                    v               = res[region][dend]['vm']
                    # potential
                    a2[cs].plot(t,v, color=colors[cs])
                    if extreme_values == -1: continue   #print(extreme_values, dend)
                    max_index = extreme_values['i']['max']
                    min_index = extreme_values['i']['min']
                    a2[cs].plot(t[max_index], v[max_index], '.', ms=12, mew=1, mec='w', color=colors[cs])
                    # min voltage AHP
                    a2[cs].plot(t[min_index], v[min_index], '.', ms=12, mew=1, mec='w', color=colors[cs])
                    
                    all_data[res['file']][region][dend] = {'cs':cs, 'extreme_values': extreme_values}
                    
            else:
                    
                extreme_values  = res[region]['extreme_values']
                cs              = res[region]['cs']
                v               = res[region]['vm']
                # potential
                ax[cs,r].plot(t,v, color=colors[cs])
                max_index = extreme_values['i']['max']
                min_index = extreme_values['i']['min']
                ax[cs,r].plot(t[max_index], v[max_index], '.', ms=12, mew=1, mec='w', color=colors[cs])
                # min voltage AHP
                ax[cs,r].plot(t[min_index], v[min_index], '.', ms=12, mew=1, mec='w', color=colors[cs])
                
                all_data[res['file']][region] = {'cs':cs, 'extreme_values': extreme_values}
        
    return all_data
all_data = allocate_to_workers(sys.argv[1:], load=False, regions=['soma', 'axon', 'dendrites'])
with open('extreme_values_extracted.pkl', 'wb') as handle:   
    pickle.dump(all_data, handle, pickle.HIGHEST_PROTOCOL)
fig.savefig('extreme_values_org.png')
f2.savefig('extreme_values_dend_org.png')
plt.show()
'''

# load and plot data
with open('extreme_values_extracted.pkl', 'rb') as handle:
    all_data = pickle.load(handle)

singles = {0:{'i':[],'v':[]},1:{'i':[],'v':[]}}    
dipp = {0:[], 1:[]}
peak = {0:[], 1:[]}
for f in all_data:
    
    extreme_values  = all_data[f]['extreme_values']
    cs              = all_data[f]['cs']
    
    # peak of spikes
    if extreme_values == -1: pass #continue
    else:
        max_index   = extreme_values['i']['max']
        min_index   = extreme_values['i']['min']
        max_voltage = extreme_values['v']['max']
        min_voltage = extreme_values['v']['min']
        
        diff = np.diff(max_voltage)
        if len(diff) == 0: 
            #ax[0].plot(max_index, max_voltage, '*', ms=30, mew=3, mec='w', color=endC[cs])
            singles[cs]['i'].append( max_index[0] )
            singles[cs]['v'].append( max_voltage[0] )
            continue
        elif len(diff) > 1:
            if diff[-1] > diff[-2]: 
                diff = diff[:-1]
                max_index = max_index[:-1]
                max_voltage = max_voltage[:-1]
            
        ax[0,0].plot(max_index, max_voltage, '-o', ms=5, mew=1, mec='w', color=colors[cs])
        # min voltage AHP
        ax[1,0].plot(min_index, min_voltage, '-o', ms=5, mew=1, mec='w', color=colors[cs])
        
        i = -1
        ax[0,0].plot(max_index[i], max_voltage[i], 'o', ms=10, mew=1, mec='w', color=endC[cs])
        peak[cs].append( max_voltage[i] )
        
        if len(min_index) > 1: i = -2
        else: continue
        ax[1,0].plot(min_index[i], min_voltage[i], 'o', ms=10, mew=1, mec='w', color=endC[cs])
        dipp[cs].append( min_voltage[i] )
        
        # diff amp
        
        #if max_index[-1] > 15000 and len(diff) > 1: i = -2
        #else: i = -1 
        #ax[2].plot(max_index[1:], diff, '-o', ms=5, mew=1, mec='w', color=colors[cs])
        #ax[2].plot(max_index[-1], diff[-1], 'o', ms=10, mew=1, mec='w', color=endC[cs])
    
for cs in [0, 1]:
    ax[0,0].plot(singles[cs]['i'], singles[cs]['v'], '*', ms=20, mew=1, mec='w', color=endC[cs]) 
    print(cs, len( singles[cs]['i'] ) )
    print( singles[cs]['i'] )
    print( singles[cs]['v'] )  

ax[1,1].hist(dipp[0], 20, histtype='stepfilled',
            orientation='horizontal', color=endC[0], alpha=0.5)
ax[1,1].hist(dipp[1], 20, histtype='stepfilled',
            orientation='horizontal', color=endC[1], alpha=0.5)
ax[0,1].hist(peak[0], 20, histtype='stepfilled',
            orientation='horizontal', color=endC[0], alpha=0.5)
ax[0,1].hist(peak[1], 20, histtype='stepfilled',
            orientation='horizontal', color=endC[1], alpha=0.5)
ax[1,1].set_ylim([-80,-35])
ax[0,1].set_ylim([-30, 40])
ax[0,1].axis('off')
ax[1,1].axis('off')
#ax[0,0].set_xlim([])
fig.savefig('extreme_value_peaks.png')
plt.show()
'''


'''
# load model parameters
with open('D1_rNaf_axon_19bestFit.pkl', 'rb') as handle:
    params_axon_only = pickle.load(handle)
with open('D1_rNaf_14bestFit.pkl', 'rb') as handle:
    params_all = pickle.load(handle)

columns     = ['vrest', 'cs']
parameters  = ['naf', 'naf_shift', 'kaf', 'kas', 'kir', 'sk', 'c32', 'c33', 'can']
for chan in parameters:
    for p in range(len(params_all[0]['variables'][chan])):
        if chan.find('_') >= 0:
            chan = chan.split('_')[1]
        columns.append( '%s_%d' % (chan,p) )

halflen = int( (len(columns)-2)/2 )

array_all   = np.zeros( (14,len(columns)) )
array_axon  = np.zeros( (19,len(columns)) )
count       = {'all':0, 'axon':0}

gs      = GridSpec( 3,len(columns)-2, height_ratios=[2, 4, 1], hspace = .1)

f2      = plt.figure(figsize=(24,18))
ax1     = plt.subplot(gs[0,:halflen])
ax2     = plt.subplot(gs[0, halflen+1:])
a2      = plt.subplot(gs[1, :])
axes2   = []
for i in range(len(columns)-2):
    axes2.append( plt.subplot(gs[2,i]) )
    
f3      = plt.figure(figsize=(24,18))
ax3     = plt.subplot(gs[0,:halflen])
ax4     = plt.subplot(gs[0, halflen+1:])
a3      = plt.subplot(gs[1, :])
axes3   = []
for i in range(len(columns)-2):
    axes3.append( plt.subplot(gs[2,i]) )
    
ax      = [[ax1,ax2],[ax3,ax4]]
'''



        


    
