from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')
    

# simulation length
tstop = 1100

# create cell
dend = h.Section(name='dend')
dend.insert('pas')
dend.e_pas = -70
dend.g_pas = 1.25e-6

# add new receptors

# create synapse
x = 0.5
syn         = h.tmGabaA(x, sec=dend)
#syn.use_stp = 0

stim = h.VecStim()
vec = h.Vector(np.array([0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,1.0])*1e3)
stim.play(vec)


# create NetCon object
ncon             = h.NetCon(stim, syn)
ncon.delay       = 1
ncon.weight[0]   = 2000.5e-3 # (uS) = 1.5 nS
#ncon.threshold   = 0.1

# record vectors
vm = h.Vector()
vm.record(dend(0.5)._ref_v)
tm  = h.Vector()
tm.record(h._ref_t)
im  = h.Vector()
im.record(syn._ref_i)


lw = [9,6,3]
c  = ['y', 'g', 'r']
for i in range(3):

    h.finitialize(-70)
    while h.t < tstop:
        h.fadvance()
    
    plt.plot(tm,vm, lw=lw[i], c=c[i], label=str(i))

for i in range(3,6):
    
    # create cell
    dend = h.Section(name='dend')
    dend.insert('pas')
    dend.e_pas = -70
    dend.g_pas = 1.25e-6

    # add new receptors

    # create synapse
    x = 0.5
    syn         = h.tmGabaA(x, sec=dend)
    #syn.use_stp = 0

    stim = h.VecStim()
    vec = h.Vector(np.array([0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,1.0])*1e3)
    stim.play(vec)


    # create NetCon object
    ncon             = h.NetCon(stim, syn)
    ncon.delay       = 1
    ncon.weight[0]   = 2000.5e-3 # (uS) = 1.5 nS
    #ncon.threshold   = 0.1

    # record vectors
    vm = h.Vector()
    vm.record(dend(0.5)._ref_v)
    tm  = h.Vector()
    tm.record(h._ref_t)
    im  = h.Vector()
    im.record(syn._ref_i)

    h.finitialize(-70)
    while h.t < tstop:
        h.fadvance()
    
    plt.plot(tm,vm, lw=2, c='k', label=str(i) )

plt.legend()
plt.savefig('testing_reinitiation.png')
plt.show()
