#!/bin/bash
# run_Genesis.sh 
# Changes certain instances from genesis files and launch genesis.
# Enables looping over variables
# Ver. 1.1, 2014-02-13; Auth. Robert Lindroos

inFile="NeuronComparison.g"
outFile="Results/newCoordList.dat"

#for line in $ (cat Results/coordList.dat) do echo "$ line" done 

comp="COMP"
name="NAME"

while read line           
do           
    echo $line
    splitName=(${line//_/ })
    neuron=`echo ${splitName[0]}`
    genesis=`echo ${splitName[1]}`
    neuron="\"$neuron\""
    genesis="\"$genesis\""
    echo $neuron $genesis
    

    sed -i "s/$comp/$genesis/" $inFile
	 sed -i "s/$name/$neuron/" $inFile
	 #genesis $inFile 
	 sed -i "s/$genesis/$comp/" $inFile
	 sed -i "s/$neuron/$name/" $inFile
          
done <$outFile


