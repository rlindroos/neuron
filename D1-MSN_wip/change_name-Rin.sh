#!/bin/bash

# switch two strings in a filename for all files in a folder.

for name in ./InVivo*


do
	#split name on -
	splitName=(${name//mixed/ })

	#extract compartment name
	start=`echo ${splitName[0]}`
	end=`echo ${splitName[1]}`
	
	splitEnd=(${end//_D1/ })
    end=`echo ${splitEnd[1]}`
    
	tag="mixed16from32_D1"
	
	#echo $start $newEnd


	#concatenate to make new name correcting for wrong sign in current
	#newName="$begOfName$first$second-$third.dat"
	newName=$start$tag$end	#"./Results/Rin/Rin_${comp}_morph${morph}_current$-{current}.dat"
	
	echo $name
	echo $newName
	echo ''

	#finaly, uncheck to rename 
	#DON'T UNCHECK UNLESS U KNOW WHAT YOU ARE DOING. YOU MIGHT ERASE THE FILES...
	mv $name $newName

done

exit -1
