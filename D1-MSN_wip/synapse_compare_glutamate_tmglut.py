
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import  numpy                as np
import  MSN_builder          as build
import  common_functions     as use
from    scipy            import stats
from    scipy.optimize   import curve_fit
import  glob
import  pickle

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        


def create_segment_list(cell, dist_groups):
    
    # create segment lists
    segments    = {} 
    
    for d in dist_groups:
        segments[d] = []
          
    # sort segments into list            
    for sec in cell.dendlist:
             
        for seg in sec:
            
            dist = h.distance(seg.x, sec=sec)
            
            if dist < 60:
                segments[0].append(seg)
            elif dist < 120:
                segments[1].append(seg)
            elif dist < 180:
                segments[2].append(seg)
            else:
                segments[3].append(seg)
    
    return segments
    



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    

    
    


            
            


def set_stimuli(cell,               \
                activationPattern,  \
                syn_fact=False,     \
                gabaMod=False       ):
    
    rand    = {}
    stim    = {}
    ncon    = {}
    vmL     = {}
    cur     = {}
    detect  = {}
    delay   = np.arange(1000, 1010, 1)
    
    # set random stimuli
    for sec in cell.dendlist:
        
        for seg in sec:  
        
            if seg in activationPattern:
                
                key = sec.name() + '_' + str(seg.x)
                
                # get index of activation
                index = activationPattern.index(seg)
                
                # create synapse
                rand[key]        =   h.glutamate(seg.x, sec=sec) # changed from x -> seg.x
                rand[key].ratio  =   1.0/3.0
                
                if syn_fact:
                    rand[key].ampa_scale_factor = syn_fact[0]
                    rand[key].nmda_scale_factor = syn_fact[1]


                # create NetStim object
                stim[key]            = h.NetStim()
                stim[key].number     = 2
                stim[key].start      = delay[index]
                stim[key].interval   = 10 # interval between two spikes in ms (1000 / 50 Hz = 20 ms)

                # create NetCon object
                ncon[key]            = h.NetCon(stim[key], rand[key])
                ncon[key].delay      = 1
                ncon[key].weight[0]  = 1.5e-3 # (uS) = 1.5 nS
                
                #vmL[key] = h.Vector()
                #vmL[key].record(sec(seg.x)._ref_v)
                cur[key] = h.Vector()
                cur[key].record(rand[key]._ref_I)
                
                # set pointer to right target...
                pointer             = rand[key]._ref_i_ampa
    
    return cur, rand, ncon, stim 
    
            

    
def func(x, a, b, c):
    
    return a * np.exp( (x-b) / c )   
        
    
def find_time_constants(trace, t, ax=False):
    
    # rise time. 10-90% ------------------------
    
    
    # make sure positive deflection by ... 
    shifted     =       np.subtract(trace, trace[0]) 
    pos         =       list(np.abs(shifted))
    
    # get peak value and index of peak
    Max         =       max(pos)
    maxIndex    =       pos.index(Max)
    
    # calc 10% and 90% 
    m10         =       Max * 0.1
    m90         =       Max * 0.9
    
    # get index
    m10Index    =       next(x[0] for x in enumerate(pos) if x[1] > m10)
    m90Index    =       next(x[0] for x in enumerate(pos[m10Index:]) if x[1] > m90) + m10Index
    
    # calc time span
    t_10_90     =       t[m90Index] - t[m10Index]
    
    
    # fit decay to curve and get decay time constant---
    X           =       t[  maxIndex+100:]
    Y           =       pos[maxIndex+100:]
    popt, pcov  =       curve_fit(func, X, Y, p0=[1.0,Y[0],-20.0])
    
    if ax:
        y = np.add( func(X, *popt), trace[0] )
        ax.plot(X, y, 'g--', alpha=0.9 )
        ax.plot(t[m10Index], trace[m10Index], '+g', ms=10) 
        ax.plot(t[m90Index], trace[m90Index], '+g', ms=10)
    
    return t_10_90, popt[2]
       

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    import matplotlib.pyplot as plt
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    channels    = ['bk', 'sk', 'car', 'can', 'cal13', 'cal12', 'kir', 'kdr', 'kas', 'kaf', 'naf']
    
    base_index  = 1800
    color       = ['r', 'g']
    label       = ['glut33%', 'tmglut']
    ls          = ['-', '--']
    
    for version in range(30):
        
        # load parameters and random set
        parameters      =   use.load_obj('Libraries/'+cell_type+'_71bestFit.pkl')[version]['variables'] # parameter set 0 (cell index)
        
        
        for synapse_type in range(2):
            
            # initiate cell
            cell = build.MSN(  params=par,                  \
                               morphology=morphology,       \
                               variables=parameters         )
            
            
            
            # create segment lists
            dist_groups = range(4)
            segments    = create_segment_list(cell, dist_groups)
                      
            
            # set cascade 
            #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
            casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
            pointer =   casc._ref_Target1p    
            
            
            
            # set pointers (for all channel instances in mod_list); 
            # needed since same mechanisms are used for dynamic modulation of channels.
            # Modulation of channels is not used in this script
            if cell_type=='D1':
                mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
            else:
                mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
            
            
            maxCond = set_pointers(cell, pointer, mod_list)
            
            
            ## ---------------------------------------------------------
            # create the synapse and set in soma; to be moved.    
            
            ns              =   h.VecStim()
            vec             =   h.Vector([45])
            ns.play(vec)
            
            r = 5.0
            gmax = 1880e-6
            
            # specify synapse
            if synapse_type == 0:
                glutamate           = h.glutamate(0.5, sec=cell.soma)
                glutamate.ratio     = 1.0  / r   # 5.0  Du et al 2017; spine values (1880/340); for dendritic noise 2.764705882352941 (705/255)
                
                nc                  = h.NetCon(ns, glutamate)
                nc.weight[0]        = gmax
                
            else:
                glutamate           = h.tmGlut(0.5, sec=cell.soma)
                glutamate.nmda_ratio = r
                
                nc                  = h.NetCon(ns, glutamate)
                nc.weight[0]        = gmax * 10 / (3*r)
                
            
            glutamate.q         = 2
            glutamate.tau1_ampa = 1.9
            glutamate.tau2_ampa = 4.8
            glutamate.tau1_nmda = 12.13     # chapman 2003, Kai is dividing the value in ventral striatum by (2*e) for some unclear reason (15.31/(2*e) to get 5.63/2)
            glutamate.tau2_nmda = 231       # chapman 2003 (same in du et al 2017; Kai)
            nc.delay            = 0
            
            
            
            # solver------------------------------------------------------------------------------            
            cvode   = h.CVode()
            
            tstop   = 100
            
            
            # reset record vectors
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            
            # run simulation
            h.finitialize(-80)
            while h.t < tstop:
            
                h.fadvance()
            
            
            plt.plot(tm, vm, color=color[synapse_type], label=label[synapse_type], lw=3, ls=ls[synapse_type])
            
            '''
            tau = find_time_constants( vm.to_python()[base_index:], tm.to_python()[base_index:], ax=a[0])
            
            TAU1.append(tau[0])
            if tau[1] < 500:
                TAU2.append(tau[1])'''
            
        plt.xlim([20,100])
        plt.ylim([-83.5,-83])
        plt.legend(loc='best')
        #plt.savefig('../../../Desktop/comparing_tmglut_glutamate.png')
        plt.show()
        break
        


    
    
