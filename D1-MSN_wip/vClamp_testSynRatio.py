
# script used for checking ampa/nmda ratio using Maya's protocol.
# in voltage clamp; 
# -approximate AMPA current as max holding current amplitude at -70 mV
# -approximate NDMA current as holding current amplitude 50 ms after activation at +40 mV
# ratio = AMPA / NMDA

from __future__ import print_function, division
from neuron import h
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import pickle

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

import matplotlib.pyplot as plt
    

# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # TODO plot ampa and nmda conductances individually
    
    cvode           =   h.CVode()
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    # load random sets
    path_to_lib         = './Libraries/'
    all_random_variables    = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')
    
    f,a = plt.subplots(2,1)
    c = ['b', 'r']
    
    N = 10
    
    RES = {'ampa':np.zeros(N), 'nmpa':np.zeros(N), 'ratio':np.zeros(N)}
    
    for model_version in range(N):
        
        print( model_version )
        
        random_variables = all_random_variables[model_version]['variables']
        
        # initiate cell ------------------------------------------------------------------
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=random_variables   )
        
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        # modulation factors
        if cell_type == 'D1':
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
        else:
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        use.set_pointers(cell, pointer, mod_list)
        
        # block sodium
        '''
        cell.soma(0.5).naf.gbar = 0.0
        for sec in cell.axonlist:
            for seg in sec:
                seg.naf.gbar = 0.0
        '''
        
        # create synapse
        x = 0.5
        dend = cell.soma
        
        ncon = {}
        syn  = {}
        stim = {}
        
        vec = h.Vector(np.array([0.1,1.1])*1e3)
        
        for sec in cell.dendlist:
            dend = sec
            syn[sec.name()]   = h.glutamate(x, sec=dend)
            syn[sec.name()].ratio = 1.5
            # vecstim object
            stim[sec.name()] = h.VecStim()
            stim[sec.name()].play(vec)
            # NetCon object
            ncon[sec.name()]             = h.NetCon(stim[sec.name()], syn[sec.name()])
            ncon[sec.name()].delay       = 0
            ncon[sec.name()].weight[0]   = 30e-3 # (uS) = 1.5 nS
        
        res = {}
        for clampVal in [-70, 40]:
        
            # set V-clamp
            clamp       = h.SEClamp(dend(0.5))
            clamp.rs    = 0.01
            clamp.dur1  = 1000
            clamp.amp1  = clampVal
            
            # record vectors
            s = h.Vector()
            s.record(clamp._ref_i)
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm = h.Vector()
            vm.record(dend(0.5)._ref_v)

            h.finitialize(clampVal)
            while h.t < 500:
                h.fadvance()
            
            if model_version == 0:
                iii = next(i for i,t in enumerate(tm) if t >91)
            
            s2 = [s[i]-s[iii]  for i,t in enumerate(tm) if t > 90 and t < 200]
            v2 = [vm[i] for i,t in enumerate(tm) if t > 90 and t < 200]
            res[clampVal] = {'s':s2, 'vm':v2}

        t2 = [t for t in tm if t > 90 and t < 200]
        for i,clampVal in enumerate([-70, 40]):
            a[0].plot(t2,res[clampVal]['vm'], c=c[i])
            a[1].plot(t2,res[clampVal]['s'], c=c[i])

        sc1 = res[-70]['s']
        ampa = np.abs(min(sc1))
        sc2 = res[40]['s']
        nmda = next(sc2[i] for i,t in enumerate(t2) if t >150)

        RES['ampa'][model_version] = ampa
        RES['nmpa'][model_version] = nmda
        RES['ratio'][model_version] = ampa / nmda
        
    a[0].set_ylim([-80,50])
    f.savefig('Figures/estimating_glut_ratio.png')
    
    
    print( np.mean(RES['ratio']) )
    
    plt.show()
    
             
        

    
    
    
    

        

