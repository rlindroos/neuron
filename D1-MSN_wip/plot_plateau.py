from joblib import Parallel, delayed
import multiprocessing
import numpy                as np
import matplotlib.pyplot    as plt
import glob
import common_functions     as use
import pandas               as pd


def load_file(f):
    
    x,y = np.loadtxt(f, unpack=True)
    
    dy = np.gradient(y)
    
    
    dist = int( f.split('_')[2] ) 
    
    if dist < 110:
        c = 'b'
    elif dist < 120:
        c = 'r'
    else:
        c = 'k'
    
    
    return [x, y, dy, f, c]
    


def simple_plot(ax):
    
    '''
    plot local, somatic and derivative of plateaus of type "cell_type". 
    Include "N" solutions.
    '''
    
    COLOR      = {}
    color      = ['b', 'r', 'k']
    dist       = [  '              dist <   110', \
                    '110  <= dist <   120', \
                    '120  <= dist']
    
    num_cores  =   multiprocessing.cpu_count()
    
    
    for j,t in enumerate(['dend', 'soma']):
        
        fString   =   'Results/Plateau/plateau_' + t + '_*'
        files     =   glob.glob(fString)

         
        M = Parallel(n_jobs=num_cores)(delayed(load_file)( f ) for f in files)

        for x,y,dy,f,c in M:
            
            if j == 0:
                label    = ''
            elif c not in COLOR:
                COLOR[c] = c
                label    = dist[color.index(c)]
                print label, j
            else:
                label    = ''

            ax[j].plot(x,y, lw=3, color=c, label=label)
            

def plot_plateau():
    
    #fig,ax     =   plt.subplots(2,1, figsize=(8, 12))    
    fig = plt.figure(figsize=(8, 12))
    ax  = fig.add_subplot(111)    # The big subplot
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    simple_plot([ax1,ax2])
    for i,a in enumerate([ax1,ax2]):
        a.set_ylim([-90, 0])
        a.set_yticks([-85,-45,-5])
        a.set_yticklabels([-85,-45,-5], fontsize=30)
        
        if i == 0:
            a.set_title('Dendrite', fontsize=40) 
            a.set_xlim([40, 460])
            a.set_xticks([])     
        else:
            a.set_title('Soma', fontsize=40)
            a.legend(fontsize=24, loc='best')
            a.set_xlim([40, 460])
            a.set_xticks([100, 250, 400])
            a.set_xticklabels([0, 150, 300], fontsize=30)
            
    # Turn off axis lines and ticks of the big subplot
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')

    ax.set_ylabel('Membrane potential (mV)', fontsize=30, labelpad=40)
    ax.set_xlabel('Time (ms)', fontsize=30,labelpad=40) 
    plt.tight_layout(pad=0.4)  
    fig.savefig('../../../Desktop/plateau.png')    
    plt.show()



def FWHM(tm, Y, baseIndex):
    '''
    calculates full width half max of a voltage trace.
    The trace can only have one epsp, i.e. it is assumed that the curve is monotonic
    before and after the maximum.
    '''
    
    # get half-max vm
    half_max = (max(Y)+Y[baseIndex]) / 2.

    indx = []
    for i,y in enumerate(Y):
        
        # get all values above half max (assumes monotonic epsp in trace)
        if y - half_max >= 0:
            indx.append(i)
            
    return tm[indx[-1]] - tm[indx[0]]
    
def HMDur(tm, Y, baseIndex):
    '''
    calculates half max duration from end of stimulation of a voltage trace.
    -The trace can only have one epsp, i.e. it is assumed that the curve is monotonic
    before and after the maximum.
    -The end of the voltage stimulation is assumed to be after 140 ms (100 delay + 20x2 ms ISI)
    '''
    
    # get half-max vm
    half_max = (max(Y)+Y[baseIndex]) / 2.

    indx = []
    for i,y in enumerate(Y):
        
        # get all values above half max (assumes monotonic epsp in trace)
        if y - half_max >= 0:
            indx.append(i)
            
    return tm[indx[-1]] - 140



def Plateau_area(tm, Y, baseIndex):
    
    # set baseline to 0
    Y = np.subtract( Y, Y[baseIndex] ) 
    
    # return area under curve
    return np.trapz( Y[baseIndex:], dx=tm[1]-tm[0] )


    
def extract_fetures_into_array(fileBase, cell_type, da='0', SM='0'):
    
    files   =   glob.glob(fileBase+da+'*_SM'+SM+'_'+cell_type+'*.pkl')
    start   =   False
    
    for f in files:   
        
        # load data
        container = use.load_obj(f)
        
        tm      =   container['tm']
        data    =   container['res']
        
        if not start:
            start = next(x[0] for x in enumerate(tm) if x[1] > 95 )
            end   = next(x[0] for x in enumerate(tm) if x[1] > 350)
        
        array   =   np.zeros((len(data),8,1))
        
        for i in range(len(data)):
            
            section =   data[i][0]
            vm      =   data[i][1].to_python()
            vml     =   data[i][2].to_python() 
            dist    =   data[i][3]
            
            # extract features
            samp    =   max( vm[start:end] ) - vm[start]
            sdur    =   use.HMDur(       tm, vm, start)
            sarea   =   use.Plateau_area(tm, vm, start)
            
            damp    =   max( vml[start:end] ) - vml[start]
            ddur    =   use.HMDur(       tm, vml, start)
            darea   =   use.Plateau_area(tm, vml, start)
            
            spikes  =   use.getSpikedata_x_y( tm, vm )
            
            
            
            if len(spikes) > 0:
                spike = 1
                print spikes, f
            else:
                spike = 0  
                
            v       =   [samp, sdur, sarea, damp, ddur, darea, spike, dist]
            
            
            # update array 
            array[i,:,0] = v   
            
        #print('Saving...', j, ':', len(files))
        use.save_obj(array, 'Results/Plateau/DAstatic/plateauFeatures_DA'+da+'_SM'+SM+'_control_'+cell_type+'_section'+str(section) )
        
 
 
def merge_arrays():
    
    
    # specify sets
    settings    =   {'control': { 'D1': {'DA':'0', 'SM':'0'},       \
                                  'D2': {'DA':'0', 'SM':'0'} },     \
                     'mod_all': { 'D1': {'DA':'1', 'SM':'1'},       \
                                  'D2': {'DA':'1', 'SM':'1'} },     \
                     'chanMod': { 'D1': {'DA':'1', 'SM':'0'},       \
                                  'D2': {'DA':'1', 'SM':'0'} }      }
                                  
    
    # cell type
    for cell_type in settings['control']:
        
        if cell_type == 'D1':
            N = 58
        else:
            N = 46
        
        # set up of master array
        MA  = np.zeros((30,6,61,N))
        DD  = np.zeros((N))
        
        # section    
        for sec in range(N):
            
            # paradigm
            for k,key in enumerate(['control', 'mod_all', 'chanMod']):
                
                S = settings[key][cell_type]
                
                # import array
                files = glob.glob('Results/Plateau/DAstatic/plateauFeatures_DA'+S['DA']+'_SM'+S['SM']+'_*_'+cell_type+'_section'+str(sec)+'.pkl')
                
                for f in files:
                    
                    array   =   use.load_obj(f)
                    
                    if k == 2 and N == 58:
                        if sec == 0:
                            print array[0,:,28], f
                            files = glob.glob('plateauFeatures_DA'+S['DA']+'_SM'+S['SM']+'_*_'+cell_type+'_section'+str(sec)+'.pkl')
                            a2    = use.load_obj(files[0])
                            
                            array[:,:,28:] = a2[:,:,28:]
                            print array[0,:,28], array[0,:,29]
                            
                        
                    
                    # check if any row contains spiking. 
                    #       if so replace row with None
                    spikes  =   array[:, 6, :]
                    I,J     =   spikes.shape
                    for i in range(I):
                        for j in range(J):
                            if spikes[i,j] > 0:
                                array[i,:,j] = None
                    
                    
                    if   k == 0:
                        MA[:,:,0,sec]   = array[:,0:-2,0  ]
                        DD[sec]         = array[0,7,0]
                    elif k == 1:
                        MA[:,:,1:31,sec] = array[:,0:-1,0:30]
                    elif k == 2:
                        MA[:,:,31:,sec]  = array[:,0:-1,0:30]
        
        
        # save arrays
        use.save_obj(MA, 'Results/Plateau/DAstatic/plateauFeatures_MASTERextended-1-30-30_'+cell_type)
        #use.save_obj(DD, 'Results/Plateau/DAstatic/plateauFeatures_DISTANCES_'+cell_type)
                                




def build_and_plot_morphology(  root,       \
                                cell_type,  \
                                ax=False,   \
                                target=[],  \
                                plotSoma=True,  \
                                skipAxon=True   ):
                    
    from   neuron       import  h
    import MSN_builder  as      build
     
    
    h.load_file('stdlib.hoc')
    h.load_file('import3d.hoc')
    
    if cell_type == 'D1':
        morphology='WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        par="./params_dMSN.json"
    else:
        morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        par="./params_iMSN.json"
    
    cell = build.MSN(  params=par,                  \
                       morphology=morphology        )  
    
    
    # get subtree (wholetree could possibly also be used).
    if root == 'soma':
        stem = cell.soma
    else:
        stem_name = 'dend['+stem_nr+']'
        for sec in h.allsec():
            if sec.name() == stem_name:
                stem = sec
                break
        
    # create list of sections to plot
    tree = h.SectionList()
    tree.subtree(sec=stem)
    
    
    if not ax:
        fig,ax = plt.subplots(1,1)
    
    
    # loop over sections
    for sec in tree:
        
        if skipAxon:
            if 'axon' in sec.name():
                continue
        
        x = []
        y = []
        z = []
        
        arc = []
        
        # loop over segments in section (including 0 and 1)
        for i in range( int(h.n3d(sec=sec)) ):
            
            # collect 3d points 
            x.append( h.x3d(i, sec=sec) )
            y.append( h.y3d(i, sec=sec) )
            z.append( h.z3d(i, sec=sec) )
            
            arc.append( h.arc3d(i, sec=sec) )
            
        if sec.name() in target:
            c = 'm'
            # find approximate possition of injection site and plot it as a dot
            i = next( i[0] for i in enumerate(arc) if i[1] > sec.L*target[sec.name()] )
            ax.plot(x[i], y[i], 'o', color='m', ms=15)
        else:
            c = 'grey'
                
        # plot section
        ax.plot(x, y, color=c, lw=2)
    
    if plotSoma:
        import matplotlib.lines  as  fs
        ax.plot([0,0],[0,0], c='k',
                       marker='o',
                       markersize=20,
                       markerfacecoloralt='r',
                       fillstyle=fs.Line2D.fillStyles[2])
        
    if ax:
        return ax
    else:
        fig.show()
        

                    
                    
            
def plot_the_lot():
    
    
    f1,ax1      = plt.subplots(2,2, figsize=(10,6), sharex=True )
    f2,ax2      = plt.subplots(2,2, figsize=(10,6), sharex=True )
    
    fmorph1, amorph1 = plt.subplots(1,1)
    fmorph2, amorph2 = plt.subplots(1,1)
    amorph           = [amorph1, amorph2]
    
    fex1, aex1  = plt.subplots(2,1, figsize=(4,6), sharex=True ) 
    fex2, aex2  = plt.subplots(2,1, figsize=(4,6), sharex=True )
    aex         = [aex1, aex2]
    
    
    
    features    = ['Amplitude', 'Duration', 'Area']
    
    target      = { 'D1' : {'dend[20]':0.5, 'dend[5]':0.5}, \
                    'D2' : {'dend[28]':0.5, 'dend[5]':0.5}  }
    
    # Split on subtype
    for i,cell_type in enumerate(['D1', 'D2']):
        
        
        # load master arrays [ models x features x iters x sections ]
        #   features from spiking traces (~20) have been replaced by None
        A       =   use.load_obj('Results/Plateau/DAstatic/plateauFeatures_MASTERextended-1-30-30_'+cell_type+'.pkl')
        
        # load distance vectors (same order as sections in the master array)
        D       =   use.load_obj('Results/Plateau/DAstatic/plateauFeatures_DISTANCES_'+cell_type+'.pkl') 
        
        # get parameters from 30 best (same order as models in master array)
        P       =   use.load_obj(cell_type+'_30bestFit.pkl')
        
        # number of sections:
        N       =   len(A[0,0,0,:])
        
        # number of models (30!?)
        M       =   len(A[:,0,0,0])
        
        # number of features (30!?)
        F       =   len(A[0,:,0,0])
        
        
        # Reduce dimensions by averaging over trials
        control     =   A[:,:,0,:]
        mean_all    =   np.mean(A[:,:,1:31,:], axis=2)
        mean_intr   =   np.mean(A[:,:,31: ,:], axis=2)
        
        # subtract control from mean
        allmod      =   np.subtract(mean_all,  control)
        ionmod      =   np.subtract(mean_intr, control)
        
        # count number of soulations
        
        # percent
        allmod      =   np.divide(allmod, control)
        ionmod      =   np.divide(ionmod, control)
        allmod      =   np.multiply(allmod, 100)
        ionmod      =   np.multiply(ionmod, 100)
        
        # sort vectors based on somatic distance
        sort_index  =   np.argsort(D)
        
        
        # plot morphology
        build_and_plot_morphology(  'soma',         \
                                    cell_type,      \
                                    ax=amorph[i],   \
                                    target=target[cell_type]       )
                                    
        # plot example traces
        color       =   ['k', 'r']
        for s,sec in enumerate(target[cell_type]):
            
            sec_num     =   sec.split('[')[1][0:-1]
            files       =   glob.glob('Results/Plateau/DAstatic/plateaus_DA0-run*'+cell_type+'*section'+sec_num+'.pkl')
            container   =   use.load_obj(files[0])
            tm          =   container['tm']
            data        =   container['res']
            
            vmtraces    =   np.zeros(( len(data),len(tm) ))
            vmltraces   =   np.zeros(( len(data),len(tm) ))
        
            for j in range(len(data)):
                
                vm        =   data[j][1]
                vml       =   data[j][2]
                
                aex[i][s].plot(tm, vml, color='m',     alpha=0.1)
                aex[i][s].plot(tm, vm, color=color[s], alpha=0.1) 
                
                vmltraces[j,:] = vml
                vmtraces[ j,:] = vm
            
            aex[i][s].plot(tm, np.mean(vmltraces,axis=0), color='m',      alpha=1, lw=3)
            aex[i][s].plot(tm, np.mean(vmtraces ,axis=0), color=color[s], alpha=1, lw=3)
                
                 
        # plot extracted features
        for j in range(2):  # F
            for m in range(M):
                
                if i == 0:
                    
                    ax1[0,j].plot(D[sort_index], control[m,j,:][sort_index], '-o', color='k', alpha=0.1)
                    ax1[1,j].plot(D[sort_index], ionmod[ m,j,:][sort_index], '-o', color='b', alpha=0.1)
                    ax1[1,j].plot(D[sort_index], allmod[ m,j,:][sort_index], '-o', color='r', alpha=0.1)
                    
                else:
                    
                    ax2[0,j].plot(D[sort_index], control[m,j,:][sort_index], '-o', color='k', alpha=0.1)
                    ax2[1,j].plot(D[sort_index], ionmod[ m,j,:][sort_index], '-o', color='b', alpha=0.1)
                    ax2[1,j].plot(D[sort_index], allmod[ m,j,:][sort_index], '-o', color='r', alpha=0.1)
                    
                    
                
                '''
                if j < 3:
                    a_cont[i,j].plot(D[sort_index], control[m,j,:][sort_index], '-o', color='k', alpha=0.1)
                    
                    ax1[i,j].plot(D[sort_index], allmod[m,j,:][sort_index], '-o', color='r', alpha=0.1)
                    ax1[i,j].plot(D[sort_index], ionmod[m,j,:][sort_index], '-o', color='b', alpha=0.1)
                    if i == 0:
                        ax1[i,j].set_title(features[j], fontsize=30)
                        a_cont[i,j].set_title(features[j], fontsize=30)
                        
                else:
                    k = j-3
                    ax2[i,k].plot(D[sort_index], allmod[m,j,:][sort_index], '-o', color='r', alpha=0.1)
                    ax2[i,k].plot(D[sort_index], ionmod[m,j,:][sort_index], '-o', color='b', alpha=0.1)
                    if i == 0:
                        ax2[i,k].set_title(features[k], fontsize=30)'''
                        
    
    
    
    
    # fix layout
    for ax in [ ax1[0,:], ax1[1,:], ax2[0,:], ax2[1,:] ]: #, a_cont[0,:], a_cont[1,:] ]:
        for a in ax:
            
            xticks = np.arange(0,301,100)
            a.set_xticks(xticks)
            a.set_xticklabels(xticks, fontsize=20)
            
            yticks = a.get_yticks()
            yticks = [ int(yticks[0]), 0, int(yticks[-1]) ]
            a.set_yticks(yticks)
            a.set_yticklabels(yticks, fontsize=20)
    
    for i in range(len(aex)):
        for j in range(len(aex[i])):
            aex[i][j].set_ylim([-90, -20])
            aex[i][j].set_xlim([ 90, 250])
            if j == 0:
                aex[i][j].plot([200,200], [-50,-30], 'k', lw=5)
                aex[i][j].plot([200,220], [-50,-50], 'k', lw=5)
            aex[i][j].axis('off')
    
    
    #f1.suptitle('Soma',     fontsize=50)
    #f2.suptitle('Dendrite', fontsize=50)
    #f_cont.suptitle('Control', fontsize=50)
    
    amorph1.axis('off')
    amorph2.axis('off')
    
    f1.savefig(     '../../../Desktop/plateau_D1_Ding.png',         transparent=True)  
    f2.savefig(     '../../../Desktop/plateau_D2_Ding.png',     transparent=True)
    
    fmorph1.savefig('../../../Desktop/morph_D1.png',            transparent=True)  
    fmorph2.savefig('../../../Desktop/morph_D2.png',            transparent=True)
    
    fex1.savefig(   '../../../Desktop/example_traces_D1.png',   transparent=True)  
    fex2.savefig(   '../../../Desktop/example_traces_D2.png',   transparent=True)
    
    plt.close('all')
    
                                     

    

def analyse_multiple_plateaus(fileString, c='b', ax=False):
    
    files   =   glob.glob(fileString)
    
    start   = False
    
    if not ax.all():
        fig,ax = plt.subplots(1,3, figsize=(4,12) )
        
    RES = {}
    
    if 'D1' in fileString:
        model = [0, 1, 3, 4, 5, 9, 12, 14, 15, 17, 19, 20, 22, 26, 28]
    else:
        model = [1,2,4,6,7,8,9,10,11,12,13,17,19,20,21,24,25,26,27,28,29]
        
        
    
    for f in files:
        
        
        # container structure: data = { 'res':P,  'chanfact':RES[r]['factors'],  'synfact':RES[r]['synfact'], 'tm':tm }
        #                                     P = [ [ section, vm, vml, dist ]<1-N> ]
        
        container =   use.load_obj(f)
        
        # factors used in channel modulation
        #chanfact  =   container['chanfact']
        #synfact   =   container['synfact']
        tm        =   container['tm']
        data      =   container['res']
        
        if not start:
            start = next(x[0] for x in enumerate(tm) if x[1] > 95 )
            end   = next(x[0] for x in enumerate(tm) if x[1] > 350)
        
        for i in range(len(data)):
            
            if i == 22 and section == 3:
                continue
            
            if i not in RES:
                RES[i] = { 'amp':[], 'dur':[], 'dist':[] }
            
            if i in model:
                c = 'k'
            else:
                c = 'b'
        
            section   =   data[i][0]
            vm        =   data[i][1].to_python()
            vml       =   data[i][2]
            dist      =   data[i][3]
            n         =   int(dist)/30
            
            RES[i]['dist'].append(dist)
            
            # check if spiking
            '''
            flag=False
            for v in vm:
                if v > 0:
                    flag = True
                    print f, i, section, dist
                    break
            if flag:
                continue
            '''
             
            
            # amp
            amp     =   max( vm[start:end] ) - vm[start]
            ax[0].plot( dist, amp,  'o', color=c, ms=10, alpha=0.2 )
            RES[i]['amp'].append(amp)
            
            # duration (full width half max; fwhm)
            fwhm    =   HMDur(tm, vm, start)
            ax[1].plot( dist, fwhm, 'o', color=c, ms=10, alpha=0.2 )
            RES[i]['dur'].append(fwhm)
            
            # area???
            area    =   Plateau_area(tm, vm, start)
            ax[2].plot( dist, area, 'o', color=c, ms=10, alpha=0.2 )
    
    
    f2, a2 = plt.subplots( 1,2, figsize=(24,12) )
    
    
    for key in range(30):
        
        if key == 22:
            continue
        sort_index  =   np.argsort(RES[key]['dist'])
        x           =   [-1]*len(sort_index)
        a           =   [-1]*len(sort_index)
        d           =   [-1]*len(sort_index)
        
        for i,ind in enumerate(sort_index):
            
            x[i] = RES[key]['dist'][ind] 
            a[i] = RES[key]['amp' ][ind] 
            d[i] = RES[key]['dur' ][ind] 
                    
        
        
        
        if key in model:
            c = 'k'
        else:
            c = 'b'
        
        a2[0].plot(x, a, '-o', color=c, ms=10, lw=2, alpha=0.3)
        a2[1].plot(x, d, '-o', color=c, ms=10, lw=2, alpha=0.3)
    
    
    
    #a2[0].set_ylim(0, 40)
    
    
'''    
plt.close('all')
fig,ax = plt.subplots(2,3, figsize=(24,12) )

analyse_multiple_plateaus('Results/Plateau/DAstatic/plateaus_DA0-run*D1*.pkl', ax=ax[0,:])
analyse_multiple_plateaus('Results/Plateau/DAstatic/plateaus_DA0-run*D2*.pkl', ax=ax[1,:])

plt.show()
'''

#extract_fetures_into_array('Results/Plateau/DAstatic/plateaus_DA', 'D1')
#extract_fetures_into_array('Results/Plateau/DAstatic/plateaus_DA', 'D2')

#merge_arrays()

plot_the_lot()

            
            
    
    
    
    
    
    
    
    
    
    
