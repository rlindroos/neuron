#!/bin/bash -l


#sbash pgenesis_Ri.job <nodes> <pfile> <TES/DOP> <rcr> <freq>

ARG0=16
while [  $ARG0 -lt 35 ]; do

    ARG1=0
    while [  $ARG1 -lt 7 ]; do

        ARG2=0
        while [  $ARG2 -lt 110 ]; do
            
            echo $ARG1 $ARG2 
            
            # start simulations
            sbatch InVivo/MixedPattern/inVivo_mixed.job $ARG1 1000 $ARG2 $ARG0
            sleep 1s
            
            # increase ARG2
            let ARG2=ARG2+50 
            
        done

        let ARG1=ARG1+3

    done
    
    let ARG0=ARG0+16

done

