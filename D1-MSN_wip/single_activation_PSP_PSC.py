
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import  numpy                as np
import  MSN_builder          as build
import  common_functions     as use
from    scipy            import stats
from    scipy.optimize   import curve_fit
import  glob
import  pickle

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        


def create_segment_list(cell, dist_groups):
    
    # create segment lists
    segments    = {} 
    
    for d in dist_groups:
        segments[d] = []
          
    # sort segments into list            
    for sec in cell.dendlist:
             
        for seg in sec:
            
            dist = h.distance(seg.x, sec=sec)
            
            if dist < 60:
                segments[0].append(seg)
            elif dist < 120:
                segments[1].append(seg)
            elif dist < 180:
                segments[2].append(seg)
            else:
                segments[3].append(seg)
    
    return segments
    



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    


    

def set_modulation( cell,           \
                    maxCond,        \
                    factors,        \
                    mod_list        ):
    
    # use factors stored externally
    mod_fact = factors[0:len(mod_list)]
    
    for mech in maxCond:
        
        # get factor from list
        factor = mod_fact[mod_list.index(mech.name() )]
        
        if mech.name()[0] == 'c':
            
            # Ca channels
            mech.pbar   = maxCond[mech] * factor
            
        else:
        
            # non Ca channels
            mech.gbar   = maxCond[mech] * factor
    
    


       

def set_bg_noise(cell,               \
                 syn_fact=False,     \
                 gabaMod=False       ):
    
    ns      = {}
    nc      = {}
    Syn     = {}
    for sec in cell.allseclist:
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 0.5e-3
        else:
            gbase = 0.2e-3
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,           \
                                NS_interval=1000.0/20.0,    \
                                NC_conductance=gbase )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/5.0,     \
                                NC_conductance=gbase*3      )
        
        Syn[sec.name()+'_glut'].ratio = 1.0
        
        if syn_fact:
            Syn[sec.name()+'_glut'].ampa_scale_factor = syn_fact[0]
            Syn[sec.name()+'_glut'].nmda_scale_factor = syn_fact[1]
            
        
        if gabaMod:
            # scale gaba
            nc[sec.name()+'_gaba'].weight[0] = gbase * 3 * gabaMod
        
    
    return Syn, nc, ns
            
            


def set_stimuli(cell,               \
                activationPattern,  \
                syn_fact=False,     \
                gabaMod=False       ):
    
    rand    = {}
    stim    = {}
    ncon    = {}
    vmL     = {}
    cur     = {}
    detect  = {}
    delay   = np.arange(1000, 1010, 1)
    
    # set random stimuli
    for sec in cell.dendlist:
        
        for seg in sec:  
        
            if seg in activationPattern:
                
                key = sec.name() + '_' + str(seg.x)
                
                # get index of activation
                index = activationPattern.index(seg)
                
                # create synapse
                rand[key]        =   h.glutamate(seg.x, sec=sec) # changed from x -> seg.x
                rand[key].ratio  =   1.0/3.0
                
                if syn_fact:
                    rand[key].ampa_scale_factor = syn_fact[0]
                    rand[key].nmda_scale_factor = syn_fact[1]


                # create NetStim object
                stim[key]            = h.NetStim()
                stim[key].number     = 2
                stim[key].start      = delay[index]
                stim[key].interval   = 10 # interval between two spikes in ms (1000 / 50 Hz = 20 ms)

                # create NetCon object
                ncon[key]            = h.NetCon(stim[key], rand[key])
                ncon[key].delay      = 1
                ncon[key].weight[0]  = 1.5e-3 # (uS) = 1.5 nS
                
                #vmL[key] = h.Vector()
                #vmL[key].record(sec(seg.x)._ref_v)
                cur[key] = h.Vector()
                cur[key].record(rand[key]._ref_I)
                
                # set pointer to right target...
                pointer             = rand[key]._ref_i_ampa
    
    return cur, rand, ncon, stim         

    
def func(x, a, b, c):
    
    return a * np.exp( (x-b) / c )   
        
    
def find_time_constants(trace, t, ax=False):
    
    # rise time. 10-90% ------------------------
    
    
    # make sure positive deflection by ... 
    shifted     =       np.subtract(trace, trace[0]) 
    pos         =       list(np.abs(shifted))
    
    # get peak value and index of peak
    Max         =       max(pos)
    maxIndex    =       pos.index(Max)
    
    # calc 10% and 90% 
    m10         =       Max * 0.1
    m90         =       Max * 0.9
    
    # get index
    m10Index    =       next(x[0] for x in enumerate(pos) if x[1] > m10)
    m90Index    =       next(x[0] for x in enumerate(pos[m10Index:]) if x[1] > m90) + m10Index
    
    # calc time span
    t_10_90     =       t[m90Index] - t[m10Index]
    
    
    # fit decay to curve and get decay time constant---
    X           =       t[  maxIndex+100:]
    Y           =       pos[maxIndex+100:]
    popt, pcov  =       curve_fit(func, X, Y, p0=[1.0,Y[0],-20.0])
    
    if ax:
        y = np.add( func(X, *popt), trace[0] )
        ax.plot(X, y, 'g--', alpha=0.9 )
        ax.plot(t[m10Index], trace[m10Index], '+g', ms=10) 
        ax.plot(t[m90Index], trace[m90Index], '+g', ms=10)
    
    return t_10_90, popt[2]
       

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    import matplotlib.pyplot as plt
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    channels    = ['bk', 'sk', 'car', 'can', 'cal13', 'cal12', 'kir', 'kdr', 'kas', 'kaf', 'naf']
    
    GRAND_MAX   = []
    ALL_MAX     = []
    
    GRAND_TAU1  = []
    GRAND_TAU2  = []
    
    base_index = 1800
    
    for version in range(30):
        
        # load parameters and random set
        parameters      =   use.load_obj(cell_type+'_30bestFit.pkl')[version]['variables'] # parameter set 0 (cell index)
        
        
        # initiate cell
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=parameters         )
        
        
        
        # create segment lists
        dist_groups = range(4)
        segments    = create_segment_list(cell, dist_groups)
                  
        
        # set cascade 
        #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        # Modulation of channels is not used in this script
        if cell_type=='D1':
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
        else:
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
        
        maxCond = set_pointers(cell, pointer, mod_list)
        
        
        ## ---------------------------------------------------------
        # create the synapse and set in soma; to be moved.    
        
        glutamate       =   h.glutamate(0.5, sec=cell.soma)
        ns              =   h.VecStim()
        vec             =   h.Vector([45])
        nc              =   h.NetCon(ns, glutamate)
        ns.play(vec)
        
        # specify synapse
        glutamate.ratio     = 1.0
        glutamate.q         = 1
        glutamate.tau1_ampa = 1.9
        glutamate.tau2_ampa = 4.8
        glutamate.tau1_nmda = 6
        glutamate.tau2_nmda = 116
        
        # def NetCon
        nc.weight[0]    = 0.4e-3
        nc.delay        = 0

        tm = h.Vector()
        tm.record(h._ref_t)

        vm = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        # V-clamp; -80 mV 
        '''
        clamp       = h.SEClamp(cell.soma(0.5))
        clamp.rs    = 0.001
        clamp.dur1  = 100
        clamp.amp1  = -80
        '''
        
        # solver------------------------------------------------------------------------------            
        cvode   = h.CVode()
        
        tstop   = 100

        h.finitialize(-80)
        while h.t < tstop:
            h.fadvance()
        
        f,a = plt.subplots(2,1, figsize=(6,10))
        
        count = 0
        for sec in h.allsec():
            count += 1
        
        res = np.zeros( (count, len(tm)) )
        
        TAU1 = []
        TAU2 = []
        
        for s,sec in enumerate(h.allsec()):
            
            print('starting section', s, 'model', version)
            
            # mv stim object
            glutamate.loc(0.5, sec=sec)
            
            # reset record vectors
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            #c   = h.Vector()
            #c.record(cell.soma(0.5)._ref_i)
            
            # record axial currents
            axial = use.record_axial_current(cell.soma)
            
            # record channel currents
            C = {}
            for mech in cell.soma(0.5):
                if mech.name() in channels:
                    C[mech.name()] = h.Vector()
                    C[mech.name()].record(mech._ref_I)
            
            # run simulation
            h.finitialize(-80)
            while h.t < tstop:
            
                h.fadvance()
                 
            
            # store in array 
            currents = np.zeros( (len(channels), len(tm)) )
            
            for mech in cell.soma(0.5):
                if mech.name() in channels:
                    index = channels.index( mech.name() )
                    currents[index,:] = C[mech.name()]
            
            # ... and summate currents
            c = np.sum(currents, axis=0)
            
            res[s,:] = vm
            
            a[0].plot(tm, vm,'k', alpha=0.3)
            a[1].plot(tm, c, 'k', alpha=0.3)
            
            use.plot_axial_current( axial, tm, vm, a[1], mark_section=[sec.name()] )
            
            tau = find_time_constants( vm.to_python()[base_index:], tm.to_python()[base_index:], ax=a[0])
            
            TAU1.append(tau[0])
            if tau[1] < 500:
                TAU2.append(tau[1])
            
        a[0].set_xlim([40,70])
        a[1].set_xlim([40,70])
        a[1].legend(['sum(channels)', 'axial stem', 'axial other'], loc='best')
        
        mean = np.mean(res, axis=0)
        std  = stats.sem( res, 0)
        
        a[0].set_ylim( [mean[base_index]-1, mean[base_index]+3] )
        
        # calc mean max amp
        Max = []
        for i in range(count):
            base = res[i,base_index]
            rebase = np.subtract(res[i,base_index:-100], base)
            Max.append( max(rebase) )
        
        GRAND_MAX += Max
        ALL_MAX.append( np.mean(Max) )
        GRAND_TAU1 += TAU1
        GRAND_TAU2 += TAU2
        
        print()
        print('maximum val:', np.mean(Max),'+/-', stats.sem( Max ), 'tau1:', np.mean(TAU1), 'tau2:', np.mean(TAU2) )
        print()
        
        a[0].fill_between(tm, np.subtract(mean, std), np.add(mean, std), alpha=0.5, color='m')
        a[0].plot(tm, mean, 'k', lw=3)
        
        plt.savefig('../../../Desktop/EPSP/V'+str(version)+'.png', transparent=True)
        plt.savefig('../../../Desktop/EPSP/V'+str(version)+'.pdf', transparent=True)
        plt.close('all')
    
    print()
    print('Grand average max amp:', np.mean( GRAND_MAX ),'+/-', stats.sem( GRAND_MAX ), 'min', min(ALL_MAX), 'max', max(ALL_MAX) )
    print()
    print('Grand average max amp:', np.mean( GRAND_TAU1 ),'+/-', stats.sem( GRAND_TAU1 ), '\t', np.mean( GRAND_TAU2 ),'+/-', stats.sem( GRAND_TAU2 ) )


    
    
