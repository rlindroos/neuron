#
'''
MSN model used in Lindroos et al., (2018). Frontiers

Robert Lindroos (RL) <robert.lindroos at ki.se>
 
The MSN class and most channels were implemented by 
Alexander Kozlov <akozlov at kth.se>
with updates by RL

Implemented in colaboration with Kai Du <kai.du at ki.se>
'''



from __future__ import print_function, division
from neuron import h
import numpy                as np
import MSN_builder          as build
import common_functions     as use


h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')



# global result dict
RES = {}



   
#=========================================================================================


# in the dynamimcal modulation, the channels are connected to one substrate of the cascade.
# base modulation (control) is assumed for base value of the substrate and full modulation
# is assumed when the substrate level reaches its maximal value. Linear scaling is used 
# between these points.
def main(   par="./params-msn.json",   \
            morphology=None,           \
            sim='vm',       \
            distParams=None,\
            amp=400e-12,    \
            run=None,       \
            simDur=1.0,     \
            stimDur=0.9,    \
            not2mod=[],     \
            largeScan=0,    \
            modulate_axon=False): 
    
    
    print('-iter:', run, '-amp:', amp, '[nA]')
    
    
    
    # initiate cell
    cell    =   build.MSN(  params=par,                             \
                            morphology=morphology,                   \
                            variables=distParams )
    
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set current injection
    stim        =   h.IClamp(0.5, sec=cell.soma)
    stim.amp    =   amp*1e9 
    stim.delay  =   0.1*1e3
    stim.dur    =   stimDur*1e3 
    
    
    # record vectors
    tm = h.Vector()
    tm.record(h._ref_t)
    vm = h.Vector()
    vm.record(cell.soma(0.5)._ref_v) 
     
    
    tstop       = simDur*1e3
    # dt = default value; 0.025 ms (25 us)
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
                    
                    
                    
    # static modulation ================================================================
    
    if sim == 'directMod':
        
        
        # channels to modulate channels
        mod_list    = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car'] 
          
        
        if run[1] == 0 and run[2] == 0:
            
            # draw mod factors from [min, max] ranges (as percent of control). 
            # Channel ranges are in the following order:
            # ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car' ]
            
            if largeScan == 0:
                mod_fact = [    np.random.uniform(0.9,1.30),   \
                                np.random.uniform(1.0,1.2),   \
                                np.random.uniform(0.95,1.10),   \
                                np.random.uniform(0.7,1.0),   \
                                np.random.uniform(0.7,0.8),   \
                                np.random.uniform(0.7,0.8),   \
                                np.random.uniform(0.95,1.5),   \
                                np.random.uniform(0.65,0.75)        ]
            else:
                mod_fact = [    np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90),   \
                                np.random.uniform(0.1,1.90)    ]                                                     
            print('factors drawn:', mod_fact)
            RES[run[0]]['factors'] = mod_fact
            
        else:
            
            # use same factors for all models and current injections (new every new cycle)
            mod_fact = RES[run[0]]['factors']
            
            
    
        for sec in h.allsec():
            
            # modulate axon?
            if sec.name().find('axon') >= 0:
                if not modulate_axon:
                    continue
            
            for seg in sec:
                
                for mech in seg:
                    
                    if mech.name() in not2mod:
                        continue
                    
                    elif mech.name() in mod_list: 
                        
                        # get factor from list
                        factor = mod_fact[mod_list.index(mech.name() )]
                        
                        if mech.name()[0] == 'c':
                            # Ca channels
                            pbar        = mech.pbar
                            mech.pbar   = pbar * factor
                            
                        else:
                            # non Ca channels
                            gbar        = mech.gbar
                            mech.gbar   = gbar * factor
                        
                            
                        
    # solver------------------------------------------------------------------------------
                
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop: 
        
        #print( h.t )        
        h.fadvance()
        
    
    # save output ------------------------------------------------------------------------
    # Extract spikes and save
    
    spikes      = use.getSpikedata_x_y(tm,vm) 
    
    RES[run[0]][run[1]][run[2]] = spikes



# Start the simulation.
# Function needed for HBP compability  ===================================================
if __name__ == "__main__":
    
    #######################################################
    # simulation setup, only change these three parameters!
    
    axonMod   = 1
    kafMod    = 0
    largeScan = 0       # need kafMod == 0; set manually
    
    #######################################################
    
    if kafMod == 1:
        not2mod = ['kaf']
    else:
        not2mod = []
    
    if axonMod == 1:
        modulate_axon=True
    else:
        modulate_axon=False
        
    
    cell_type   =   'D2'
    par         =   './params_iMSN.json'
    morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
    mod_list    =  ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    data        =    use.load_obj(cell_type+'_30bestFit.pkl')
    RES         =    {}
    
    for i in range(400):
        RES[i] = {}
        
        for j in range(30):
            RES[i][j] = {}
            
            parameters  = data[j]['variables']
            rheobase    = data[j]['rheobase' ]
            currents    = np.arange(rheobase-10,rheobase+11,10)
        
            print('starting sim', i, j, rheobase)
            
                
            for c,current in enumerate(currents):
                main(   par=par,                    \
                        morphology=morphology,      \
                        distParams=parameters,      \
                        amp=current*1e-12,          \
                        run=[i,j,c],                \
                        simDur=1.0,                 \
                        stimDur=0.9,                \
                        sim='directMod',            \
                        not2mod=not2mod,            \
                        largeScan=largeScan,        \
                        modulate_axon=modulate_axon )
                
            if j % 5 == 0:
                
                print('inside save loop', j, i)
            
                if i == 0 and j == 0:
                    
                    mod_fact = RES[i]['factors']
                
                    ID = ''
                    
                    for m,mech in enumerate(mod_list):
                        
                        ID = ID + mech + str( int(mod_fact[m]*100) )
            
                use.save_obj(RES, ''.join(['./staticMod-', cell_type, '-AIS', str(axonMod), '-KAF', str(kafMod), '_', ID]) )
                    
    
    
