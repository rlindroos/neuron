#
'''
MSN model updated from model used in Lindroos et al., (2018). Frontiers

Robert Lindroos (RL) <robert.lindroos at ki.se>
 
The original MSN class and most channels were implemented by 
Alexander Kozlov <akozlov at kth.se>

'''



from __future__ import print_function, division
from neuron import h
from joblib import Parallel, delayed
import multiprocessing
import numpy                as np
import matplotlib.pyplot    as plt
import MSN_builder          as build
import common_functions     as use
import glob



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


plt.close('all')   






def main(   par="./params_iMSN.json",                       \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            sim='vm',                                       \
            amp=265*1e-12,                                  \
            run=None,                                       \
            distParams=None,                                \
            simDur=1,                                       \
            stimDur=0.9                             ):
            
    print('\t\t', amp, simDur, stimDur)
    
    if distParams:
        # use best fit parameters
        random_variables = distParams
        print( '------ loading "random" variables' )
    else:
        # draw random parameters
        random_variables = [    np.random.uniform(0.5,1.5), \
                                np.random.uniform(0.8,1.0), \
                                np.random.uniform(10.0,60.0),   \
                                np.random.uniform(1.0,30.0),    \
                                np.random.uniform(0.5,1.5), \
                                np.random.uniform(0.0,0.9), \
                                np.random.uniform(1.0,130.0),   \
                                np.random.uniform(-3.0,-70.0),  \
                                np.random.uniform(0.5,2.0),    \
                                np.random.uniform(0.5,1.5), \
                                np.random.uniform(0.8,1.0), \
                                np.random.uniform(10.0,60.0),   \
                                np.random.uniform(1.0,30.0),    \
                                np.random.uniform(-2.0,2.0), \
                                np.random.uniform(1.0,9.0), \
                                np.random.uniform(1.0,130.0),   \
                                np.random.uniform(-3.0,-70.0),    \
                                np.random.uniform(-2.0,2.0), \
                                np.random.uniform(1.0,9.0), \
                                np.random.uniform(1.0,130.0),   \
                                np.random.uniform(-3.0,-70.0)      ]
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    # set current injection
    stim        =   h.IClamp(0.5, sec=cell.soma)
    stim.amp    =   amp * 1e9
    stim.delay  =   100    #ms
    stim.dur    =   int(stimDur * 1e3)   
    
    tstop       = simDur * 1e3
    # dt        = default value; 0.025 ms (25 us)
    
    
    # all channels with pointers
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
     
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p
    
                  
    # set pointers; need since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    # set pointer (for all channel instances in mod_list)
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
                if mech.name() == 'sk' and run == 'sk':
                    
                    mech.gbar = mech.gbar * 0.0
    
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    
              
    # solver------------------------------------------------------------------------------            
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop:
                
        h.fadvance()
        
    spikes = use.getSpikedata_x_y(tm,vm)
    print( spikes )
    return len( spikes ), tm, vm
    
    
    
        
        
def start_protocol(     par="./params_iMSN.json",                       \
                        morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
                        sim='vm',                                       \
                        simDur=1.0,                                     \
                        stimDur=0.9,                                    \
                        cell_type=None,                                 \
                        solution=None                                 ):       
    
    data = use.load_obj(cell_type+'_3bestFit.pkl')[solution]
    parameters  = data['variables' ]
    prev_curr   = data['rheobase'  ]
    
    print('set:', solution, 'prev_I:', prev_curr)
    
    # find detailed rheobas
    rheobase_found  = False
    diff            = 10
        
    while not rheobase_found:
        
        # calc current amp
        current = prev_curr - diff
        
        print( '\tstarting sim:', current, '-set:', solution, '-rheo:', prev_curr )
        
        # run simulation
        results   = main(   par=par,                    \
                            amp=current*1e-12,          \
                            run=solution,               \
                            simDur=1.0,                 \
                            morphology=morphology,      \
                            distParams=parameters,      \
                            stimDur=0.9                 )
        
        print(results[0])
        
        # recalculate diff (and update rheobase_found?)
        if diff == 10:
            # +/- 5
            diff        = -5 + 10*np.ceil(results[0]/100.0)
            prev_curr   = current
            prev_spikes = results[0]
        
        else:
            
            if diff < 3:
                # rheobase found?
                if prev_spikes > 0 and results[0] == 0:
                    rheobase        = prev_spikes
                    rheobase_found  = True
                    
                elif prev_spikes == 0 and results[0] > 0:
                    rheobase        = results[0]
                    rheobase_found  = True
                    tm              = results[1]
                    vm              = results[2]
                    
                else:
                    tm  = results[1]
                    vm  = results[2]
            else:
                tm   = results[1]
                vm   = results[2]
                    
            
            # +/- 1
            diff        = -1 + 2*np.ceil(results[0]/100.0)
            prev_curr   = current
            prev_spikes = results[0]
            
            
            
    # run SK block simulation
    #for c in [0, 20, 40]:
    
    return rheobase, tm, vm
        
            
            
        

# Start the simulation.
# Function needed for HBP compability  ===================================================
if __name__ == "__main__":
    
    
    print('starting sim')  
    
    num_cores       = multiprocessing.cpu_count()  
    
    for ct in [1, 2]:    
    
        if ct == 1:
            cell_type   =   'D1'
            par         =   './params_dMSN.json'
            morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
            currents    =   np.arange(330,371,20)
            
        elif ct == 2:
            cell_type   =   'D2'
            par         =   './params_iMSN.json'
            morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
            currents    =   np.arange(200,265,20)
    
        print( cell_type, par, morphology )
        
        R = Parallel(n_jobs=num_cores)(delayed(start_protocol)(   par=par,        \
                                            morphology=morphology,      \
                                            sim='vm',                   \
                                            cell_type=cell_type,        \
                                            solution=i                  \
                                        ) for i in range(30)  )
        
        use.save_obj(R, cell_type+'_rheobase')
        plt.plot(R[0][1], R[0][2])
        plt.show()
        
        
