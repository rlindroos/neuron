import sys
sys.path.append('./')
import numpy                as np
import matplotlib.pyplot    as plt
import common_functions     as use
import glob
from joblib import Parallel, delayed
import multiprocessing
import sys



def read_and_format_data(f, N):
    
    data = use.load_obj(f)
    
    # extract distribution from file name
    dist = f.split('_')[3]
    
    # get sim index
    if dist == 'clustered':
        i       =   0
        d       =   int(f.split('dist')[1].split('_')[0])
        group   =   int(d/60)
    else:
        i       =   1
        d       =   int(f.split('run')[1].split('.')[0])
        norm    =   d
        if norm < 20:
            group   =   0
        elif norm < 37: 
            group   =   1
        elif norm < 54: 
            group   =   2
        else:
            group   =   3
    
    if group > 3:
        group = 3
    
    I       =   np.zeros((10,N))    
    I_tot   =   np.zeros((10,N))
    V_tot   =   np.zeros((10,N))
    V_trunc =   np.zeros((10,N))
    
    SPIKES  =   np.zeros((10,20))
    
    # loop over bg
    for b,bg in enumerate(range(10)):
            
        results     =   data[bg]
        
        vm          =   results[    'vm']
        V_tot[ b,:] =   vm
        spikes      =   results['spikes']   
        
        for s,S in enumerate(spikes):
            SPIKES[b,s]     =   S
        
        if dist == 'clustered':
            I_tot[b,:]  =   results['cur']
        else:
            for k,key in enumerate(results['cur']):
                I[k,:]  =   results['cur'][k]
            I_tot[b,:]  =   np.sum(I, axis=0) 
        
        V_trunc[b,:] =   [v if v < -50 else -50 for v in vm ]
    
    
    # calc average
    I_mean  =   np.mean(I_tot, axis=0) 
    
    
    return [V_tot, I_tot, I_mean, I, i, group, V_trunc, SPIKES]





ISI             =       '1ms'
V               =       sys.argv[-1]
path            =       'Results/InVivo_sim_res/Weak'

repickled_name  =       'repickled_'+V+'_'+ISI

#fig, ax         =       plt.subplots(3,4, figsize=(20,10), gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)

color           =       ['r', 'b']
alpha           =       [0.2, 0.2]
ls              =       ['-', '-']
mew             =       1
ms              =       20

cell_type       =       ['D1', 'D2']
distribution    =       ['clustered', 'dispersed'] #['dispersed'] #

time            =       np.arange(-500,500.001,25e-3)
N               =       len(time)
max_time        =       200         # ms, following activation pattern
sort_index      =       range(10) #[3,1,8,10,5,6,9,7,2,4]



try:
    RES = use.load_obj(path+repickled_name+'.pkl')
    print '--loading repickled data' 
except:   
    print '--repickeling'
    files       =   glob.glob(path+'/InVivo*'+ISI+'_*_D1_'+V+'*')   
    num_cores   =   multiprocessing.cpu_count() 
    RES         =   Parallel(n_jobs=num_cores)(delayed(read_and_format_data)( f, N ) for f in files)
    use.save_obj(RES, path+repickled_name )



fig,ax = plt.subplots(1,1)

for m,M in enumerate(RES): 
    
    VM      =   M[0]
    I_tot   =   M[1]
    I_mean  =   M[2]
    i       =   M[4]
    group   =   M[5]
    V_trunc =   M[6]
    spikes  =   M[7]
    
    V_mean  =   np.mean(V_trunc, axis=0)
    V_std   =   np.std( V_trunc, axis=0)
    
    for bg in range(10):
        if group == 1:
            # plot all raw traces (top line).
            ax.plot(time, VM[bg,:], color=color[i], alpha=alpha[i], ls=ls[i])


ax.set_xlim([-30, max_time])
ax.spines['bottom'].set_linewidth(4)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(False)

ax.set_yticks([])   

ax.set_xticks( [-500,0,500] )
ax.set_xticklabels([-500,0,500], fontsize=30)

#fig.savefig('../../../Dropbox/Manuscripts/Figures_2018/deterministic_exampleTrace.png')

'''

NSC = {'spike_over_bg':{}}
for bg in range(10):
    NSC[bg] = {}
    for group in range(4):
        NSC['spike_over_bg'][group] = {}
        NSC[bg][group]              = {}
        for activation_pattern in range(2):
        
            NSC['spike_over_bg'][group][activation_pattern] = {'mean':[], 'std':[]}
            
            NSC[bg][group][activation_pattern] = {  'total_traces':0,       \
                                                    'non_spiking':0,        \
                                                    'spikes_in_traces':[],  \
                                                    'time2spike':[]         }
            


for m,M in enumerate(RES): 
    
    VM      =   M[0]
    I_tot   =   M[1]
    I_mean  =   M[2]
    i       =   M[4]
    group   =   M[5]
    V_trunc =   M[6]
    spikes  =   M[7]
    
    V_mean  =   np.mean(V_trunc, axis=0)
    V_std   =   np.std( V_trunc, axis=0)
    
    # plot mean current over bg (dispersed current is the summed current from all 10 synapses).
    ax[2,group].plot(time, I_mean, color=color[i], alpha=alpha[i] )
    
    
    flag       = True
    spike_list = []
    
    for bg in range(10):
        
        # plot all raw traces (top line).
        ax[0,group].plot(time, VM[bg,:], color=color[i], alpha=alpha[i], ls=ls[i])
        
        # count traces and check if spiking
        NSC[bg][group][i]['total_traces'] += 1  
        
        if not any(spikes[bg]) or spikes[bg,0] > max_time:
            NSC[bg][group][i]['non_spiking'] += 1
            #spike_list.append(max_time)             # if no spike within range -> append 
            flag = False
        else:
            spike_list.append(spikes[bg,0])
            
        
        # plot spikes times (second line from top). Marker size is large for first spike and shrinks with following spikes
        for s_ind,s in enumerate(spikes[bg,:]):
            if s > 0 and s < max_time:
                ax[1,group].plot(s, bg+1, '|', color=color[i], ms=ms/(s_ind+1), mew=mew)
                if s_ind == 0:
                    NSC[bg][group][i]['time2spike'].append( spikes[bg,0] )
            else:
                # save number of spikes in trace (append to list)
                NSC[bg][group][i]['spikes_in_traces'].append(s_ind)  
                break
    
    if flag:        
        NSC['spike_over_bg'][group][i]['mean'].append( np.mean(spike_list) )  
        NSC['spike_over_bg'][group][i][ 'std'].append( np.std( spike_list) )   
    
    
'''    


# QUANTIFY AND PLOT

def format_boxplot(bp, median=False, lw=2, colors=None):
    '''adjust boxplot to stadardized format
    '''
    if not colors:
        for box in bp['boxes']:
            box.set(color='k', linewidth=lw)
        for w in bp['whiskers']:
            w.set(color='k', linewidth=lw)
        for cap in bp['caps']:
            cap.set(color='k', linewidth=lw) 
    else:
        for c,box in enumerate(bp['boxes']):
            box.set(color=colors[c], linewidth=lw)
        for c,w in enumerate(bp['whiskers']):
            c = c/2
            w.set(color=colors[c], linewidth=lw)
        for c,cap in enumerate(bp['caps']):
            c=c/2
            cap.set(color=colors[c], linewidth=lw)
        for c,flier in enumerate(bp["fliers"]):
            flier.set(marker='*', markersize=10, markerfacecolor=colors[c], markeredgecolor=colors[c])
    if not median:
        for med in bp['medians']:
            med.set(color='w', linewidth=0)


'''

f1, a1 = plt.subplots(5,4, figsize=(20,15), gridspec_kw={'height_ratios':[3,2,4,2,4]} )
#f2, a2 = plt.subplots(3,4, figsize=(20,10), gridspec_kw={'height_ratios':[2,2,2,]} )

# index of plot window (for easy rearangement)
a = 0
b = 1
c = 1
d = 3
e = 4
f = 2

width = 0.5

for group in range(4):
    
    # number of groups and groups that are not spiking
    for i in range(2):       
        tot_trace   = 0
        non_spike   = 0
        for bg in range(10):
            tot_trace   += NSC[bg][group][i]['total_traces']
            non_spike   += NSC[bg][group][i][ 'non_spiking']
        spike       = tot_trace - non_spike
        a1[a,group].bar(i, spike, width, color=color[i])
        a1[a,group].bar(i, non_spike, width, bottom=spike, color='grey')
    max_hight = 200
    a1[a,group].set_ylim((0,max_hight))
    a1[a,group].set_xticks([])
    
    
    
    
    
    # fastest over bg mean time to first spike over bg (individually)
    bp1 = a1[c,group].boxplot(  [ NSC['spike_over_bg'][group][0]['mean'], NSC['spike_over_bg'][group][1]['mean'] ],  positions = [1,2],      \
                                vert=False,                                       \
                                widths=0.6,                                       \
                                medianprops=dict(linewidth=2,color='k'),     \
                                boxprops=dict(   linewidth=4,color=color))
    format_boxplot(bp1, median=True, lw=4, colors=color)
    
    for i in range(2):
        x = NSC['spike_over_bg'][group][i]['mean']
        y = np.ones(len(x))*3
        a1[b,group].plot(  x, y, '|', color=color[i], alpha=0.8,  ms=ms, mew=mew)
    a1[b,group].set_yticks([])
    a1[b,group].set_xlim((0,max_time))
    a1[b,group].set_ylim((0,4))
    
    
    # fastest split on bg
    Y = range(1,11)
    for i in range(2):    
        CV = []; CM = []
        for bg in range(10):
            CM.append( np.mean( NSC[bg][group][i]['time2spike'] ) )
            CV.append( np.std(  NSC[bg][group][i]['time2spike'] ) )
        Xc = CM #[CM[index-1] for index in sort_index]
        a1[f,group].plot( Xc, Y, '-o', color=color[i], alpha=0.8,  ms=10, mew=mew, lw=3)
        a1[f,group].set_xlim((0,max_time))
    
    
    # most close in time (first spike) over bg
    bp1 = a1[d,group].boxplot(  [NSC['spike_over_bg'][group][0]['std'],NSC['spike_over_bg'][group][1]['std']],  positions = [1,2],      \
                                vert=False,                                       \
                                widths=0.6,                                       \
                                medianprops=dict(linewidth=2,color='k'),     \
                                boxprops=dict(   linewidth=4,color=color))
    format_boxplot(bp1, median=True, lw=4, colors=color)
    for i in range(2):
        x = NSC['spike_over_bg'][group][i]['std']
        y = np.ones(len(x))*3
        a1[d,group].plot(  x, y, '|', color=color[i], alpha=0.8,  ms=ms, mew=mew)
    a1[d,group].set_yticks([])
    a1[d,group].set_xlim((0,max_time/2))
    a1[d,group].set_ylim((0,4))
    
    

    # most spikes
    for i in range(2):    
        CV = []; CM = []
        for bg in range(10):
            CM.append( np.mean( NSC[bg][group][i]['spikes_in_traces'] ) )
            CV.append( np.std(  NSC[bg][group][i]['spikes_in_traces'] ) )
        Xc = CM #[CM[index-1] for index in sort_index]
        a1[e,group].plot( Xc, Y, '-o', color=color[i], alpha=0.8,  ms=10, mew=mew, lw=3)
        a1[e,group].set_xlim((0,4))
    

    # fastest within bg

    # most similar within bg
    

for group in range(4):
    ax[2,group].set_ylim([-0.4, 0.05])
    ax[2,group].set_xlim([-30, max_time])
    ax[1,group].set_ylim([0, 11])
    ax[1,group].set_xlim([-30, max_time])
    ax[1,group].set_yticks(range(2,11,2))
    ax[0,group].set_xlim([-30, max_time])
    
    y = np.ones( len(clustered[group]) )*10
    ax[3,group].plot(clustered[group], y, '|', color=color[0], alpha=0.8,  ms=ms, mew=mew)
    
    y = np.ones( len(dispersed[group]) )*10
    ax[3,group].plot(dispersed[group], y, '|', color=color[1], alpha=0.8,  ms=ms, mew=mew)
    '''
#print 'clustered:', NSC, 'dispersed:', NSD   

#f1.savefig('../../../Desktop/'+repickled_name+'.png') 
       
#plt.show()
        
    
        
        
    
    
