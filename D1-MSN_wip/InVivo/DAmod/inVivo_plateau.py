
# script used for running in vivo simultion of clustered synaptic activation
# This script is based on plateaus_in_vivo_saveFeatures.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_clustered_D<>_section<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')



# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    DAmod       =   True
    
    ISI         =   1
    tstop       =   1500
    cvode       =   h.CVode()
    
    if DAmod:
        modulate_synapse    =   True
        modulate_gaba       =   True
        modulate_axon       =   False
    else:
        modulate_synapse    =   False
        modulate_gaba       =   False
        modulate_axon       =   False
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    58
    norm_val    =    0
    
    model_version = int(np.floor(id/N))
    section       = id - model_version*N
    model_version = model_version + int(h.start_value)
    
    
    print(cell_type, morphology, par, 'DAmod', DAmod)
    
    
    # load random sets
    path_to_lib         = './Libraries/'
    randomSets          = use.load_obj(path_to_lib+cell_type+'_30randomSet.pkl')
    random_variables    = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    
    
    # result dict    
    RES     = {'par':{  'DAmod':DAmod,                          \
                        'modulate_synapse':modulate_synapse,    \
                        'modulate_gaba':modulate_gaba,          \
                        'modulate_axon':modulate_axon,          \
                        'synaptic_strength':int(h.synaptic_strength)  },\
               'spikes':{}                                              }
    
    for feature in ['ratio', 'c1', 'c2', 't2s']:
        RES[feature]    =   np.zeros((len(randomSets)))
    
     
    # random loop ------------------------------------------------------------------------
    for r in range(len(randomSets)):
        
        print('-random set:', r, '-cell_type:', cell_type, '-section:', section)
        
        randomSet   = randomSets[r]
        SPIKES      = np.zeros((10,20))
        
        if modulate_synapse:
            syn_fact = randomSet[-2:]
        else:
            syn_fact = False
        
        if modulate_gaba:
            gabaMod = use.load_obj(path_to_lib+'GABA_plusMinus30_30randomSet.pkl')[r]
        else:
            gabaMod = False
        
        
        # initiate cell ------------------------------------------------------------------
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=random_variables   )
        
        # set cascade 
        #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        # Modulation of channels is not used in this script
        if cell_type=='D1':
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
        else:
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
            
        use.set_pointers(cell, pointer, mod_list)
                        
                        
        # load modulation factors and scale conductances  
        if DAmod:
            org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
            use.set_channel_modulation( org_chan_gbar,          \
                                        mod_list,               \
                                        randomSet,              \
                                        modulate_axon=False     )
        
        # set bg noise (including modulation)
        Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                                syn_fact=syn_fact,              \
                                                gabaMod=gabaMod                 )
        
        
        # set plateau inducing stimuli (including stimuli)
        syn, stim, ncon, d2soma = use.set_clustered_stimuli(    cell,               \
                                                                section,            \
                                                                syn_fact=syn_fact,  \
                                                                ISI=ISI             )
                
        
        # simulation loop ----------------------------------------------------------------
        for i in range(10):
            
            print (i)
            
            # record vectors
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            #cur = h.Vector()
            #cur.record(syn._ref_I)
                      
            # finalize and run
            h.finitialize(-70)
            while h.t < tstop:
            
                h.fadvance()  
            
            # shift spike traces 
            t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= 20000]
            v   = [x[1]      for x in enumerate( vm) if x[0] >= 20000]  
            #c   = [x[1]      for x in enumerate(cur) if x[0] >= 20000]  
            
            # get spikes
            spikes = use.getSpikedata_x_y( t, v )[:20]
            SPIKES[i,:len(spikes)]  = spikes
        
        print( '--analyse data')
        
        ratio, c1, c2, t2s = use.analyse_spike_data(SPIKES, max_time=200, t2s_cutoff=2)
            
        RES['spikes'][r]    = SPIKES
        RES['ratio'][r]     = ratio
        RES['c1'][   r]     = c1
        RES['c2'][   r]     = c2
        RES['t2s'][  r]     = t2s
        
        VAL  = {'values':[      np.mean(   np.delete(RES['ratio'], range(r+1,len(randomSets)) )),       \
                                np.mean(   np.delete(RES['c1'],    range(r+1,len(randomSets)) )),       \
                                np.mean(   np.delete(RES['c2'],    range(r+1,len(randomSets)) )),       \
                                np.nanmean(np.delete(RES['t2s'],   range(r+1,len(randomSets)) ))        ], \
                'spikes':RES['spikes'], \
                'par':   RES['par']    }  
        
        print('*saving' )  
        use.save_obj(VAL, 'InVivoDAmod'+str(ISI)+'ms_clustered_'+cell_type+'_V'+str(model_version)+'_dist'+str(d2soma)+'_section'+str(section) )  
       
        
   
    
