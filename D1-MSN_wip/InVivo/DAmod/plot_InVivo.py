import sys
sys.path.append('./')
import numpy                as np
import matplotlib.pyplot    as plt
import common_functions     as use
import glob
from joblib import Parallel, delayed
import multiprocessing
from    scipy           import stats




def create_result_structure(N):
    array = {}
    group_len   = [[20,17,17,4], [20,17,17,10]]
    for t,apt in enumerate(['clust', 'disp']):
        array[t] = {}
        for g in range(4):
            array[t][g] = {     'spike_ratio':np.zeros( (N,group_len[t][g]) ),     \
                                'spike_count':np.zeros( (N,group_len[t][g]) ),     \
                                'time2spike': np.zeros( (N,group_len[t][g]) ),     }
    
    return array
    
    
    
    
    

def analyse_spike_data(f, max_time=200, t2s_cutoff=2):
    
    data = use.load_obj(f)
    
    # extract distribution from file name
    
    dist = f.split('_')[3]
    V    = int(f.split('_')[5][1:])
    
    # get sim index
    if dist == 'clustered':
        apt     =   0
        d       =   int(f.split('dist')[1].split('_')[0])
        group   =   int(d/60)
    else:
        apt     =   1
        d       =   int(f.split('run')[1].split('.')[0])
        norm    =   d
        if norm < 20:
            group   =   0
        elif norm < 37: 
            group   =   1
        elif norm < 54: 
            group   =   2
        else:
            group   =   3
    
    if group > 3:
        group = 3
    
    
    Spikes          = np.zeros((30,10,20))      # spikes in trace
    C_after_0       = np.zeros((30,10))         # mean count after pattern trigger
    C_before_0      = np.zeros((30,10))         # mean count before pattern trigger
    Ratio           = np.zeros((30))            # mean ratio after pattern trigger
    Var_t2s         = np.zeros((30,10))         # variance of time to  first spike (after 0)
    
    M               = 0                         # maximum length of spike train
    
    
    #loop over modulation parameters
    for r in range(30):
        
        if r not in data:
            break
        
        spiking_traces      =   0
        time2spike          =   np.zeros((10))
        
        # loop over bg
        for b,bg in enumerate(range(10)):
            
            spikes                      =   data[r][bg]['spikes'] 
            
            if len(spikes) > 20:
                spikes = spikes[:20]
             
            Spikes[r,b,:len(spikes)]    =   spikes
            
            
            # update max value?
            if len(spikes) > M:
                M = len(spikes)
            
            
            # index and value of first spike within range (0-max_time)
            i = [ i for i,s in enumerate(spikes) if s > 0 and s < max_time]
            
            I = len(i)
            
            # check if spikes
            if I > 0:
                
                spiking_traces += 1
                s               = spikes[i[0]]
                i               = i[0]
                
            else:
                i   =   0
                s   =   np.nan
                
            # extract values
            C_after_0[ r,b]     =   I
            C_before_0[r,b]     =   i
            time2spike[b]       =   s
            
            
        # calc mean and variance
        Ratio[r]    =  1.0 * spiking_traces / 10 
        
        
        # calc number of non nan values in time2spike
        L = time2spike.size - np.count_nonzero(np.isnan(time2spike))
        
        if L >= t2s_cutoff:
            Var_t2s[r]  =  np.var(time2spike) 
        else:
            Var_t2s[r]  =  np.nan
                
    
               
    # remove excess index from arrays  
    if M < 20:
        S = np.delete(Spikes, range(M,20),2)
    else:
        S = Spikes
          
    if r == 29:
        return [S, apt, group, V, \
                                np.mean(Ratio),         \
                                np.mean(C_after_0),     \
                                np.mean(C_before_0),    \
                                np.nanmean(Var_t2s)     ]
    else:
        return [S, apt, group, V, \
                                np.mean(np.delete(Ratio,      range(r,30) )),       \
                                np.mean(np.delete(C_after_0,  range(r,30) )),       \
                                np.mean(np.delete(C_before_0, range(r,30) )),       \
                                np.mean(np.delete(Var_t2s,    range(r,30) ))        ]


  
  
  
    
    
def fill_result_structure(DATA, array):
    
    
    # set index
    index = {}
    for m in range(4):
        index[m] = {}
        for apt in [0,1]:
            index[m][apt]={}
            for g in range(4):
                index[m][apt][g] = 0
    
    
    # loop over activation pattern in DATA
    for M in DATA: 
        
        # activation pattern type (apt). clustered or dispersed, 0 and 1 respectively
        spikes  =   M[0]        # spike times (array of len 20 filled with zeros at the end)
        apt     =   M[1] 
        group   =   M[2]        # distance group (0-4)
        model   =   M[3]
        ratio   =   M[4]
        c1      =   M[5]
        c2      =   M[6]
        t2s     =   M[7]
        
        # save to array
        i = index[model][apt][group]
        
        array[apt][group]['spike_ratio'][model,i] = ratio
        array[apt][group]['spike_count'][model,i] = c1
        array[apt][group]['time2spike' ][model,i] = t2s
        
        index[model][apt][group] += 1   
    
    return array  



def plot_spike_raster(DATA, axes, color):
    
    count = {}
    for model in range(4):
        count[model] = {}
        for group in range(4):
            count[model][group] = 0
    
    for M in DATA:
        
        spikes  = M[0]
        apt     = M[1]
        group   = M[2]
        model   = M[3]
        
        
        for d1 in range(spikes.shape[0]):
            for d2 in range(spikes.shape[1]):
                
                #if len(spikes[d1,d2,:]) > 0:
                train   =   spikes[d1,d2,:]
                train   =   train[np.nonzero(train)]
                y       =   np.ones((len(train)))*count[model][group]
                axes[model,group].plot(train, y, '.', c=color[apt])
                
                count[model][group] += 1


def plot_spike_histogram(DATA, axes, color):
    
    count = {}
    for apt in range(2):
        count[apt] = {}
        for model in range(4):
            count[apt][model] = {}
            for group in range(4):
                count[apt][model][group] = {'list':np.zeros((300000)), 'index':0}
    
    for M in DATA:
        
        spikes  = M[0]
        apt     = M[1]
        group   = M[2]
        model   = M[3]
        
        
        for d1 in range(spikes.shape[0]):
            for d2 in range(spikes.shape[1]):
                
                #if len(spikes[d1,d2,:]) > 0:
                train   =   spikes[d1,d2,:]
                train   =   train[np.nonzero(train)]
                
                index   =   count[apt][model][group]['index']
                
                count[apt][model][group]['list'][index:index+len(train)] = train
                count[apt][model][group]['index']                       += len(train)
    
    lw = [6,3]
    ls = ['-', '--'] 
    norm = [False,False,False,True]
              
    
    for group in range(4):
        for model in range(4):
            for apt in range(2):
                    
                train   =   count[apt][model][group]['list']
                train   =   train[np.nonzero(train)]
                
                axes[model,group].hist(train, bins=100, normed=norm[group], color=color[apt], histtype='step', linewidth=lw[apt], linestyle=ls[apt])
            
            axes[model,group].set_yticks([])
            axes[model,0].set_ylabel('model v. '+str(model+1), fontsize=16)
        axes[0,group].set_title('group '+str(group+1)+' - normed='+str(norm[group]), fontsize=20)
            
                
                


ISI             =       '1ms'
V               =       'V[0-3]'
path            =       'Results/InVivo_sim_res/Oct2018/DAmod/'
N               =       4


repickled_name  =       'sorted_'+ISI

fig, ax         =       plt.subplots(N,4, figsize=(20,10))  #, gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)


color           =       ['r', 'b']
alpha           =       [0.2, 0.2]
ls              =       ['-', '-']
mew             =       1
ms              =       20

cell_type       =       ['D1', 'D2']
distribution    =       ['clustered', 'dispersed'] #['dispersed'] #

time            =       np.arange(-500,500.001,25e-3)
max_time        =       200         # ms, following activation pattern
sort_index      =       range(10) #[3,1,8,10,5,6,9,7,2,4]



try:
    RES = use.load_obj(path+repickled_name+'.pkl')
    print '--loading repickled data' 
except:   
    print '--repickeling'
    files       =   glob.glob(path+'/InVivo*'+ISI+'_*_D1_'+V+'*')   
    num_cores   =   multiprocessing.cpu_count() 
    RES         =   Parallel(n_jobs=num_cores)(delayed(analyse_spike_data)( f ) for f in files)
    use.save_obj(RES, path+repickled_name )

array = create_result_structure(N)
array = fill_result_structure(RES, array)
#use.save_obj(array, path+repickled_name )






# plot result

plot_spike_histogram(RES, ax, color)
fig.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAmodRandom_spikeHist.png', transparent=True)

f1,a1 = plt.subplots(1,1, figsize=(10, 6))
f2,a2 = plt.subplots(1,1, figsize=(10, 6))
f3,a3 = plt.subplots(1,1, figsize=(10, 6))

axes  = [a1,a2,a3]
color = ['r', 'b']


for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
    add = 0
    for g in range(4):
        for apt in range(2):
            
            a   = array[apt][g][feature]
            
            # delete row 10 (since all zero due to spike before stimuli)
            #a   = np.delete(a, 10, 0)    
            
            if i == 2:
                # remove nan elements
                a    = a[~np.isnan(a)]
            
            # calc mean and sem
            # -global
            mean= np.mean( a )
            std = np.std( a, axis=None )    # stats.sem
            s   = mean - std
            e   = mean + std
            x   = (apt+1+g+add)*10
            w   = 2
            
            # -model version
            if i == 2:
                mv_mean = []
                # loop over model versions and calc mean for each model separately
                for v in range(N):
                    if v == 10:
                        continue
                    b = array[apt][g][feature][v,:]
                    mv_mean.append( np.mean( b[~np.isnan(b)] ) )
            else:
                mv_mean = np.mean( a, axis=1 )
            
            X       = np.ones( (1,len(mv_mean)) ) * x
            
            # connect dots between clust/disp
            if apt == 0:
                mvm_old = mv_mean
                X_old   = X
            else:
                # create lines
                m_zip = zip(mvm_old,mv_mean)
                X_zip = zip(X_old[0],X[0])
                
                for z in range(len(m_zip)):
                    axes[i].plot(X_zip[z], m_zip[z], 'k', alpha=0.3)
                    
                axes[i].plot(X_old[0], mvm_old, 'o', ms=10, color=color[0], alpha=0.2)
                axes[i].plot(X[0],     mv_mean, 'o', ms=10, color=color[1], alpha=0.2)
                
            
            
            # plotting
            axes[i].plot([x,x],     [s,e], '-', lw=2,  color='k')
            axes[i].plot([x-w,x+w], [s,s], '-', lw=2,  color='k')
            axes[i].plot([x-w,x+w], [e,e], '-', lw=2,  color='k')
            axes[i].plot(x, mean,  'o',   ms=20, color=color[apt], mew=2, mec='k')
            
            axes[i].set_xlim([0, 120])
            #axes[i].set_title(titles[p], fontsize=24)
            
            axes[i].set_xticks([])
            axes[i].yaxis.set_ticks_position('left')
        
            axes[i].spines['left'].set_linewidth(4)
            axes[i].spines['right'].set_visible(False)
            axes[i].spines['top'].set_visible(False)
            axes[i].spines['bottom'].set_visible(False)
            
        add += 2

# set ranges
a1.set_ylim([-0.2, 1.3])
a1.set_yticks( [0,0.2,0.4,0.6,0.8,1.0] )
a1.set_yticklabels([0,0.2,0.4,0.6,0.8,1.0], fontsize=20)

a2.set_ylim([-0.5, 4.3])
a2.set_yticks( range(0,4) )
a2.set_yticklabels(range(0,4), fontsize=20)

a3.set_ylim([-100, 1200])
a3.set_yticks( range(0,1041,200) )
a3.set_yticklabels(range(0,1041,200), fontsize=20)

# set ylabel
a1.set_ylabel('Spike probability', fontsize=24)
a2.set_ylabel('Spike count', fontsize=24)
a3.set_ylabel('Var; time to first spike', fontsize=24)

# set xlabel
placeholders = [15,45,75,105]
for i,a in enumerate(axes):
    a.set_xticks( placeholders )
    a.set_xticklabels(['proximal', 'medial', 'distal', 'terminal'], fontsize=20)
    
        

# statistical significance
hight = [1.2, 4, 1100]
for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
    T = []
    P = []
    for g in range(4):
        for apt in range(2):
            
            b   = array[apt][g][feature]
            
            # delete row 10 (since all zero due to spike before stimuli)
            #b   = np.delete(b, 10, 0)  
            
            # flatten
            b   = b.flatten()
            
            # remove nan
            b   = b[~np.isnan(b)]
            
            if apt == 0:
                # save array
                a = b
            else:
                # calc statistical significance
                t2, p2 = stats.ttest_ind(a,b, equal_var=False)
                
                if p2 < 0.001:
                    axes[i].plot(placeholders[g], hight[i], '*', c='k', ms=20)
                elif p2 < 0.05:
                    axes[i].plot(placeholders[g], hight[i], 'd', c='k', ms=20)
        
        # append to lists
        T.append(t2)
        P.append(p2)
    
    # print 
    print feature
    print '\t', T
    print '\t', P
                
             
f1.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAmodRandom_spike_ratio.png', transparent=True)
f2.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAmodRandom_spike_count.png', transparent=True)
f3.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAmodRandom_time2spike.png', transparent=True)

plt.show()














'''

NSC = {'spike_over_bg':{}}
for bg in range(10):
    NSC[bg] = {}
    for group in range(4):
        NSC['spike_over_bg'][group] = {}
        NSC[bg][group]              = {}
        for activation_pattern in range(2):
        
            NSC['spike_over_bg'][group][activation_pattern] = {'mean':[], 'std':[]}
            
            NSC[bg][group][activation_pattern] = {  'total_traces':0,       \
                                                    'non_spiking':0,        \
                                                    'spikes_in_traces':[],  \
                                                    'time2spike':[]         }
            


for m,M in enumerate(RES): 
    
    VM      =   M[0]
    I_tot   =   M[1]
    I_mean  =   M[2]
    i       =   M[4]
    group   =   M[5]
    V_trunc =   M[6]
    spikes  =   M[7]
    
    V_mean  =   np.mean(V_trunc, axis=0)
    V_std   =   np.std( V_trunc, axis=0)
    
    # plot mean current over bg (dispersed current is the summed current from all 10 synapses).
    ax[2,group].plot(time, I_mean, color=color[i], alpha=alpha[i] )
    
    
    flag       = True
    spike_list = []
    
    for bg in range(10):
        
        # plot all raw traces (top line).
        ax[0,group].plot(time, VM[bg,:], color=color[i], alpha=alpha[i], ls=ls[i])
        
        # count traces and check if spiking
        NSC[bg][group][i]['total_traces'] += 1  
        
        if not any(spikes[bg]) or spikes[bg,0] > max_time:
            NSC[bg][group][i]['non_spiking'] += 1
            #spike_list.append(max_time)             # if no spike within range -> append 
            flag = False
        else:
            spike_list.append(spikes[bg,0])
            
        
        # plot spikes times (second line from top). Marker size is large for first spike and shrinks with following spikes
        for s_ind,s in enumerate(spikes[bg,:]):
            if s > 0 and s < max_time:
                ax[1,group].plot(s, bg+1, '|', color=color[i], ms=ms/(s_ind+1), mew=mew)
                if s_ind == 0:
                    NSC[bg][group][i]['time2spike'].append( spikes[bg,0] )
            else:
                # save number of spikes in trace (append to list)
                NSC[bg][group][i]['spikes_in_traces'].append(s_ind)  
                break
    
    if flag:        
        NSC['spike_over_bg'][group][i]['mean'].append( np.mean(spike_list) )  
        NSC['spike_over_bg'][group][i][ 'std'].append( np.std( spike_list) )   
    
    
'''    


# QUANTIFY AND PLOT

def format_boxplot(bp, median=False, lw=2, colors=None):
    '''adjust boxplot to stadardized format
    '''
    if not colors:
        for box in bp['boxes']:
            box.set(color='k', linewidth=lw)
        for w in bp['whiskers']:
            w.set(color='k', linewidth=lw)
        for cap in bp['caps']:
            cap.set(color='k', linewidth=lw) 
    else:
        for c,box in enumerate(bp['boxes']):
            box.set(color=colors[c], linewidth=lw)
        for c,w in enumerate(bp['whiskers']):
            c = c/2
            w.set(color=colors[c], linewidth=lw)
        for c,cap in enumerate(bp['caps']):
            c=c/2
            cap.set(color=colors[c], linewidth=lw)
        for c,flier in enumerate(bp["fliers"]):
            flier.set(marker='*', markersize=10, markerfacecolor=colors[c], markeredgecolor=colors[c])
    if not median:
        for med in bp['medians']:
            med.set(color='w', linewidth=0)


'''

f1, a1 = plt.subplots(5,4, figsize=(20,15), gridspec_kw={'height_ratios':[3,2,4,2,4]} )
#f2, a2 = plt.subplots(3,4, figsize=(20,10), gridspec_kw={'height_ratios':[2,2,2,]} )

# index of plot window (for easy rearangement)
a = 0
b = 1
c = 1
d = 3
e = 4
f = 2

width = 0.5

for group in range(4):
    
    # number of groups and groups that are not spiking
    for i in range(2):       
        tot_trace   = 0
        non_spike   = 0
        for bg in range(10):
            tot_trace   += NSC[bg][group][i]['total_traces']
            non_spike   += NSC[bg][group][i][ 'non_spiking']
        spike       = tot_trace - non_spike
        a1[a,group].bar(i, spike, width, color=color[i])
        a1[a,group].bar(i, non_spike, width, bottom=spike, color='grey')
    max_hight = 200
    a1[a,group].set_ylim((0,max_hight))
    a1[a,group].set_xticks([])
    
    
    
    
    
    # fastest over bg mean time to first spike over bg (individually)
    bp1 = a1[c,group].boxplot(  [ NSC['spike_over_bg'][group][0]['mean'], NSC['spike_over_bg'][group][1]['mean'] ],  positions = [1,2],      \
                                vert=False,                                       \
                                widths=0.6,                                       \
                                medianprops=dict(linewidth=2,color='k'),     \
                                boxprops=dict(   linewidth=4,color=color))
    format_boxplot(bp1, median=True, lw=4, colors=color)
    
    for i in range(2):
        x = NSC['spike_over_bg'][group][i]['mean']
        y = np.ones(len(x))*3
        a1[b,group].plot(  x, y, '|', color=color[i], alpha=0.8,  ms=ms, mew=mew)
    a1[b,group].set_yticks([])
    a1[b,group].set_xlim((0,max_time))
    a1[b,group].set_ylim((0,4))
    
    
    # fastest split on bg
    Y = range(1,11)
    for i in range(2):    
        CV = []; CM = []
        for bg in range(10):
            CM.append( np.mean( NSC[bg][group][i]['time2spike'] ) )
            CV.append( np.std(  NSC[bg][group][i]['time2spike'] ) )
        Xc = CM #[CM[index-1] for index in sort_index]
        a1[f,group].plot( Xc, Y, '-o', color=color[i], alpha=0.8,  ms=10, mew=mew, lw=3)
        a1[f,group].set_xlim((0,max_time))
    
    
    # most close in time (first spike) over bg
    bp1 = a1[d,group].boxplot(  [NSC['spike_over_bg'][group][0]['std'],NSC['spike_over_bg'][group][1]['std']],  positions = [1,2],      \
                                vert=False,                                       \
                                widths=0.6,                                       \
                                medianprops=dict(linewidth=2,color='k'),     \
                                boxprops=dict(   linewidth=4,color=color))
    format_boxplot(bp1, median=True, lw=4, colors=color)
    for i in range(2):
        x = NSC['spike_over_bg'][group][i]['std']
        y = np.ones(len(x))*3
        a1[d,group].plot(  x, y, '|', color=color[i], alpha=0.8,  ms=ms, mew=mew)
    a1[d,group].set_yticks([])
    a1[d,group].set_xlim((0,max_time/2))
    a1[d,group].set_ylim((0,4))
    
    

    # most spikes
    for i in range(2):    
        CV = []; CM = []
        for bg in range(10):
            CM.append( np.mean( NSC[bg][group][i]['spikes_in_traces'] ) )
            CV.append( np.std(  NSC[bg][group][i]['spikes_in_traces'] ) )
        Xc = CM #[CM[index-1] for index in sort_index]
        a1[e,group].plot( Xc, Y, '-o', color=color[i], alpha=0.8,  ms=10, mew=mew, lw=3)
        a1[e,group].set_xlim((0,4))
    

    # fastest within bg

    # most similar within bg
    

for group in range(4):
    ax[2,group].set_ylim([-0.4, 0.05])
    ax[2,group].set_xlim([-30, max_time])
    ax[1,group].set_ylim([0, 11])
    ax[1,group].set_xlim([-30, max_time])
    ax[1,group].set_yticks(range(2,11,2))
    ax[0,group].set_xlim([-30, max_time])
    
    y = np.ones( len(clustered[group]) )*10
    ax[3,group].plot(clustered[group], y, '|', color=color[0], alpha=0.8,  ms=ms, mew=mew)
    
    y = np.ones( len(dispersed[group]) )*10
    ax[3,group].plot(dispersed[group], y, '|', color=color[1], alpha=0.8,  ms=ms, mew=mew)
    '''
#print 'clustered:', NSC, 'dispersed:', NSD   

#f1.savefig('../../../Desktop/'+repickled_name+'.png') 
       
#plt.show()
        
    
        
        
    
    
