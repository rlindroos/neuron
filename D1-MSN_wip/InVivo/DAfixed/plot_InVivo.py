import sys
sys.path.append('./')
import numpy                as np
import matplotlib.pyplot    as plt
import common_functions     as use
import glob
from joblib import Parallel, delayed
import multiprocessing
from    scipy           import stats




def create_result_structure_3d(N,Z):
    array = {}
    group_len   = [[20,17,17,4], [20,17,17,10]]
    for t,apt in enumerate(['clust', 'disp']):
        array[t] = {}
        for g in range(4):
            array[t][g] = {     'spike_ratio':np.zeros( (N,group_len[t][g],Z) ),     \
                                'spike_count':np.zeros( (N,group_len[t][g],Z) ),     \
                                'time2spike': np.zeros( (N,group_len[t][g],Z) ),     }
    
    return array
    
    
    
def fill_result_structure(DATA, array, N, mod_transformer):
    
    
    # set index
    index = {}
    for m in range(N):
        index[m] = {}
        for apt in [0,1]:
            index[m][apt]={}
            for g in range(4):
                index[m][apt][g] = {0:0, 50:0, 100:0}
    
    
    # loop over activation pattern in DATA
    for M in DATA: 
        
        # activation pattern type (apt). clustered or dispersed, 0 and 1 respectively
        spikes  =   M[0]        # spike times (array of len 20 filled with zeros at the end)
        apt     =   M[1] 
        group   =   M[2]        # distance group (0-4)
        model   =   M[3]
        ratio   =   M[4]
        c1      =   M[5]
        c2      =   M[6]
        t2s     =   M[7]
        mod     =   M[8]
        
        # save to array
        i = index[model][apt][group][mod]
        z = mod_transformer[mod]
        
        array[apt][group]['spike_ratio'][model,i,z] = ratio
        array[apt][group]['spike_count'][model,i,z] = c1
        array[apt][group]['time2spike' ][model,i,z] = t2s
        
        index[model][apt][group][mod] += 1   
    
    return array     
    




def standardize_data_structure(f):
    
    # load data
    data = use.load_obj(f)
    
    # extract features from file name
    distribution    =   f.split('_')[3]
    mod_percentage  =   int(f.split('mod')[1].split('.')[0])
    model_v         =   int(f.split('_V')[1].split('_')[0])
    
    if distribution == 'clustered':
        act_pat_type    =   0
        d               =   int(f.split('dist')[1].split('_')[0])
        dist_group      =   int(d/60)
        
    else:
        act_pat_type    =   1
        d               =   int(f.split('run')[1].split('_')[0])
        norm            =   d
        if norm < 20:
            dist_group   =   0
        elif norm < 37: 
            dist_group   =   1
        elif norm < 54: 
            dist_group   =   2
        else:
            dist_group   =   3
    
    if dist_group > 3:
        dist_group = 3
    
    # return data in form 
    #   [spikes, apt, group, model, ratio, c1, c2, t2s, modulation_percentage]
    spikes  =   data['spikes']
    ratio   =   data['values'][0]
    c1      =   data['values'][1]
    c2      =   data['values'][2]
    t2s     =   data['values'][3]
    
    return [spikes, act_pat_type, dist_group, model_v, ratio, c1, c2, t2s, mod_percentage]

 





def plot_spike_raster(DATA, axes, color, N):
    
    count = {}
    for model in range(N):
        count[model] = {}
        for group in range(4):
            count[model][group] = 0
            axes[model,group].set_xlim([-600,600])
    
    for M in DATA:
        
        spikes  = M[0]
        apt     = M[1]
        group   = M[2]
        model   = M[3]
        
        for d1 in range(spikes.shape[0]):
                
            #if len(spikes[d1,d2,:]) > 0:
            train   =   spikes[d1,:]
            train   =   train[np.nonzero(train)]
            y       =   np.ones((len(train)))*count[model][group]
            axes[model,group].plot(train, y, '.', c=color[apt])
            
            count[model][group] += 1




def plot_spike_histogram(DATA, axes, color, N):
    
    count = {}
    for apt in range(2):
        count[apt] = {}
        for model in range(N):
            count[apt][model] = {}
            for z in [0, 50, 100]:
                count[apt][model][z] = {'list':np.zeros((300000)), 'index':0}
    
    for M in DATA:
        
        spikes  = M[0]
        apt     = M[1]
        group   = M[2]
        model   = M[3]
        z       = M[8]
        
        
        for d1 in range(spikes.shape[0]):
                
            #if len(spikes[d1,d2,:]) > 0:
            train   =   spikes[d1,:]
            train   =   train[np.nonzero(train)]
            
            index   =   count[apt][model][z]['index']
            
            count[apt][model][z]['list'][index:index+len(train)] = train
            count[apt][model][z]['index']                       += len(train)
    
    lw = [4,2]
    ls = ['-', '--']       
    
    for model in range(N):
        axes[0,model].set_title('model v. '+str(model+1), fontsize=20 )
        for apt in range(2):
            for i,z in enumerate([0, 50, 100]):
                axes[i,0].set_ylabel('mod '+str(z), fontsize=16)
                
                axes[i,model].set_xlim([-500,500])
                #axes[i,model].set_ylim([0,500])
                #axes[i,model].set_yticks([])
                
                train   =   count[apt][model][z]['list']
                train   =   train[np.nonzero(train)]
                
                axes[i,model].hist(train, bins=100, range=(-500,500), color=color[apt], histtype='step', linewidth=lw[apt], linestyle=ls[apt])
                  
                
                
                
def plot_features_averages(array,axes,color,N):

    for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
        for d in range(3):
            add = 0
            for g in range(4):
                for apt in range(2):
                
                    a   = array[apt][g][feature][:,:,d]  
                    
                    # calc mean and std---------------------------------------------------
                    # -global
                    mean= np.nanmean( a )
                    std = np.nanstd( a, axis=None )    # stats.sem
                    s   = mean - std
                    e   = mean + std
                    x   = (apt+1+g+add)*10
                    w   = 2
                    
                    # loop over model versions and calc mean for each model separately
                    #a   = array[apt][g][feature][:,:,d]
                    if i == 2:
                        mv_mean = []
                        
                        for v in range(N):
                            b = array[apt][g][feature][v,:,d]
                            mv_mean.append( np.nanmean( b[~np.isnan(b)] ) )
                    else:
                        mv_mean = np.nanmean( a, axis=1 )
                        
                    
                    # plotting -----------------------------------------------------------
                    
                    # fill area between mean +/- std
                    axes[i][d].fill_between([x-w,x+w], [s,s], [e,e], alpha=1, color=color[apt])
                    
                    # mean value
                    axes[i][d].plot([x-2*w,x+2*w], [mean,mean], color=color[apt], lw=4  )
                    
                    X       = np.ones( (1,len(mv_mean)) ) * x
                    
                    # connect dots between clust/disp
                    if apt == 0:
                        mvm_old = mv_mean
                        X_old   = X
                    else:
                        # create lines
                        m_zip = zip(mvm_old,mv_mean)
                        X_zip = zip(X_old[0],X[0])
                        
                        for z in range(len(m_zip)):
                            axes[i][d].plot(X_zip[z], m_zip[z], 'k', alpha=1)
                            
                        axes[i][d].plot(X_old[0], mvm_old, 'o', ms=10, color='grey', alpha=1, mew=1, mec='k')
                        axes[i][d].plot(X[0],     mv_mean, 'o', ms=10, color='grey', alpha=1, mew=1, mec='k')
                    
                add += 2        
        

def plot_features_inner(a, apt, g, i, d, add, axes, color, w=2, index=[]):
    
    if len(index) > 0:
        b = []
        for v in range(a.shape[0]):
            b = np.concatenate([b,a[v,:index[v]]]) 
    else:
        b = a
    
    # calc mean and std---------------------------------------------------
    mean= np.nanmean( b )
    std = np.nanstd ( b, axis=None )    # stats.sem
    s   = mean - std
    e   = mean + std
    x   = (apt+1+g+add)*10
    
    
    # over model version
    mv_mean = []
    for v in range(a.shape[0]):
        if len(index) > 0:
            c = a[v,:index[v]]
        else:
            c = a[v,:]
        array = c[np.nonzero(c)]
        if i < 2 and len(array) == 0:
            mv_mean.append( 0 )
        else:
            mv_mean.append( np.nanmean( c[np.nonzero(c)] ) )
    
    # plotting -----------------------------------------------------------
    
    # fill area between mean +/- std
    axes[i][d].fill_between([x-w,x+w], [s,s], [e,e], alpha=1, color=color[apt])
    
    # mean value
    axes[i][d].plot([x-2*w,x+2*w], [mean,mean], color=color[apt], lw=4  )
    
    return mv_mean, x, b


def plot_feature_dots(mean,m_old,X,X_old, i, d, axes, color):
    
    m_zip   = zip(m_old,mean)
    X_zip   = zip(X_old,X)
    
    for z in range(len(m_zip)):
        axes[i][d].plot(X_zip[z], m_zip[z], 'k', alpha=1)
        
    axes[i][d].plot(X_old, m_old, 'o', ms=10, color='grey', alpha=1, mew=1, mec='k')
    axes[i][d].plot(X,     mean,  'o', ms=10, color='grey', alpha=1, mew=1, mec='k')    
    
    

def plot_features_pooled(array,axes,color,M):
    
    FEATURES    =   ['spike_ratio', 'spike_count', 'time2spike']    
    pooled      =   {}
    all_in_one  =   {}
    
    
    for i,feature in enumerate(FEATURES):
        pooled[feature]         = {}
        all_in_one[feature]     = {}
        
        add = 0
        
        for g in range(4):
            for d in range(3):
                for apt in range(2):
                     
                    if apt not in pooled[feature]:
                        # sum all activation patterns (could use any feature, here 0).
                        N = array[apt][0][FEATURES[0]].shape[1] +   \
                            array[apt][1][FEATURES[0]].shape[1] +   \
                            array[apt][2][FEATURES[0]].shape[1] +   \
                            array[apt][3][FEATURES[0]].shape[1]
                        
                        pooled[feature][apt] = {'values':np.zeros((M,30000,3)), 'index':np.zeros((3,M),dtype=int), 'n':np.zeros((3),dtype=int) }
                
                    
                    a           = array[apt][g][feature][:,:,d]  
                    mean, x     = plot_features_inner(a, apt, g, i, d, add, axes, color)[0:2]
                    
                    
                    # plot and connect mean values of individual models between clust/disp
                    if apt == 0:
                        m_old   = mean
                        X_old   = np.ones( (len(mean)) ) * x
                    else:
                        # create lines
                        X       = np.ones( (len(mean)) ) * x
                        
                        plot_feature_dots(  mean, m_old,    \
                                            X,    X_old,    \
                                            i, d,           \
                                            axes, color     )
                        
                    
                     
                    # pool values
                    # loop over model versions and add to array
                    for v in range(M):
                        
                        # setup: index, model version and values
                        #n       = pooled[feature][apt]['n'][d]
                        index   = pooled[feature][apt]['index'][d,v]
                        b       = array[apt][g][feature][v,:,d]
                        b       = b[~np.isnan(b)]
                        
                        # add to array
                        pooled[feature][apt]['values'][v,index:index+len(b),d] = b
                        
                        # update index and model version
                        pooled[feature][apt]['n'][d]        += 1
                        pooled[feature][apt]['index'][d,v]  += len(b)
                    
            add += 2
    
        for d in range(3):
            all_in_one[feature][d]  = [[],[]]
            for apt in range(2):
                
                index   =   pooled[feature][apt]['index'][d,:]
                a       =   pooled[feature][apt]['values'][:,:,d]
                
                mean, x, b  = plot_features_inner(a, apt, g, i, d, add+2+apt, axes, color, w=5, index=index)
                
                # plot and connect mean values of individual models between clust/disp
                if apt == 0:
                    m_old   = mean
                    X_old   = np.ones( (len(mean)) ) * x
                else:
                    # create lines
                    X       = np.ones( (len(mean)) ) * x
                    
                    plot_feature_dots(  mean, m_old,    \
                                        X,    X_old,    \
                                        i, d,           \
                                        axes, color     )
                
                all_in_one[feature][d][apt].append(b)
    
    return all_in_one
        
                
                
    
                
                
                

        
    
def calc_significance(array,axes,placeholders,hight,pooled=False):
    
    for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
        for d in range(3):
            
            if pooled:
                 
                # calc statistical significance
                t2, p2 = stats.ttest_ind(array[feature][d][0],array[feature][d][1], equal_var=False, axis=None)
                
                print feature, '\n\t', d, t2,p2
                
                if p2 < 0.01:
                    axes[i][d].plot(placeholders[-1], hight[i], '*', c='k', ms=20)
                elif p2 < 0.05:
                    axes[i][d].plot(placeholders[-1], hight[i], 'd', c='k', ms=20)
            
            else:
                    
                for g in range(4):
                    for apt in range(2):
                        
                        b   = array[apt][g][feature][:,:,d]
                        
                        # flatten
                        b   = b.flatten()
                        
                        # remove nan
                        b   = b[~np.isnan(b)]
                        
                        if apt == 0:
                            # save array
                            a = b
                        else:
                            # calc statistical significance
                            t2, p2 = stats.ttest_ind(a,b, equal_var=False)
                            
                            if p2 < 0.01:
                                axes[i][d].plot(placeholders[g], hight[i], '*', c='k', ms=20)
                            elif p2 < 0.05:
                                axes[i][d].plot(placeholders[g], hight[i], 'd', c='k', ms=20)
                
                       
            
                
                


ISI             =       '1ms'
V               =       'V[0-5]'
path            =       'Results/InVivo_sim_res/Oct2018/DAfixed/'
N               =       6


repickled_name  =       'sorted_'+ISI


color           =       ['r', 'b']
alpha           =       [0.2, 0.2]
ls              =       ['-', '-']
mew             =       1
ms              =       20

cell_type       =       ['D1', 'D2']
distribution    =       ['clustered', 'dispersed'] #['dispersed'] #

time            =       np.arange(-500,500.001,25e-3)
max_time        =       200         # ms, following activation pattern
sort_index      =       range(10) #[3,1,8,10,5,6,9,7,2,4]




files       =   glob.glob(path+'/InVivo*'+ISI+'_*_D1_'+V+'*')   
num_cores   =   multiprocessing.cpu_count() 
RES         =   Parallel(n_jobs=num_cores)(delayed(standardize_data_structure)( f ) for f in files)
#use.save_obj(RES, path+'intermediate_toBeRemoved' )

array = create_result_structure_3d(N,3)
array = fill_result_structure(RES, array, N, {0:0, 50:1, 100:2})
use.save_obj(array, path+repickled_name )




# plot result
'''
fig, ax         =       plt.subplots(N,4, figsize=(20,10))  #, gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)
plot_spike_raster(RES, ax, color, N)
fig.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAfixed_spikeraster.png')
'''
fig2, ax2         =       plt.subplots(3,N, figsize=(20,10))  #, gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)
plot_spike_histogram(RES, ax2, color, N)
fig2.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAfixed_spikehist.png')


f1,a1 = plt.subplots(3,1, figsize=(10, 12))
f2,a2 = plt.subplots(3,1, figsize=(10, 12))
f3,a3 = plt.subplots(3,1, figsize=(10, 12))

axes  = [a1,a2,a3]
color = ['r', 'b']

placeholders = [15,45,75,105,150]
hight        = [1.2, 5.5, 350]

# plot features
all_in_one = plot_features_pooled(array,axes,color,N)

# calc statistical significance
calc_significance(array,axes,placeholders,hight)
calc_significance(all_in_one,axes,placeholders,hight,pooled=True)

# format plot --------------------------------------------------------
for a in axes:
    for d in range(3):
        a[d].set_xlim([0, 170])
        
        a[d].set_xticks([])
        a[d].yaxis.set_ticks_position('left')

        a[d].spines['left'].set_linewidth(4)
        a[d].spines['right'].set_visible(False)
        a[d].spines['top'].set_visible(False)
        a[d].spines['bottom'].set_visible(False)

# set ranges
for d in range(3):
    a1[d].set_ylim([-0.2, 1.3])
    a1[d].set_yticks( [0,0.2,0.4,0.6,0.8,1.0] )
    a1[d].set_yticklabels([0,0.2,0.4,0.6,0.8,1.0], fontsize=20)
for d in range(3):
    a2[d].set_ylim([-0.5, 6])
    a2[d].set_yticks( range(0,6) )
    a2[d].set_yticklabels(range(0,6), fontsize=20)
for d in range(3):
    a3[d].set_ylim([-100, 400])
    a3[d].set_yticks( range(0,441,200) )
    a3[d].set_yticklabels(range(0,441,200), fontsize=20)

# set ylabel
for a in axes:
    a[0].set_ylabel('0% DA mod', fontsize=24)
    a[1].set_ylabel('50% DA mod', fontsize=24)
    a[2].set_ylabel('100% DA mod', fontsize=24)

# set xlabel
for i,a in enumerate(axes):
    for d in range(3):
        a[d].set_xticks( placeholders )
        a[d].set_xticklabels(['proximal', 'medial', 'distal', 'terminal', 'pooled'], fontsize=20)

# set title
a1[0].set_title('Spike probability',        fontsize=30)
a2[0].set_title('Spike count',              fontsize=30)
a3[0].set_title('Spike latency variance',   fontsize=30)
    
        
f1.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAfixed_spike_ratio.png')
f2.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAfixed_spike_count.png')
f3.savefig('../../../Dropbox/Manuscripts/Figures_2018/DAfixed_ramping_time2spike.png')

plt.show()




 

        
    
        
        
    
    
