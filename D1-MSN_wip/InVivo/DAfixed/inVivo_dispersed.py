
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('./Mech/x86_64/special')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        


    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    DAmod       =   True
    
    if DAmod:
        modulate_synapse    =   True
        modulate_gaba       =   True
        modulate_axon       =   False
    else:
        modulate_synapse    =   False
        modulate_gaba       =   False
        modulate_axon       =   False
        
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    64
    
    print(cell_type, morphology, par, 'DAmod', DAmod)
    
    model_version = int(np.floor(id/N))
    norm          = id - model_version*N
    model_version = model_version + int(h.start_value)
    
    # load parameters and random set
    path_to_lib     = './Libraries/'
    parameters      =   use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables'] # parameter set 0 (cell index)
    randActPat      =   use.load_obj(path_to_lib+'randomActivationPattern.pkl')[cell_type]   # preselected indexes to use with lists of segments (see create_segment_list below)
    
    ISI         =   1
    tstop       =   1500
    cvode       =   h.CVode()
    
    # group and pattern
    group, p    =   use.get_group_and_pattern_index(    norm,               \
                                                        randActPat          )
    pattern     =   randActPat[group][p]
    
    
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    modulation_factors = use.get_fixed_modulation_factors(  cell_type,                          \
                                                            mod_list+['ampa','namda','gaba'],   \
                                                            h.modulation_percentage             )
    
        
    print('-cell_type:', cell_type, '-group:', group, '-pattern:', p)
    
    
    if modulate_synapse:
        syn_fact = modulation_factors[-3:-1]
    else:
        syn_fact = False
    
    if modulate_gaba:
        gabaMod = modulation_factors[-1]
    else:
        gabaMod = False


    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
   
    # create segment lists
    dist_groups = range(4)
    segments    = use.create_segment_list(cell, dist_groups)
    
    # set stimuli
    activationPattern   = [ segments[group][s] for s in pattern ]
    
    # set cascade 
    #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
    
    
    # modulation of channels
    if DAmod:
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )
                     
    # set background noise
    Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                            syn_fact=syn_fact,              \
                                            gabaMod=gabaMod                 )
    
    # (re)set Vectors for recording of currents
    cur,rand,ncon,stim  = use.set_dispersed_stimuli(cell,                       \
                                                    activationPattern,          \
                                                    ISI=ISI,                    \
                                                    syn_fact=syn_fact,          \
                                                    gabaMod=gabaMod             )
    
    # spike array
    SPIKES      = np.zeros((10,20))
        
    for i in range(10):
        
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        
        # run simulation
        h.finitialize(-70)
        
        # run simulation
        while h.t < tstop:
        
            h.fadvance()
                
        # remove first 500 ms and shift time so that start of stimuli is 0
        cutoff_index = 20000
        t   = [x[1]-1000 for x in enumerate( tm)        if x[0] >= cutoff_index]
        v   = [x[1]      for x in enumerate( vm)        if x[0] >= cutoff_index] 
        
        spikes                  = use.getSpikedata_x_y( t, v )[:20]
        SPIKES[i,:len(spikes)]  = spikes
    
    
    ratio, c1, c2, t2s = use.analyse_spike_data(SPIKES, max_time=200, t2s_cutoff=2)
    
    VAL  = {'values':[  ratio, c1, c2, t2s  ], \
            'spikes': SPIKES, \
            'par':   {  'DAmod':DAmod,                          \
                        'modulate_synapse':modulate_synapse,    \
                        'modulate_gaba':modulate_gaba,          \
                        'modulate_axon':modulate_axon,          \
                        'synaptic_strength':int(h.synaptic_strength)  }    }  
    
    use.save_obj(VAL, 'InVivoDAfixed'+str(ISI)+'ms_dispersed_'+cell_type+'_V'+str(model_version)+'_run'+str(norm)+'_mod'+str(int(h.modulation_percentage)) ) 
    
    
    
        
    
    
