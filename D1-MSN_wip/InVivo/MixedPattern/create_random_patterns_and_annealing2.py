import sys
sys.path.append('./')
import numpy as np
import common_functions as use


M               =   58 # sections in compartment
N               =   32
n               =   N
steps           =   []
res             =   {'patterns':{}, 'donors':{}, 'acceptors':{}, 'steps':steps}




def create_resticted_section_list(min_dist, max_dist):
    
    # import pattern library
    dist_map = use.load_obj('Libraries/map_sec_dist_to_soma.pkl')
    
    # create list of sections within range
    within_range_list = []
    for s,sec in enumerate(dist_map):
        if dist_map[sec] < min_dist or dist_map[sec] > max_dist: continue
        within_range_list.append( sec )
    
    return np.random.permutation( within_range_list )
    


    
    
def draw_random_patterns(within_range_list, number_of_patterns, M, N, res, steps=[]):


    # draw random section numbers (the d1 morph have 58 dendritic sections)
    for i in range(number_of_patterns):
        
        if N <= M:
            # draw N-1 random sections from within_range_list
            number  = N - 1
            pattern = np.random.choice( within_range_list, number, replace=False )
            
            # check if end section in pattern and if so redraw until it is not.
            count = 0
            while within_range_list[i%len(within_range_list)] in pattern:
                pattern = np.random.choice( within_range_list, number, replace=False )
                print 'redraw number:', count
                count +=1
            
            #print
            #print pattern
            #print i%len(within_range_list), i, len(within_range_list), within_range_list[i%len(within_range_list)]
            res['patterns' ][i] = np.concatenate((pattern, [ within_range_list[i%len(within_range_list)] ])) 
            #print res['patterns' ][i]
            #print '---'
            
        
        else:
            # will only work if N < 2*M
            # diff number of random sections are activated twice 
            # (extra spike are added at the end of the train)
            diff    = N - M
            number  = M - 1
            res['patterns'][i] = np.concatenate((   np.random.permutation( within_range_list ),          \
                                                    np.random.choice( range(M), diff, replace=False )   \
                                               )) + [ within_range_list[i%len(within_range_list)] ]
            
        
        res['donors'   ][i] = list(np.random.permutation( range(number) ))
        if len(steps) == 0:
            res['acceptors'][i] = [ number ] * (number)
        else:
            agregated = 0
            res['acceptors'][i] = []
            for s in steps:
                agregated = agregated + s 
                acceptors = list(res['donors'   ][i][agregated:]) + [number]
                res['acceptors'][i] = res['acceptors'][i] + list(np.random.permutation(acceptors))
        
        print res['patterns'][i]    
        print res['donors'   ][i]
        print res['acceptors'][i]
        print
    
    return res




# patterns to merge each iteration, eg [8, 4, 4, 2, 1] (used for 20 synapses)
while n > 1:
    n = n/2
    steps.append(n)

if N > M:
    # compensate so first run merges only M - (N/2) synapses (leaving 32 if 64 used)
    steps[0] = M-steps[0]


within_range_list   = create_resticted_section_list(50, 180)
M                   = len(within_range_list)
print M
res                 = draw_random_patterns(within_range_list, 100, M, N, res, steps=steps)
    
res['map'] = np.arange( 100 )
use.save_obj(res, 'Libraries/mixActPatRestricted'+str(N) )
'''
for i in range(100):
    print '-----------'
    print i, i%len(within_range_list), within_range_list[ i%len(within_range_list) ]
    print within_range_list
    for key in res:
        if key in ['map', 'steps']: continue
        print i, key, res[key][i]'''
    
    
        
        
