import sys
sys.path.append('./')
import numpy                as np
import matplotlib.pyplot    as plt
import common_functions     as use
import glob
from joblib import Parallel, delayed
import multiprocessing
from    scipy           import stats





def transform_to_pandas(array, file_name, D=5, mode='a', pattern_len=16):
    
    # loop over all entries in the result array, extract fetures and writes to csv file 
    # this file can then be read into a pandas frame from a graphical tool for further instection
    
    columns = [ 'Model',
                'DA',
                'dt',
                'pattern',
                'sub_pattern',
                'pattern_len',
                'ratio',
                'count',
                'latency',
                'broken?']
    
    file = open(file_name, mode)
    
    if mode == 'w':
        file.write(",".join(columns))
    
    # create master array with all parameters
    m   =   len(array)*D
    n   =   len(columns)
    A   =   np.zeros((m,n))
    
    # loop array & pattern
    counter = 0
    for entry in array:
        model_version   = entry['V']
        dopamine_mod    = entry['mod']
        pattern         = entry['P']
        for d in range(D):
            
            # fill array
            line          = [   model_version,
                                dopamine_mod,
                                0,
                                pattern,
                                d,
                                pattern_len,
                                entry[d][1],
                                entry[d][2],
                                entry[d][3],
                                entry[d][4]     ]
            
            file.write('\n')
            file.write(",".join([str(l) for l in line]))
    
    file.close()
            
                                
    


def create_result_structure_3d(P=40, D=5, V=3):
    
    # 40 patterns (P)
    # 5  clust degrees (D)
    # 3  DA levels (da)
    # 3  model versions (V)
    
    array = {}
    
    for da in [0, 50, 100]:
        array[da] = {   'spike_ratio':np.zeros( (V,D,P) ),     \
                        'spike_count':np.zeros( (V,D,P) ),     \
                        'time2spike': np.zeros( (V,D,P) )       }
        
        for d in range(D):
            array[da][d] = {}
            for v in range(V):
                array[da][d][v] = 0
                
    return array
    

    
    
def fill_result_structure(DATA, array, D=5):
    
    # loop over activation pattern in DATA
    for M in DATA: 
        
        model   =   M['V']
        da      =   M['mod']
        
        
        for i in range(D):
            ratio   =   M[i][1]
            c1      =   M[i][2]
            t2s     =   M[i][3]
            
            z       =   array[da][i][model]
            
        
            # save to array
            array[da]['spike_ratio'][model,i,z] = ratio
            array[da]['spike_count'][model,i,z] = c1
            array[da]['time2spike' ][model,i,z] = t2s
            
            array[da][i][model] += 1
    
    return array     
    




def standardize_data_structure(f, D=5):
    
    # load data
    data = use.load_obj(f)
    
    # extract features from file name
    mod_percentage  =   int(f.split('mod')[1].split('.')[0])
    model_v         =   int(f.split('_V')[1].split('_')[0])
    pattern         =   int(f.split('pattern')[1].split('_')[0])
        
    M               =   {'mod':mod_percentage, 'V':model_v, 'P':pattern}
    
    for key in range(D):
        M[key] = [data[key]['spikes'], data[key]['values'][0], data[key]['values'][1], data[key]['values'][3] ]
    
    return M





def check_sliding_average(trace):
    
    N = len(trace)
    n = N/40
    
    x = []
    y = []
    
    over = {'x':[],'y':[]}
    
    #print len(trace), n
    for s in range(N-n,0,-n):
        e = s+n
        y.append(np.mean(trace[s:e]))
        x.append(s)
        if np.mean(trace[s:e]) > -40:
               
            over['x'].append(s)
            over['y'].append(np.mean(trace[s:e]))
            
            if len(over['x']) > 4:
                return True
                
    return False
        


def get_all_data(f, time, D=5):
    
    # extention of "standardize_data_structure" that also checks if file is "broken"
    
    
    data    =   use.load_obj(f)
    
    V       =   int(f.split('_V')[ 1].split('_')[0])
    da      =   int(f.split('mod')[1].split('.')[0])
    P       =   int(f.split('pattern')[1].split('_')[0])
    
    M       =   {'mod':da, 'V':V, 'P':P}
    
    for key in range(D):
        
        flag    =   0
        
        for bg in range(10):    
            if check_sliding_average(data[key]['vm'][bg][len(time)/2:]):
                flag = 1   
                break
        
        M[key] = [data[key]['spikes'], data[key]['values'][0], data[key]['values'][1], data[key]['values'][3], flag, data[key]['values'][4] ]
            
            
    return M
    
    


def check_if_broken(f, time, D=5):
    
    # checks if a voltage trace is "broken" (i.e. if it stop spiking and stay depolarized)
    
    data    =   use.load_obj(f)
    
    A       =   {}
    flag    =   False
    array   =   np.zeros((D,10))
    
    for pattern in range(D):
        for bg in range(10):    
            #if data[pattern]['vm'][bg][-1] > -40:
            #    flag = True   
            #    array[pattern,bg] = 1
            if check_sliding_average(data[pattern]['vm'][bg][len(time)/2:]):
                flag = True   
                array[pattern,bg] = 1
        
    if flag:
        V  = int(f.split('_V')[ 1].split('_')[0])
        da = int(f.split('mod')[1].split('.')[0])
        P  = int(f.split('pattern')[1].split('_')[0])
        if V not in A:
            A[V] = {}
        if da not in A[V]:
            A[V][da] = {} 
        A[V][da][P] = {'array':array, 'file':f}
    
    return A
        
    
def plot_broken(array):
    
    broken  =   0
    model   =   []
    da      =   []
    
    
    for a in array: 
        if len(a) > 0:
            
            print a
        
            m = a.keys()[0]
            model.append(m)
            
            d = a[m].keys()[0]
            da.append(d)
    
    
    plt.hist(model, 6)
    plt.xlim([0,5])
    plt.figure()
    plt.hist(da, 3, range=(0,100))
    plt.xlim([0,100])
    plt.show()
            
            
    
    



def get_vm(f, time, D=5):
    
    # load data
    data = use.load_obj(f)
    
    # extract features from file name
    mod_percentage  =   int(f.split('mod')[1].split('.')[0])
    model_v         =   int(f.split('_V')[ 1].split('_')[0])
    M               =   {'mod':mod_percentage, 'V':model_v}
    #array           =   np.zeros( (10,len(time)) )
    
    for pattern in range(D):
        M[pattern]  =   {}
        for bg in range(10):
            trace = data[pattern]['vm'][bg]
            M[pattern][bg] = trace          # array[bg,:]
        #M[pattern] = array
    
    del data
    
    return M



def plot_vm(M, time, axes, color, D=5):
    
    DA      =   [0, 50, 100]
    
    f,a = plt.subplots(1,3)
    
    for m in M: 
        if m['V'] in [2]: 
            continue
        
        mod         =   int(DA.index(int(m['mod'])))
            
        for pattern in range(D):
            for bg in range(10):
                axes[mod,0].plot( time, m[pattern][bg], color=color[pattern] )
                a[mod].plot( time, m[pattern][bg], color=color[pattern] )
            
 





def plot_spike_raster(DATA, axes, color, N):
    
    count = {}
    for model in range(N):
        count[model] = {}
        for group in range(4):
            count[model][group] = 0
            axes[model,group].set_xlim([-600,600])
    
    for M in DATA:
        
        spikes  = M[0]
        apt     = M[1]
        group   = M[2]
        model   = M[3]
        
        for d1 in range(spikes.shape[0]):
                
            #if len(spikes[d1,d2,:]) > 0:
            train   =   spikes[d1,:]
            train   =   train[np.nonzero(train)]
            y       =   np.ones((len(train)))*count[model][group]
            axes[model,group].plot(train, y, '.', c=color[apt])
            
            count[model][group] += 1




def plot_spike_histogram(DATA, axes, color, P, xlabels, pattern_length):
    
    # create result structure ---------------------------------------------------------
    count = {}
    for pattern in range(P):
        count[pattern] = {}
        for z in [0, 50, 100]:
            count[pattern][z] = {'list':np.zeros((300000)), 'index':0}
    
    
    # fill structure ---------------------------------------------------------
    for M in DATA:
        z   =   M['mod']
        for pattern in range(P):
        
            spikes  = M[pattern][0]
        
            for d1 in range(spikes.shape[0]):
                    
                #if len(spikes[d1,d2,:]) > 0:
                train   =   spikes[d1,:]
                train   =   train[np.nonzero(train)]
                index   =   count[pattern][z]['index']
                
                count[pattern][z]['list'][index:index+len(train)] = train
                count[pattern][z]['index']                       += len(train)
    
          
    # plot results ---------------------------------------------------------
    for i,z in enumerate([0, 50, 100]):
        
        axes[i].set_title('DA modulation '+str(z)+'%', fontsize=30)
        axes[i].axis([-500,500, 0,3000])
        axes[i].set_xticks([-500,-250,0,250,500])
        axes[i].set_xticklabels([-500,-250,0,250,500], fontsize=24)
        #axes[i,pattern].set_ylim([0,500])
        #axes[i,pattern].set_yticks([])
        
        for pattern in range(P):   
            
            train   =   count[pattern][z]['list']
            train   =   train[np.nonzero(train)]
            axes[i].hist(train, bins=100, range=(-500,500), color=color[pattern], histtype='step', linewidth=range(P,0,-1)[pattern], label=xlabels[pattern])
            
    axes[0].legend(loc=2, fontsize=20)
                  
                
                
                
def plot_features_averages(array,axes, pattern_length, color):
    
    #N       = 30
    #color   = [(1-1.0*x/(N-1), 0, 1.0*x/(N-1)) for x in range(N)]
    #marker  = ['o', 'v']
    
    mixActPat            = use.load_obj('Libraries/mixActPat'+pattern_length+'.pkl')
    somatic_distance_map = use.load_obj('Libraries/map_sec_dist_to_soma.pkl')
    
    for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
        for d,D in enumerate([0,50,100]):
            dimensions = array[D][feature].shape
            X          = []
            for g in range(dimensions[1]):
                     
                a   = array[D][feature][:,g,:]
                
                # delete model v. 2 since too excitable (and breaks under high stimuli) 
                a   = np.delete(a, 2, axis=0) 
                
                # calc mean and std---------------------------------------------------
                # -global
                mean= np.nanmean( a )
                std = np.nanstd( a, axis=None )    # stats.sem
                s   = mean - std
                e   = mean + std
                x   = (1+g*2)*10
                w   = 3
                
                
                # plotting -----------------------------------------------------------
                
                # fill area between mean +/- std
                axes[i][d].fill_between([x-w,x+w], [s,s], [e,e], alpha=1, color='m')
                
                # mean value
                axes[i][d].plot([x-2*w,x+2*w], [mean,mean], color='m', lw=4  )
                
                X.append(x)  
            
            # plot patterns individally ---------------------------------------------
             
            # loop over model version
            for v in range(dimensions[0]):
                if v == 2: continue
                # loop over pattern
                for p in range(dimensions[2]):
                    
                    # plot pattern
                    axes[i][d].plot(X, array[D][feature][v,:,p], 'o', color=color[v], ms=10, alpha=0.5)
                    axes[i][d].plot(X, array[D][feature][v,:,p], '-', color='grey',          alpha=0.2)
                    
                    # get last sec of pattern and map to somatic distance
                    map_pattern = mixActPat['map']
                    section     = mixActPat['patterns'][map_pattern[p]][-1]
                    
                    # get distance from map
                    distance    = somatic_distance_map[str(section)]
                    
                    # plot distance
                    x = [ X[-1]+10,    X[-1] + 10 + np.round(distance/10)   ]
                    y = [array[D][feature][v,-1,p], array[D][feature][v,-1,p]]
                    axes[i][d].plot(x,y, color=color[v], lw=4, alpha=0.3)
                    
            
            if D == 100:        
                axes[i][d].plot([x[0],x[0]+10], [0,0], 'k', lw=5)
                    
        

              
                

        
    
def calc_significance(array,axes,placeholders,hight,pooled=False):
    
    for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
        for d in range(3):
            
            if pooled:
                 
                # calc statistical significance
                t2, p2 = stats.ttest_ind(array[feature][d][0],array[feature][d][1], equal_var=False, axis=None)
                
                print feature, '\n\t', d, t2,p2
                
                if p2 < 0.01:
                    axes[i][d].plot(placeholders[-1], hight[i], '*', c='k', ms=20)
                elif p2 < 0.05:
                    axes[i][d].plot(placeholders[-1], hight[i], 'd', c='k', ms=20)
            
            else:
                    
                for g in range(4):
                    for apt in range(2):
                        
                        b   = array[apt][g][feature][:,:,d]
                        
                        # flatten
                        b   = b.flatten()
                        
                        # remove nan
                        b   = b[~np.isnan(b)]
                        
                        if apt == 0:
                            # save array
                            a = b
                        else:
                            # calc statistical significance
                            t2, p2 = stats.ttest_ind(a,b, equal_var=False)
                            
                            if p2 < 0.01:
                                axes[i][d].plot(placeholders[g], hight[i], '*', c='k', ms=20)
                            elif p2 < 0.05:
                                axes[i][d].plot(placeholders[g], hight[i], 'd', c='k', ms=20)
                
                       
            
                
                


ISI             =       '1ms'
V               =       'V[0-5]'
path            =       'Results/InVivo_sim_res/Oct2018/MixedPattern/Restricted/'
tag             =       'Restricted'
N               =       3

L               =       {'16':5, '32':6, '64':7}
'''
if L[pattern_length] <= 5:
    xlabels = ['dispersed', '8x2', '4x4', '2x8', 'clustered']
elif L[pattern_length] == 6:
    xlabels = ['32x1', '16x2', '8x4', '4x8', '2x16', '1x32']
elif L[pattern_length] == 7:
    xlabels = ['64x1', '32x2', '16x4', '8x8', '4x16', '2x32', '1x64']'''

repickled_name  =       'sorted_'+ISI

multi_color     =       ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#e6ab02','#a6761d','#666666']
color           =       ['r', 'b']
alpha           =       [0.2, 0.2]
ls              =       ['-', '-']
mew             =       1
ms              =       20

cell_type       =       ['D1', 'D2']
distribution    =       ['clustered', 'dispersed'] #['dispersed'] #

time            =       np.arange(-500,500.001,50e-3)
max_time        =       200         # ms, following activation pattern
sort_index      =       range(10) #[3,1,8,10,5,6,9,7,2,4]



for pattern_length  in ['16', '32']:
    try:
        RES = use.load_obj(path+tag+pattern_length+'_removethisifwhanttouseold_gmax1000.pkl')
        print '--loading repickled data' 
    except:   
        files       =   glob.glob(path+'InVivoDAfixed'+tag+'*'+ISI+'_mixed'+pattern_length+'_D1_'+V+'*') 
        num_cores   =   multiprocessing.cpu_count() 
        RES         =   Parallel(n_jobs=num_cores)(delayed(get_all_data)( f, time, D=L[pattern_length] ) for f in files)
        use.save_obj(RES, path+tag+pattern_length+'_gmax1000' )



RES16 = use.load_obj(path+tag+'16_gmax1000.pkl')
RES32 = use.load_obj(path+tag+'32_gmax1000.pkl')
#RES64 = use.load_obj(path+tag+'64_gmax1000.pkl')
transform_to_pandas(RES16, path+'array_large.csv', D=L['16'], mode='w', pattern_len=16)
transform_to_pandas(RES32, path+'array_large.csv', D=L['32'], mode='a', pattern_len=32)
#transform_to_pandas(RES64, path+'array_large.csv', D=L['64'], mode='a', pattern_len=64)





''' 
array = create_result_structure_3d(D=L[pattern_length], V=N)
array = fill_result_structure(RES, array, D=L[pattern_length])
use.save_obj(array, path+repickled_name )



# plot result

fig, ax         =       plt.subplots(3,6, figsize=(20,10))  #, gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)
plot_spike_raster(RES, ax, color, N)
fig.savefig('../../../Dropbox/Manuscripts/Figures_2018/Mixed_spikeraster.png')

fig2, ax2       =       plt.subplots(1,3, figsize=(40,10))  #, gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)
plot_spike_histogram(   RES,                \
                        ax2,                \
                        multi_color,        \
                        L[pattern_length],  \
                        xlabels,            \
                        pattern_length      )
fig2.savefig('../../../Dropbox/Manuscripts/Figures_2018/Mixed'+pattern_length+'_spikehist.png')

#fig3, ax3       =   plt.subplots(3,L[pattern_length], figsize=(40,20))  #, gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)
try:
    check       =   use.load_obj( path+'check'+pattern_length+'_gmax1000.pkl' )
except:
    files       =   glob.glob(path+'/InVivo*'+ISI+'_mixed'+pattern_length+'_D1_'+V+'*')
    num_cores   =   multiprocessing.cpu_count()
    check       =   Parallel(n_jobs=num_cores)(delayed(check_if_broken)( f, time, D=L[pattern_length] ) for f in files)
    use.save_obj(check, path+'check'+pattern_length+'_gmax1000' )
    gggh
    VM          =   Parallel(n_jobs=num_cores)(delayed(get_vm)( f, time, D=L[pattern_length] ) for f in files)
    print 'saving'
    use.save_obj(VM, path+'vm'+pattern_length+'_gmax1000' )

plot_broken(check)
#plot_vm(VM[:10], time, ax3, multi_color, D=L[pattern_length])
#fig3.savefig('../../../Dropbox/Manuscripts/Figures_2018/Mixed'+pattern_length+'_vm.png')

f1,a1 = plt.subplots(3,1, figsize=(10, 12))
f2,a2 = plt.subplots(3,1, figsize=(10, 12))
f3,a3 = plt.subplots(3,1, figsize=(10, 12))

axes  = [a1,a2,a3]
color = ['r', 'b']

placeholders = range(10, 11+(L[pattern_length]-1)*20, 20)
hight        = [1.2, 5.5, 350]



# plot features
#all_in_one = plot_features_pooled(array,axes,color,N)

plot_features_averages(array, axes, pattern_length, multi_color)



# calc statistical significance
#calc_significance(array,axes,placeholders,hight)
#calc_significance(all_in_one,axes,placeholders,hight,pooled=True)

# format plot --------------------------------------------------------

for a in axes:
    for d in range(3):
        a[d].set_xlim([0, L[pattern_length]*20+30])
        
        a[d].set_xticks([])
        a[d].yaxis.set_ticks_position('left')

        a[d].spines['left'].set_linewidth(4)
        a[d].spines['right'].set_visible(False)
        a[d].spines['top'].set_visible(False)
        a[d].spines['bottom'].set_visible(False)

# set ranges
for d in range(3):
    a1[d].set_ylim([-0.2, 1.3])
    a1[d].set_yticks( [0,0.2,0.4,0.6,0.8,1.0] )
    a1[d].set_yticklabels([0,0.2,0.4,0.6,0.8,1.0], fontsize=20)
for d in range(3):
    a2[d].set_ylim([-0.5, L[pattern_length]-1+int(L[pattern_length])%5*4])
    a2[d].set_yticks( range(0,5) )
    a2[d].set_yticklabels(range(0,5), fontsize=20)
for d in range(3):
    a3[d].set_ylim([-100, 550])
    a3[d].set_yticks( range(0,441,200) )
    a3[d].set_yticklabels(range(0,441,200), fontsize=20)

# set ylabel
for a in axes:
    a[0].set_ylabel('0% DA mod', fontsize=24)
    a[1].set_ylabel('50% DA mod', fontsize=24)
    a[2].set_ylabel('100% DA mod', fontsize=24)

# set xlabel
for i,a in enumerate(axes):
    for d in range(3):
        a[d].set_xticks( placeholders )
        a[d].set_xticklabels(xlabels, fontsize=20)

# set title
a1[0].set_title('Spike probability',        fontsize=30)
a2[0].set_title('Spike count',              fontsize=30)
a3[0].set_title('Spike latency variance',   fontsize=30)
    
        
f1.savefig('../../../Dropbox/Manuscripts/Figures_2018/Mixed'+pattern_length+'_1000_spike_ratio.png')
f2.savefig('../../../Dropbox/Manuscripts/Figures_2018/Mixed'+pattern_length+'_1000_spike_count.png')
f3.savefig('../../../Dropbox/Manuscripts/Figures_2018/Mixed'+pattern_length+'_1000_time2spike.png')
'''
#plt.show()




 

        
    
        
        
    
    
