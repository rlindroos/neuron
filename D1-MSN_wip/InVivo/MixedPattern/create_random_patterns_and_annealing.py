import sys
sys.path.append('./')
import numpy as np
import common_functions as use


  

restrict_set    =   True
N               =   16

M               =   58      # number of dendritic sections in morph

if restrict_set:
    
    # import pattern library
    dist_map = use.load_obj('Libraries/map_sec_dist_to_soma.pkl')

    # count sections within range and create map to these compartments
    M = 0
    within_range_map = []
    for s,sec in enumerate(dist_map):
        if dist_map[sec] < 50 or dist_map[sec] > 180: continue
        M += 1
        within_range_map.append( sec )


print M

n               =   N
steps           =   []

# patterns to merge each iteration, eg [8, 4, 4, 2, 1] (used for 20 synapses)
while n > 1:
    n = n/2
    steps.append(n)

if N > M:
    # compensate so first run merges only M - (N/2) synapses (leaving 32 if 64 used)
    steps[0] = M-steps[0]
        



res             =   {'patterns':{}, 'donors':{}, 'acceptors':{}, 'steps':steps}




# draw random section numbers (the d1 morph have 58 dendritic sections)
for i in range(100):
    if N > M:
        # will only work if N < 2*M
        # diff number of random sections are activated twice 
        # (extra spike are added at the end of the train)
        diff = N - M
        res['patterns'][i] = np.concatenate((   np.random.permutation( range(M) ),                  \
                                                np.random.choice( range(M), diff, replace=False )   \
                                           ))
    else:
        res['patterns'][i] = np.random.permutation( range(M) )[0:N]





# draw donors (order of annealing)
for i in range(100):
    if N <= M:
        res['donors'][i] = np.random.permutation( range(N) )
    else:
        res['donors'][i] = np.random.permutation( range(M) )
    





# draw acceptors (where to move the donor spikes)
last = np.zeros((100))
for i in range(100):
    
    if N <= M:
        acceptors   = np.arange(N)
    else:
        acceptors   = np.arange(M)
    T               = []
    c               = 0
    
    for s,step in enumerate(steps):
        
        # possible acceptors: acceptors - donors
        acceptors       = [ a for a in acceptors if a not in res['donors'][i][c:c+step] ]  #acceptors[ ~np.isin(acceptors,res['donors'][i][c:c+step]) ]
        
        # draw "step" number of targets (8, 4, 4, 2, 1). 
        #   if replace=True; one target can have multiple donors
        targets         = np.random.choice( acceptors, step, replace=False ) 
        
        T               = T+targets.tolist()
        c              += step
    
    res['acceptors'][i] = T
    
    last[i] = ( res['patterns'][i][res['donors'][i][-1]] )


if restrict_set:
    # map set to sections within range
    for i in range(100):
        for j in range(len(res['patterns'][i])):
            res['patterns'][i][j] = within_range_map[j]
        

if N > M:
    res['map'] = np.arange( N )
else:
    u,i         = np.unique(last,return_index=True)  
    not_unique  = [x[0] for x in enumerate(last) if x[0] not in i]
    res['map']  = np.sort(i).tolist()  + np.sort(not_unique).tolist()

if restrict_set:
    use.save_obj(res, 'Libraries/mixActPatRestricted'+str(N) )
else:
    use.save_obj(res, 'Libraries/mixActPat'+str(N) )
    
        
        
