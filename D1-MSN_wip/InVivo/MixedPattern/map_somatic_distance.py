
# script used for running in vivo simultion of clustered synaptic activation
# This script is based on plateaus_in_vivo_saveFeatures.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_clustered_D<>_section<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')



# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    DAmod       =   True
    
    ISI         =   1
    tstop       =   1500
    cvode       =   h.CVode()
    
    if DAmod:
        modulate_synapse    =   True
        modulate_gaba       =   True
        modulate_axon       =   False
    else:
        modulate_synapse    =   False
        modulate_gaba       =   False
        modulate_axon       =   False
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    50
    norm_val    =    0
    
    norm          = id - int(np.floor(id/N))*N
    model_version = int(np.floor(id/N)) + int(h.start_value)
    
    
    print(cell_type, morphology, par, 'DAmod', DAmod)
    
    
    # load random sets
    path_to_lib         = './Libraries/'
    random_variables    = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    mixActPat           = use.load_obj(path_to_lib+'mixActPat.pkl')
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    modulation_factors = use.get_fixed_modulation_factors(  cell_type,                          \
                                                            mod_list+['ampa','namda','gaba'],   \
                                                            h.modulation_percentage             )
    
    
    if modulate_synapse:
        syn_fact = modulation_factors[-3:-1]
    else:
        syn_fact = False
    
    if modulate_gaba:
        gabaMod = modulation_factors[-1]
    else:
        gabaMod = False
    
       
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    MAP = {}
    for sec in cell.dendlist:
        
        section_number = sec.name().split('[')[1][:-1]        
        d2soma = h.distance(0.5, sec=sec)
        
        MAP[section_number] = d2soma
    
    use.save_obj(MAP, 'Libraries/map_sec_dist_to_soma')
    
    
    
    ggg
        
        
        
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
                    
                    
    # load modulation factors and scale conductances  
    if DAmod:
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )
    
    
    # set stimuli (including modulation)
    rand, ncon, stim, spike_list = use.set_mixed_stimuli(   cell,               \
                                                            mixActPat,          \
                                                            norm,               \
                                                            syn_fact=syn_fact,  \
                                                            ISI=ISI             )
    
    VAL = {'par':    {  'DAmod':DAmod,                          \
                        'modulate_synapse':modulate_synapse,    \
                        'modulate_gaba':modulate_gaba,          \
                        'modulate_axon':modulate_axon,          \
                        'synaptic_strength':int(h.synaptic_strength)  }}
    
    
        
    for i in range(6): 
        
        VAL[i] = {'vm':{}}
        
        # (re-)set bg and shift annealing
        
        if i > 0:
            spike_list, stim, ncon  = use.mixed_stimuli_annealing(mixActPat, norm, i-1, stim, ncon, rand, spike_list)
        
        # set bg noise (including modulation)
        Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                                syn_fact=syn_fact,              \
                                                gabaMod=gabaMod                 )
                
        # spike array
        SPIKES      = np.zeros((10,20)) 
        
        print('_V'+str(model_version)+'_pattern'+str(norm)+'_mod'+str(int(h.modulation_percentage)), i )
        
        # simulation loop ----------------------------------------------------------------
        for j in range(10):
            
            print (i,j)
            
            # record vectors
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
                      
            # finalize and run
            h.finitialize(-70)
            while h.t < tstop:
            
                h.fadvance()  
            
            
            # shift spike traces 
            t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= 20000 and x[0]%2==0]
            v   = [x[1]      for x in enumerate( vm) if x[0] >= 20000 and x[0]%2==0]  
            
            # get spikes (crop at 20 spikes)
            spikes = use.getSpikedata_x_y( t, v )[:20]
            SPIKES[j,:len(spikes)]  = spikes
            
            VAL[i]['vm'][j] = v
                
            
        ratio, c1, c2, t2s = use.analyse_spike_data(SPIKES, max_time=200, t2s_cutoff=2)
            
        VAL[i]['values'] = [   ratio, c1, c2, t2s   ]
        VAL[i]['spikes'] = SPIKES   
     
        use.save_obj(VAL, 'InVivoDAfixed'+str(ISI)+'ms_mixed_'+cell_type+'_V'+str(model_version)+'_pattern'+str(norm)+'_mod'+str(int(h.modulation_percentage)) )  
        
        
   
    
