import sys
sys.path.append('./')
import numpy                as np
import common_functions     as use
import glob
from joblib import Parallel, delayed
import multiprocessing





def transform_to_pandas(array, file_name, D=5, mode='a', pattern_len=16):
    
    # loop over all entries in the result array, extract fetures and writes to csv file 
    # this file can then be read into a pandas frame from a graphical tool for further instection
    
    columns = [ 'Model',
                'DA',
                'dt',
                'pattern',
                'sub_pattern',
                'pattern_len',
                'ratio',
                'count',
                'dt_var',
                'dt_mean',
                'broken?']
    
    file = open(file_name, mode)
    
    if mode == 'w':
        file.write(",".join(columns))
    
    # create master array with all parameters
    m   =   len(array)*D
    n   =   len(columns)
    A   =   np.zeros((m,n))
    
    # loop array & pattern
    counter = 0
    for entry in array:
        model_version   = entry['V']
        dopamine_mod    = entry['mod']
        pattern         = entry['P']
        for d in range(D):
            
            # fill array
            line          = [   model_version,
                                dopamine_mod,
                                0,
                                pattern,
                                d,
                                pattern_len,
                                entry[d][1],
                                entry[d][2],
                                entry[d][3],
                                entry[d][5],
                                entry[d][4]     ]
            
            file.write('\n')
            file.write(",".join([str(l) for l in line]))
    
    file.close()
            
                                
                                

def get_all_data(f, D=5):
    
    # extention of "standardize_data_structure" that also checks if file is "broken"
    
    
    data    =   use.load_obj(f)
    
    V       =   int(f.split('_V')[ 1].split('_')[0])
    da      =   int(f.split('mod')[1].split('.')[0])
    P       =   int(f.split('pattern')[1].split('_')[0])
    
    M       =   {'mod':da, 'V':V, 'P':P}
    
    for key in range(D):
        
        if any( data[key]['broken'] == 1 ): flag = 1
        else:                               flag = 0 
        
        M[key] = [data[key]['spikes'], data[key]['values'][0], data[key]['values'][1], data[key]['values'][3], flag, data[key]['values'][4] ]
            
            
    return M
    
                


ISI             =       '1ms'
V               =       'V[0-9]'
path            =       'Results/InVivo_sim_res/Oct2018/MixedPattern/Restricted/'
tag             =       'Restricted'
N               =       9
L               =       {'16':5, '32':6, '64':7}


for pattern_length in ['16', '32']:
    files       =   glob.glob(path+'InVivoDAfixed'+tag+'*'+ISI+'_mixed'+pattern_length+'_D1_'+V+'*') 
    num_cores   =   multiprocessing.cpu_count() 
    RES         =   Parallel(n_jobs=num_cores)(delayed(get_all_data)( f, D=L[pattern_length] ) for f in files)
    use.save_obj(RES, path+tag+pattern_length+'_gmax1000' )


RES16 = use.load_obj(path+tag+'16_gmax1000.pkl')
RES32 = use.load_obj(path+tag+'32_gmax1000.pkl')
transform_to_pandas(RES16, 'Results/Panda_ready/'+tag+'.csv', D=L['16'], mode='w', pattern_len=16)
transform_to_pandas(RES32, 'Results/Panda_ready/'+tag+'.csv', D=L['32'], mode='a', pattern_len=32)



        
    
    
