import numpy                as np
import matplotlib.pyplot    as plt
import common_functions     as use
import glob
from joblib import Parallel, delayed
import multiprocessing
import sys



def read_and_format_data(f, N):
    
    data = use.load_obj(f)
    
    # extract distribution from file name
    distribution = f.split('_')[3]
    
    # get sim index
    if distribution == 'clustered':
        act_pat_type    =   0
        d               =   int(f.split('dist')[1].split('_')[0])
        dist_group      =   int(d/60)
        delta           =   int(f.split('delta')[1].split('.')[0])
    else:
        act_pat_type    =   1
        d               =   int(f.split('run')[1].split('_')[0])
        norm            =   d
        delta           =   int(f.split('delta')[1].split('.')[0])
        if norm < 20:
            dist_group   =   0
        elif norm < 37: 
            dist_group   =   1
        elif norm < 54: 
            dist_group   =   2
        else:
            dist_group   =   3
    
    if dist_group > 3:
        dist_group = 3
    
    #I       =   np.zeros((10,N))    
    #I_tot   =   np.zeros((10,N))
    V_tot   =   np.zeros((10,N))
    V_trunc =   np.zeros((10,N))
    
    SPIKES  =   np.zeros((10,20))
    
    # loop over bg
    for b,bg in enumerate(range(10)):
            
        results     =   data[bg]
        
        vm          =   results[    'vm']
        V_tot[ b,:] =   vm
        spikes      =   results['spikes']   
        
        for s,S in enumerate(spikes):
            SPIKES[b,s]     =   S
        
        '''if distribution == 'clustered':
            I_tot[b,:]  =   results['cur']
        else:
            for k,key in enumerate(results['cur']):
                I[k,:]  =   results['cur'][k]
            I_tot[b,:]  =   np.sum(I, axis=0) '''
        
        V_trunc[b,:] =   [v if v < -50 else -50 for v in vm ]
    
    
    # calc average
    #I_mean  =   np.mean(I_tot, axis=0) 
    
    
    return [V_tot, [], [], [], act_pat_type, dist_group, V_trunc, SPIKES, delta]





ISI             =       '1ms'
V               =       sys.argv[-1]  #'V3'

repickled_name  =       'repickledRamping_'+V+'_'+ISI
path            =       'Results/InVivo_sim_res/Oct2018/Ramping/'

fig, ax         =       plt.subplots(3,4, figsize=(20,10), gridspec_kw={'height_ratios':[1,2,1]} )     # fs = (w,h)

color           =       ['r', 'b']
alpha           =       [0.2, 0.2]
ls              =       ['-', '-']
mew             =       1
ms              =       20

cell_type       =       ['D1', 'D2']
distribution    =       ['clustered', 'dispersed'] #['dispersed'] #

time            =       np.arange(-500,500.001,25e-3)
N               =       len(time)
max_time        =       200         # ms, following activation pattern
sort_index      =       range(10) #[3,1,8,10,5,6,9,7,2,4]



try:
    RES = use.load_obj(path+repickled_name+'.pkl')
    print '--loading repickled data' 
except:   
    print '--repickeling'
    files       =   glob.glob(path+'InVivoRamping'+ISI+'_*_D1_'+V+'_*')   
    num_cores   =   multiprocessing.cpu_count() 
    RES         =   Parallel(n_jobs=num_cores)(delayed(read_and_format_data)( f, N ) for f in files)
    use.save_obj(RES, path+repickled_name )

'''
# setup of result dict for spike count data
NSC = {'spike_over_bg':{}}
for bg in range(10):
    NSC[bg] = {}
    for group in range(4):
        NSC['spike_over_bg'][group] = {}
        NSC[bg][group]              = {}
        for activation_pattern in range(2):
        
            NSC['spike_over_bg'][group][activation_pattern] = {'mean':[], 'std':[]}
            
            NSC[bg][group][activation_pattern] = {  'total_traces':0,       \
                                                    'non_spiking':0,        \
                                                    'spikes_in_traces':[],  \
                                                    'time2spike':[]         }



bg_list = []
for m,M in enumerate(RES): 
    
    VM      =   M[0]
    I_tot   =   M[1]
    I_mean  =   M[2]
    apt     =   M[4] # activation pattern type (apt). clustered or dispersed, 0 and 1 respectively
    group   =   M[5]
    V_trunc =   M[6]
    spikes  =   M[7]
    
    V_mean  =   np.mean(V_trunc, axis=0)
    V_std   =   np.std( V_trunc, axis=0)
    
    # plot mean current over bg (dispersed current is the summed current from all 10 synapses).
    ax[2,group].plot(time, I_mean, color=color[apt], alpha=alpha[apt] )
    
    
    flag       = True
    spike_list = []
    
    for bg in range(10):
        
        if m==0:
            # calc mean vm level about 0.5 s before pattern activation
            bg_list.append( np.mean(VM[bg,5000:19000]) )   
        
        # plot all raw traces (top line).
        ax[0,group].plot(time, VM[bg,:], color=color[apt], alpha=alpha[apt], ls=ls[apt])
        
        # count traces and check if spiking
        NSC[bg][group][apt]['total_traces'] += 1  
        
        if not any(spikes[bg]) or spikes[bg,0] > max_time:
            NSC[bg][group][apt]['non_spiking'] += 1
            #spike_list.append(max_time)             # if no spike within range -> append 
            flag = False
        else:
            spike_list.append(spikes[bg,0])
            
        
        # plot spikes times (second line from top). Marker size is large for first spike and shrinks with following spikes
        for s_ind,s in enumerate(spikes[bg,:]):
            if s > 0 and s < max_time:
                ax[1,group].plot(s, bg+1, '|', color=color[apt], ms=ms/(s_ind+1), mew=mew)
                if s_ind == 0:
                    NSC[bg][group][apt]['time2spike'].append( spikes[bg,0] )
            else:
                # save number of spikes in trace (append to list)
                NSC[bg][group][apt]['spikes_in_traces'].append(s_ind)  
                break
    
    if flag:        
        NSC['spike_over_bg'][group][apt]['mean'].append( np.mean(spike_list) )  
        NSC['spike_over_bg'][group][apt][ 'std'].append( np.std( spike_list) )   
    
    
'''   


# QUANTIFY AND PLOT

def format_boxplot(bp, median=False, lw=2, colors=None):
    '''adjust boxplot to stadardized format
    '''
    if not colors:
        for box in bp['boxes']:
            box.set(color='k', linewidth=lw)
        for w in bp['whiskers']:
            w.set(color='k', linewidth=lw)
        for cap in bp['caps']:
            cap.set(color='k', linewidth=lw) 
    else:
        for c,box in enumerate(bp['boxes']):
            box.set(color=colors[c], linewidth=lw)
        for c,w in enumerate(bp['whiskers']):
            c = c/2
            w.set(color=colors[c], linewidth=lw)
        for c,cap in enumerate(bp['caps']):
            c=c/2
            cap.set(color=colors[c], linewidth=lw)
        for c,flier in enumerate(bp["fliers"]):
            flier.set(marker='*', markersize=10, markerfacecolor=colors[c], markeredgecolor=colors[c])
    if not median:
        for med in bp['medians']:
            med.set(color='w', linewidth=0)


'''

f1, a1 = plt.subplots(5,4, figsize=(20,15), gridspec_kw={'height_ratios':[3,2,4,2,4]} )
#f2, a2 = plt.subplots(3,4, figsize=(20,10), gridspec_kw={'height_ratios':[2,2,2,]} )

# index of plot window (for easy rearangement)
a = 0
b = 1
c = 1
d = 3
e = 4
f = 2

width = 0.5

for group in range(4):
    
    # number of groups and groups that are not spiking
    for apt in range(2):       
        tot_trace   = 0
        non_spike   = 0
        for bg in range(10):
            tot_trace   += NSC[bg][group][apt]['total_traces']
            non_spike   += NSC[bg][group][apt][ 'non_spiking']
        spike       = tot_trace - non_spike
        a1[a,group].bar(apt, spike, width, color=color[apt])
        a1[a,group].bar(apt, non_spike, width, bottom=spike, color='grey')
    max_hight = 200
    a1[a,group].set_ylim((0,max_hight))
    a1[a,group].set_xticks([])
    
    
    
    
    
    # fastest over bg mean time to first spike over bg (individually)
    bp1 = a1[c,group].boxplot(  [ NSC['spike_over_bg'][group][0]['mean'], NSC['spike_over_bg'][group][1]['mean'] ],  positions = [1,2],      \
                                vert=False,                                       \
                                widths=0.6,                                       \
                                medianprops=dict(linewidth=2,color='k'),     \
                                boxprops=dict(   linewidth=4,color=color))
    format_boxplot(bp1, median=True, lw=4, colors=color)
    
    for apt in range(2):
        x = NSC['spike_over_bg'][group][apt]['mean']
        y = np.ones(len(x))*3
        a1[b,group].plot(  x, y, '|', color=color[apt], alpha=0.8,  ms=ms, mew=mew)
    a1[b,group].set_yticks([])
    a1[b,group].set_xlim((0,max_time))
    a1[b,group].set_ylim((0,4))
    
    
    # fastest split on bg
    Y = range(1,11)
    for apt in range(2):    
        CV = []; CM = []
        for bg in range(10):
            CM.append( np.mean( NSC[bg][group][apt]['time2spike'] ) )
            CV.append( np.std(  NSC[bg][group][apt]['time2spike'] ) )
        Xc = CM #[CM[index-1] for index in sort_index]
        a1[f,group].plot( Xc, Y, '-o', color=color[apt], alpha=0.8,  ms=10, mew=mew, lw=3)
        a1[f,group].set_xlim((0,max_time))
    
    
    # most close in time (first spike) over bg
    bp1 = a1[d,group].boxplot(  [NSC['spike_over_bg'][group][0]['std'],NSC['spike_over_bg'][group][1]['std']],  positions = [1,2],      \
                                vert=False,                                       \
                                widths=0.6,                                       \
                                medianprops=dict(linewidth=2,color='k'),     \
                                boxprops=dict(   linewidth=4,color=color))
    format_boxplot(bp1, median=True, lw=4, colors=color)
    for apt in range(2):
        x = NSC['spike_over_bg'][group][apt]['std']
        y = np.ones(len(x))*3
        a1[d,group].plot(  x, y, '|', color=color[apt], alpha=0.8,  ms=ms, mew=mew)
    a1[d,group].set_yticks([])
    a1[d,group].set_xlim((0,max_time/2))
    a1[d,group].set_ylim((0,4))
    
    

    # most spikes
    for apt in range(2):    
        CV = []; CM = []
        for bg in range(10):
            CM.append( np.mean( NSC[bg][group][apt]['spikes_in_traces'] ) )
            CV.append( np.std(  NSC[bg][group][apt]['spikes_in_traces'] ) )
        Xc = CM #[CM[index-1] for index in sort_index]
        a1[e,group].plot( Xc, Y, '-o', color=color[apt], alpha=0.8,  ms=10, mew=mew, lw=3)
        a1[e,group].set_xlim((0,4))
    

    # fastest within bg

    # most similar within bg
    

for group in range(4):
    ax[2,group].set_ylim([-0.4, 0.05])
    ax[2,group].set_xlim([-30, max_time])
    ax[1,group].set_ylim([0, 11])
    ax[1,group].set_xlim([-30, max_time])
    ax[1,group].set_yticks(range(2,11,2))
    ax[0,group].set_xlim([-30, max_time])'''
    
'''
y = np.ones( len(clustered[group]) )*10
ax[3,group].plot(clustered[group], y, '|', color=color[0], alpha=0.8,  ms=ms, mew=mew)

y = np.ones( len(dispersed[group]) )*10
ax[3,group].plot(dispersed[group], y, '|', color=color[1], alpha=0.8,  ms=ms, mew=mew)
'''
#print 'clustered:', NSC, 'dispersed:', NSD   
'''
f1.savefig('../../../Desktop/simulatin_results_in_vivio_201810/'+repickled_name+'.png') 
fig.savefig('../../../Desktop/simulatin_results_in_vivio_201810/traces/'+repickled_name+'_traces.png')

plt.close('all')

print np.mean( bg_list ) 
print bg_list'''


def best_fit_parameter_distribution(best_fit, N):
    '''
    plot distribution range of best fit solutions (normalized to parameter range)
    
    sigmoidal distributions
    y = 1-a1 + a1/(1 + np.exp((x-a2)/a3) )
    
    
    '''
    colors = ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99','#b15928']
    
    # parameter ranges
    random_variables = { 'naf': [   [-0.5,0.5], \
                                    [0.8,1.0], \
                                    [10.0,60.0],   \
                                    [1.0,30.0] ],  \
                         'kaf': [   [-0.5,0.5], \
                                    [0.0,0.9], \
                                    [1.0,130.0],   \
                                    [-3.0,-70.0] ],  \
                         'kas': [   [-0.5,0.5],    \
                                    [-5.0,60.0],  \
                                    [1.0,70.0] ], \
                         'kir': [   [-0.5,0.5] ], \
                         'sk' : [   [-0.5,0.5] ], \
                         'can': [   [-7.0,-5.0], \
                                    [0.8,1.0], \
                                    [10.0,60.0],   \
                                    [1.0,30.0] ],    \
                         'c32': [   [-9.0,-6.0], \
                                    [1.0,130.0],   \
                                    [-3.0,-70.0] ], \
                         'c33': [   [-9.0,-6.0], \
                                    [1.0,130.0],   \
                                    [-3.0,-70.0] ]   }
    
    
    fd,ad       =   plt.subplots(1,1, figsize=(16,8))
    #fd2,(aK,aN) =   plt.subplots(1,2, figsize=(16,8))
    parameters  =   ['naf0', 'naf1', 'naf2', 'naf3', \
                     'kaf0', 'kaf1', 'kaf2', 'kaf3', \
                     'kas0',         'kas2', 'kas3', \
                     'Kir', \
                     'sk',  \
                     'can0', 'can1', 'can2', 'can3',\
                     'c32-0', '32-2', '32-3',\
                     'c33-0', '33-2', '33-3'] 
    
    # loop solutions 
    for index in range(N):
        
        # create list
        y = []
        x = []
        c = 0
        for key in reversed(['c33', 'c32', 'can', 'sk', 'kir', 'kas', 'kaf', 'naf']):
            for i in range(len(best_fit[index]['variables'][key])):
            
                val     =   best_fit[index]['variables'][key][i]
                A       =   random_variables[key][i][0]
                B       =   random_variables[key][i][1]
                factor  =   (val-A) / (B-A)
                y.append(   factor  )
                x.append(   c       )
                c+=1
            
        # plot
        ad.plot(x, y, '-o', ms=20, color=colors[index], alpha=1.0, label=str(index))
        
        '''
        X = np.arange(300)
        
        # Sodium (Na)
        a5 =  best_fit[index]['variables']['naf'][1]
        a4 =  1-a5
        a6 =  best_fit[index]['variables']['naf'][2]
        a7 =  best_fit[index]['variables']['naf'][3]
        Y  =  a4 + a5/(1 + np.exp((X-a6)/a7) )  # best_fit[index]['var']['naf'][0]*
        aN.plot(X,Y, color=colors[index], alpha=1)
        aN.set_ylim([0,2])
        
        # Potassium (K)
        a5 =  best_fit[index]['variables']['kaf'][1]
        a4 =  1
        a6 =  best_fit[index]['variables']['kaf'][2]
        a7 =  best_fit[index]['variables']['kaf'][3]
        Y  =  a4 + a5/(1 + np.exp((X-a6)/a7) )  # best_fit[index]['var']['kaf'][0]*
        aK.plot(X,Y, color=colors[index], alpha=1)
        aK.set_ylim([0,2])'''
        
    
    ad.set_xticks(x)
    ad.set_xticklabels(parameters, fontsize=20, rotation=45) 
    ad.legend()
    
    #fd.savefig( 'Figures/distribution_variables_'+cell+'.png', transparent=True)
    #fd2.savefig('Figures/distribution_variables_naf_kaf_'+cell+'.png', transparent=True )

'''  
models = use.load_obj('D1_71bestFit.pkl')

rheo = []
mod  = []
for i in range(24):
    
    rheo.append(models[i]['rheobase'])
    mod.append(i)
       
baseline = [-69.0946646845, -70.9303890779, -67.4307022158, -73.3584415242, -74.0978066919, -72.6521600275, -69.8342745301, -70.7291438357, -70.5456963842, -74.7568591371, -60.2955628215, -70.7363711849, -69.8151923281, -63.9226317861, -72.1827334546, -75.4379243907, -71.0460529296, -65.1041191764, -67.6406407261, -66.5769959389, -71.2772690967, -70.4778241126, -70.3362535487, -71.0273067148]
        
plt.plot(mod, rheo, 'k-o', mec='r', mew=3)  
plt.savefig('../../../Desktop/simulatin_results_in_vivio_201810/repickled_rheobase.png')
plt.figure()
plt.plot(mod, baseline, 'k-o', mec='r', mew=3)
plt.savefig('../../../Desktop/simulatin_results_in_vivio_201810/repickled_baseline.png')

best_fit_parameter_distribution(models, 12)

plt.close('all') '''
        
        
    
    
