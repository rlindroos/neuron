
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('./Mech/x86_64/special')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        
            
# if run from terminal...   ==============================================================
if __name__ == "__main__":
    
    ISI         =   1
    tstop       =   1500
    cvode       =   h.CVode()
    deltaT      =   int(h.activation_delay)
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =   64
    
    model_version = int(np.floor(id/N))
    norm          = id - model_version*N
    model_version = model_version + int(h.start_value)
    
    
    # load parameters and random set
    path_to_lib     = './Libraries/'
    parameters      =   use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables'] # parameter set 0 (cell index)
    randActPat      =   use.load_obj(path_to_lib+'randomActivationPattern.pkl')[cell_type]   # preselected indexes to use with lists of segments (see create_segment_list below)
    randActTime     =   use.load_obj(path_to_lib+'randomActTimes.pkl')
    
    
    # group and pattern
    group, p    =   use.get_group_and_pattern_index(    norm,               \
                                                        randActPat          )
    pattern     =   randActPat[group][p]
    
    
    print(cell_type, morphology, par)
    
    
    
    # initiate cell ----------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
    
    
    # create segment lists
    dist_groups = range(4)
    segments    = use.create_segment_list(cell, dist_groups)
              
    
    # set cascade 
    #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    if cell_type=='D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
    use.set_pointers(cell, pointer, mod_list)
                     
                
    # result dict
    RES     = {'par':{  'synaptic_strength':int(h.synaptic_strength)  }}
    
    
    # set background noise
    Syn, nc, ns         = use.set_bg_noise( cell )
        
    
    # set stimuli
    activationPattern   = [ segments[group][s] for s in pattern ]
    
    
    # ** MOVE INTO LOOP IF RECORDING CURRENT **
    #   returned by function: cur,rand,ncon,stim
    cur,rand,ncon,stim  = use.set_dispersed_stimuli(    cell,               \
                                                        activationPattern,  \
                                                        delta=deltaT,       \
                                                        ISI=ISI )
    
    # simulation loop --------------------------------------------------------------------
    for i in range(10):
        
        RES[i]  = {}
        
        print( cell_type, group, p, i)
        
        # set background noise
        #   values returned by function: Syn, nc, ns
        Syn, nc, ns   =   use.set_bg_noise(cell , delays=randActTime[i])
        
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        # run simulation
        h.finitialize(-70)
        
        # run simulation
        tstop   = 1500
        while h.t < tstop:
        
            h.fadvance()
                
        # remove first 500 ms and shift time so that start of stimuli is 0
        cutoff_index = 20000
        t   = [x[1]-1000 for x in enumerate( tm)        if x[0] >= cutoff_index]
        v   = [x[1]      for x in enumerate( vm)        if x[0] >= cutoff_index] 
        
        spikes = use.getSpikedata_x_y( t, v )
        
        RES[i][    'vm'] =   v 
        RES[i]['spikes'] =   spikes
        '''RES[i][   'cur'] =   {}
        
        for k,key in enumerate(cur): 
            I  = [x[1]   for x in enumerate(cur[key])   if x[0] >= cutoff_index] 
            
            RES[i]['cur'][k]    =   I'''
        
    use.save_obj(RES, 'InVivoRamping'+str(ISI)+'ms_dispersed_'+cell_type+'_V'+str(model_version)+'_run'+str(norm)+'_delta'+str(deltaT) )
    
    
        
    
    
