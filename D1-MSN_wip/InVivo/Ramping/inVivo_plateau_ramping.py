
# script used for running in vivo simultion of clustered synaptic activation during
#   ramping background stimuli.
#
# the delay between the onset of the ramp and the stimuli is controlled by

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')





# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    
    ISI         =   1
    tstop       =   1500
    cvode       =   h.CVode()
    deltaT      =   int(h.activation_delay)
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    58
    
    model_version = int(np.floor(id/N))
    section       = id - model_version*N
    model_version = model_version + int(h.start_value)
    
    
    # load random sets
    path_to_lib         =   './Libraries/'
    random_variables    =   use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    randActTime         =   use.load_obj(path_to_lib+'randomActTimes.pkl')
    
    
    # result dict
    RES     = {'par':{  'synaptic_strength':int(h.synaptic_strength)  }}
    
    
    print(section, cell_type, morphology, par)
    
    
        
    # initiate cell ----------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
        
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    if cell_type=='D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    use.set_pointers(cell, pointer, mod_list)
    
        
    # set clustered input 
    #   values returned by the function are; syn, stim, ncon, d2soma
    syn, stim, ncon, d2soma = use.set_clustered_stimuli( cell,               \
                                        section,            \
                                        delta=deltaT        )
    
    
    # simulation loop --------------------------------------------------------------------
    for i in range(10):
        
        print(cell_type, section, i)
        
        RES[i]  =   {}
        
        # set bg noise
        #   values returned by function: Syn, nc, ns
        Syn, nc, ns     =   use.set_bg_noise(cell , delays=randActTime[i])
        
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        #cur = h.Vector()
        #cur.record(syn._ref_I)
                  
        # finalize and run
        h.finitialize(-70)
        while h.t < tstop:
        
            h.fadvance()
          
        
        # shift spike traces
        t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= 20000]
        v   = [x[1]      for x in enumerate( vm) if x[0] >= 20000]  
        #c   = [x[1]      for x in enumerate(cur) if x[0] >= 20000]  
        
        # get spikes
        spikes = use.getSpikedata_x_y( t, v )
        
        # save
        RES[i]['spikes'] = spikes
        RES[i]['vm'    ] = v
        #RES[i]['cur'   ] = c
        
        
    use.save_obj(RES, 'InVivoRamping'+str(ISI)+'ms_clustered_'+cell_type+'_V'+str(model_version)+'_dist'+str(d2soma)+'_section'+str(section)+'_delta'+str(deltaT) )
    
    

