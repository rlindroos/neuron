import sys
sys.path.append('./')
import numpy                as np
import matplotlib.pyplot    as plt
import common_functions     as use
from    scipy           import stats


def create_result_structure(N):
    array = {}
    group_len   = [[20,17,17,4], [20,17,17,10]]
    for t,apt in enumerate(['clust', 'disp']):
        array[t] = {}
        for g in range(4):
            array[t][g] = {     'spike_ratio':np.zeros( (N,group_len[t][g]) ),     \
                                'spike_count':np.zeros( (N,group_len[t][g]) ),     \
                                'time2spike': np.zeros( (N,group_len[t][g]) ),     }
    
    return array
    
    
def create_result_structure_ramping(N):
    array = {}
    group_len   = [[20,17,17,4], [20,17,17,10]]
    for t,apt in enumerate(['clust', 'disp']):
        array[t] = {}
        for g in range(4):
            array[t][g] = {     'spike_ratio':np.zeros( (N,group_len[t][g],3) ),     \
                                'spike_count':np.zeros( (N,group_len[t][g],3) ),     \
                                'time2spike': np.zeros( (N,group_len[t][g],3) ),     }
    
    return array
    


def merge_arrays(A1, A2):
    
    for t,apt in enumerate(['clust', 'disp']):
        for g in range(4):
            for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
                #A1[t][g][feature][0:3,:,:]
                A1[t][g][feature][0:3,:,:] = A2[t][g][feature]
    
    return A1




def fill_result_structure(DATA, array, v, max_time=300, t2s_cutoff=2):
    
    bg_list = []
    
    # (re)set index
    for apt in [0,1]:
        for g in range(4):
            array[apt][g]['index'] = 0
    
    # loop over activation pattern in DATA
    for m,M in enumerate(DATA): 
        
        # activation pattern type (apt). clustered or dispersed, 0 and 1 respectively
        apt     =   M[4] 
        group   =   M[5]        # distance group (0-4)
        spikes  =   M[7]        # spike times (array of len 20 filled with zeros at the end)
        
        number_of_spikes    =   []
        spiking_traces      =   0
        time2spike          =   []
        
        flag = False
        # loop over backgrounds and extract spike count features
        for bg in range(10):
            
            if any(spikes[bg]) and spikes[bg,0] < max_time:
                
                if spikes[bg,0] > 0:
                    number_of_spikes.append( len(np.trim_zeros(spikes[bg],'b')) )
                    time2spike.      append( spikes[bg,0]                       )
                    spiking_traces += 1
                else:
                    print v, 'spikes before activation - skipping model'
                    flag = True
                    break
            else:
                number_of_spikes.append( 0 )
        
        if flag:
            break
                
        # save to array
        index = array[apt][group]['index']
        
        #first_zero_index    =   np.where(array[apt][group][v] == 0)[0][0]
        
        #print apt, group, index
        array[apt][group]['spike_ratio'][v,index]  =  1.0 * spiking_traces / 10 
        array[apt][group]['spike_count'][v,index]  =  np.mean(number_of_spikes)
        if len(time2spike) >= t2s_cutoff:
            array[apt][group]['time2spike'][v,index]  =  np.var(time2spike) 
        else:
            array[apt][group]['time2spike'][v,index]  =  np.nan  
        
        array[apt][group]['index'] += 1   
    
    return array  
    
    
    
                
def fill_result_structure_ramping(DATA, array, v, max_time=300, t2s_cutoff=2):
    
    bg_list = []
    
    # (re)set index
    for apt in [0,1]:
        for g in range(4):
            array[apt][g]['index'] = {0:0, 50:0, 100:0}
    
    # loop over activation pattern in DATA
    for m,M in enumerate(DATA): 
        
        # activation pattern type (apt). clustered or dispersed, 0 and 1 respectively
        apt     =   M[4] 
        group   =   M[5]        # distance group (0-4)
        spikes  =   M[7]        # spike times (array of len 20 filled with zeros at the end)
        delta   =   M[8]        # delay from start of ramp
        
        delta_i =   {0:0, 50:1, 100:2}
        
        number_of_spikes    =   []
        spiking_traces      =   0
        time2spike          =   []
        
        flag = False
        # loop over backgrounds and extract spike count features
        for bg in range(10):
            
            if any(spikes[bg]) and spikes[bg,0] < max_time:
                
                if spikes[bg,0] > 0:
                    number_of_spikes.append( len(np.trim_zeros(spikes[bg],'b')) )
                    time2spike.      append( spikes[bg,0]                       )
                    spiking_traces += 1
                else:
                    print v, 'spikes before activation - skipping model'
                    flag = True
                    break
            else:
                number_of_spikes.append( 0 )
        
        if flag:
            break
                
        # save to array
        index   = array[apt][group]['index'][delta]
        di      = delta_i[delta]
        
        #print apt, group, index
        array[apt][group]['spike_ratio'][v,index,di]  =  1.0 * spiking_traces / 10 
        array[apt][group]['spike_count'][v,index,di]  =  np.mean(number_of_spikes)
        if len(time2spike) >= t2s_cutoff:
            array[apt][group]['time2spike'][v,index,di]  =  np.var(time2spike) 
        else:
            array[apt][group]['time2spike'][v,index,di]  =  np.nan  
        
        array[apt][group]['index'][delta] += 1   
    
    return array    

path            =       'Results/InVivo_sim_res/Oct2018/Ramping/'    
ISI             =       '1ms'
N               =       12
'''
array           =       create_result_structure_ramping(N)

for v in range(3,12):
    
    print v, '--------------------------------------------'
    
    V               =   'V' + str(v)
    repickled_name  =   'repickledRamping_'+V+'_'+ISI
    
    # import data
    print '-loading data'
    DATA            =   use.load_obj(path+repickled_name+'.pkl')
    print '-filling array'
    # sort spike times data into arrays
    array           =   fill_result_structure_ramping(DATA, array, v)


# save array
print '* saving/loading array'
use.save_obj(array, path + 'global_spike_time_features_' + str(N) )

array = use.load_obj(path+'global_spike_time_features_' + str(N) + '.pkl')
'''

array3 = use.load_obj(path+'global_spike_time_features_3.pkl')
array  = use.load_obj(path+'global_spike_time_features_12.pkl')

array  = merge_arrays(array, array3)

use.save_obj(array, path + 'global_spike_time_features_' + str(N) )


# plot result
f1,a1 = plt.subplots(3,1, figsize=(10, 12))
f2,a2 = plt.subplots(3,1, figsize=(10, 12))
f3,a3 = plt.subplots(3,1, figsize=(10, 12))

axes  = [a1,a2,a3]
color = ['r', 'b']

print '\tplotting'
for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
    for d in range(3):
        add = 0
        for g in range(4):
            for apt in range(2):
            
            
                a   = array[apt][g][feature][:,:,d]
                
                # delete row 10 (since all zero due to spike before stimuli)
                a   = np.delete(a, 10, 0)    
                
                if i == 2:
                    # remove nan elements
                    a    = a[~np.isnan(a)]
                
                # calc mean and sem
                # -global
                mean= np.mean( a )
                std = np.std( a, axis=None )    # stats.sem
                s   = mean - std
                e   = mean + std
                x   = (apt+1+g+add)*10
                w   = 2
                
                # -model version
                a   = array[apt][g][feature][:,:,d]
                a   = np.delete(a, 10, 0)  
                if i == 2:
                    mv_mean = []
                    # loop over model versions and calc mean for each model separately
                    for v in range(N):
                        if v == 10:
                            continue
                        b = array[apt][g][feature][v,:,d]
                        mv_mean.append( np.mean( b[~np.isnan(b)] ) )
                else:
                    mv_mean = np.nanmean( a, axis=1 )
                    
                
                # plotting
                axes[i][d].plot([x,x],     [s,e], '-', lw=2,  color='k')
                axes[i][d].plot([x-w,x+w], [s,s], '-', lw=2,  color='k')
                axes[i][d].plot([x-w,x+w], [e,e], '-', lw=2,  color='k')
                axes[i][d].plot(x, mean,  'o',   ms=20, mfc=color[apt], mew=2, mec='k')
                
                axes[i][d].set_xlim([0, 120])
                #axes[i].set_title(titles[p], fontsize=24)
                
                axes[i][d].set_xticks([])
                axes[i][d].yaxis.set_ticks_position('left')
            
                axes[i][d].spines['left'].set_linewidth(4)
                axes[i][d].spines['right'].set_visible(False)
                axes[i][d].spines['top'].set_visible(False)
                axes[i][d].spines['bottom'].set_visible(False)
                
                X       = np.ones( (1,len(mv_mean)) ) * x
                
                # connect dots between clust/disp
                if apt == 0:
                    mvm_old = mv_mean
                    X_old   = X
                else:
                    # create lines
                    m_zip = zip(mvm_old,mv_mean)
                    X_zip = zip(X_old[0],X[0])
                    
                    for z in range(len(m_zip)):
                        axes[i][d].plot(X_zip[z], m_zip[z], 'k', alpha=0.3)
                        
                    axes[i][d].plot(X_old[0], mvm_old, 'o', ms=10, color=color[0], alpha=0.7)
                    axes[i][d].plot(X[0],     mv_mean, 'o', ms=10, color=color[1], alpha=0.7)
                
            add += 2


# set ranges
for d in range(3):
    a1[d].set_ylim([-0.2, 1.3])
    a1[d].set_yticks( [0,0.2,0.4,0.6,0.8,1.0] )
    a1[d].set_yticklabels([0,0.2,0.4,0.6,0.8,1.0], fontsize=20)
for d in range(3):
    a2[d].set_ylim([-0.5, 3])
    a2[d].set_yticks( range(0,3) )
    a2[d].set_yticklabels(range(0,3), fontsize=20)
for d in range(3):
    a3[d].set_ylim([-100, 1200])
    a3[d].set_yticks( range(0,1041,200) )
    a3[d].set_yticklabels(range(0,1041,200), fontsize=20)

# set ylabel
for a in axes:
    a[0].set_ylabel('$\Delta$t = 0 ms', fontsize=24)
    a[1].set_ylabel('$\Delta$t = 50 ms', fontsize=24)
    a[2].set_ylabel('$\Delta$t = 100 ms', fontsize=24)

# set xlabel
placeholders = [15,45,75,105]
for i,a in enumerate(axes):
    for d in range(3):
        a[d].set_xticks( placeholders )
        a[d].set_xticklabels(['proximal', 'medial', 'distal', 'terminal'], fontsize=20)

# set title
a1[0].set_title('Spike probability', fontsize=30)
a2[0].set_title('Spike count', fontsize=30)
a3[0].set_title('Var; time to first spike', fontsize=30)
    
        

                
          
f1.savefig('../../../Dropbox/Manuscripts/Figures_2018/deterministic_ramping_spike_ratio.png')
f2.savefig('../../../Dropbox/Manuscripts/Figures_2018/deterministic_ramping_spike_count.png')
f3.savefig('../../../Dropbox/Manuscripts/Figures_2018/deterministic_ramping_time2spike.png')

plt.show()


'''
# statistical significance
hight = [1.2, 4, 1100]
for i,feature in enumerate(['spike_ratio', 'spike_count', 'time2spike']):
    T = []
    P = []
    for g in range(4):
        for apt in range(2):
            
            b   = array[apt][g][feature]
            
            # delete row 10 (since all zero due to spike before stimuli)
            b   = np.delete(b, 10, 0)  
            
            # flatten
            b   = b.flatten()
            
            # remove nan
            b   = b[~np.isnan(b)]
            
            if apt == 0:
                # save array
                a = b
            else:
                # calc statistical significance
                t2, p2 = stats.ttest_ind(a,b, equal_var=False)
                
                if p2 < 0.001:
                    axes[i].plot(placeholders[g], hight[i], '*', c='k', ms=20)
                elif p2 < 0.05:
                    axes[i].plot(placeholders[g], hight[i], 'd', c='k', ms=20)
        
        # append to lists
        T.append(t2)
        P.append(p2)
    
    # print 
    print feature
    print '\t', T
    print '\t', P
'''

# ******************************************************************************

'''
# plot BG only
bg_data =   use.load_obj('InVivoRampingBGonly_D1_V0.pkl')

fig, ax =   plt.subplots(1,1)

time    =   np.arange(-500,500.001,25e-3)

for i in bg_data:
    
    ax.plot(time, bg_data[i]['vm'], 'k')
    
ax.plot([-500,500], [-77,-77], '--k', lw=5)
ax.plot([0,500   ], [-68,-68], '--k', lw=5)

ax.spines['bottom'].set_linewidth(4)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(False)

ax.set_yticks([])   

ax.set_xticks( [-500,0,500] )
ax.set_xticklabels([-500,0,500], fontsize=30)

ax.set_xlim([-500,500])
fig.savefig('../../../Dropbox/Manuscripts/Figures_2018/deterministic_ramping_onlyBG.png')
plt.show()
'''     

