#! /usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.mlab import griddata
import glob
import sys
import os
from joblib import Parallel, delayed
import plot_functions as plot
import multiprocessing
import pickle
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from scipy.stats import linregress
from scipy import stats
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from IPython.display import display, HTML
import seaborn as sns


def load_obj(name ):
    with open(name, 'rb') as f:
        return pickle.load(f) 
        
        
        
def plot_exp_data(afi, aca, \
                  FI_string='Exp_data/FI/Planert2013-D1-FI-trace*', \
                  Ca_string='Exp_data/bAP/bAP-DayEtAl2006-D1.csv', \
                  single_type=True):
    
    if single_type:
        color = 'brown'
    else:
        if Ca_string == 'Exp_data/bAP/bAP-DayEtAl2006-D1.csv':
            color = '#4d97ff'
        else:
            color = '#4337b5'
    
    
    for i,f in enumerate(glob.glob(FI_string) ):
    
        [x_i,y_i] = np.loadtxt(f, unpack=True)
        
        if i == 0:
            label = 'Planert'
        else:
            label = ''
            
        afi.plot(x_i, y_i, ls='--', color=color, lw=5, alpha=0.5, label=label, clip_on=False)
        
    
    [x1,y1] = np.loadtxt(Ca_string, unpack=True)
    aca.plot(x1, y1, ls='--', color=color, lw=6)
    aca.plot(x1, y1, 'o', color=color, ms=10, mec='k', mew=2, label='Day 2008')
        
      
def def_axis(ax):
    
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'  ].set_visible(False) 
    
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')  
    
    ax.tick_params(width=2, length=4)  
    
    # size of frame
    for axis in ['bottom','left']:
        ax.spines[axis].set_linewidth(4)
        
        
def configure_plot_window(afi, aca, \
                          plot_best_fit=True, \
                          cell='D1', \
                          single_type=True):
    
    def_axis(afi)
    def_axis(aca)
    
    # set x-range
    afi.set_xlim([100, 800])
    afi.set_ylim([0, 60])
    
    # set ticks
    yticks = np.arange(10,41,10)
    afi.set_yticks(yticks)
    afi.set_yticklabels(yticks, fontsize=30)
    xticks = np.arange(150,760,150)
    afi.set_xticks(xticks)
    afi.set_xticklabels(xticks, fontsize=30)
    
    
        
    # ----------------------------------------------------------
    
    if plot_best_fit:
        if cell == 'D1':
            dist = np.arange(40,301)
            func = np.exp( (dist-40)/-27.0 )
            aca.plot(dist, func, 'k-.', lw=3)
        else:
            dist = np.arange(49,301)
            func = np.exp( (dist-49)/-63.0 )
            aca.plot(dist, func, 'k-.', lw=3)
    
    
    # set ticks
    yticks = [0,1]
    aca.set_yticks(yticks)
    aca.set_yticklabels(yticks, fontsize=35)
    xticks = [40, 120, 200]
    aca.set_xticks(xticks)
    aca.set_xticklabels(xticks, fontsize=35)
    aca.set_xlim([0,300])
    
    aca.legend(loc='best')
    afi.legend(loc='best')        
     
     
     
        
def plot_feature_comparison(D, exp, cell, cal_data=None):
    # vizualize spike features extracted from rheobase trace using efel
    
    fig, ax = plt.subplots(1,6, figsize=(14, 6))
    
    if cell == 'D1':
        j = 0
        c = '#4d97ff'
    else:
        j = 2
        c = '#4337b5'
    
    titles = ['threshold', 'AP amp', 'AP dur', 'Vm rest', 'Rin', 'Tau']
    
    for i in range(7):
        if i < 4:
            p=i
        elif i == 4:
            continue
        elif i > 4:
            p = i-1
        # experimental data
        s = exp[i][j] - exp[i][j+1]
        e = exp[i][j] + exp[i][j+1]
        ax[p].plot(0,     exp[i][j],  'o', ms=20,color='k')
        ax[p].plot([0,0],  [s,e], '-', lw=2, color='k')
        ax[p].plot([-5,5], [s,s], '-', lw=2, color='k')
        ax[p].plot([-5,5], [e,e], '-', lw=2, color='k')
        
        # simulated data
        if i < 4:
            X    = np.ones((1, len(D[i])))*10
            mean = np.mean(D[i])
            std  = np.std (D[i])
            s    = mean - std
            e    = mean + std
            ax[p].plot(X[0], D[i],   'o',   ms=10, color=c, alpha=0.3)
        else:
            mean = cal_data[i-5][j]
            std  = cal_data[i-5][j+1]
            s    = mean - std
            e    = mean + std
            
        ax[p].plot([10,10], [s,e], '-', lw=2,  color='k')
        ax[p].plot([5,15],  [s,s], '-', lw=2,  color='k')
        ax[p].plot([5,15],  [e,e], '-', lw=2,  color='k')
        ax[p].plot(10, mean,     'o',   ms=20, color=c, mew=2, mec='k')
        
        ax[p].set_xlim([-20, 40])
        ax[p].set_title(titles[p], fontsize=24)
        
        ax[p].set_xticks([])
        ax[p].yaxis.set_ticks_position('left')
    
        ax[p].spines['left'].set_linewidth(4)
        ax[p].spines['right'].set_visible(False)
        ax[p].spines['top'].set_visible(False)
        ax[p].spines['bottom'].set_visible(False)
        
    fig.savefig('Figures/feature_comparison_' + cell + '.png')
    
    
def plot_Vm(best_fit, axes=None, cell='D1'):
    
    if not axes:
        fvm,avm = plt.subplots(1,1, figsize=(12,8))
    else:
        fvm,avm = axes
    
    if cell == 'D1':
        c = '#4d97ff'
    else:
        c = '#4337b5'
    
    # plot voltage traces (for rheobase current)
    for i in best_fit:
        tm    =   best_fit[i]['tm']
        vm    =   best_fit[i]['vm']
        avm.plot(tm, vm, color=c, alpha=0.5)
    
    if not axes:
        fvm.savefig('Figures/voltagetrace_examples_'+cell+'.png',  transparent=True) 
           
    
 
           
def plot_FI_bAPdCa(best_fit, axes=None, cell='D1'):
    
    if not axes:
        fig,(aca, afi)  = plt.subplots(1,2, figsize=(12,8))
        single_type     = True
    else:
        aca, afi        = axes
        single_type     = False
    
    if not axes:
        color = 'b'
    else:
        if cell == 'D1':
            color = '#4d97ff'
        else:
            color = '#4337b5'
            
    first = True
    for i in best_fit:
        
        if first:
            if cell == 'D1':
                l = 'dSPN model'
            else:
                l = 'iSPN model'
            first=False
        else:
            l = ''
            
        # load data
        data        =   best_fit[i]
        
        aca.plot( data['bAP']['dist'], data['bAP']['dCa'], color=color, alpha=0.4, label=l)
        afi.plot( data['FI']['I'], data['FI']['F'], color=color, alpha=0.4, label=l)
    
    # experimental data
    plot_exp_data(afi, aca, 
                  FI_string='Exp_data/FI/Planert2013-'+cell+'-FI-trace*',
                  Ca_string='Exp_data/bAP/bAP-DayEtAl2006-'+cell+'.csv',
                  single_type=single_type)
    
    if not axes:
        plt.show()
    else:
        return


def plot_plateau(traces, axes=None, cell='dSPN'):
    
    tm = traces['tm']
    
    
    if not axes:
        fig,ax  = plt.subplots(1,1, figsize=(10,10))
    else:
        ax,ax2  = axes
    
    
    if cell == 'dSPN':
        c   = '#4d97ff'
        ct  = 'D1'
        shift = 200
    elif cell == 'iSPN':
        c   = '#4337b5'
        ct  = 'D2'
        shift = 250
    
    
    first = True    
    for t in traces:
        
        if t == 'tm':
            continue
            
        if first:
            label=cell
            first=False
        else:
            label=''
        
        ax.plot( np.subtract(tm,150), traces[t]['vml'], color=c, label=label)
        ax2.plot(np.subtract(tm,150), traces[t]['vm'],  color=c, label=label)
    '''
    #[x1,y1] = np.loadtxt('../../results/plateau/Plotkin_plateau_'+ct+'.csv', unpack=True)
    #ax2.plot((x1*1000-shift), (y1*1000-3), 'k-.o', lw=3, ms=2, alpha=0.6, label='Plotkin '+cell)
    
    if cell == 'dSPN':
        
        [x1,y1] = np.loadtxt('../../results/plateau/du_plateau.csv', unpack=True)
        print max(y1), min(y1)  
        ax2.plot(x1, np.fliplr([y1])[0], 'k-.o', lw=3, ms=2, alpha=0.6, label='Plotkin '+cell)'''
           
   
def best_fit_parameter_distribution(best_fit, N, cell='D1'):
    '''
    plot distribution range of best fit solutions (normalized to parameter range)
    
    sigmoidal distributions
    y = 1-a1 + a1/(1 + np.exp((x-a2)/a3) )
    
    
    '''
    
    if cell == 'D1':
        color = '#4d97ff'
    else:
        color = '#4337b5'
    
    # parameter ranges
    random_variables = { 'naf': [   [-0.5,0.5], \
                                    [0.8,1.0], \
                                    [10.0,60.0],   \
                                    [1.0,30.0] ],  \
                         'kaf': [   [-0.5,0.5], \
                                    [0.0,0.9], \
                                    [1.0,130.0],   \
                                    [-3.0,-70.0] ],  \
                         'kas': [   [-0.5,0.5],    \
                                    [-5.0,60.0],  \
                                    [1.0,70.0] ], \
                         'kir': [   [-0.5,0.5] ], \
                         'sk' : [   [-0.5,0.5] ], \
                         'can': [   [-7.0,-5.0], \
                                    [0.8,1.0], \
                                    [10.0,60.0],   \
                                    [1.0,30.0] ],    \
                         'c32': [   [-9.0,-6.0], \
                                    [1.0,130.0],   \
                                    [-3.0,-70.0] ], \
                         'c33': [   [-9.0,-6.0], \
                                    [1.0,130.0],   \
                                    [-3.0,-70.0] ]   }
    
    
    fd,ad       =   plt.subplots(1,1, figsize=(16,8))
    fd2,(aK,aN) =   plt.subplots(1,2, figsize=(16,8))
    parameters  =   ['naf0', 'naf1', 'naf2', 'naf3', \
                     'kaf0', 'kaf1', 'kaf2', 'kaf3', \
                     'kas0',         'kas2', 'kas3', \
                     'Kir', \
                     'sk',  \
                     'can0', 'can1', 'can2', 'can3',\
                     'c32-0', '32-2', '32-3',\
                     'c33-0', '33-2', '33-3'] 
    
    # loop solutions 
    for index in range(N):
        
        # create list
        y = []
        x = []
        c = 0
        for key in reversed(['c33', 'c32', 'can', 'sk', 'kir', 'kas', 'kaf', 'naf']):
            for i in range(len(best_fit[index]['variables'][key])):
            
                val     =   best_fit[index]['variables'][key][i]
                A       =   random_variables[key][i][0]
                B       =   random_variables[key][i][1]
                factor  =   (val-A) / (B-A)
                y.append(   factor  )
                x.append(   c       )
                c+=1
            
        # plot
        ad.plot(x, y, '-o', ms=20, color=color, alpha=0.3)
        
        X = np.arange(300)
        
        # Sodium (Na)
        a5 =  best_fit[index]['variables']['naf'][1]
        a4 =  1-a5
        a6 =  best_fit[index]['variables']['naf'][2]
        a7 =  best_fit[index]['variables']['naf'][3]
        Y  =  a4 + a5/(1 + np.exp((X-a6)/a7) )  # best_fit[index]['var']['naf'][0]*
        aN.plot(X,Y, color=color, alpha=0.5)
        aN.set_ylim([0,2])
        
        # Potassium (K)
        a5 =  best_fit[index]['variables']['kaf'][1]
        a4 =  1
        a6 =  best_fit[index]['variables']['kaf'][2]
        a7 =  best_fit[index]['variables']['kaf'][3]
        Y  =  a4 + a5/(1 + np.exp((X-a6)/a7) )  # best_fit[index]['var']['kaf'][0]*
        aK.plot(X,Y, color=color, alpha=0.5)
        aK.set_ylim([0,2])
        
    
    ad.set_xticks(x)
    ad.set_xticklabels(parameters, fontsize=20, rotation=45) 
    
    fd.savefig( 'Figures/distribution_variables_'+cell+'.png', transparent=True)
    fd2.savefig('Figures/distribution_variables_naf_kaf_'+cell+'.png', transparent=True )  


def make_array(best_fit):
    
    # restructure features storage from dict to array -> make pandas frame -> calc pair vice correlation
    
    N = len(best_fit)
    
    
    
    first = True
    for solution in best_fit:
    
        # (re-)initiate feature list
        features = []
        
        # loop over features
        for channel in reversed(['c33', 'c32', 'can', 'sk', 'kir', 'kas', 'kaf', 'naf']):
            
            # add features to list
            features = features + best_fit[solution]['variables'][channel]
        
        if first:
            # initiate array
            A = np.zeros( (N, len(features)) )
            first = False
        
        # add features to array
        A[solution,:] = features
    
    return A





def plot_pairvice_scatter(A, ax, cell, ranges=None):
    
    l = len(A[0,:])
    
    if not ranges:
        ranges = [  [-0.5,0.5], \
                    [0.8,1.0], \
                    [10.0,60.0],   \
                    [1.0,30.0] ,  \
                    [-0.5,0.5], \
                    [0.0,0.9], \
                    [1.0,130.0],   \
                    [-3.0,-70.0] ,  \
                    [-0.5,0.5],    \
                    [-5.0,60.0],  \
                    [1.0,70.0] , \
                    [-0.5,0.5] , \
                    [-0.5,0.5] , \
                    [-7.0,-5.0], \
                    [0.8,1.0], \
                    [10.0,60.0],   \
                    [1.0,30.0] ,    \
                    [-9.0,-6.0], \
                    [1.0,130.0],   \
                    [-3.0,-70.0] , \
                    [-9.0,-6.0], \
                    [1.0,130.0],   \
                    [-3.0,-70.0] ]
    
    if cell == 'dSPN':
        
        for i in range(l):
            
            for j in range(l):
                if j < i:
                    ax[i,j].scatter(A[:,i],A[:,j], color = '#4d97ff')
                    ax[i,j].set_xlim(ranges[i])
                    ax[i,j].set_ylim(ranges[j])
                    ax[i,j].set_yticks([])
                    ax[i,j].set_xticks([])
                elif j == i:
                    ax[i,j].axis('off')
                else:
                    break
                    
    else:
        
        for i in range(l):
            for j in range(l):
                if j > i:
                    ax[i,j].scatter(A[:,i],A[:,j], color = '#4337b5')
                    ax[i,j].set_xlim(ranges[i])
                    ax[i,j].set_ylim(ranges[j])
                    ax[i,j].set_yticks([])
                    ax[i,j].set_xticks([])
                else:
                    continue
    
    
        


def calc_pairvice_corr(best_fit_list):
    
    # creates arrays, calcs pair vice correlation and plots.
    #
    # best_fit_list = list of structures holding the data, i.e [best_fit_dSPN, best_fit_iSPN]
    
    f1,a1   = plt.subplots(1,1, figsize=(14,14) )
    f2,a2   = plt.subplots(1,1, figsize=(14,14) )
    f5,a5   = plt.subplots(1,1, figsize=(14,14) )
    f6,a6   = plt.subplots(1,1, figsize=(14,14) )
    
    fig     = [f1,f2]
    ax      = [a1,a2]
    fig2    = [f5,f6]
    ax2     = [a5,a6]
    
    # make pd frame 
    columns     =   ['naf0', 'naf1', 'naf2', 'naf3', \
                     'kaf0', 'kaf1', 'kaf2', 'kaf3', \
                     'kas0',         'kas2', 'kas3', \
                     'Kir', \
                     'sk',  \
                     'can0', 'can1', 'can2', 'can3',\
                     'c32-0', '32-2', '32-3',\
                     'c33-0', '33-2', '33-3'] 
    
    for b, best_fit in enumerate(best_fit_list):
        
        A = make_array(best_fit)
        
        # second array without can and c32
        B = np.delete(A, [9,10]+range(13,20)+[21,22], 1)
        
        if b == 0:
            
            cell = 'dSPN'
        
            # specify axis for pair vice correlation
            
            l       = len(A[0,:])
            f3,a3   = plt.subplots(l,l,   figsize=(30,20) )
            lb      = len(B[0,:])
            f4,a4   = plt.subplots(lb,lb, figsize=(30,20) )
            
            for i in range(l):
                a3[i,0  ].set_ylabel( columns[i], fontsize=24, rotation=0 )
                a3[l-1,i].set_xlabel( columns[i], fontsize=24, rotation=45)
            i = 0
            col2 = []
            for chan in columns:
                if chan in ['kas2', 'kas3','can0', 'can1', 'can2', 'can3', 'c32-0', '32-2', '32-3', '33-2', '33-3']:
                    continue
                a4[i,0   ].set_ylabel( chan, fontsize=24, rotation=0 )
                a4[lb-1,i].set_xlabel( chan, fontsize=24, rotation=45)
                i += 1
                col2.append(chan)
                
        else:
            cell = 'iSPN'
        
    
        # make pandas frame
        df  = pd.DataFrame(A, columns=columns)
        df2 = pd.DataFrame(B, columns=col2)
        
        # calc pair vice correlation and plot in hinton diagram
        corrmat     = df.corr()
        corrmat2    = df2.corr()
        
        plot.hinton(corrmat,  ax=ax[ b])  
        plot.hinton(corrmat2, ax=ax2[b])
        
        fig[ b].savefig( 'Figures/correlation_hinton_'+cell+'.png', transparent=True)
        fig2[b].savefig( 'Figures/correlation_hinton_'+cell+'small.png', transparent=True)
        
        # plot scatter plots       
        plot_pairvice_scatter(A, a3, cell)
        plot_pairvice_scatter(B, a4, cell, ranges = [   [-0.5,0.5], \
                                                        [0.8,1.0], \
                                                        [10.0,60.0],   \
                                                        [1.0,30.0] ,  \
                                                        [-0.5,0.5], \
                                                        [0.0,0.9], \
                                                        [1.0,130.0],   \
                                                        [-3.0,-70.0] ,  \
                                                        [-0.5,0.5],    \
                                                        [-0.5,0.5] , \
                                                        [-0.5,0.5] , \
                                                        [-9.0,-6.0]])
    
    f3.savefig('Figures/distribution_var_scatter.png', transparent=True)
    f4.savefig('Figures/distribution_var_scatter_small.png', transparent=True)
    
    
    
          
best_fit1   = load_obj('Libraries/D1_71bestFit.pkl')
best_fit2   = load_obj('Libraries/D2_34bestFit.pkl')
''' 
calc_pairvice_corr( [best_fit1,best_fit2] )

plt.show()
#plt.close('all')

'''
#  specify fig axes
fig_ca,aca      = plt.subplots(1,1, figsize=(10,10) )
fig_fi,afi      = plt.subplots(1,1, figsize=(10,10) )
fvm,avm         = plt.subplots(1,1, figsize=(12,8)  )
fig_p,ax_p      = plt.subplots(1,1, figsize=(10,10) )
fig_p2,ax_p2    = plt.subplots(1,1, figsize=(10,10) )



plot_FI_bAPdCa(best_fit1, cell='D1', axes=[aca,afi])
plot_FI_bAPdCa(best_fit2, cell='D2', axes=[aca,afi])
configure_plot_window(afi, aca, \
                          plot_best_fit=False, \
                          single_type=False)
fig_ca.savefig('Figures/subpops_bAP.png', transparent=True)
fig_fi.savefig('Figures/subpops_FI.png',  transparent=True)

plot_Vm(best_fit1, cell='D1', axes=[fvm,avm])
plot_Vm(best_fit2, cell='D2', axes=[fvm,avm])

traces1     = load_obj('Pickles/Plateaus_D1.pkl')
traces2     = load_obj('Pickles/Plateaus_D2.pkl')

plot_plateau(traces1, axes=[ax_p,ax_p2], cell='dSPN')
plot_plateau(traces2, axes=[ax_p,ax_p2], cell='iSPN')


def_axis(avm)
def_axis(ax_p)
def_axis(ax_p2)

# set ticks
ax_p.set_xlim([-30, 150])
yticks = np.arange(-80,-5,10)
ax_p.set_yticks(yticks)
ax_p.set_yticklabels(yticks, fontsize=30)
xticks = np.arange(0,160,50)
ax_p.set_xticks(xticks)
ax_p.set_xticklabels(xticks, fontsize=30)

ax_p2.set_xlim([-30, 150])
yticks = np.arange(-80,-5,10)
ax_p2.set_yticks(yticks)
ax_p2.set_yticklabels(yticks, fontsize=30)
xticks = np.arange(0,160,50)
ax_p2.set_xticks(xticks)
ax_p2.set_xticklabels(xticks, fontsize=30)

avm.set_xlim([0, 1000])
yticks = np.arange(-80,50,20)
avm.set_yticks(yticks)
avm.set_yticklabels(yticks, fontsize=30)
xticks = np.arange(0,1010,200)
avm.set_xticks(xticks)
avm.set_xticklabels(xticks, fontsize=30)

# legend
ax_p.legend(loc='best', fontsize=30)

fig_p.savefig( 'Figures/plateaus_local.png', transparent=True)
fig_p2.savefig('Figures/plateaus_soma.png',  transparent=True)
fvm.savefig('Figures/voltagetrace_examples_.png',  transparent=True)

# plot extracted features
data = [[-42.73, 2.65, -44.29, 4.44],
        [78.75, 7.73, 78.77, 9.42],
        [0.77,  0.10, 0.75,  0.11,],
        [-78.65, 4.75, -78.91, 3.93],
        [0.12, 0.08, 0.15, 0.09],
        [85.26, 48.77, 96.44, 48.97],
        [5.16, 2.39, 5.98, 3.00] ]
        
calculated_values = [[44.16, 10.24, 51.60, 12.12],
                     [4.67, 1.39, 4.90, 1.46]]

features1     = load_obj('Pickles/FEATURES_D1.pkl')   
features2     = load_obj('Pickles/FEATURES_D2.pkl')                 
plot_feature_comparison(features1, data, 'D1', cal_data=calculated_values)
plot_feature_comparison(features2, data, 'D2', cal_data=calculated_values)

# random parameter distribution
best_fit_parameter_distribution(best_fit1, 71, cell='D1')
best_fit_parameter_distribution(best_fit2, 34, cell='D2')

plt.close('all')
   
  
   
   
   
   
   
        

