#!/bin/bash -l


#sbash pgenesis_Ri.job <nodes> <pfile> <TES/DOP> <rcr> <freq>

ARG0=0
while [  $ARG0 -lt 10 ]; do

    ARG1=10
    while [  $ARG1 -lt 51 ]; do

        ARG2=30
        while [  $ARG2 -lt 71 ]; do
            
            echo $ARG0 $ARG1 $ARG2 
            
            # start simulations
            python morphology_functions.py $ARG0 $ARG1 $ARG2
            
            # increase ARG2
            let ARG2=ARG2+20 
            
        done

        let ARG1=ARG1+10

    done
    
    let ARG0=ARG0+1

done

