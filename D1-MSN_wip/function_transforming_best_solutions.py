
import common_functions as use
import json
import numpy as np


def reshape_best(cell_type):
    
    BEST      = use.load_obj(cell_type+'_30bestFit.pkl')
    
    if cell_type == 'D1':
        params  =   './params_dMSN.json'
    else:
        params  =   './params_iMSN.json'

    with open(params) as file:
        par = json.load(file)    

    res = {}

    for key in BEST:
        res[key] = {}
        for key2 in BEST[key]['variables']:
            
            if key2 in ['rheobase', 'tm', 'vm']:
                continue
            
            # def data
            data = BEST[key]['variables'][key2]
            
            # rename key    
            if   key2 == 'c32':
                k2 = 'cav32'
            elif key2 == 'c33':
                k2 = 'cav33'
            else:
                k2 = key2
            
            # formula for maximal conductance = base * gmax
            base        = np.power( 10, data[0] )
            if key2[0] != 'c':
                gmax    = float(par['gbar_' + k2 + '_basal']['Value'])
            else:
                gmax    = 1.0
            
            print key, key2, gmax, base
            
            # set content function, formula, parameters
            if key2 in ['sk', 'kir']:
                func    = 'uniform'
                form    = 'p0'
                p       = [ base * gmax ]
            else:
                func    = 'sigmoidal'
                form    = 'p0*(p1 + p2/( 1 + exp{(dist-p3)/p4} ))'
                
                if   key2 == 'naf':
                    p       = [ base*gmax, 1-data[1], data[1], data[2], data[3] ]
                elif key2 == 'kaf':
                    p       = [ base*gmax, 1,         data[1], data[2], data[3] ]
                elif key2 == 'kas':
                    p       = [ base*gmax, 0.1,       0.9,     data[1], data[2] ]
                elif key2 == 'can':
                    p       = [ base,      1-data[1], data[1], data[2], data[3] ]
                elif key2 == 'c32':
                    p       = [ base,      0,         1,       data[1], data[2] ]
                elif key2 == 'c33':
                    p       = [ base,      0,         1,       data[1], data[2] ]
            
            # save to structure    
            res[key][k2] = {    'parameters': p,    \
                                'formula': form,    \
                                'function': func,   \
                                'gmax': gmax        }
    
    # save
    with open('bestSolutions_'+cell_type+'.json', 'w') as outfile:
        json.dump(res, outfile)
        

for spn in ['D1', 'D2']:                                
    reshape_best(spn)
    
