#
'''
MSN model used in Lindroos et al., (2018). Frontiers

Robert Lindroos (RL) <robert.lindroos at ki.se>
 
The MSN class and most channels were implemented by 
Alexander Kozlov <akozlov at kth.se>
with updates by RL

Implemented in colaboration with Kai Du <kai.du at ki.se>
'''



from __future__ import print_function, division
from neuron import h
#from joblib import Parallel, delayed
#import multiprocessing
import numpy                as np
import matplotlib.pyplot    as plt
import plot_functions       as fun
import MSN_builder          as build
import glob
import pickle
import sys



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


# global result dicts
RES     = {}

# D1

D1_params =   [[0.5739438400147591, 114.46525970607432, -24.778683242914425, 0.9656307062939439, 40.55882910975278, 4.924556428134727],    \
                [0.6486015000884895, 37.19526177848555, -51.44257878295532, 0.9199040036041007, 38.773590273202544, 4.138269856798566],     \
                [0.34835213066115794, 92.06973911679478, -13.043854781662562, 0.9812668369096036, 35.31855413027535, 4.764644405812756],    \
                [0.8115471666044635, 44.7944466983775, -51.05269981676671, 0.9310487434283193, 40.02623282398899, 3.698685454575806],       \
                [0.5009368290963206, 26.47283142378077, -40.15544309999925, 0.9376314551458907, 34.918864668708565, 5.689649054327928],     \
                [0.11337316688781601, 74.85203079871395, -37.663738588683266, 0.9791107718457724, 40.313411748972555, 1.7599610703626931],  \
                [0.7178317755261724, 55.78999694946826, -40.594540879418844, 0.934360843812911, 24.039770691171, 11.85002736955439],        \
                [0.7084541798016345, 60.5127880861534, -55.54871428461576, 0.9063982542677244, 44.50455979565052, 1.2727982786963663],      \
                [0.6035708031273619, 46.80870702319583, -64.09248569745006, 0.9280326409654382, 37.58186874484719, 2.729291505716056],      \
                [0.561072695250727, 30.279814655445957, -5.915582850103075, 0.9240749040725246, 47.175148898308336, 3.9786457316868438],    \
                [0.7084541798016345, 60.5127880861534, -55.54871428461576, 0.9063982542677244, 44.50455979565052, 1.2727982786963663] ]

D1_rheobase = [360, 400, 360, 420, 400, 320, 420, 400, 400, 400, 400, 400]

# D2
D2_params =   [[0.36519026711426983, 85.9950693571762, -29.308683882971575, 0.9148525825598594, 31.715617529156468, 14.265211741761483],   \
                [0.16856005736302448, 126.23264636214776, -24.844746770683354, 0.9321044414025511, 30.258105136928478, 16.71932312947103],  \
                [0.8998189135933885, 119.36936307317013, -46.95178922978055, 0.8971817650810714, 33.820535805627614, 13.259594919674436],   \
                [0.3212471483859052, 73.2110386772533, -21.537721797784236, 0.9086172801642267, 36.981483525375666, 10.22848048793786],     \
                [0.5820977302857862, 57.946759035757786, -18.187141446438467, 0.8928497341805598, 46.779147877476554, 11.121000433080683],  \
                [0.46297432359551316, 54.020547269954754, -41.038717738089424, 0.9112928426829239, 23.74013590665613, 21.09496685904177],   \
                [0.20862895117356128, 14.373675087979382, -69.54059375795693, 0.9336951592433631, 43.40456122099932, 15.291555103979722],   \
                [0.42405610469134825, 2.083973089688305, -64.58731908139677, 0.9006809103056398, 31.54569527007518, 11.894971817963789],    \
                [0.2935952963686902, 84.25383011698389, -43.048666783931345, 0.9299719258473008, 42.89522927693975, 14.821301506581849],    \
                [0.813828871477326, 84.51778812354367, -22.93434820961655, 0.9024356980563085, 36.356176317731936, 16.866648293828433] ]
    
D2_rheobase = [220, 200, 220, 220, 220, 220, 200, 220, 200, 240]



def save_vector(t, v, outfile):
    
    with open(outfile, "w") as out:
        for time, y in zip(t, v):
            out.write("%g %g\n" % (time, y))                     
 
                  
# 'save/'+ 
def save_obj(obj, name ):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

        
def load_obj(name ):
    with open(name, 'rb') as f:
        return pickle.load(f)
        






def main(   par="./params_iMSN.json",               \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            sim='vm',                               \
            amp=0.265,                              \
            run=None,                               \
            distParams=None,                        \
            simDur=7000,                            \
            stimDur=900                             ):
            
    
    if distParams:
        # load best fit parameters
        random_variables = distParams
    else: 
        # load "random" variables
        random_variables = D2_params[run]
        
        
    # initiate cell
    cell = build.MSN(params=par, morphology=morphology, variables=random_variables)
    
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
       
    
    # set edge of soma as reference for dendritic distance 
    h.distance(1, sec=h.soma[0])
    
    
    # set current injection
    stim        =   h.IClamp(0.5, sec=cell.soma)
    stim.amp    =   amp  
    stim.delay  =   100
    stim.dur    =   stimDur    
     
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    tstop       = simDur 
    # dt = default value; 0.025 ms (25 us)
                  
    
    # set pointers; need since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    for sec in h.allsec():
        
        for seg in sec:
            
            
            # naf and kas is in all sections
            h.setpointer(pointer, 'pka', seg.kas )
            h.setpointer(pointer, 'pka', seg.naf )
            
            if sec.name().find('axon') < 0:    
                
                
                # these channels are not in the axon section
                h.setpointer(pointer, 'pka', seg.kaf )
                h.setpointer(pointer, 'pka', seg.cal12 )
                h.setpointer(pointer, 'pka', seg.cal13 )
                h.setpointer(pointer, 'pka', seg.kir )
                
                if sec.name().find('soma') >= 0:
                    
                    
                    # N-type Ca (can) is only distributed to the soma section
                    h.setpointer(pointer, 'pka', seg.can )
            
    
              
    # solver------------------------------------------------------------------------------            
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop:
                
        h.fadvance()
        
    
    # save output ------------------------------------------------------------------------
    
            
        
    c               = int(amp*1000)                
    RES[run][c]     = vm
    spikes          = fun.getSpikedata_x_y(tm,vm)
    
    if len(spikes) > 0:
        RES['tm'] = tm
        return False
    else:
        return True
        
        
        
                


# Start the simulation.
# Function needed for HBP compability  ===================================================
if __name__ == "__main__":
    
    
    print('starting sim')
    
    
    ct = 1
    
    rheobase_currents = []
    
    if ct == 1:
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        parameters  =   D1_params
        currents    =   np.arange(320,425,20)
        rheobase    =   D1_rheobase
        
    elif ct == 2:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        parameters  =   D2_params
        currents    =   np.arange(200,265,20)
        rheobase    =   D2_rheobase
    
     
    
    if len(sys.argv) > 1 and \
        sys.argv[1] == 'plot':
        
        RES = load_obj('Vm_20pA.pkl' )
        
    else:
        
        # numer of iterations:
        for n in range(len(parameters)):
            
            RES[n] = {}
            
            c           =   0
            
            not_spiking =   True
            
            if sys.argv[1] == 'rheoBase':
                # find finer rheobase current if know in interval of 20 pA
                
                RES_20 = load_obj('Results/Vm_'+cell_type+'_20pA.pkl' )
                
                for i in range(2):
                    
                    if i == 0:
                        # first run; half interval
                        current     =   rheobase[n] - 10
                        
                    elif not_spiking:
                        # second run and no spike in first -> increase current (5 pA)
                        current     =   current + 5
                        
                    else:
                        # second run and spike in first run -> decrease current (5 pA)
                        current     =   current - 5
                    
                    not_spiking = main(     par=par,    \
                                            amp=current*1e-3,           \
                                            run=n,                      \
                                            simDur=1000,                \
                                            distParams=parameters[n],   \
                                            morphology=morphology,      \
                                            stimDur=900                 )
                    
                    print(rheobase[n], i, current, not_spiking)
                    
                    
                if not_spiking:
                    C       =   current+5     
                    
                else:
                    C       =   current
                    
                not_spiking = False
                counter     = 0
                current     = C
                 
                while not not_spiking: 
                    current     -= 1
                    counter     += 1
                    
                    not_spiking  = main(par=par,    \
                                        amp=current*1e-3,           \
                                        run=n,                      \
                                        simDur=1000,                \
                                        distParams=parameters[n],   \
                                        morphology=morphology,      \
                                        stimDur=900                 )
                    
                    print(current, not_spiking)
                
                C = current+1    
                rheobase_currents.append(C)
                
                # remove all traces but the one that spikes    
                if C in RES[n]:
                    RES[n]  =   {C: RES[n][C]}
                else:
                    RES[n]  =   {C: RES_20[n][C]}
                    print('from old file RES_20', C)
                
                save_obj( RES, 'Results/Vm_'+cell_type+'_1pA' )
                print('-----------------------')
            
            else:
                
                # find rheobase in 20 pA interval
                current     =   currents[c]
                while ( current <= currents[-1] and not_spiking ):
                    
                    not_spiking = main(     par=par,    \
                                            amp=current*1e-3,           \
                                            run=n,                      \
                                            simDur=1000,                \
                                            distParams=parameters[n],   \
                                            morphology=morphology,      \
                                            stimDur=900                 )
                            
                    
                    if not not_spiking:
                        print('rheobase', current)
                        rheobase_currents.append(current)
                        
                    if c < len(currents) -1:
                        c          +=   1
                        current     =   currents[c]
                    else:
                        break
                
                save_obj( RES, 'Vm_20pA' )
                
            
        print(rheobase_currents)
            
           
                            
    fig, ax = plt.subplots(2,6, figsize=(16, 5) )   
    tm      = RES['tm'] 
    for i in range(len(parameters)):
        
        if i < 6:
            a1=0
            a2=i
        else:
            a1=1
            a2=i-6
        
        print( a1, a2)
        
        for c in RES[i]:
            vm = RES[i][c]
            ax[a1,a2].plot(tm, vm, 'k', lw=2)
    
    plt.show()
        
                                                               
                                                    
                                                    
                                                    
    
    
    
          
    
        

