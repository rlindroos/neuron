from __future__ import print_function, division
from neuron import h
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import pickle
import pandas               as pd

from joblib import Parallel, delayed
import multiprocessing

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
import matplotlib.pyplot as plt


def main(cell_index, model_sets):
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    tstop       =   1000
    cvode       =   h.CVode()
                    # load parameters and random set
    parameters  =   model_sets[cell_index]['variables'] 
    rheobase    =   model_sets[cell_index]['rheobase']
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
    res = {'rheobase':rheobase}
    
    for modulation_percentage in [0, 50, 100]:
        
        res[modulation_percentage] = {}
       
        modulation_factors = use.get_fixed_modulation_factors(  cell_type,                  \
                                                                mod_list,                   \
                                                                modulation_percentage       )
        
        
        # initiate cell ------------------------------------------------------------------
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=parameters         )
        
        # set cascade 
        #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        use.set_pointers(cell, pointer, mod_list)
        
        # da mod
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )
        
        for shift in [-20, 0, 20]:
            
            # set current injection
            Istim        =   h.IClamp(0.5, sec=cell.soma)
            Istim.delay  =   100
            Istim.dur    =   1000
            Istim.amp    =   (rheobase+shift) *1e-3
            
                
            # record vectors
            tm  = h.Vector()
            tm.record(h._ref_t)
            vm  = h.Vector()
            vm.record(cell.soma(0.5)._ref_v)
            
            
            # run simulation
            h.finitialize(-70)
            
            # run simulation
            while h.t < tstop:
                h.fadvance()
            
            res[modulation_percentage][shift] = use.getSpikedata_x_y( tm, vm )
                        
    return res

def pie_autopct(pct):
    #print pct
    return ('%1.0f' % pct) if pct > 10 else ''
    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # load parameters and random set
    '''
    path_to_lib     = './Libraries/'
    model_sets      =   use.load_obj(path_to_lib+'D1_71bestFit.pkl')
    
    num_cores       = multiprocessing.cpu_count()  
        
    R = Parallel(n_jobs=num_cores)(delayed(main)( cell_index, model_sets ) for cell_index in model_sets  )
    
    #R = use.load_obj('Maximum_DA_modulation_comparison.pkl')
    use.save_obj(R, 'checking_DAmod_direction' )'''
    
    R       = use.load_obj('checking_DAmod_direction.pkl')
    array   = np.zeros((len(R)*9,4))
    a       = 0
    
    fig,ax  = plt.subplots(4,1, figsize=(6,16) )
    y       = np.zeros((3))
    x       = [1,2,3]
    quant   = []
    skvallra = {'le':{'n':0, 'l':[]}, 'eq':{'n':0, 'l':[]}, 'mo':{'n':0, 'l':[]}}
        
    for r,res in enumerate(R):
        
        rheobase = res['rheobase']
        
        for i,shift in enumerate([-20, 0, 20]):
            
            for j,modulation_percentage in enumerate([0, 50, 100]):
                
                y[j]            =   len(res[modulation_percentage][shift])
                
                array[a, 0]     =   rheobase
                array[a, 1]     =   modulation_percentage
                array[a, 2]     =   shift
                array[a, 3]     =   len(res[modulation_percentage][shift])
                
                a += 1
                     
                
            # plotting
            
            if   y[0] <  y[2]:      
                color = 'g'; lw = 1
                if shift == 0:
                    quant.append(0)
            elif y[0] == y[2]:      
                color = 'b'; lw = 1
                if shift == 0:
                    quant.append(1)
                    skvallra['eq']['n'] += 1
                    skvallra['eq']['l'].append( r )
            else:                   
                color = 'r'; lw = 2
                if shift == 0:
                    quant.append(2)
                    skvallra['le']['n'] += 1
                    skvallra['le']['l'].append( r )
            
                
            
            ax[i].plot(x, y, '-o', color=color, ms=10, alpha=0.5, mec='w', mew=2, lw=2)
    
    #ax[3].hist(quant, bins=3, range=(0,4))
    N = np.bincount(quant)
    print ( N)    
    ax[3].pie(N, colors=['g', 'b', 'r'],
        autopct=pie_autopct, shadow=False)            
    
    ax[3].axis('equal')
    ax[3].set_xlabel('Eq: ('+str(skvallra['eq']['n'])+
                            ') '+
                            ' '.join(str(x) for x in skvallra['eq']['l'])+
                            ';\nLess: ('+
                            str(skvallra['le']['n'])+') '+
                            ', '.join(str(x) for x in skvallra['le']['l']), 
                            fontsize=24 )
    fig.savefig('Figures/excitability_underDA_D1_71bestFit.png')
    
    plt.show()
    #df = pd.DataFrame( data=array )    
                
    
    
        
    
    
        
    
        
    
