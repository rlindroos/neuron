#
# MSN model
#
# Robert Lindroos
# 
# Original version by 
# Alexander Kozlov <akozlov@kth.se>
# Kai Du <kai.du@ki.se>
#

from __future__ import print_function, division
from neuron import h
import numpy                as np
import matplotlib.pyplot    as plt
import MSN_builder          as build
import common_functions     as use
import sys
import json
import pickle



with open('./substrates.json') as file:
    SUBSTRATES = json.load(file)



#h.nrn_load_dll('/pdc/vol/neuron/7.4-py27/x86_64/.libs/libnrnmech.so')
h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')



# global result dict
RES = {}

                  
   
#=========================================================================================



def main(   par="./params_iMSN.json",                   \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',    \
            run=None,       \
            modulation=1,   \
            simDur=2000,    \
            randMod=None,   \
            glutMod=1,      \
            gabaMod=1,      \
            chanMod=1,      \
            testMode=False, \
            target=None,    \
            chan2mod=['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can'] ): 
    
    
    print(locals())
    
    
    
    # initiate cell
    #     single point soma. somatic conductances scaled accordingly.
    cell    =   build.MSN(  params=par )
                            
                            
    # all channels to modulate
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    
    
    # check if some channel(s) are omitted from chan2mod. If so, add to not2mod.
    not2mod     = []
    
    for chan in mod_list:
        
        if chan not in chan2mod:
            
            not2mod.append(chan)
                            
                            
        
    # set cascade ---- move to MSN def?
    casc = h.D1_reduced_cascade2_0(0.5, sec=cell.soma) # other cascades also possible...
    
    
    if target:
        cmd = 'pointer = casc._ref_'+target
        exec(cmd)
        
        base_mod    = SUBSTRATES[target][0]
        max_mod     = SUBSTRATES[target][1] # if run cascade 1.5 s: 2187.86794857 # 
        
    else:
        pointer     = casc._ref_Target1p    #Target1p   #totalActivePKA    (if full cascade used)
        base_mod    = casc.init_Target1p
        max_mod     = 2317.1
    
    
    
    # set pointer (for all channel instances in mod_list)
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
    
    
    # record vectors
    tm = h.Vector()
    tm.record(h._ref_t)
    vm = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    
    
    # substrates
    if run == 0:
        pka = h.Vector()
        pka.record(casc._ref_Target1p)
        camp = h.Vector()
        camp.record(casc._ref_cAMP)
        gprot = h.Vector()
        gprot.record(casc._ref_D1RDAGolf) #D1RDAGolf
        gbg   = h.Vector()
        gbg.record(casc._ref_Gbgolf) #Gbgolf
        
    
    
    # peak n dipp parameters
    da_peak   = 500     # concentration     [nM]
    da_tstart = 1000    # stimulation time  [ms]
    da_tau    = 500     # time constant     [ms]
    
    
    tstop = simDur      # simulation length [ms]
    
    
    
    
    # select (random) modulation factors for channels ====================================
    if randMod == 1:
    
        # new factors every run
        mod_fact = [    np.random.uniform(0.60,0.80),   \
                        np.random.uniform(0.65,0.85),   \
                        np.random.uniform(0.75,0.85),   \
                        np.random.uniform(0.85,1.25),   \
                        np.random.uniform(1.00,2.00),   \
                        np.random.uniform(1.00,2.00),   \
                        np.random.uniform(0.00,1.00)        ]
    else:
        mod_fact = [ 0.8, 0.8, 0.8, 1.25, 2.0, 2.0, 0.5  ]
           
    
    
                    
                    
    # synaptic modulation ================================================================
    
    # draw random modulation factors; possible to draw directly in right interval, 
    #       but then not coherent with static mod regarding saving
    glut_f  =   [   np.random.uniform(0.9,1.6),     \
                    np.random.uniform(0.9,1.6)      ] 
    
    gaba_f  =   [   np.random.uniform(0.6,1.4)      ]
    
    syn_fact  = glut_f + gaba_f
        
    # rebase factors for use in dynamic modulation
    factors             = [f-1 for f in mod_fact]
    synaptic_factors    = [f-1 for f in syn_fact]
    
    
    
    # result dicts    
    I_d = {}
    ns  = {}
    nc  = {}
    Syn = {}
    
    
    # create synapses and set modulation
    for sec in h.allsec():
        
        if sec.name().find('dend') >= 0:
            
            # create a glut synapse in midle of section (0.5)
            use.random_synapse(ns, nc, Syn, sec, 0.5, 0,        \
                                    NS_interval=1000.0/28.0,    \
                                    NC_conductance=0.50e-3,     \
                                    NS_start=200)
                                    
            # create a gaba synapse in proximal part of section (0.0)
            use.random_synapse(ns, nc, Syn, sec, 0.0, 0,        \
                                    Type='tmgabaa',             \
                                    NS_interval=1000.0/7.0,     \
                                    NC_conductance=1.50e-3,     \
                                    S_tau_dep=100,              \
                                    NS_start=100      )
            
            
            # set pointer(s)
            h.setpointer(pointer, 'pka', Syn[sec.name()+'_glut'])
            h.setpointer(pointer, 'pka', Syn[sec.name()+'_gaba'])
            
            
            
            #randMod?
            if glutMod == 1:
                
                # normalize modulation range to substrate concentration
                Syn[sec.name()+'_glut'].base    = base_mod
                Syn[sec.name()+'_glut'].maxDiff = (max_mod - base_mod)
                # set max modulation
                Syn[sec.name()+'_glut'].f_ampa  = synaptic_factors[0]
                Syn[sec.name()+'_glut'].f_nmda  = synaptic_factors[1]
            else:
                Syn[sec.name()+'_glut'].f_ampa  = 0
                Syn[sec.name()+'_glut'].f_nmda  = 0
            
            if gabaMod == 1:
                Syn[sec.name()+'_gaba'].base    = base_mod
                Syn[sec.name()+'_gaba'].maxDiff = (max_mod - base_mod)
                Syn[sec.name()+'_gaba'].f_gaba  = synaptic_factors[2]
            else:
                Syn[sec.name()+'_gaba'].f_gaba  = 0
            
            
            # plotting currents--remove?    
            if not 'a' in I_d:
                I_d['a'] = 1
                               
                glut = h.Vector()
                glut.record(Syn[sec.name()+'_glut']._ref_i)
                gaba = h.Vector()
                gaba.record(Syn[sec.name()+'_gaba']._ref_i)
        
        
        elif sec.name().find('axon') >= 0: 
            
            # don't modulate the axon
            continue   
        
        if chanMod == 1:
            
            # activate dynamic modulation of intrinsic channels
            for seg in sec:
                
                for mech in seg:
                    
                    if mech.name() in not2mod:
                        
                        # no modulation of mech in not2mod
                        mech.factor = 0.0
                        print(mech.name(), 'not modulated:', not2mod, mech.factor, sec.name())
                        
                    elif mech.name() in mod_list:
                    
                        # normalize range
                        mech.base       = base_mod
                        mech.maxDiff    = (max_mod - base_mod)
                        # set max modulation
                        index           = mod_list.index( mech.name() )
                        mech.factor     = factors[index]
                    
    
    # plot dynamic modulation factor--remove?                
    mod = {}
    for chan in mod_list:
        cmd = 'chanRef = cell.soma(0.5).'+chan+'._ref_mod'
        exec(cmd)
        mod[chan]   = h.Vector()
        mod[chan].record(chanRef) #Gbgolf
    
    mod['ampa'] = h.Vector()
    mod['ampa'].record(Syn[sec.name()+'_glut']._ref_modA)
    mod['nmda'] = h.Vector()
    mod['nmda'].record(Syn[sec.name()+'_glut']._ref_modN)
    mod['gaba'] = h.Vector()
    mod['gaba'].record(Syn[sec.name()+'_gaba']._ref_mod)
    
    cav32 = h.Vector()
    cav32.record(sec(0.5).cav32._ref_ical)
    
    cat32 = h.Vector()
    cat32.record(sec(0.5).cat32._ref_ical)
    
    
    # solver------------------------------------------------------------------------------            
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop:
    
        if modulation == 1:
        
            if h.t > da_tstart: 
                
                # set DA and ACh values (using alpha function)
                casc.DA = use.alpha(h.t, da_tstart, da_peak, da_tau) 
                
        h.fadvance()
    
    
    
    
    # plot results--remove? -------------------------------------------------------------
    F = mod_fact+syn_fact  
    C = mod_list+['ampa', 'nmda', 'gaba']
    
    if testMode == 'spiking':
        
        use.save_vector(tm, vm, ''.join(['spiking_', str(run), '.out']) )
        
        if run == 0:
            targets = ['Target1p', 'cAMP', 'Gbgolf',  'D1RDAGolf']
            use.save_vector(tm, pka,   ''.join(['substrate_', targets[0] , '.out']) )
            use.save_vector(tm, camp,  ''.join(['substrate_', targets[1] , '.out']) )
            use.save_vector(tm, gbg,   ''.join(['substrate_', targets[2] , '.out']) )
            use.save_vector(tm, gprot, ''.join(['substrate_', targets[3] , '.out']) ) 
        
    elif testMode == 'control':
        
        use.save_vector(tm, vm, ''.join(['../BugFixedFig6Sim/test_', str(run), '.out']) )
        
    elif testMode:
        
        plt.close('all')
        fig, ax = plt.subplots(4,1, figsize=(20,25))
        
        colors = ['#bf5b17', '#4daf4a', '#984ea3', '#ff7f00']
        ax[0].plot(tm, np.divide(pka,     max(pka)),  lw=3, color=colors[0] )
        ax[0].plot(tm, np.divide(camp,   max(camp)),  lw=3, color=colors[1] )
        ax[0].plot(tm, np.divide(gbg,     max(gbg)),  lw=3, color=colors[2] )
        ax[0].plot(tm, np.divide(gprot, max(gprot)),  lw=3, color=colors[3] ) 
        
        colors = ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a']
        
        for c,chan in enumerate(C):
            print( chan, F[c], colors[c] )
            ax[1].plot(tm,mod[chan],  lw=4, color=colors[c], label=chan)
            ax[1].plot([1800,2200], [F[c], F[c]], '--', lw=3, color=colors[c])
        ax[1].legend(loc=1)
        ax[2].plot(tm,vm,   lw=3,  color='k')
        ax[3].plot(tm,cat32, lw=3,  color='k')
        ax[3].plot(tm,cav32, lw=3,  color='g')
        
    else:
        
        # save output
        
        spikes      = use.getSpikedata_x_y(tm,vm) 
        RES[run]    = {'factors': mod_fact + syn_fact, 'spikes': spikes}
        
        '''
        ran = np.random.randint(1,10000)
        use.save_vector(tm, vm, ''.join(['largeGABA_vm', str(ran), '.out']) )
        use.save_vector(tm, gaba2, ''.join(['largeGABA_mod', str(ran), '.out']) )
        use.save_vector(tm, gaba, ''.join(['largeGABA_cur', str(ran), '.out']) )'''
        
                



# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    print('starting sim')
    
    
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can']
    syn_mod  = ['amp', 'nmd', 'gab']
    
    # 
    allChan = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can']
    not2mod = '' 
    
    
    # find channels that should not be modulated
    for chan in allChan:
        
        if chan not in mod_list:
            
            not2mod += chan
    
    testMode = True
    glutMod  = 1
    gabaMod  = 1   
    chanMod  = 1
    
    if testMode:
        n_runs = 1
    else:
        n_runs = 400
        
    #targets = ['Target1p', 'cAMP', 'Gbgolf',  'D1RDAGolf']
    #for n,target in enumerate(targets):
    for n in range(n_runs):
        target='Target1p'
        main( par="./params_iMSN.json",         \
                    run=n,                      \
                    simDur=2000,                \
                    modulation=1,               \
                    randMod=1,                  \
                    glutMod=glutMod,            \
                    gabaMod=gabaMod,            \
                    chanMod=chanMod,            \
                    testMode=testMode,          \
                    target=target,              \
                    chan2mod=mod_list           )
        
        
        if not testMode and n % 10 == 0:  
            
            print('in save loop')  
                        
            mod_fact = RES[0]['factors']
            
            ID = ''
            
            for i,mech in enumerate(mod_list+syn_mod):
                
                ID = ID + mech + str( int(mod_fact[i]*100) )
            
            use.save_obj(RES, ''.join(['strongGaba_E', str(glutMod), '_I', str(gabaMod), '_C', str(chanMod), '_', target, '_', ID]) )
            #use.save_obj(RES, ''.join(['bugFixes_E', str(glutMod), '_I', str(gabaMod), '_C', str(chanMod), not2mod, '_', target, '_', ID]) )
            #use.save_obj(RES, ''.join(['bugFixes_E', str(glutMod), '_I', str(gabaMod), '_C', str(chanMod), 'long_', target, '_', ID]) )
        
    if testMode:
        plt.savefig('../../../Desktop/update_alpha.png')
        plt.show()
                               
                                                    
                                                     
                                                    
                                                    
    
    
    
          
    
        

