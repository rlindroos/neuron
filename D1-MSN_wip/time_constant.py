
# script used for calculating time constant in a similar fashion as the Silberberg lab.
#
# a 5 ms current pulse with amplitude of -0.5 to -1 nA.
# From the resulting voltage trace the time constant is extracted by fitting an 
#    exponential function to the rising phase.

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
from    scipy            import stats
from    scipy.optimize   import curve_fit
import glob
import pickle


# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        

def func(x, a, b, c):
    
    return a * np.exp( (x-b) / c )  
    



def find_time_constants(trace, t, ax=False):
    
    # make sure positive deflection by ... 
    shifted     =       np.subtract(trace, trace[0]) 
    pos         =       list(np.abs(shifted))
    
    # get peak value and index of peak
    Max         =       max(pos)
    maxIndex    =       pos.index(Max)
    
    # calc 10% and 90% 
    m10         =       Max * 0.1
    m90         =       Max * 0.9
    
    # fit decay to curve and get decay time constant---
    X           =       t[  maxIndex+10:]
    Y           =       pos[maxIndex+10:]
    popt, pcov  =       curve_fit(func, X, Y, p0=[100,Y[0],-10.0])
    
    if ax:
        y1  =   np.multiply( func(X, *popt), -1 )
        y   =   np.add( y1, trace[0] )
        ax.plot(X, y, 'k--', alpha=0.9, lw=2 )
    
    return popt[2]*-1  
     



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    


    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    T = 1
    
    if T == 1:                                           
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        norm        =   id
        N           =   71
        
                    
    elif T == 2:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        norm        =   id   
        N           =   34 
    
    else:
        print('cell type not understood')
        ghghghgh
    
    
    print(cell_type, par, morphology)
    
    import matplotlib.pyplot as plt
    fig,ax = plt.subplots(1,1, figsize=(10,10) )  
    
    cell_versions   = use.load_obj(cell_type+'_'+str(N)+'bestFit.pkl')
    
    
    
    Tau = []
    
    for i in range(N):
        
        # load parameters
        parameters      =   cell_versions[i]['variables'] # parameter set 0 (cell index)
        rheobase        =   cell_versions[i]['rheobase']


        # initiate cell
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=parameters         )
        
        # set cascade 
        #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        # Modulation of channels is not used in this script
        if cell_type=='D1':
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
        else:
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
        
        maxCond = set_pointers(cell, pointer, mod_list)
        
        # stimulation
        Istim        =   h.IClamp(0.5, sec=cell.soma)
        Istim.delay  =   50
        Istim.dur    =   5
        Istim.amp    =  -500e-3
        
        # solver------------------------------------------------------------------------------            
        cvode   = h.CVode()
        
                
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        
        # finalize
        h.finitialize(-80)
        
        # run simulation
        tstop   = 80
        while h.t < tstop:
            h.fadvance()
        
        
        base_index = 1990
            
        ax.plot(tm, vm)
        ax.plot(tm[base_index], vm[base_index], 'o', color='r', ms=10)
        
        
        tau = find_time_constants( vm.to_python()[base_index:], tm.to_python()[base_index:], ax=ax)
        
        Tau.append( tau )
        
        if np.abs(tau - 5.16) > 2*2.39:
            print('larger than 2 std: ', str(i) )
        else:
            print('run', i)
    
    
    
    print( 'Tau, mean std:', np.mean( Tau ), np.std(Tau) )
          
    fig.savefig('./Figures/time_constant_'+cell_type+'_'+str(N)+'best.png', bbox_inches='tight')
    plt.show()
    
    
        
    
    
