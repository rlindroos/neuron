
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        


def create_segment_list(cell, dist_groups):
    
    # create segment lists
    segments    = {} 
    
    for d in dist_groups:
        segments[d] = []
          
    # sort segments into list            
    for sec in cell.dendlist:
             
        for seg in sec:
            
            dist = h.distance(seg.x, sec=sec)
            
            if dist < 60:
                segments[0].append(seg)
            elif dist < 120:
                segments[1].append(seg)
            elif dist < 180:
                segments[2].append(seg)
            else:
                segments[3].append(seg)
    
    return segments
    



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    


    

def set_modulation( cell,           \
                    maxCond,        \
                    factors,        \
                    mod_list        ):
    
    # use factors stored externally
    mod_fact = factors[0:len(mod_list)]
    
    for mech in maxCond:
        
        # get factor from list
        factor = mod_fact[mod_list.index(mech.name() )]
        
        if mech.name()[0] == 'c':
            
            # Ca channels
            mech.pbar   = maxCond[mech] * factor
            
        else:
        
            # non Ca channels
            mech.gbar   = maxCond[mech] * factor
    
    


       

def set_bg_noise(cell,               \
                 syn_fact=False,     \
                 gabaMod=False       ):
    
    ns      = {}
    nc      = {}
    Syn     = {}
    for sec in cell.allseclist:
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 0.7e-3
        else:
            gbase = 0.2e-3
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,           \
                                NS_interval=1000.0/20.0,    \
                                NC_conductance=gbase )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/5.0,     \
                                NC_conductance=gbase*3      )
        
        Syn[sec.name()+'_glut'].ratio = 1.0/3
        
        if syn_fact:
            Syn[sec.name()+'_glut'].ampa_scale_factor = syn_fact[0]
            Syn[sec.name()+'_glut'].nmda_scale_factor = syn_fact[1]
            
        
        if gabaMod:
            # scale gaba
            nc[sec.name()+'_gaba'].weight[0] = gbase * 3 * gabaMod
        
    
    return Syn, nc, ns
            
            


def set_stimuli(cell,               \
                activationPattern,  \
                syn_fact=False,     \
                gabaMod=False       ):
    
    rand    = {}
    stim    = {}
    ncon    = {}
    vmL     = {}
    cur     = {}
    detect  = {}
    delay   = np.arange(1000, 1010, 1)
    
    # set random stimuli
    for sec in cell.dendlist:
        
        for seg in sec:  
        
            if seg in activationPattern:
                
                key = sec.name() + '_' + str(seg.x)
                
                # get index of activation
                index = activationPattern.index(seg)
                
                # create synapse
                rand[key]        =   h.glutamate(seg.x, sec=sec) # changed from x -> seg.x
                rand[key].ratio  =   1.0/3.0
                
                if syn_fact:
                    rand[key].ampa_scale_factor = syn_fact[0]
                    rand[key].nmda_scale_factor = syn_fact[1]


                # create NetStim object
                stim[key]            = h.NetStim()
                stim[key].number     = 2
                stim[key].start      = delay[index]
                stim[key].interval   = 10 # interval between two spikes in ms (1000 / 50 Hz = 20 ms)

                # create NetCon object
                ncon[key]            = h.NetCon(stim[key], rand[key])
                ncon[key].delay      = 1
                ncon[key].weight[0]  = 1.5e-3 # (uS) = 1.5 nS
                
                #vmL[key] = h.Vector()
                #vmL[key].record(sec(seg.x)._ref_v)
                cur[key] = h.Vector()
                cur[key].record(rand[key]._ref_I)
                
                # set pointer to right target...
                pointer             = rand[key]._ref_i_ampa
    
    return cur, rand, ncon, stim         

    
    

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    T = 2
    
    if T == 1:                                           
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        norm        =   id
        N           =   71
        section     =   'dend[46]'
        
                    
    elif T == 2:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        norm        =   id   
        N           =   34   
        section     =   'dend[43]'
    
    
    import matplotlib.pyplot as plt
    fig,ax = plt.subplots(2,1, figsize=(10,10) )  
    
    
    # load parameters and random se
    cell_versions   = use.load_obj(cell_type+'_'+str(N)+'bestFit.pkl')
    
    print(cell_type, par, morphology)
    
    Rin         = []
    Plateaus    = {}
    
    for i in range(N):
    
        parameters      =   cell_versions[i]['variables'] # parameter set 0 (cell index)
        rheobase        =   cell_versions[i]['rheobase']


        # initiate cell
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=parameters         )
        
        # set cascade 
        #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        # Modulation of channels is not used in this script
        if cell_type=='D1':
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
        else:
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
        
        maxCond = set_pointers(cell, pointer, mod_list)
        
        # stimulation
        Istim        =   h.IClamp(0.5, sec=cell.soma)
        Istim.delay  =   50
        Istim.dur    =   50
        Istim.amp    =  -100e-3
        
        # create synapse
        for sec in h.allsec():
            if sec.name() == section:
                syn         =   h.glutamate(0.5, sec=sec) # changed from x -> seg.x
                syn.ratio   =   1.0/3.0
                break
        
        # create NetStim object
        stim            = h.NetStim()
        stim.number     = 20
        stim.start      = 150
        stim.interval   = 1 # interval between two spikes in ms (1000 / 50 Hz = 20 ms)

        # create NetCon object
        ncon            = h.NetCon(stim, syn)
        ncon.delay      = 0
        ncon.weight[0]  = 1.5e-3 # (uS) = 1.5 nS
        
        # solver------------------------------------------------------------------------------            
        cvode   = h.CVode()
        
                
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        I   = h.Vector()
        I.record(Istim._ref_i)
        S   = h.Vector()
        S.record(syn._ref_I)
        
        # plateau triggering section
        vmL = h.Vector()
        vmL.record(sec(0.5)._ref_v)
        
        
        # finalize
        h.finitialize(-80)
        
        # run simulation
        tstop   = 300
        while h.t < tstop:
            h.fadvance()
        
        
        if i == 0:
            l1 = 'inj'
            l2 = 'syn'
            l3 = 'local'
            l4 = 'soma'
        else:
            l1 = ''
            l2 = ''
            l3 = ''
            l4 = ''
            
        ax[0].plot(tm, I, label=l1, color='b')       
        ax[0].plot(tm, S, label=l2, color='m')
        ax[1].plot(tm, vmL, label=l3, color='b')
        ax[1].plot(tm, vm, label=l4, color='m')
        
        plt.plot(tm[1990], vm[1990], 'o', ms=10, color='r')
        plt.plot(tm[3990], vm[3990], 'o', ms=10, color='r')
        
        vm_rest = vm[1990]
        vm_dipp = vm[3990]
        
        rin = (vm_rest - vm_dipp) *1e-3 / ( 100e-6 )
        Rin.append( rin )
        
        Plateaus[i] = {'vm': vm, 'vml':vmL}
        
        if np.abs(rin - 96.44) > 2*48.97:
            print('larger than 2 std: ', str(i) )
        else:
            print('run', i)
    
    Plateaus['tm'] = tm
    
    ax[0].legend()
    ax[1].legend()
    
    print( 'Rin, mean std:', np.mean( Rin ), np.std(Rin) )
    
    use.save_obj(Plateaus, 'Plateaus_' + cell_type )
    
    if T == 1:
        t = 'd'
    elif T == 2:
        t = 'i'
        
    fig.savefig('./Figures/plateaus_'+t+'SPN_'+str(N)+'best.png', bbox_inches='tight')
    plt.show()
    
    
        
    
    
