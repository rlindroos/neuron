
# script used for running in vivo simultion of clustered synaptic activation
# This script is based on plateaus_in_vivo_saveFeatures.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_clustered_D<>_section<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        
def main(   par="./params_iMSN.json",               \
            simDur=0.7,                             \
            distParams=None,                        \
            cell_type='D2',                         \
            DAmod=False,                            \
            randomSet=None,                         \
            gabaMod=False,                          \
            modulate_axon=False,                    \
            modulate_synapse=True,                  \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            model_version=0,                        \
            ISI=1,                                  \
            section=None,           ): 
    
    
    print(section, cell_type, morphology, par, DAmod)
    
    
    # channel distribution factors
    if distParams:
        # use best fit parameters
        random_variables = distParams
    else:
        # use default model (Lindroos et al., 2018)
        random_variables = None
        
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    
                
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    if cell_type=='D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list[0:7]:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
                    
    
    
    # load modulation factors
    #      and scale conductances
    #
    if DAmod:
        
        # use factors stored externally
        mod_fact = randomSet[0:len(mod_list)]
        
        
        # modulate synapic channels?
        if modulate_synapse:
            
            # last two entries in the lists 
            syn_fact = randomSet[-2:]
            
        
        for sec in h.allsec():
            
            # modulate axon?
            if sec.name().find('axon') >= 0:
                if not modulate_axon:
                    continue
            
            for seg in sec:
                
                for mech in seg:
                    
                    if mech.name() in mod_list: 
                        
                        # get factor from list
                        factor = mod_fact[mod_list.index(mech.name() )]
                        
                        if mech.name()[0] == 'c':
                            # Ca channels
                            pbar        = mech.pbar
                            mech.pbar   = pbar * factor
                            
                        else:
                            # non Ca channels
                            gbar        = mech.gbar
                            mech.gbar   = gbar * factor
    
    
    # set ampa and nmda epsp's--what compartments to use?
    dend_name   = 'dend[' + str(int(section)) + ']'
    Flag        = False
    
    for sec in h.allsec():
                                                    
        if sec.name() == dend_name:
            # set plateau inducing stimuli ------------------------------------------------
            
            x = 0.5
            
            syn         = h.glutamate(x, sec=sec)
            syn.ratio   = 1.0/3.0
            
            if modulate_synapse:
                syn.ampa_scale_factor = syn_fact[0]
                syn.nmda_scale_factor = syn_fact[1]


            # create NetStim object
            deltaT          = int(h.activation_delay)
            stim            = h.NetStim()
            stim.number     = 20
            stim.start      = 1000+deltaT
            stim.interval   = ISI # mean interval between two spikes in ms (1000 / 500 Hz = 2 ms)
            

            # create NetCon object
            ncon             = h.NetCon(stim, syn)
            ncon.delay       = 0
            ncon.weight[0]   = 1.5e-3 # (uS) = 1.5 nS
            
            
            d2soma = int(h.distance(x, sec=sec))
            
            plateau_inducing_section = sec
            Flag = True
            
    
    # solver  
    cvode   = h.CVode()
    tstop   = simDur * 1e3
    RES     = {}
    
    randActTime     =   use.load_obj('randomActTimes.pkl')
    # simulation loop --------------------------------------------------------------------
     
    for i in range(10):
        
        print(cell_type, section, i)
        
        RES[i]  = {}
        
        # set bg noise
        Syn, nc, ns         = use.set_bg_noise( cell,                           \
                                                syn_fact=modulate_synapse,      \
                                                gabaMod=gabaMod,                \
                                                delays=randActTime[i]       )
        
        # record vectors
        if not Flag:
            print('section not found:', section)
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        #cur = h.Vector()
        #cur.record(syn._ref_I)
                  
        # finalize and run
        h.finitialize(-70)
        while h.t < tstop:
        
            h.fadvance()
        
        # save spikes trace (after shift)  
        
        # shift traces
        t   = [x[1]-1000 for x in enumerate( tm) if x[0] >= 20000]
        v   = [x[1]      for x in enumerate( vm) if x[0] >= 20000]  
        #c   = [x[1]      for x in enumerate(cur) if x[0] >= 20000]  
        
        # get spikes
        spikes = use.getSpikedata_x_y( t, v )
        
        # save
        RES[i]['spikes'] = spikes
        RES[i]['vm'    ] = v
        #RES[i]['cur'   ] = c
        
    
    use.save_obj(RES, 'InVivoRamping'+str(ISI)+'ms_clustered_'+cell_type+'_V'+str(model_version)+'_dist'+str(d2soma)+'_section'+str(section)+'_delta'+str(deltaT) )
    



    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    ISI = 1
    
    DAmod   = False
    if DAmod:
        SYNmod  = True
        inhmod  = True
    
    
                                             
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =   58
    norm_val    =   0
    
    model_version = int(np.floor(id/N))
    sec           = id - model_version*N
    
    model_version = model_version + int(h.start_value)
    
    if DAmod:
        
        R  = 30
        da = '1'
        
        if SYNmod:     
            SM   = '1'
        else:
            SM   = '0'
            
        if inhmod:
            inh  = '1'
        else:
            inh  = '0'
    else:
        SYNmod  = False
        inhmod  = False
        da   = '0'
        SM   = '0'
        inh  = '0'
        R    =  1
    
    
    
    # load random set
    randomSet = use.load_obj(cell_type+'_30randomSet.pkl')
    
    
    
    # result structure
    
    
    parameters = use.load_obj(cell_type+'_71bestFit.pkl')[model_version]['variables']
    
    for r in range(R):
        
        if inhmod:
            gabaMod   = use.load_obj('GABA_plusMinus30_30randomSet.pkl')[r]
        else:
            gabaMod   = False
        
        res = main( par=par,                    \
                    simDur=1.5,                 \
                    morphology=morphology,      \
                    section=sec,                \
                    cell_type=cell_type,        \
                    DAmod=DAmod,                \
                    randomSet=randomSet[r],     \
                    gabaMod=gabaMod,            \
                    modulate_axon=False,        \
                    modulate_synapse=SYNmod,    \
                    model_version=model_version,\
                    ISI=ISI,                    \
                    distParams=parameters)
        
        
        
    
    
