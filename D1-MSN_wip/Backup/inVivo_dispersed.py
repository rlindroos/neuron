
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('./Mech/x86_64/special')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        


def create_segment_list(cell, dist_groups):
    
    # create segment lists
    segments    = {} 
    
    for d in dist_groups:
        segments[d] = []
          
    # sort segments into list            
    for sec in cell.dendlist:
             
        for seg in sec:
            
            dist = h.distance(seg.x, sec=sec)
            
            if dist < 60:
                segments[0].append(seg)
            elif dist < 120:
                segments[1].append(seg)
            elif dist < 180:
                segments[2].append(seg)
            else:
                segments[3].append(seg)
    
    return segments
    



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    


    

def set_modulation( cell,           \
                    maxCond,        \
                    factors,        \
                    mod_list        ):
    
    # use factors stored externally
    mod_fact = factors[0:len(mod_list)]
    
    for mech in maxCond:
        
        # get factor from list
        factor = mod_fact[mod_list.index(mech.name() )]
        
        if mech.name()[0] == 'c':
            
            # Ca channels
            mech.pbar   = maxCond[mech] * factor
            
        else:
        
            # non Ca channels
            mech.gbar   = maxCond[mech] * factor
    
    


       

def set_bg_noise(cell,               \
                 syn_fact=False,     \
                 gabaMod=False       ):
    
    ns      = {}
    nc      = {}
    Syn     = {}
    for sec in cell.allseclist:
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 0.3e-3
        else:
            gbase = 0.2e-3
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,           \
                                NS_interval=1000.0/12.0,    \
                                NC_conductance=gbase )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/3,     \
                                NC_conductance=gbase*3      )
        
        Syn[sec.name()+'_glut'].ratio = 1.0
        
        if syn_fact:
            Syn[sec.name()+'_glut'].ampa_scale_factor = syn_fact[0]
            Syn[sec.name()+'_glut'].nmda_scale_factor = syn_fact[1]
            
        
        if gabaMod:
            # scale gaba
            nc[sec.name()+'_gaba'].weight[0] = gbase * 3 * gabaMod
        
    
    return Syn, nc, ns
            
            


def set_stimuli(cell,               \
                activationPattern,  \
                ISI=1,              \
                syn_fact=False,     \
                gabaMod=False       ):
    
    interv  = ISI*10
    
    rand    = {}
    stim    = {}
    ncon    = {}
    vmL     = {}
    cur     = {}
    detect  = {}
    delay   = np.arange(1000, 1000+interv, ISI)
    
    # set random stimuli
    for sec in cell.dendlist:
        
        for seg in sec:  
        
            if seg in activationPattern:
                
                key = sec.name() + '_' + str(seg.x)
                
                # get index of activation
                index = activationPattern.index(seg)
                
                # create synapse
                rand[key]        =   h.glutamate(seg.x, sec=sec) # changed from x -> seg.x
                rand[key].ratio  =   1.0/3.0
                
                if syn_fact:
                    rand[key].ampa_scale_factor = syn_fact[0]
                    rand[key].nmda_scale_factor = syn_fact[1]


                # create NetStim object
                stim[key]            = h.NetStim()
                stim[key].number     = 2
                stim[key].start      = delay[index]
                stim[key].interval   = interv # interval between two spikes in ms (e.g. ISI = 2 -> 1000 / 50 Hz = 20 ms)

                # create NetCon object
                ncon[key]            = h.NetCon(stim[key], rand[key])
                ncon[key].delay      = 1
                ncon[key].weight[0]  = ((1000.0-d2soma)/1000.0)*1e-3 # (uS) = 1.5 nS
                
                #vmL[key] = h.Vector()
                #vmL[key].record(sec(seg.x)._ref_v)
                cur[key] = h.Vector()
                cur[key].record(rand[key]._ref_I)
                
                # set pointer to right target...
                pointer             = rand[key]._ref_i_ampa
    
    return cur, rand, ncon, stim         

    
    

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    ISI = 1
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    
    N           =   64
    model_version = int(np.floor(id/N))
    norm          = id - model_version*N
    
    model_version = model_version + h.start_value
    
    # load parameters and random set
    parameters      =   use.load_obj(cell_type+'_71bestFit.pkl')[model_version]['variables'] # parameter set 0 (cell index)
    randActPat      =   use.load_obj('randomActivationPattern.pkl')[cell_type]   # preselected indexes to use with lists of segments (see create_segment_list below)
    
    # group index 0-3 and pattern index n[group]
    n = [20, 17, 17, 10]
    if norm < 20:
        group   =   0
        C       =   0
    elif norm < 37: 
        group   =   1
        C       =   20
    elif norm < 54: 
        group   =   2
        C       =   37
    else:
        group   =   3
        C       =   54
        
    
    # pattern index 0-n[group]
    p       =   norm-C
    pattern =   randActPat[group][p]
    
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
    
    
    
    # create segment lists
    dist_groups = range(4)
    segments    = create_segment_list(cell, dist_groups)
              
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    if cell_type=='D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
    maxCond = set_pointers(cell, pointer, mod_list)
                     
    
    # solver------------------------------------------------------------------------------            
    cvode   = h.CVode()
    
    # result dict    
    RES     =   {}
    
    
    # set background noise
    Syn, nc, ns         = use.set_bg_noise(cell )
        
    # set stimuli
    activationPattern   = [ segments[group][s] for s in pattern ]
    
    for i in range(10):
        
        RES[i] = {}
        
        print( cell_type, group, p, i)
    
        cur,rand,ncon,stim  = set_stimuli(cell, activationPattern, ISI=ISI )
        
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        
        # run simulation
        h.finitialize(-70)
        
        # run simulation
        tstop   = 1500
        while h.t < tstop:
        
            h.fadvance()
                
        # remove first 500 ms and shift time so that start of stimuli is 0
        cutoff_index = 20000
        t   = [x[1]-1000 for x in enumerate( tm)        if x[0] >= cutoff_index]
        v   = [x[1]      for x in enumerate( vm)        if x[0] >= cutoff_index] 
        
        spikes = use.getSpikedata_x_y( t, v )
        
        RES[i][    'vm'] =   v 
        RES[i]['spikes'] =   spikes
        RES[i][   'cur'] =   {}
        
        for k,key in enumerate(cur): 
            I  = [x[1]   for x in enumerate(cur[key])   if x[0] >= cutoff_index] 
            
            RES[i]['cur'][k]    =   I
        
    use.save_obj(RES, 'InVivoDistdep'+str(ISI)+'ms_dispersed_'+cell_type+'_V'+str(model_version)+'_run'+str(norm) )
    
    
        
    
    
