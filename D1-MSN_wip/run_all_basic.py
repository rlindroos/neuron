
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

from joblib import Parallel, delayed
import multiprocessing

import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
import matplotlib.pyplot as plt


def main(cell_index, model_sets):
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    64
    
    print(cell_type, morphology, par)
    
    tstop           =   1000
    cvode           =   h.CVode()
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
    
    # load parameters and random set
    parameters      =   model_sets[cell_index]['variables'] 
    rheobase        =   model_sets[cell_index]['rheobase']
    
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
    
    # set cascade 
    #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
    
    # set current injection
    Istim        =   h.IClamp(0.5, sec=cell.soma)
    Istim.delay  =   100
    Istim.dur    =   1000
    Istim.amp    =   rheobase *1e-3
    
    # result dict
    RES = { 'dCa':{},       \
            'pools':{'L':{},'R':{}} }
    
    
    # rheobase----------------------------------------------------------------------------
    '''    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    
    # run simulation
    h.finitialize(-70)
    
    # run simulation
    while h.t < tstop:
        h.fadvance()
    
    RES['rheobase']['tm'] = tm
    RES['rheobase']['vm'] = vm
    
    
    
    '''
    # bAP induced dCa --------------------------------------------------------------------
    
    Istim.dur    =   2
    Istim.amp    =   2000 *1e-3
    
    if cell_type == 'D1':
        distances   = np.arange(40,200, 10)
    else:
        distances   = np.arange(49,200, 10)
    
    '''
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    '''
    dCa = {'L':{}, 'R':{}, 'dist':{}}
    count = 0
    for i,sec in enumerate(h.allsec()):
        if sec.name().find('axon') < 0: # don't record in axon
            for j,seg in enumerate(sec):
                
                key     =   sec.name().split('[')[1][:-1] + '_' + str(count)
                count  +=   1
                
                # N, P/Q, R Ca pool
                dCa['L'][key] = h.Vector()
                dCa['L'][key].record(seg._ref_cali)
                
                # the L-type Ca
                dCa['R'][key] = h.Vector()
                dCa['R'][key].record(seg._ref_cai)
                
                dCa['dist'][key]    =    int(np.round( h.distance(seg.x, sec=sec) ))
                
                
    # run simulation
    h.finitialize(-70)
    
    while h.t < 200:
        h.fadvance()
                    
    
    # get peak [Ca] and sort based on somatic distance
    sorted_on_dist = {}
    for key in dCa['L']:
        
        L           = dCa['L'][key].to_python()
        R           = dCa['R'][key].to_python()
        
        # this is were concentrations are summed 
        # (instead of averaged, doesn't matter since relative concen is used)
        V           = np.add( L, R )
        
        dca         = max(V[3300:-1]) - V[3300]
        dca_R       = max(R[3300:-1]) - R[3300]
        dca_L       = max(L[3300:-1]) - L[3300]
        
        dist        = dCa['dist'][key]
         
        for d in distances:
    
            if dist > d-5 and dist < d+5:
                
                if d not in sorted_on_dist:
                    sorted_on_dist[d] = {'sum':[], 'L':[], 'R':[]}
                
        
                sorted_on_dist[d]['sum'].append(dca)
                sorted_on_dist[d]['L'].append(dca_L)
                sorted_on_dist[d]['R'].append(dca_R)
                break
                
    # FI curve ---------------------------------------------------------------------------
    '''
    currents = range(240,485,20)
    
    F = np.zeros(len(currents))
    
    for index in range(len(currents)-1,0,-1):
            
        current = currents[index]
        
        Istim.dur    =   1000
        Istim.amp    =   current *1e-3
        
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        # run simulation
        h.finitialize(-70)
        
        # run simulation
        while h.t < tstop:
            h.fadvance()
        
        spikes = use.getSpikedata_x_y( t, v )
        
        F[index] = len(spikes)
        
        if len(spikes) == 0:
            break
    '''
    bAP = {'L':[],'R':[],'sum':[], 'dist':distances}
    for d in distances:
        for t in ['L','R','sum']: bAP[t].append(    np.mean(sorted_on_dist[d][t])    )
    
    return bAP
        
        
    

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # load parameters and random set
    path_to_lib     = './Libraries/'
    model_sets      =   use.load_obj(path_to_lib+'D1_71bestFit.pkl')
    
    num_cores       = multiprocessing.cpu_count()  
    '''
    for cell_index in model_sets:
        R= main( cell_index, model_sets )'''
        
    R = Parallel(n_jobs=num_cores)(delayed(main)( cell_index, model_sets ) for cell_index in model_sets  )
    use.save_obj(R, 'Pickles/BAPs_over_models_D1' )
    
    
        
    
    
        
    
    
