#! /usr/bin/python

import numpy                as np
import matplotlib.pyplot    as plt
import glob
import pickle
import sys
'''#import matplotlib.gridspec as gridspec
#from matplotlib.mlab import griddata
import os
from joblib import Parallel, delayed
import multiprocessing

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from scipy.stats import linregress
from scipy import stats
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from IPython.display import display, HTML
import seaborn as sns'''



def load_obj(name ):
    with open(name, 'rb') as f:
        return pickle.load(f) 
        


def check_fit(trace, cell='D1'):
    # fit to Day et al 2008
    
    fit = 0
    if cell=='D1':
        # (dMSN)
        
        dist = np.arange(40,200,10)
        for i,d in enumerate(dist):
            diff = np.exp( (d-40)/-27.0 ) - trace[i]
            fit += np.abs(diff)
    else:
        # (iMSN)
        
        dist = np.arange(49,200,10)
        for i,d in enumerate(dist):
            diff = np.exp( (d-49)/-63.0 ) - trace[i]
            fit += np.abs(diff)
        
    return fit    
    
    
def best_fit_parameter_distribution(best_fit, min_list):
    '''
    plot distribution range of best fit solutions (normalized to parameter range)
    
    sigmoidal distributions
    y = a4 + a5/(1 + np.exp((x-a6)/a7) )
    
    kaf:
    a4  1
    a5  np.random.uniform(0.1,0.9)
    a6  np.random.uniform(1,130)
    a7  np.random.uniform(-3,-70)
    
    naf:
    a4  1-a5
    a5  np.random.uniform(0.0,1.0)
    a6  np.random.uniform(20,50)
    a7  np.random.uniform(1,50)
    '''
    
    # parameter ranges
    random_variables = [    [0.1,0.9],     \
                            [1,130],       \
                            [-3,-70],      \
                            [0.0,1.0],     \
                            [20,50],       \
                            [1,50]         ]
                            
    v           =   np.arange(6)
    X           =   np.arange(300)
    N           =   len(min_list)
    sort_index  =   np.argsort(min_list)
    min_val     =   np.argmin(min_list)
    fd,ad       =   plt.subplots(1,1, figsize=(16,8))
    fd2,(aK,aN) =   plt.subplots(1,2, figsize=(16,8))
    parameters  =   ['ka5', 'ka6', 'ka7', 'na5', 'na6', 'na7'] 
    gradient    =   [(1.0*x/(N-1), 0, 1-1.0*x/(N-1)) for x in range(N)]
    
    # loop solutions ordered max -> min
    for index in reversed(sort_index):
        
        # create list
        y = []
        for i in v:
            
            val     =   best_fit[index]['var'][i]
            A       =   random_variables[i][0]
            B       =   random_variables[i][1]
            factor  =   (val-A) / (B-A)
            y.append(   factor  )
            
            #print factor, random_variables[i], val
        
        # plot
        ad.plot(v, y, '-o', ms=20, color=gradient[index], alpha=gradient[index][0])
        
        # Potassium (K)
        a4 =  1
        a5 =  best_fit[index]['var'][0]
        a6 =  best_fit[index]['var'][1]
        a7 =  best_fit[index]['var'][2]
        Y  =  a4 + a5/(1 + np.exp((X-a6)/a7) )
        aK.plot(X,Y, color=gradient[index], alpha=gradient[index][0])
        aK.set_ylim([0,2])
        
        # Sodium (Na)
        a5 =  best_fit[index]['var'][3]
        a4 =  1-a5
        a6 =  best_fit[index]['var'][4]
        a7 =  best_fit[index]['var'][5]
        Y  =  a4 + a5/(1 + np.exp((X-a6)/a7) )
        aN.plot(X,Y, color=gradient[index], alpha=gradient[index][0])
        aK.set_ylim([0,2])
        
        #print Y, X
        
    ad.set_xticklabels(parameters, fontsize=35)
    
    

    
def plot_exp_data(afi, aca, \
                  FI_string='Exp_data/FI/Planert2013-D1-FI-trace*', \
                  Ca_string='Exp_data/bAP/bAP-DayEtAl2006-D1.csv', \
                  single_type=True):
    
    if single_type:
        color = 'brown'
    else:
        if Ca_string == 'Exp_data/bAP/bAP-DayEtAl2006-D1.csv':
            color = 'grey'
        else:
            color = 'black'
    for i,f in enumerate(glob.glob(FI_string) ):
    
        [x_i,y_i] = np.loadtxt(f, unpack=True)
        
        if i == 0:
            label = 'D1planert'
        else:
            label = ''
            
        afi.plot(x_i, y_i, ls='--', color=color, lw=5, alpha=1, label=label, clip_on=False)
    
    
    [x1,y1] = np.loadtxt(Ca_string, unpack=True)
    aca.plot(x1, y1, color=color, lw=6)
        
        
def configure_plot_window(afi, aca, \
                          plot_best_fit=True, \
                          cell='D1', \
                          single_type=True):
    '''
    plot experimental data and configure plot window
    '''
    
    if cell=='D1':
        plot_exp_data(afi, aca, single_type=single_type)
    else:
        plot_exp_data(afi, aca, 
                      FI_string='Exp_data/FI/Planert2013-D2-FI-trace*',
                      Ca_string='Exp_data/bAP/bAP-DayEtAl2006-D2.csv',
                      single_type=single_type)
    
    # Hide the right and top spines
    afi.spines['right'].set_visible(False)
    afi.spines['top'].set_visible(False)

    # Only show ticks on the left and bottom spines
    afi.yaxis.set_ticks_position('left')
    afi.xaxis.set_ticks_position('bottom')
    
    # set x-range
    afi.set_xlim([100, 800])
    afi.set_ylim([0, 40])
    
    # set ticks
    yticks = np.arange(10,31,10)
    afi.set_yticks(yticks)
    afi.set_yticklabels(yticks, fontsize=30)
    xticks = np.arange(150,760,150)
    afi.set_xticks(xticks)
    afi.set_xticklabels(xticks, fontsize=30)
    afi.tick_params(width=3, length=6)
    
    # size of frame
    for axis in ['bottom','left']:
        afi.spines[axis].set_linewidth(4)
        
    # ----------------------------------------------------------
    
    if plot_best_fit:
        if cell == 'D1':
            dist = np.arange(40,301)
            func = np.exp( (dist-40)/-27.0 )
            aca.plot(dist, func, 'k-.', lw=3)
        else:
            dist = np.arange(49,301)
            func = np.exp( (dist-49)/-63.0 )
            aca.plot(dist, func, 'k-.', lw=3)
    
    # Hide the right and top spines
    aca.spines['right'].set_visible(False)
    aca.spines['top'].set_visible(False)

    # Only show ticks on the left and bottom spines
    aca.yaxis.set_ticks_position('left')
    aca.xaxis.set_ticks_position('bottom')
    
    
    # set ticks
    yticks = [0,1]
    aca.set_yticks(yticks)
    aca.set_yticklabels(yticks, fontsize=35)
    xticks = [40, 120, 200]
    aca.set_xticks(xticks)
    aca.set_xticklabels(xticks, fontsize=35)
    aca.set_xlim([0,300])
    
    aca.tick_params(width=2, length=4)
    
    # size of frame
    for axis in ['bottom','left']:
        aca.spines[axis].set_linewidth(4)
    
        
    
def compare_FI_dCa(file_string, plot_best_fit=True, cell='D1'):
    
    fig,(aca, afi)     = plt.subplots(1,2, figsize=(12,8))
    files              = glob.glob(file_string)
    best_fit           = {'fit':100, 'file':'None', 'key':'None', 'var':'None'} 
    
    for f in files:
        
        # load pickle file
        data        =   load_obj( f )
        distances   =   data['dist']
        currents    =   data['current']
        
        # loop over traces
        for key in data:
            
            if key in ['dist', 'current']:
                continue
            
            fit = check_fit( data[key]['Ca'], cell=cell )
            
            if fit < best_fit['fit']:
                best_fit    = {'fit':fit, 'file':f, 'key':key, 'var':data[key]['variables']}    
                
            aca.plot( distances, data[key]['Ca'], 'b', alpha=0.4)
            afi.plot( currents,  data[key]['FI'], 'b', alpha=0.4)
    
    
    # plot best fit
    
    if plot_best_fit:
        #print best_fit['file']
        #print best_fit['key']
        #print best_fit['fit']
        #print best_fit['var']

        data    =   load_obj( best_fit['file'] )
        key     =   best_fit['key'] 
        aca.plot( distances, data[key]['Ca'], 'k', lw=4)
        afi.plot( currents,  data[key]['FI'], 'k', lw=4)
    
    configure_plot_window(afi, aca, 
                          plot_best_fit=plot_best_fit, 
                          cell=cell)
        
    plt.show()



                                     

def compare_FI_dCa_multiple(file_string,        
                            best_list_len,       
                            plot_best_fit=True, 
                            cell='D1',
                            axes = False):
    
    if not axes:
        fig,(aca, afi)  = plt.subplots(1,2, figsize=(12,8))
        single_type     = True
    else:
        aca, afi        = axes
        single_type     = False
    
    files       = glob.glob(file_string)
    
    #best_fit    = {'fit':100, 'file':'None', 'key':'None', 'var':'None'} 
    best_fit    = {}
    
    vMax        = 100
    lMax        = [100]*best_list_len
    
    # first loop; find 100 best traces 
    for f in files:
        
        # load pickle file
        data        =   load_obj( f )
        
        # loop over traces
        for key in data:
            
            if key in ['dist', 'current']:
                continue
            
            fit = check_fit( data[key]['Ca'], cell=cell )
            
            if fit < vMax:
                
                # replace highest fit value in list
                i               = lMax.index( vMax )
                lMax[ i ]       = fit
                vMax            = max( lMax )
                best_fit[i]     = {'fit':fit, 'file':f, 'key':key, 'var':data[key]['variables']}
            
    
    if not axes:
        color = 'b'
    else:
        if cell == 'D1':
            color = 'blue'
        else:
            color = 'green'
    
    # second loop; plot best values
    for i in best_fit:
        
        # load pickle file
        data        =   load_obj( best_fit[i]['file'] )
        distances   =   data['dist']
        currents    =   data['current']
        key         =   best_fit[i]['key']
        
        aca.plot( distances, data[key]['Ca'], color=color, alpha=0.4)
        afi.plot( currents,  data[key]['FI'], color=color, alpha=0.4)
    
    # plot best fit
    
    vMin        =   min( lMax )
    i           =   lMax.index( vMin )
    data        =   load_obj( best_fit[i]['file'] )
    distances   =   data['dist']
    currents    =   data['current']
    key         =   best_fit[i]['key'] 
    
    #print best_fit[i]['var']
    
    if not axes:
        color = 'k'
        
    aca.plot( distances, data[key]['Ca'], color=color, lw=4)
    afi.plot( currents,  data[key]['FI'], color=color, lw=4)
    
    
    configure_plot_window(afi, aca, 
                          plot_best_fit=plot_best_fit, 
                          cell=cell,
                          single_type=single_type)
    
    
    
    if single_type:
        best_fit_parameter_distribution(best_fit, lMax)
        plt.show()




def plot_block_dCa( pickle,  \
                    plot_best_fit=True, 
                    cell='D1',
                    axes = False):
    
    if not axes:
        fig,(aca, afi)  = plt.subplots(1,2, figsize=(12,8))
        single_type     = True
    else:
        aca, afi        = axes
        single_type     = False
        
    data        =   load_obj( pickle )
    block       =   ['kas', 'kaf', 'naf']
    cell_types  =   ['D1', 'D2'] 
    color       =   ['b', 'g', 'm']
    
    for i,b in enumerate(block):
        for c in cell_types:
            
            if b == 'naf' and c == 'D1':
                continue
            
            for n in data[c][b]:
                print c, b, n
                aca.plot( data[c]['dist'], data[c][b][n], color=color[i], alpha=0.4, lw=2)
                
                
    configure_plot_window(afi, aca, 
                          plot_best_fit=plot_best_fit, 
                          cell=cell,
                          single_type=True)
    configure_plot_window(afi, aca, 
                          plot_best_fit=plot_best_fit, 
                          cell='D2',
                          single_type=True)

plt.show()
fig,(aca, afi)  = plt.subplots(1,2, figsize=(12,8) )
plot_block_dCa( sys.argv[1], plot_best_fit=False, axes=[aca, afi] ) 
plt.show()     
#compare_FI_dCa_multiple(sys.argv[1], int(sys.argv[2]), cell='D2')  
#compare_FI_dCa( sys.argv[1] )
    
