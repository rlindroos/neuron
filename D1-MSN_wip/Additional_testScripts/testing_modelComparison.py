

from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt
import  matplotlib.image     as mpimg
import  json
import  common_functions     as use
import  MSN_builder          as build

#from mpl_toolkits.axes_grid.inset_locator import inset_axes


h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

plt.close('all')

vinit  = -80
tstop  = 400
use_Ca = True


morphology=['MSN_corrected.swc',                 \
            'WT-dMSN_P270-20_1.02_SGA1-m24.swc', \
            'WT-iMSN_P270-09_1.01_SGA2-m1.swc'   ]
            
parameters=["./params_dMSN.json", \
            "./params_dMSN.json", \
            "./params_iMSN.json"  ]
            
fig, ax = plt.subplots(4,3, figsize=(12,12) )

# simulate
X = [0.9, 0.5, 0.5]
S = ['dend[108]', 'dend[28]', 'dend[33]']
M = ['dend[102]', 'dend[19]', 'dend[26]']

iMSN30 = use.load_obj('D2_30bestFit.pkl')
dMSN30 = use.load_obj('D1_30bestFit.pkl')

V = [ dMSN30[1]['variables'], dMSN30[1]['variables'], iMSN30[10]['variables'] ]

# loop over models            
for m,morph in enumerate(morphology):
    
    par=parameters[m]       
    
    # initiate cell
    cell = build.MSN(  params=par,              \
                       morphology=morph,        \
                       variables=V[m]           )
                       
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p
    
    # all channels with pointers
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
                  
    # set pointers; need since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    # set pointer (for all channel instances in mod_list)
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
    
    
    x           = X[m]
    s           = S[m]
    mother_stem = M[m]
    
    target    = {}
    target[s] = x

    for sec in h.allsec():
        if sec.name() == s:
            break

    
    syn         = h.glutamate(x, sec=sec)
    syn.ratio   = 1.0/3.0


    # create NetStim object
    stim            = h.NetStim()
    stim.number     = 20
    stim.start      = 100
    stim.interval   = 1000/300 # mean interval between two spikes in ms (1000 / 300 Hz)
    #stim.noise      = 1.0


    #vecStim = h.VecStim()
    #vec = h.Vector([100, 120])
    #vecStim.play(vec)


    # create NetCon object
    #ncon             = h.NetCon(stim,syn) 
    ncon             = h.NetCon(stim, syn)
    ncon.delay       = 1
    if m > 0:
        ncon.weight[0]   = 3e-3 # (uS) = 1.5 nS
    else:
        ncon.weight[0]   = 1.5e-3 # (uS) = 1.5 nS
    
    '''
        
    # max receptor current (uS). 
    ampa_max  = 10e-3                # ~20 ampa synapses in genesis model (1 = 510 pS)
    nmda_max  = 80e-3                #                      genesis model (1 = 1410 ps)
        
    # insert receptors (activated after 100 ms)
    ampa       = h.ampa(x, sec=sec)
    ampa.onset = 100
    ampa.gmax  = ampa_max
    nmda       = h.nmda(x, sec=sec)
    nmda.onset = 100
    nmda.gmax  = nmda_max
    
    '''

    # record vectors
    vm = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    vmL = h.Vector()
    vmL.record(sec(x)._ref_v)
    tm  = h.Vector()
    tm.record(h._ref_t)
    
    
    #h.topology()
    
    for stem in cell.soma.children():  
        use.plot_morphology(stem,           \
                            ax=ax[0,m],     \
                            target=target   )
    
    
    
    
    # record axial current flowing from the soma compartment  
    AXIAL = use.record_axial_current(cell.soma)
    
    for stem in cell.soma.children():
        print stem.name()
        
    for sec in h.allsec():
        for seg in sec:
            dist = h.distance(seg.x, sec=sec)
            if dist > 110:
                    print sec.name(), seg.x, dist  


    
    h.finitialize(vinit)
    while h.t < tstop:
        h.fadvance()
        
    ax[1,m].plot(tm,vmL, 'k', lw=3)
    ax[1,m].plot(tm,vm,  'k', lw=3)
    
    
    use.plot_axial_current( AXIAL, tm, vm, ax[2,m] )
    
    '''
    i = 0
    removed_stems = []
    while i < len(cell.soma.children())-1:
        
        randint = np.random.randint( len(cell.soma.children()) )
        while cell.soma.children()[randint].name() in [mother_stem, 'axon[0]']:
            randint = np.random.randint( len(cell.soma.children()) )
            
        print i, '-removed stem:', cell.soma.children()[randint].name(), 'mother stem:', mother_stem
        
        removed_stems.append(cell.soma.children()[randint])
        i+=1
        
        h.disconnect(sec=cell.soma.children()[randint])
        
        # record axial current flowing from the soma compartment  
        AXIAL = use.record_axial_current(cell.soma)
        
        h.finitialize(vinit)
        while h.t < tstop:
            h.fadvance()
            
        ax[1,m].plot(tm,vmL, 'g')
        ax[1,m].plot(tm,vm,  'r')
        
        use.plot_axial_current( AXIAL, tm, vm, ax[2,m] )
        
        if len(use.getSpikedata_x_y(tm,vm)) > 0:
            break
    
    ax[0, m].set_xlim([-200,200])
    ax[0, m].set_ylim([-200,200])
    
    ax[1, m].set_ylim(-90, 10)
    ax[2, m].set_ylim(-0.2, 0.2)
    
    
    # reconnect disconnected stems
    for stem in removed_stems:
        stem.connect(cell.soma(0.5))'''
    
    
plt.show()
