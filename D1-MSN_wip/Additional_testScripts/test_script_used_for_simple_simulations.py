#
'''
MSN model updated from model used in Lindroos et al., (2018). Frontiers

Robert Lindroos (RL) <robert.lindroos at ki.se>
 
The original MSN class and most channels were implemented by 
Alexander Kozlov <akozlov at kth.se>

'''



from __future__ import print_function, division
from neuron import h
from joblib import Parallel, delayed
import multiprocessing
import numpy                as np
import matplotlib.pyplot    as plt
import MSN_builder          as build
import common_functions     as use
import glob



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


plt.close('all')   






def main(   par="./params_iMSN.json",                       \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            sim='vm',                                       \
            amp=265*1e-12,                                  \
            run=None,                                       \
            distParams=None,                                \
            simDur=1,                                       \
            stimDur=0.9                             ):
            
    
    # initiate cell
    cell = build.MSN(   params=par,             \
                        morphology=morphology   )
    
    
    # set current injection
    stim        =   h.IClamp(0.5, sec=cell.soma)
    stim.amp    =   amp * 1e9
    stim.delay  =   100    #ms
    stim.dur    =   int(stimDur * 1e3)   
    
    tstop       = simDur * 1e3
    # dt        = default value; 0.025 ms (25 us)
    
    
    # all channels with pointers
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
     
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p
    
                  
    # set pointers; need since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    # set pointer (for all channel instances in mod_list)
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
                if mech.name() == 'sk':
                    
                    mech.gbar = mech.gbar * 0.0
    
    
    
    # record vectors
    RES         = {}
    RES['vm']   = h.Vector()
    RES['vm']      .record(cell.soma(0.5)._ref_v)
    
    for mechanism in cell.soma(0.5):
        mech = mechanism.name()
        if mech in cell.somatic_channels:
            RES[mech] = h.Vector()
            RES[mech]    .record(mechanism._ref_I)
    
    if run == 0:
        RES['channels'] =   cell.somatic_channels
        RES['tm']       =   h.Vector()
        RES['tm']            .record(h._ref_t)
    
    
              
    # solver------------------------------------------------------------------------------            
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop:
                
        h.fadvance()
        
    
    return RES
        
        
        
                


# Start the simulation.
# Function needed for HBP compability  ===================================================
if __name__ == "__main__":
    
    
    print('starting sim')    
    
    ct = 1    
    
    if ct == 1:
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        currents    =   np.arange(330,371,20)
        
    elif ct == 2:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        currents    =   np.arange(200,265,20)
    
    num_cores       = multiprocessing.cpu_count()
    
    R = Parallel(n_jobs=num_cores)(delayed(main)(   par=par,                    \
                                                    amp=current*1e-12,          \
                                                    run=n,                      \
                                                    simDur=0.5,                 \
                                                    morphology=morphology,      \
                                                    stimDur=0.3                 \
                                    ) for n,current in enumerate(currents)  )
    
    
    fig, ax     = plt. subplots(3, len(R), figsize=(36,24))  
    tm          = R[0]['tm']
    channels    = R[0]['channels'] 
    colors      = ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99']             
    
    for r,RES in enumerate(R):
        for c, chan in enumerate(channels):
            if c < 5:
                ax[1,r].plot(tm, RES[chan], color=colors[c], lw=3, label=chan)
            else:
                ax[2,r].plot(tm, RES[chan], color=colors[c], lw=3, label=chan)
            
        ax[0,r].plot(tm, RES['vm'], color='k', lw=3)
        ax[0,r].set_title(str(currents[r]))
    
    ax[1,0].legend(loc='best')   
    ax[2,0].legend(loc='best')     
        
    plt.savefig('../../../Desktop/test/SK_blocked/_SK_blocked100.png', transparent=True)
    
    plt.show()
    
    
        
                                                               
                                                    
                                                    
                                                    
    
    
    
          
    
        

