#
'''
MSN model updated from model used in Lindroos et al., (2018). Frontiers

Robert Lindroos (RL) <robert.lindroos at ki.se>
 
The original MSN class and most channels were implemented by 
Alexander Kozlov <akozlov at kth.se>

'''



from __future__ import print_function, division
from neuron import h
from joblib import Parallel, delayed
import multiprocessing
import numpy                as np
import matplotlib.pyplot    as plt
import MSN_builder          as build
import common_functions     as use
import glob



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


plt.close('all')   






def main(   par="./params_iMSN.json",                       \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            sim='vm',                                       \
            amp=265*1e-12,                                  \
            run=None,                                       \
            distParams=None,                                \
            simDur=1,                                       \
            stimDur=0.9                             ):
            
    
    if distParams:
        random_variables = distParams
    else:
        random_variables = [    np.random.uniform(0.0,2.0), \
                                np.random.uniform(0.8,1.0), \
                                np.random.uniform(10.0,60.0),   \
                                np.random.uniform(1.0,30.0),    \
                                np.random.uniform(0.0,2.0), \
                                np.random.uniform(0.1,0.9), \
                                np.random.uniform(1.0,130.0),   \
                                np.random.uniform(-3.0,-70.0),  \
                                np.random.uniform(0.0,2.0),    \
                                np.random.uniform(0.0,2.0), \
                                np.random.uniform(0.8,1.0), \
                                np.random.uniform(10.0,60.0),   \
                                np.random.uniform(1.0,30.0)     ]
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    # set current injection
    stim        =   h.IClamp(0.5, sec=cell.soma)
    stim.amp    =   amp * 1e9
    stim.delay  =   100    #ms
    stim.dur    =   int(stimDur * 1e3)   
    
    tstop       = simDur * 1e3
    # dt        = default value; 0.025 ms (25 us)
    
    
    # all channels with pointers
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
     
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p
    
                  
    # set pointers; need since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    # set pointer (for all channel instances in mod_list)
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
    
    
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    
              
    # solver------------------------------------------------------------------------------            
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop:
                
        h.fadvance()
        
    
    return tm, vm
        
        
        
def start_sim(par=None,         \
              cell_type=None,   \
              run=None,         \
              morphology=None   ):
    
    data       = use.load_obj(cell_type+'_30bestFit.pkl')[run]
    parameters = data['variables']
    current    = data['rheobase']
    
    result     = main(  par=par,                    \
                        amp=current*1e-12,          \
                        simDur=1.0,                 \
                        distParams=parameters,      \
                        morphology=morphology,      \
                        stimDur=0.9                 )    
            
    return [result, [data['tm'], data['vm']]]

# Start the simulation.
# Function needed for HBP compability  ===================================================
if __name__ == "__main__":
    
    plt.close('all')
    
    num_cores   = multiprocessing.cpu_count()
    
    for ct in [1,2]:  
    
        if ct == 1:
            cell_type   =   'D1'
            par         =   './params_dMSN.json'
            morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
            
        elif ct == 2:
            cell_type   =   'D2'
            par         =   './params_iMSN.json'
            morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        
        
        R  = Parallel(n_jobs=num_cores)(delayed(start_sim)(   par=par,           \
                                                morphology=morphology,      \
                                                run=n,                      \
                                                cell_type=cell_type         \
                                                ) for n in range(num_cores) )
    
    
        fig, ax = plt.subplots(4,2, figsize=(10,20))
        
        for simulation in range(num_cores):
            i = int(simulation%2)
            j = int(simulation/2)
            ax[j,i].plot(R[simulation][1][0], R[simulation][1][1],   'r', lw=6, alpha=0.5)
            ax[j,i].plot(R[simulation][0][0], R[simulation][0][1], '--k', lw=3)
        #fig.savefig('../../../Desktop/comparing_localBeskow_'+cell_type+'.png')  
        plt.suptitle(cell_type, fontsize=30)  
        plt.show()
    
    
        
                                                               
                                                    
                                                    
                                                    
    
    
    
          
    
        

