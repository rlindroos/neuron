from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt
import  matplotlib.image     as mpimg
import  json

from mpl_toolkits.axes_grid.inset_locator import inset_axes


h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

vinit  = -80
tstop  = 400
use_Ca = True

def calculate_distribution(d3, dist, a4, a5,  a6,  a7, g8):
    '''
    Used for setting the maximal conductance of a segment.
    Scales the maximal conductance based on somatic distance and distribution type.
    
    Parameters:
    d3   = distribution type:
         0 linear, 
         1 sigmoidal, 
         2 exponential
         3 step function
    dist = somatic distance of segment
    a4-7 = distribution parameters 
    g8   = base conductance (similar to maximal conductance)
    
    '''
    
    if   d3 == 0: 
        value = a4 + a5*dist
    elif d3 == 1: 
        value = a4 + a5/(1 + np.exp((dist-a6)/a7) )
    elif d3 == 2: 
        value = a4 + a5*np.exp((dist-a6)/a7)
    elif d3 == 3:
        if (dist > a6) and (dist < a7):
            value = a4
        else:
            value = a5
            
    if value < 0:
        value = 0
        
    value = value*g8
    return value 




# import morphology (full stem; 8 or 10) --------------------------------------

Import = h.Import3d_SWC_read()
Import.input('stem8_msn_corrected.swc')
imprt = h.Import3d_GUI(Import, 0)
imprt.instantiate(None)
h.define_shape()


# make list of all dendritic sections
dendlist = h.SectionList()

for sec in h.allsec():
    dendlist.append(sec=sec)  
    
    
    
# create sink compartment and connect----------------------------------------

# get ref to soma sec (has no parent)
for sec in dendlist:
    sref = h.SectionRef(sec=sec)
    if not sref.has_parent():
        soma = sec
        break

for stem in soma.children():
    print stem.name()

sink        = h.Section(name='sink')
sink.L      = 230
sink.diam   = 3    

# connect sink to soma    
sink.connect(sec(0.5))
    
#h.psection()
#h.topology()




# set number of segments per section
for sec in dendlist:    
    sec.nseg = 2*int(sec.L/40.0)+1




# set biophysical properties ---------------------------------------------

casc    =   h.D1_reduced_cascade2_0(0.5, sec=sec) 
pointer =   casc._ref_Target1p   

with open('params_dMSN.json') as file:
    par = json.load(file)

somaChannels = [    "naf",      
                    "kaf",
                    "kas",
                    "kdr",
                    "kir",
                    "sk",
                    "bk"            ]
if use_Ca:
    somaChannels = somaChannels + [     "cal12",
                                        "cal13",
                                        "can",
                                        "car",
                                        "cav32",
                                        "cav33",
                                        "cadyn", 
                                        "caldyn"    ]


# set soma as reference for distance mesurement
h.distance(0, 0.5, sec=soma)


for sec in h.allsec():
    
    # active properties
    if 'soma' in sec.name():
        for mech in somaChannels:
            sec.insert(mech)
            
        cmd = 'sec(0.5).gbar_naf = float(par["gbar_naf_somatic"]["Value"])'
        exec(cmd)
        cmd = 'sec(0.5).gbar_kaf = float(par["gbar_kaf_somatic"]["Value"])'
        exec(cmd)   
        cmd = 'sec(0.5).gbar_kas = float(par["gbar_kas_somatic"]["Value"])'
        exec(cmd) 
        cmd = 'sec(0.5).gbar_kir = float(par["gbar_kir_somatic"]["Value"])'
        exec(cmd) 
        cmd = 'sec(0.5).gbar_sk = float(par["gbar_sk_somatic"]["Value"])'
        exec(cmd)
        cmd = 'sec(0.5).gbar_bk = float(par["gbar_bk_somatic"]["Value"])'
        
        if use_Ca:
            exec(cmd)
            cmd = 'sec(0.5).pbar_cal12 = 1e-5'
            exec(cmd)
            cmd = 'sec(0.5).pbar_cal13 = 1e-6'
            exec(cmd)
            cmd = 'sec(0.5).pbar_car = 1e-4'
            exec(cmd)
            cmd = 'sec(0.5).pbar_can = 1e-7'
    
    else:
        for mech in somaChannels+["cav32", "cav33"]:
            sec.insert(mech)
        for seg in sec:
            
            dist = h.distance(1, sec=sec)
            
            #print dist, sec.name(), seg.x
            
            cmd = 'sec(seg.x).gbar_naf = calculate_distribution(1, dist, 0.1, 0.9, 60.0, 10.0, float(par["gbar_naf_basal"]["Value"]))'
            exec(cmd)
            cmd = 'sec(seg.x).gbar_kaf = calculate_distribution(1, dist, 1, 0.5, 120.0, -30.0, float(par["gbar_kaf_basal"]["Value"]))'
            exec(cmd)   
            cmd = 'sec(seg.x).gbar_kas = calculate_distribution(2, dist, 0.1, 0.9, 0.0, -5.0, float(par["gbar_kas_basal"]["Value"]))'
            exec(cmd) 
            cmd = 'sec(seg.x).gbar_kir = float(par["gbar_kir_basal"]["Value"])'
            exec(cmd) 
            cmd = 'sec(seg.x).gbar_sk = float(par["gbar_sk_basal"]["Value"])'
            exec(cmd)
            cmd = 'sec(seg.x).gbar_bk = float(par["gbar_bk_basal"]["Value"])'
            exec(cmd)
            
            if use_Ca:
                cmd = 'sec(seg.x).pbar_cal12 = 1e-5'
                exec(cmd)
                cmd = 'sec(seg.x).pbar_cal13 = 1e-6'
                exec(cmd)
                cmd = 'sec(seg.x).pbar_car = 1e-4'
                exec(cmd)
                cmd = 'sec(seg.x).pbar_can = 1e-7'
                exec(cmd)
                cmd = 'sec(seg.x).pbar_cav32 = calculate_distribution(1, dist, 0, 1.0, 120.0, -30.0, 1e-7)'
                exec(cmd)
                cmd = 'sec(seg.x).pbar_cav33 = calculate_distribution(1, dist, 0, 1.0, 120.0, -30.0, 1e-8)'
                exec(cmd)
    
    
    # set pointers
    for seg in sec:
        for mech in seg:
            if mech.name() in [ "kaf", \
                                "kas", \
                                "naf", \
                                "kir"       ]:
                h.setpointer( pointer, 'pka', mech )
                
            elif mech.name() in [   "cal12", \
                                    "cal13", \
                                    "can"           ]:
                if use_Ca:
                    h.setpointer( pointer, 'pka', mech )
            
                
    
    
    
    # passive properties
    sec.insert('pas')
    sec.g_pas = 1e-5
    sec.e_pas = -80
    sec.cm    = 1.0
    sec.Ra    = 150
    sec.ena   = 50
    sec.ek    = -85 # -90





# simulate
x = 0.5
s = 'dend[24]'

for sec in h.allsec():
    if sec.name() == s:
        break


# max receptor current (uS). 
ampa_max = 10e-3                # ~20 ampa synapses in genesis model (1 = 510 pS)
nmda_max = 30e-3                #                      genesis model (1 = 1410 ps)

# insert receptors (activated after 100 ms)
ampa       = h.ampa(x, sec=sec)
ampa.onset = 100
ampa.gmax  = ampa_max
nmda       = h.nmda(x, sec=sec)
nmda.onset = 100
nmda.gmax  = nmda_max


# record vectors
vm = h.Vector()
vm.record(soma(0.5)._ref_v)
vmS = h.Vector()
vmS.record(sink(0.5)._ref_v)
vmL = h.Vector()
vmL.record(sec(x)._ref_v)
tm  = h.Vector()
tm.record(h._ref_t)

h.finitialize(vinit)
while h.t < tstop:
    h.fadvance()
    
plt.figure()   
plt.plot(tm,vmL, 'b')
plt.plot(tm,vmS, 'k')
plt.plot(tm,vm, 'r')
plt.ylim(-90, 10)
plt.show()
