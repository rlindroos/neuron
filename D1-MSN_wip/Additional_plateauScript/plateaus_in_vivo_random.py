
from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        


def create_segment_list(cell, dist_groups):
    
    # create segment lists
    segments    = {} 
    
    for d in dist_groups:
        segments[d] = []
          
    # sort segments into list            
    for sec in cell.dendlist:
             
        for seg in sec:
            
            dist = h.distance(seg.x, sec=sec)
            
            if dist < 60:
                segments[0].append(seg)
            elif dist < 120:
                segments[1].append(seg)
            elif dist < 180:
                segments[2].append(seg)
            else:
                segments[3].append(seg)
    
    return segments
    



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    


    

def set_modulation( cell,           \
                    maxCond,        \
                    factors,        \
                    mod_list        ):
    
    # use factors stored externally
    mod_fact = factors[0:len(mod_list)]
    
    for mech in maxCond:
        
        # get factor from list
        factor = mod_fact[mod_list.index(mech.name() )]
        
        if mech.name()[0] == 'c':
            
            # Ca channels
            mech.pbar   = maxCond[mech] * factor
            
        else:
        
            # non Ca channels
            mech.gbar   = maxCond[mech] * factor
    
    


       

def set_bg_noise(cell,               \
                 syn_fact=False,     \
                 gabaMod=False       ):
    
    ns      = {}
    nc      = {}
    Syn     = {}
    for sec in cell.allseclist:
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 0.45e-3
        else:
            gbase = 0.2e-3
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,           \
                                NS_interval=1000.0/20.0,    \
                                NC_conductance=gbase )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/5.0,     \
                                NC_conductance=gbase*3      )
        
        Syn[sec.name()+'_glut'].ratio = 1.0/3
        
        if syn_fact:
            Syn[sec.name()+'_glut'].ampa_scale_factor = syn_fact[0]
            Syn[sec.name()+'_glut'].nmda_scale_factor = syn_fact[1]
            
        
        if gabaMod:
            # scale gaba
            nc[sec.name()+'_gaba'].weight[0] = gbase * 3 * gabaMod
        
    
    return Syn, nc, ns
            
            


def set_stimuli(cell,               \
                activationPattern,  \
                syn_fact=False,     \
                gabaMod=False       ):
    
    rand    = {}
    stim    = {}
    ncon    = {}
    vmL     = {}
    cur     = {}
    detect  = {}
    delay   = np.arange(1000, 1010, 1)
    
    # set random stimuli
    for sec in cell.dendlist:
        
        for seg in sec:  
        
            if seg in activationPattern:
                
                key = sec.name() + '_' + str(seg.x)
                
                # get index of activation
                index = activationPattern.index(seg)
                
                # create synapse
                rand[key]        =   h.glutamate(seg.x, sec=sec) # changed from x -> seg.x
                rand[key].ratio  =   1.0/3.0
                
                if syn_fact:
                    rand[key].ampa_scale_factor = syn_fact[0]
                    rand[key].nmda_scale_factor = syn_fact[1]


                # create NetStim object
                stim[key]            = h.NetStim()
                stim[key].number     = 2
                stim[key].start      = delay[index]
                stim[key].interval   = 10 # interval between two spikes in ms (1000 / 50 Hz = 20 ms)

                # create NetCon object
                ncon[key]            = h.NetCon(stim[key], rand[key])
                ncon[key].delay      = 1
                ncon[key].weight[0]  = 1.5e-3 # (uS) = 1.5 nS
                
                #vmL[key] = h.Vector()
                #vmL[key].record(sec(seg.x)._ref_v)
                cur[key] = h.Vector()
                cur[key].record(rand[key]._ref_I)
                
                # set pointer to right target...
                pointer             = rand[key]._ref_i_ampa
    
    return cur, rand, ncon, stim          

    
    

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    if id < 62:                                           
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        N           =   31
        norm_val    =   0
                    
    else:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        N           =   31
        norm_val    =   62
    
    
    i = (id-norm_val)%N
    j = int((id/N))%2
    
    # load parameters and random set
    randModFact     =   use.load_obj(cell_type+'_30randomSet.pkl')
    paradigms       =   [False]+randModFact
    factors         =   paradigms[i]
    parameters      =   use.load_obj(cell_type+'_30bestFit.pkl')[0]['variables']
    randActPat      =   use.load_obj('randomActivationPattern.pkl')[cell_type]
    
    if i == 0:
        syn_fact = False
        SM       = '0'
    else:
        syn_fact = factors[-2:]
        SM       = '1'
    
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
    
    
    
    # create segment lists
    dist_groups = range(4)
    segments    = create_segment_list(cell, dist_groups)
              
    
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    if cell_type=='D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    
    maxCond = set_pointers(cell, pointer, mod_list)
                     
    
    
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
                    
    # solver------------------------------------------------------------------------------            
    cvode   = h.CVode()
        
    # loop over stimulation paradigm, activation groups and pattern (31x4x10)
        
    RES     =   {}
    for group in dist_groups:
        RES[group]   =   {}
        for p,pattern in enumerate(randActPat[group]):
            RES[group][p]    =   {}
            
            # set modulation
            if factors:
                set_modulation(cell, maxCond, factors, mod_list)
            
            # set background noise
            Syn, nc, ns = set_bg_noise(cell, syn_fact=syn_fact)
                
            # set stimuli
            activationPattern   = [ segments[group][s] for s in pattern ]
            vmL,rand,ncon,stim  = set_stimuli(cell, activationPattern, syn_fact=syn_fact)
            
            
            # run simulation
            h.finitialize(-70)
            
            # run simulation
            tstop   = 700
            while h.t < tstop:
            
                h.fadvance()
                    
            # remove first 500 ms and shift time so that start of stimuli is 0
            t   = [x[1]-500 for x in enumerate(tm)      if x[0] >= 16000]
            v   = [x[1]     for x in enumerate(vm)      if x[0] >= 16000]  
            vl  = [x[1]     for x in enumerate(vmL)     if x[0] >= 16000] 
            
            count = 0
            while count < 100:
                h.pop_section()
                count +=1
            
            
            spikes = use.getSpikedata_x_y( t, v )
    
            if 'tm' not in RES:
                RES['tm'] = tm
                
            RES[group][p]['vm' ]    =   v 
            RES[group][p]['vml']    =   vl 
            RES[group][p]['spikes'] =   spikes
            
            
        print( 'group', group, 'done!' )
    
    # save
    if factors:
        da  = '1-'+str(i)
        inh = '0'
    else:
        da  = '0' 
        SM  = '0'
        inh = '0'
               
    use.save_obj(RES, 'dispersedInVivo_DA'+da+'_SM'+SM+'_INH'+inh+'_'+cell_type )
    
    
