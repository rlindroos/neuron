
from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import matplotlib.pyplot    as plt
import MSN_builder          as build
import common_functions     as use
import glob
import pickle



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


# global result dict
RES = {}  
        
        
def main(   par="./params_iMSN.json",               \
            sim='plateau',                          \
            run=None,                               \
            simDur=0.5,                             \
            distParams=None,                        \
            cell_type='D2',                         \
            DAmod=False,                            \
            modulate_axon=False,                    \
            modulate_synapse=True,                  \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            section=None,           ): 
    
    
    print(section, cell_type, morphology, par, DAmod)
    
    
    # channel distribution factors
    if distParams:
        # use best fit parameters
        random_variables = distParams
    else:
        # draw random parameters
        random_variables = [    np.random.uniform(0.5,1.5), \
                                np.random.uniform(0.8,1.0), \
                                np.random.uniform(10.0,60.0),   \
                                np.random.uniform(1.0,30.0),    \
                                np.random.uniform(0.5,1.5), \
                                np.random.uniform(0.0,0.9), \
                                np.random.uniform(1.0,130.0),   \
                                np.random.uniform(-3.0,-70.0),  \
                                np.random.uniform(0.5,2.0),    \
                                np.random.uniform(0.5,1.5), \
                                np.random.uniform(0.8,1.0), \
                                np.random.uniform(10.0,60.0),   \
                                np.random.uniform(1.0,30.0)     ]
        
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
                
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
      
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
    
    
    # draw modulation factors
    #      and scale conductances
    #
    if DAmod > 0:
        
        
        
        if run == 0:
            
            # new factors first cell instance only
            mod_fact = [    np.random.uniform(0.60,0.80),   \
                            np.random.uniform(0.65,0.85),   \
                            np.random.uniform(0.75,0.85),   \
                            np.random.uniform(0.85,1.25),   \
                            np.random.uniform(1.00,2.00),   \
                            np.random.uniform(1.00,2.00),   \
                            np.random.uniform(0.00,1.00)        ]
            
            RES[DAmod] = {'factors':mod_fact}
            
        else:
            
            mod_fact = RES[DAmod]['factors']
            
        
        
        # modulate synapic channels?
        if modulate_synapse:
            
            if run == 0:
                # new factors first cell instance only
                syn_fact = [    np.random.uniform(0.9,1.6),     \
                                np.random.uniform(0.9,1.6)      ]
                
                RES[DAmod]['synfact'] = syn_fact
        
            else:
                
                syn_fact = RES[DAmod]['synfact']
            
        
        
        
        for sec in h.allsec():
            
            # modulate axon?
            if sec.name().find('axon') >= 0:
                if not modulate_axon:
                    continue
            
            for seg in sec:
                
                for mech in seg:
                    
                    if mech.name() in mod_list: 
                        
                        # get factor from list
                        factor = mod_fact[mod_list.index(mech.name() )]
                        
                        if mech.name()[0] == 'c':
                            # Ca channels
                            pbar        = mech.pbar
                            mech.pbar   = pbar * factor
                            
                        else:
                            # non Ca channels
                            gbar        = mech.gbar
                            mech.gbar   = gbar * factor
    
        
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
            
    # set ampa and nmda epsp's--what compartments to use?
    dend_name = 'dend[' + str(int(section)) + ']'
    
    
    for sec in h.allsec():
        
        if sec.name() == dend_name:
            
            x = 0.5
            
            syn         = h.glutamate(x, sec=sec)
            syn.ratio   = 1.0/3.0
            
            if modulate_synapse:
                syn.ampa_scale_factor = syn_fact[0]
                syn.nmda_scale_factor = syn_fact[1]


            # create NetStim object
            stim            = h.NetStim()
            stim.number     = 20
            stim.start      = 100
            stim.interval   = 1000/500 # mean interval between two spikes in ms (1000 / 300 Hz)
            #stim.noise      = 1.0


            #vecStim = h.VecStim()
            #vec = h.Vector([100, 120])
            #vecStim.play(vec)


            # create NetCon object
            #ncon             = h.NetCon(stim,syn) 
            ncon             = h.NetCon(stim, syn)
            ncon.delay       = 1
            ncon.weight[0]   = 1.5e-3 # (uS) = 1.5 nS
            
            vmL = h.Vector()
            vmL.record(sec(x)._ref_v)
            
            d2soma = int(h.distance(x, sec=sec))
            
            break
    
    
    # solver------------------------------------------------------------------------------            
    cvode   = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    tstop   = simDur * 1e3
    while h.t < tstop:
    
        h.fadvance()
            
            
    return [[section, vm, vmL, d2soma], tm]
    
    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    DAmod   = True
    SYNmod  = False
    
    if DAmod:
        R  = range(1,4)
    else:
        R       = [0]
        SYNmod  = False
    
    if id < 58:                                           
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        N           =   58
        norm_val    =   0
                    
    else:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        N           =   46
        norm_val    =   116
    
    sec = (id-norm_val)%N
    
    
    for r in R:
        
        # for the 30 parameters settings
        P   = []
        for i in range(30):
            print('Starting simulation', i)
            
            parameters  = use.load_obj(cell_type+'_30bestFit.pkl')[i]['variables']
            
            res = main( par=par,                    \
                        simDur=0.5,                 \
                        run=i,                      \
                        morphology=morphology,      \
                        sim='plateau',              \
                        section=sec,                \
                        cell_type=cell_type,        \
                        DAmod=r,                    \
                        modulate_axon=False,        \
                        modulate_synapse=SYNmod,    \
                        distParams=parameters)
            tm  = res[1]
            r2  = res[0] 
            P.append(r2)           
        
        if DAmod: 
            da = '1'
            if SYNmod:     
                DICT = { 'res':P,  'chanfact':RES[r]['factors'],  'synfact':RES[r]['synfact'], 'tm':tm }
                SM   = '1'
            else:
                DICT = { 'res':P,  'chanfact':RES[r]['factors'], 'tm':tm }
                SM   = '0'
        else:
            DICT = { 'res':P, 'tm':tm }
            da   = '0'
            SM   = '0'
            
        print('simulation', r, 'done! Saving...')
        use.save_obj(DICT, 'plateaus_DA'+da+'-run'+str(np.random.randint(1e4))+'_SM'+SM+'_'+cell_type+'_section'+str(sec) )
    
    
