
from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
        
def main(   par="./params_iMSN.json",               \
            simDur=0.7,                             \
            distParams=None,                        \
            cell_type='D2',                         \
            DAmod=False,                            \
            randomSet=None,                         \
            gabaMod=False,                          \
            modulate_axon=False,                    \
            modulate_synapse=True,                  \
            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
            section=None,           ): 
    
    
    print(section, cell_type, morphology, par, DAmod)
    
    
    # channel distribution factors
    if distParams:
        # use best fit parameters
        random_variables = distParams
    else:
        # use default model (Lindroos et al., 2018)
        random_variables = None
        
    
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    
    
                
    # set cascade 
    #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    if cell_type=='D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list[0:7]:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
                    
    
    
    # load modulation factors
    #      and scale conductances
    #
    if DAmod:
        
        # use factors stored externally
        mod_fact = randomSet[0:len(mod_list)]
        
        
        # modulate synapic channels?
        if modulate_synapse:
            
            # last two entries in the lists 
            syn_fact = randomSet[-2:]
            
        
        for sec in h.allsec():
            
            # modulate axon?
            if sec.name().find('axon') >= 0:
                if not modulate_axon:
                    continue
            
            for seg in sec:
                
                for mech in seg:
                    
                    if mech.name() in mod_list: 
                        
                        # get factor from list
                        factor = mod_fact[mod_list.index(mech.name() )]
                        
                        if mech.name()[0] == 'c':
                            # Ca channels
                            pbar        = mech.pbar
                            mech.pbar   = pbar * factor
                            
                        else:
                            # non Ca channels
                            gbar        = mech.gbar
                            mech.gbar   = gbar * factor
    
    
    
    
        
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    
    
            
    # set ampa and nmda epsp's--what compartments to use?
    dend_name = 'dend[' + str(int(section)) + ']'
    
    ns = {}
    nc = {}
    Syn = {}
    for sec in h.allsec():
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 0.45e-3
        else:
            gbase = 0.2e-3
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,          \
                                NS_interval=1000.0/20.0,    \
                                NC_conductance=gbase )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/5.0,     \
                                NC_conductance=gbase*3      )
        
        Syn[sec.name()+'_glut'].ratio = 1.0/3
        
        if modulate_synapse:
            Syn[sec.name()+'_glut'].ampa_scale_factor = syn_fact[0]
            Syn[sec.name()+'_glut'].nmda_scale_factor = syn_fact[1]
            
        
        if gabaMod:
            # scale gaba
            nc[sec.name()+'_gaba'].weight[0] = nc[sec.name()+'_gaba'].weight[0] * gabaMod
            
        
        if sec.name() == dend_name:
            # set plateau inducing stimuli
            
            x = 0.5
            
            syn         = h.glutamate(x, sec=sec)
            syn.ratio   = 1.0/3.0
            
            if modulate_synapse:
                syn.ampa_scale_factor = syn_fact[0]
                syn.nmda_scale_factor = syn_fact[1]


            # create NetStim object
            stim            = h.NetStim()
            stim.number     = 20
            stim.start      = 500
            stim.interval   = 1000/500 # mean interval between two spikes in ms (1000 / 500 Hz = 2 ms)
            

            # create NetCon object
            ncon             = h.NetCon(stim, syn)
            ncon.delay       = 1
            ncon.weight[0]   = 0.6e-3 # (uS) = 1.5 nS
            
            vmL = h.Vector()
            vmL.record(sec(x)._ref_v)
            
            d2soma = int(h.distance(x, sec=sec))
            
    
    
    # solver------------------------------------------------------------------------------            
    cvode   = h.CVode()
    
    h.finitialize(-70)
    
    # run simulation
    tstop   = simDur * 1e3
    while h.t < tstop:
    
        h.fadvance()
            
    
    count = 0
    while count < 100:
        h.pop_section()
        count +=1
    return [tm, vm, vmL, d2soma ]
    



    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    
    DAmod   = True
    if DAmod:
        SYNmod  = True
        inhmod  = True
        
        R  = 30
        da = '1'
        
        if SYNmod:     
            SM   = '1'
        else:
            SM   = '0'
            
        if inhmod:
            inh  = '1'
        else:
            inh  = '0'
    else:
        SYNmod  = False
        inhmod  = False
        da   = '0'
        SM   = '0'
        inh  = '0'
        R    =  1
    
    
    if id < 58:                                           
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        N           =   58
        norm_val    =   0
                    
    else:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        N           =   46
        norm_val    =   58
    
    
    sec = (id-norm_val)%N
    
    # load random set
    randomSet = use.load_obj(cell_type+'_30randomSet.pkl')
    
    
    
    # result structure
    RES = {}
    
    parameters = use.load_obj(cell_type+'_30bestFit.pkl')[0]['variables']
    
    for r in range(R):
        
        RES[r] = {}
        
        if inhmod:
            gabaMod   = use.load_obj('GABA_plusMinus30_30randomSet.pkl')[r]
        else:
            gabaMod   = False
        
        print('Starting simulation', sec, r)
        
        res = main( par=par,                    \
                    simDur=0.7,                 \
                    morphology=morphology,      \
                    section=sec,                \
                    cell_type=cell_type,        \
                    DAmod=DAmod,                \
                    randomSet=randomSet[r],     \
                    gabaMod=gabaMod,            \
                    modulate_axon=False,        \
                    modulate_synapse=SYNmod,    \
                    distParams=parameters)
        
        
        # remove first 400 ms and shift time so that start of stimuli is 0
        tm  = [x[1]-500 for x in enumerate(res[0]) if x[0] >= 16000]
        vm  = [x[1]     for x in enumerate(res[1]) if x[0] >= 16000]  
        vml = [x[1]     for x in enumerate(res[2]) if x[0] >= 16000] 
        
        
        spikes = use.getSpikedata_x_y( tm, vm ) 
        
        
        if 'tm' not in RES:
            RES['tm'] = tm
        RES[r]['vm' ]=vm 
        RES[r]['vml']=vml 
        RES[r]['spikes']=spikes
        
    use.save_obj(RES, 'plateauInVivo_DA'+da+'_SM'+SM+'_INH'+inh+'_'+cell_type+'_section'+str(sec) )
        
    
    
