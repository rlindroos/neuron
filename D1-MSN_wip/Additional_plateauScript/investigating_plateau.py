
from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt
import  json

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')
    
# calc channel gmax
def calculate_distribution(d3, dist, a4, a5,  a6,  a7, g8):
    '''
    Used for setting the maximal conductance of a segment.
    Scales the maximal conductance based on somatic distance and distribution type.
    
    Parameters:
    d3   = distribution type:
         0 linear, 
         1 sigmoidal, 
         2 exponential
         3 step function
    dist = somatic distance of segment
    a4-7 = distribution parameters 
    g8   = base conductance (similar to maximal conductance)
    
    '''
    
    if   d3 == 0: 
        value = a4 + a5*dist
    elif d3 == 1: 
        value = a4 + a5/(1 + np.exp((dist-a6)/a7) )
    elif d3 == 2: 
        value = a4 + a5*np.exp((dist-a6)/a7)
    elif d3 == 3:
        if (dist > a6) and (dist < a7):
            value = a4
        else:
            value = a5
            
    if value < 0:
        value = 0
        
    value = value*g8
    return value

# create cell
soma = h.Section(name='soma')
dend = h.Section(name='dend')
sink = h.Section(name='sink')

# connect the two sections
dend.connect(soma(1))
sink.connect(soma(1))

# size (and number of segments, to be added) of compartments
soma.L         = 12.2
soma.diam      = 12.2

dend.L         = 160
dend.diam      = 1
dend.nseg      = 41
x              = 0.9   

sink.L         = 230
sink.diam      = 4


# Axial resistance
Ra   = 150
Cm   = 1
Erev = -80
gpas = 1e-5


# insert mechanisms (ion channels etc)
casc    =   h.D1_reduced_cascade2_0(0.5, sec=soma) 
pointer =   casc._ref_Target1p   


with open('params_dMSN.json') as file:
    par = json.load(file)


    
somaChan = [        "naf",
                    "kaf",
                    "kas",
                    "kdr",
                    "kir",
                    "cal12",
                    "cal13",
                    "can",
                    "car",
                    "sk",
                    "bk"        ]

    
for sec in h.allsec():    
    
    # insert pas
    sec.insert('pas')
    sec.g_pas = gpas
    sec.e_pas = Erev
    sec.cm    = Cm
    sec.Ra    = Ra
    
    # Set conductance and pointers (needed for dynamic neuromodulation; not used here).
    for seg in sec:
        
        # set conductance
        if sec.name() == 'soma':
            for mech in somaChan:
                sec.insert(mech)
                
        elif sec.name() in ['dend', 'sink']:
            for mech in somaChan+["cav32", "cav33"]:
                sec.insert(mech)
        
        for mech in seg:
            if mech.name() in [ "kaf", \
                                "kas", \
                                "kv4", \
                                "naf", \
                                "kir", \
                                "cal12", \
                                "cal13", \
                                "can"           ]:

                h.setpointer( pointer, 'pka', mech )
    sec.ek  = -85
    sec.ena =  50
    


cmd = 'soma(0.5).gbar_naf = 0.0001* float(par["gbar_naf_somatic"]["Value"])'
exec(cmd)
cmd = 'soma(0.5).gbar_kaf = float(par["gbar_kaf_somatic"]["Value"])'
exec(cmd)   
cmd = 'soma(0.5).gbar_kas = float(par["gbar_kas_somatic"]["Value"])'
exec(cmd) 
cmd = 'soma(0.5).gbar_kir = float(par["gbar_kir_somatic"]["Value"])'
exec(cmd) 
cmd = 'soma(0.5).gbar_sk = float(par["gbar_sk_somatic"]["Value"])'
exec(cmd)
cmd = 'soma(0.5).gbar_bk = float(par["gbar_bk_somatic"]["Value"])'
exec(cmd)
cmd = 'soma(0.5).pbar_cal12 = 1e-5'
exec(cmd)
cmd = 'soma(0.5).pbar_cal13 = 1e-6'
exec(cmd)
cmd = 'soma(0.5).pbar_car = 1e-4'
exec(cmd)
cmd = 'soma(0.5).pbar_can = 1e-7'

h.distance(sec=soma)

for seg in dend:
    
    dist = h.distance(seg.x, sec=dend)

    cmd = 'dend(seg.x).gbar_naf = 0.000001*calculate_distribution(1, dist, 0.1, 0.9, 60.0, 10.0, float(par["gbar_naf_basal"]["Value"]))'
    exec(cmd)
    cmd = 'dend(seg.x).gbar_kaf = calculate_distribution(1, dist, 1, 0.5, 120.0, -30.0, float(par["gbar_kaf_basal"]["Value"]))'
    exec(cmd)   
    cmd = 'dend(seg.x).gbar_kas = calculate_distribution(2, dist, 0.1, 0.9, 0.0, -5.0, float(par["gbar_kas_basal"]["Value"]))'
    exec(cmd) 
    cmd = 'dend(seg.x).gbar_kir = float(par["gbar_kir_basal"]["Value"])'
    exec(cmd) 
    cmd = 'dend(seg.x).gbar_sk = float(par["gbar_sk_basal"]["Value"])'
    exec(cmd)
    cmd = 'dend(seg.x).gbar_bk = float(par["gbar_bk_basal"]["Value"])'
    exec(cmd)
    cmd = 'dend(seg.x).pbar_cal12 = 1e-5'
    exec(cmd)
    cmd = 'dend(seg.x).pbar_cal13 = 1e-6'
    exec(cmd)
    cmd = 'dend(seg.x).pbar_car = 1e-4'
    exec(cmd)
    cmd = 'dend(seg.x).pbar_can = 1e-10'
    exec(cmd)
    cmd = 'dend(seg.x).pbar_cav32 = calculate_distribution(1, dist, 0, 1.0, 120.0, -30.0, 1e-7)'
    exec(cmd)
    cmd = 'dend(seg.x).pbar_cav33 = calculate_distribution(1, dist, 0, 1.0, 120.0, -30.0, 1e-8)'
    exec(cmd)

for seg in sink:
    
    dist = h.distance(0.5, sec=sink)

    cmd = 'sink(0.5).gbar_naf = 0.0001* calculate_distribution(1, dist, 0.1, 0.9, 60.0, 10.0, float(par["gbar_naf_basal"]["Value"]))'
    exec(cmd)
    cmd = 'sink(0.5).gbar_kaf = calculate_distribution(1, dist, 1, 0.5, 120.0, -30.0, float(par["gbar_kaf_basal"]["Value"]))'
    exec(cmd)   
    cmd = 'sink(0.5).gbar_kas = calculate_distribution(2, dist, 0.1, 0.9, 0.0, -5.0, float(par["gbar_kas_basal"]["Value"]))'
    exec(cmd) 
    cmd = 'sink(0.5).gbar_kir = float(par["gbar_kir_basal"]["Value"])'
    exec(cmd) 
    cmd = 'sink(0.5).gbar_sk = float(par["gbar_sk_basal"]["Value"])'
    exec(cmd)
    cmd = 'sink(0.5).gbar_bk = float(par["gbar_bk_basal"]["Value"])'
    exec(cmd)
    cmd = 'sink(0.5).pbar_cal12 = 1e-5'
    exec(cmd)
    cmd = 'sink(0.5).pbar_cal13 = 1e-6'
    exec(cmd)
    cmd = 'sink(0.5).pbar_car = 1e-4'
    exec(cmd)
    cmd = 'sink(0.5).pbar_can = 1e-7'
    exec(cmd)
    cmd = 'sink(0.5).pbar_cav32 = calculate_distribution(1, dist, 0, 1.0, 120.0, -30.0, 1e-7)'
    exec(cmd)
    cmd = 'sink(0.5).pbar_cav33 = calculate_distribution(1, dist, 0, 1.0, 120.0, -30.0, 1e-8)'
    exec(cmd)

'''
# max receptor current (uS)
ampa_max = 10e-3 
nmda_max = 30e-3 

# insert receptors (activated after 100 ms)
ampa       = h.ampa(x, sec=dend)
ampa.onset = 100
ampa.gmax  = ampa_max
nmda       = h.nmda(x, sec=dend)
nmda.onset = 100
nmda.gmax  = nmda_max'''


# synaptic stimuli

# create synapse
syn         = h.glutamate(x, sec=dend)

# create NetStim object
stim            = h.NetStim()
stim.number     = 20
stim.start      = 100
stim.interval   = 1000/300 # mean interval between two spikes in ms
stim.noise      = 1.0


vecStim = h.VecStim()
vec = h.Vector([100, 120])
vecStim.play(vec)


# create NetCon object
#ncon             = h.NetCon(stim,syn) 
ncon             = h.NetCon(vecStim, syn)
ncon.delay       = 1
ncon.weight[0]   = 1.5e-3 # (uS) = 1.5 nS
#ncon.threshold   = 0.1


# record vectors
vm = h.Vector()
vm.record(soma(0.5)._ref_v)
vmL = {}
for seg in dend:
    if seg.x > x:
        vmL[seg.x] = h.Vector()
        vmL[seg.x].record(dend(seg.x)._ref_v)
        break
    vmL[seg.x] = h.Vector()
    vmL[seg.x].record(dend(seg.x)._ref_v)
tm  = h.Vector()
tm.record(h._ref_t)

iampa = h.Vector()
iampa.record(syn._ref_i_ampa)
inmda = h.Vector()
inmda.record(syn._ref_i_nmda)

gampa = h.Vector()
gampa.record(syn._ref_g_ampa)
gnmda = h.Vector()
gnmda.record(syn._ref_g_nmda)

Itot = h.Vector()
Itot.record(syn._ref_I)

Gtot = h.Vector()
Gtot.record(syn._ref_G)

C = {}
for mech in somaChan+["cav32", "cav33"]:
    C[mech] = h.Vector()
    cmd = 'C[mech].record(dend(x).'+mech+'._ref_I)'
    exec(cmd)
    


h.finitialize(-84)
while h.t < 500:
    h.fadvance()
    
    
    
fig, ax = plt.subplots(1,3, figsize=(14,3))

for seg in dend:   
    if seg.x > x:
        vm_scaled = np.subtract(vmL[seg.x],min(vmL[seg.x]))
        ax[0].plot(tm, vmL[seg.x], 'k', lw=3)
        break
    vm_scaled = np.subtract(vmL[seg.x],min(vmL[seg.x]))
    ax[0].plot(tm,vmL[seg.x], 'b')
    
ax[0].plot(tm,vm, 'k', lw=2)
ax[1].plot(tm, np.divide(vm_scaled, max(vm_scaled)*160), 'k', lw=4, label='vm')
ax[2].plot(tm, np.divide(vm_scaled, max(vm_scaled)), 'k', lw=4)
#ax[1].plot([101.2,101.2], [0,1], '--k', lw=4)
#ax[2].plot([101.2,101.2], [0,1], '--k', lw=4)
    

for mech in somaChan+["cav32", "cav33"]:
    
    d1 = np.gradient(C[mech])
    d2 = np.gradient(d1)
    
    if mech[0] in ['n', 'c']:
        trace = np.divide(d1,min(d1))
        ls = '-'
        lw = 1
    else:
        trace = np.divide(d1,min(d1))
        ls = '--'
        lw = 4
    ax[1].plot(tm, d1, label=mech, ls=ls, lw=lw )

ax[1].legend()

ax[2].plot(tm, np.divide(iampa,min(iampa)), 'b' )
ax[2].plot(tm, np.divide(inmda,min(inmda)), 'r')
ax[2].plot(tm, np.divide(gampa,max(gampa)), '--b' , lw=3)
ax[2].plot(tm, np.divide(gnmda,max(gnmda)), '--r', lw=3)
ax[2].plot(tm, np.divide(Itot,min(Itot)), 'g')
ax[2].plot(tm, np.divide(Gtot,max(Gtot)), '--g', lw=3)


#ax[1].set_xlim(100,105)
#ax[2].set_xlim(100,105)


plt.show()










