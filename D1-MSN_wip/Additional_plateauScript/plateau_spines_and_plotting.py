
from __future__ import print_function, division
#from mpi4py import MPI
from neuron import h
#from neuron import gui
#pc = h.ParallelContext()
#id = int(pc.id())
from joblib import Parallel, delayed
import multiprocessing
import numpy                as np
import matplotlib.pyplot    as plt
import MSN_builder          as build
import common_functions     as use
import glob
import pickle



h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')



# D1
D1_params  =   [[0.5739438400147591, 114.46525970607432, -24.778683242914425, 0.9656307062939439, 40.55882910975278, 4.924556428134727],    \
                [0.6486015000884895, 37.19526177848555, -51.44257878295532, 0.9199040036041007, 38.773590273202544, 4.138269856798566],     \
                [0.34835213066115794, 92.06973911679478, -13.043854781662562, 0.9812668369096036, 35.31855413027535, 4.764644405812756],    \
                [0.8115471666044635, 44.7944466983775, -51.05269981676671, 0.9310487434283193, 40.02623282398899, 3.698685454575806],       \
                [0.5009368290963206, 26.47283142378077, -40.15544309999925, 0.9376314551458907, 34.918864668708565, 5.689649054327928],     \
                [0.11337316688781601, 74.85203079871395, -37.663738588683266, 0.9791107718457724, 40.313411748972555, 1.7599610703626931],  \
                [0.7178317755261724, 55.78999694946826, -40.594540879418844, 0.934360843812911, 24.039770691171, 11.85002736955439],        \
                [0.7084541798016345, 60.5127880861534, -55.54871428461576, 0.9063982542677244, 44.50455979565052, 1.2727982786963663],      \
                [0.6035708031273619, 46.80870702319583, -64.09248569745006, 0.9280326409654382, 37.58186874484719, 2.729291505716056],      \
                [0.561072695250727, 30.279814655445957, -5.915582850103075, 0.9240749040725246, 47.175148898308336, 3.9786457316868438],    \
                [0.7084541798016345, 60.5127880861534, -55.54871428461576, 0.9063982542677244, 44.50455979565052, 1.2727982786963663] ]

# D2
D2_params  =   [[0.36519026711426983, 85.9950693571762, -29.308683882971575, 0.9148525825598594, 31.715617529156468, 14.265211741761483],   \
                [0.16856005736302448, 126.23264636214776, -24.844746770683354, 0.9321044414025511, 30.258105136928478, 16.71932312947103],  \
                [0.8998189135933885, 119.36936307317013, -46.95178922978055, 0.8971817650810714, 33.820535805627614, 13.259594919674436],   \
                [0.3212471483859052, 73.2110386772533, -21.537721797784236, 0.9086172801642267, 36.981483525375666, 10.22848048793786],     \
                [0.5820977302857862, 57.946759035757786, -18.187141446438467, 0.8928497341805598, 46.779147877476554, 11.121000433080683],  \
                [0.46297432359551316, 54.020547269954754, -41.038717738089424, 0.9112928426829239, 23.74013590665613, 21.09496685904177],   \
                [0.20862895117356128, 14.373675087979382, -69.54059375795693, 0.9336951592433631, 43.40456122099932, 15.291555103979722],   \
                [0.42405610469134825, 2.083973089688305, -64.58731908139677, 0.9006809103056398, 31.54569527007518, 11.894971817963789],    \
                [0.2935952963686902, 84.25383011698389, -43.048666783931345, 0.9299719258473008, 42.89522927693975, 14.821301506581849],    \
                [0.813828871477326, 84.51778812354367, -22.93434820961655, 0.9024356980563085, 36.356176317731936, 16.866648293828433] ]
    
                   
 
        
        
        
def main(par="./params_iMSN.json",                  \
                            sim='plateau',          \
                            run=None,               \
                            simDur=0.5,             \
                            distParams=None,        \
                            cell_type='D2',         \
                            morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
                            somaCond=False,         \
                            section=None,           ): 
    
    
    
    
    #print(cell_type, morphology, par, section, run)
    
    
    if distParams:
        # load best fit parameters
        random_variables = distParams
    else:
        # draw random parameters
        random_variables = [    np.random.uniform(0.1,0.9), \
                                np.random.uniform(1,130),   \
                                np.random.uniform(-3,-70),  \
                                np.random.uniform(0.0,1.0), \
                                np.random.uniform(20,50),   \
                                np.random.uniform(1,50)     ]
        
        
    # initiate cell
    cell = build.MSN(  params=par,                  \
                       morphology=morphology)
    
    '''
    # create spines; TODO customize number of spines to add?
    #                     Move this to the spine class?
    if section:
        dend_name = 'dend[' + str(int(section)) + ']'
        for sec in h.allsec(): 
        
            if sec.name() == dend_name:  
                
                spines      =   {}
                segments    =   sec.nseg
                point       =   sec         # used for plotting further down
                
                if segments/2 > N:
                    start_x = 0.5
                else: 
                    start_x = 0.0
            
                c = 0
                # loop over segments and add spines
                for seg in sec:
                    if seg.x >= start_x:
                    
                        spine_number    = c 
                        id              = 'spine'  + str(spine_number)
                        
                        # create spine
                        spine           = build.Spine(sec, id)
                        
                        # Attache spine using method in spine class
                        spine.attach(sec, seg.x, 0) 
                        
                        # add spine id to spines
                        spines[spine.id] = spine
                        
                        print "Addedd spine: %s, pos %s, sec %s" % (spine.id, seg.x, sec.name())
                        c += 1
                        if c > 19:
                            #h.define_shape()
                            
                            break
                        
                break'''
    
    
    
                
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
      
    
    # set pointers; need since same mechanisms are used for dynamic modulation of channels.
    # Modulation of channels is not used in this script
    
    # set pointer (for all channel instances in mod_list)
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
                    
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
            
    # set ampa and nmda epsp's--what compartments to use?
    dend_name = 'dend[' + str(int(section)) + ']'
    
    
    for sec in h.allsec():
        
        if sec.name() == dend_name:
            
            x = 0.5
            
            ampa = h.ampa(x, sec=sec)
            ampa.onset = 100
            ampa.gmax  = 8e-3
            #h.setpointer(pointer, 'pka', seg.kaf )
            
            nmda = h.nmda(x, sec=sec)
            nmda.onset = 100
            nmda.gmax  = 8e-2
            #nmda.tau2  = 320.0 # 160 (320 makes plateaus match exp duration better)
            #h.setpointer(pointer, 'pka', seg.kaf )
            
            vmL = h.Vector()
            vmL.record(sec(x)._ref_v)
            
            d2soma = int(h.distance(x, sec=sec))
            
            print(section, d2soma)
            break
    
    
    if somaCond:
        h.topology()
        s = h.Shape()
        s.show(0)
        #s.fastflush()
        size = 180
        # .view(mleft, mbottom, mwidth, mheight, sleft, stop, swidth, sheight)
        #s.view(-size, -size, size*2,size*2, 500, 500, 1500, 1500)
        s.size(-size, size, -size, size)
        #s.variable("v")
        #s.scale(-85, -20)
        #s.point_mark(2, "O", 10, sec=point)
        s.color(2, sec=point) # color section "a" red
        s.exec_menu("Shape Plot")
        frame=morphology.split('.')[0].split('_')[1]
        s.printfile("fig_"+frame+".ps")
    
    
    # solver------------------------------------------------------------------------------            
    cvode   = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    tstop   = simDur * 1e3
    while h.t < tstop:
    
        h.fadvance()
        
        #if h.t > 100 and flag:
            # activate spines 
            
            
            
    return [tm, vm, d2soma]
    
    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    flag = 3
    
    simulate = True
    
    #if id < 116:
    #elif id < 208
    if flag == 1:                                           
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        parameters  =   D1_params
        N           =   56 #58
        norm_val    =   0
        shift       =   100
                    
    elif flag == 2:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        parameters  =   D2_params
        N           =   46
        norm_val    =   116
        shift       =   150
    
    else:
        simulate    =   False
        cell_type   =   'D'+str(flag-2)
        shift       =   50*(flag -1)
    
    if simulate:
        sections    =  np.arange(1,N) # [4, 8, 18, 40, 57] #
        
        num_cores   = multiprocessing.cpu_count()
        M = Parallel(n_jobs=num_cores)(delayed(main)( par=par,      \
                                                    simDur=0.5,                 \
                                                    morphology=morphology,  \
                                                    sim='plateau',            \
                                                    section=sec,                 \
                                                    cell_type=cell_type,        \
                                                    distParams=parameters[0]    \
                                        ) for sec in sections)
        
        use.save_obj(M, 'plateaus' )       
    else:
        M = use.load_obj('plateaus.pkl')         
    
    for m in M:
        
        dist = m[2]
        
        if dist < 90:
            color = 'b'
        elif dist < 130:
            color = 'r'
        else:
            color = 'k'
        
        plt.plot(m[0], m[1], lw=3, color=color) 
    data = np.loadtxt('../../results/plateau/du_plateau.csv', unpack=True)
    x    = np.multiply(data[0], -1)
    x    = np.subtract(x, shift-220)
    y    = np.multiply(data[1], -1)
    y    = np.subtract(y, 147.5)
    plt.plot(x,y, color='g', lw=5, ls='--')
    data = np.loadtxt('../../results/plateau/Plotkin_plateau_'+cell_type+'.csv', unpack=True)
    x    = np.multiply(data[0], 1000)
    x    = np.subtract(x, shift)
    y    = np.multiply(data[1], 1000)
    y    = np.subtract(y, 3)
    plt.plot(x,y, color='g', lw=5, ls='-.')
    
    plt.savefig('../../../plateau_n.png')
    plt.show() 
            
    
          
           
            
            
            
