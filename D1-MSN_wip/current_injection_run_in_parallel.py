
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

from joblib import Parallel, delayed
import multiprocessing

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
import matplotlib.pyplot as plt


def main(cell_index, model_sets):
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =    64
    
    print(cell_type, morphology, par)
    
    tstop           =   1000
    cvode           =   h.CVode()
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    modulation_factors = use.get_fixed_modulation_factors(  cell_type,                          \
                                                            mod_list+['ampa','namda','gaba'],   \
                                                            h.modulation_percentage             )
    
    
    
    # load parameters and random set
    parameters      =   model_sets[cell_index]['variables'] 
    rheobase        =   model_sets[cell_index]['rheobase']
    
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
    
    # set cascade 
    #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
    
    # set current injection
    Istim        =   h.IClamp(0.5, sec=cell.soma)
    Istim.delay  =   100
    Istim.dur    =   1000
    Istim.amp    =   rheobase *1e-3
    
    print (rheobase) 
    
    for da,DAmod in enumerate([False, True]):
        
        # modulation of channels
        if DAmod:
            org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
            use.set_channel_modulation( org_chan_gbar,          \
                                        mod_list,               \
                                        modulation_factors,     \
                                        modulate_axon=False     )
        
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(cell.soma(0.5)._ref_v)
        
        
        # run simulation
        h.finitialize(-70)
        
        # run simulation
        while h.t < tstop:
            h.fadvance()
        
        spikes = use.getSpikedata_x_y( tm, vm )
        
        if not DAmod:
            control_spikes = spikes
        else:
            
            # calc diff
            D1  = len(spikes) - len(control_spikes)
            
            if len(spikes)>0 and len(control_spikes)>0:
                D2  =  control_spikes[0] - spikes[0]
            else:
                D2  =  0
                    
    return [D1, D2]

    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # load parameters and random set
    path_to_lib     = './Libraries/'
    model_sets      =   use.load_obj(path_to_lib+'D1_71bestFit.pkl')
    
    num_cores       = multiprocessing.cpu_count()  
        
    R = Parallel(n_jobs=num_cores)(delayed(main)( cell_index, model_sets ) for cell_index in model_sets  )
    
    #R = use.load_obj('Maximum_DA_modulation_comparison.pkl')
    use.save_obj(R, 'Maximum_DA_modulation_comparison_naf75' )
    
    # arrays
    D1 = np.zeros((71))
    D2 = np.zeros((71))
    
    for d, diff in enumerate(R):
        
        D1[d] = diff[0]
        D2[d] = diff[1]
    
    fig, ax = plt.subplots( 2,1, figsize=(6,10) )
    
    x = range(len(D1))   
    ax[1].plot( [-5,75], [0,0], 'k--')        
    ax[0].plot( x, D1, '-o', ms=10)
    ax[1].plot( x, D2, '-o', ms=10)
    
    plt.show()
        
    
    
        
    
    
