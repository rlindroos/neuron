'''
Script for running channel testing in V-clamp 

single compartment cell with inserted mechanisms of choice
'''

from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt


# create cell and general setup ----------------------------------------------------------
cell = h.Section(name='cell')


# insert and specify mechanisms 
for mech in [       "cav32",
                    "cav33",
                    "cat32",
                    "cat33",
                    "cadyn",
                    "caldyn"   ]:
    cell.insert(mech)


cell(0.5).pbar_cat32    = 1e-7
cell(0.5).pbar_cav32    = 1e-5
cell(0.5).pbar_cat33    = 1e-8
cell(0.5).pbar_cav33    = 1e-10
cell.ecal               = 20
h.cao0_ca_ion           = 1.5               # 1.5 mM used in Iftinca et al 2006




# V-clamp; the amplitude of the clamp is set in the loop
dur1        = 350
dur2        = 275
dur3        = 100
clampBase   = -110
clamp       = h.SEClamp(cell(0.5))
clamp.rs    = 0.01
clamp.dur1  = dur1
clamp.amp1  = clampBase
clamp.dur2  = dur2
clamp.dur3  = dur3
clamp.amp3  = clampBase




# Create plot windows
plt.close('all')   
fig,ax      = plt.subplots( 4,2, figsize=(16,20) )
f1, a1      = plt.subplots()
f2, a2      = plt.subplots()
f3, a3      = plt.subplots()
f4, a4      = plt.subplots()
AX          = [a3, a4]
f5, a5      = plt.subplots()
f6, a6      = plt.subplots()

figV, axV   = plt.subplots(1,1)




# Define result structures etc
start_index = int((dur1-10)/h.dt)
end___index = int((dur1+dur2-10)/h.dt)
voltage     = np.arange(-100,31,10)
N           = len(voltage)
color       = ['g', 'r', 'k']
gradient    = [(1-1.0*x/(N-1), 0, 1.0*x/(N-1)) for x in range(N)]
keys        = ['cat32', 'cav32', 'cat33', 'cav33']
res         = {}
Max         = {}
gates       = {}

for key in keys:
    gates[key]      = {'m':{},'h':{}}

for key in keys:
    res[key]    = {}
    Max[key]    = []
    


# run clamp protocol ---------------------------------------------------------------------
for v,V in enumerate(voltage):
    
    clamp.amp2  = V
    
    # define record vectors 
    for key in keys:
        res[key][V] = h.Vector()
        cmd = 'res[key][V].record(cell(0.5).'+key+'._ref_ical)'
        exec(cmd)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)
    
    for key in ['cav33', 'cav32']:
        for g in gates[key]:
        
            gates[key][g] = h.Vector()
            cmd = 'gates[key][g].record(cell(0.5).'+key+'._ref_'+g+')'
            exec(cmd)


    # finilize and run
    h.finitialize(clampBase)
    while h.t < dur1+dur2+100:
        h.fadvance()
    
    
    # plotting ***
    # current
    for k,key in enumerate(keys):
        i = (k) / 2
        c = (k) % 2
        ax[c,i].plot(tm, res[key][V], color=gradient[v], lw=3)
    a1.plot(tm, res['cav32'][V], color='r')
    a2.plot(tm, res['cav33'][V], color='r')
    a5.plot(tm, res['cat32'][V], color='g')
    a6.plot(tm, res['cat33'][V], color='g')
    
    # potential
    axV.plot(tm, vm, 'k')
    axV.plot(tm[start_index], vm[start_index], 'og', ms=10)
    axV.plot(tm[end___index], vm[end___index], 'og', ms=10)
    
    # get peak (negative) values of current 
    for key in keys:
        extract_peak = res[key][V].to_python()
        Max[key].append( min( extract_peak[start_index:end___index] ) )
        
    # gates
    for j,key in enumerate(['cav32', 'cav33']):
        for g in gates[key]:
            ax[2,j].plot(tm, gates[key][g], color=gradient[v], lw=3)
    
        ax[2,j].set_xlim([dur1-40, dur1+dur2+40])  
    


# plotting + configuration ---------------------------------------------------------------

# IV (+ configuration)
for k,key in enumerate(keys):
    i = (k) / 2
    c = (k) % 2
    ax[3,i].plot(voltage, np.multiply(np.divide(Max[key],min(Max[key])),-1), color=color[c], lw=3)
    
    ax[c,i].set_xlim([dur1-40, dur1+dur2+40])
    [yMin,yMax] = ax[c,i].get_ylim()
    ax[c,i].set_ylim([yMin, 0.0])
    ax[c,i].axis('off')
    
    AX[i].plot(voltage, np.multiply(np.divide(Max[key],min(Max[key])),-1), color=color[c], lw=3)

for acon in [ax[3,:],AX]:  
    for A in acon: 
        A.spines['right'].set_color('none')
        A.spines['top'  ].set_color('none')
        A.xaxis.set_ticks_position('bottom')
        A.spines['bottom'].set_position(('data',0))
        A.yaxis.set_ticks_position('left')
        A.spines['left'].set_position(('data',0))
        #A.set_ylim([-1, 0])


# configure
a1.set_xlim([dur1-40, dur1+dur2+40])
a2.set_xlim([dur1-40, dur1+dur2+40])
a5.set_xlim([dur1-40, dur1+dur2+40])
a6.set_xlim([dur1-40, dur1+dur2+40])
a1.axis('off')
a2.axis('off')  
a5.axis('off')
a6.axis('off')     


# save figures (and plot scale bars)
axV.set_xlim([dur1-40, dur1+dur2+40]) 
fig.savefig('../../../Dropbox/Channels/T-type_ca/vClamp_CaT_revision.png')
if dur2 <= 200:
    ymax        = a1.get_ylim()
    yorg        = a5.get_ylim()
    diff        = 15
    base        = dur1+dur2-37
    a1.plot([base, base+diff],  \
            [ymax[0]*0.7,ymax[0]*0.7],  \
            lw=8,                       \
            color='r',                  \
            solid_capstyle="butt")
    a5.plot([base, base+diff],  \
            [yorg[0]*0.7,yorg[0]*0.7],  \
            lw=8,                       \
            color='g',                  \
            solid_capstyle="butt")
    f1.savefig('../../../Dropbox/Channels/T-type_ca/cav32_current.png', transparent=True)
    f3.savefig('../../../Dropbox/Channels/T-type_ca/cav32_IV.png',      transparent=True)
    f5.savefig('../../../Dropbox/Channels/T-type_ca/cat32_current.png', transparent=True)
    
    bar_start   = 383
    a1.plot([bar_start, bar_start+5], [ymax[0]*0.6,ymax[0]*0.6], lw=8, color='r')
    a1.set_xlim([350, 400])
    f1.savefig('../../../Dropbox/Channels/T-type_ca/cav32_current_zoom.png', transparent=True)
    
    a5.plot([bar_start, bar_start+5], [yorg[0]*0.6,yorg[0]*0.6], lw=8, color='g')
    a5.set_xlim([350, 400])
    f5.savefig('../../../Dropbox/Channels/T-type_ca/cat32_current_zoom.png', transparent=True)
else:
    ymax = a2.get_ylim()
    yorg = a6.get_ylim()
    diff        = 50
    base        = dur1+dur2-82
    a2.plot([base, base+diff],   \
            [ymax[0]*0.7,ymax[0]*0.7],      \
            lw=8,                           \
            color='r',                      \
            solid_capstyle="butt")
    a2.plot([base, base+diff],   \
            [ymax[0]*0.7,ymax[0]*0.7],      \
            lw=1,                           \
            color='k')
    a6.plot([base, base+diff],  \
            [yorg[0]*0.7,yorg[0]*0.7],      \
            lw=8,                           \
            color='g',                      \
            solid_capstyle="butt")
    if dur2 == 250:
        # illustrate error in paper (pulse duration for 3.3 channel is 350, not 250
        f2.savefig('../../../Dropbox/Channels/T-type_ca/cav33_current_250ms.png', transparent=True)
    else:
        f2.savefig('../../../Dropbox/Channels/T-type_ca/cav33_current.png', transparent=True)
        f6.savefig('../../../Dropbox/Channels/T-type_ca/cat33_current.png', transparent=True)
    f4.savefig('../../../Dropbox/Channels/T-type_ca/cav33_IV.png', transparent=True)
    
    a2.plot([382, 387], [ymax[0]*0.7,ymax[0]*0.7], lw=8, color='r')
    a2.set_xlim([350, 400])
    f2.savefig('../../../Dropbox/Channels/T-type_ca/cav33_current_zoom.png', transparent=True)
    
    a6.plot([382, 387], [yorg[0]*0.7,yorg[0]*0.7], lw=8, color='g')
    a6.set_xlim([350, 400])
    f6.savefig('../../../Dropbox/Channels/T-type_ca/cat33_current_zoom.png', transparent=True)
plt.close('all')


'''
# Johannas code    
stim=neuron.h.SEClamp(0.5)
stim.rs=resistance
neuron.h.tstop=TB
neuron.h.v_init=V0
neuron.h.init()
stim.dur1 = TB
v_clamp = neuron.h.Vector()
v_clamp.from_python(v)
v_clamp.play(stim._ref_amp1, self.dt)'''
        
                                              
                                                    
                                                    
                                                    
    
    
    
          
    
        

