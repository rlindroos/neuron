
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

from joblib import Parallel, delayed
import multiprocessing

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
import matplotlib.pyplot as plt



if __name__ == "__main__":
    
    # load parameters and random set
    path_to_lib     = './Libraries/'
    model_sets      =   use.load_obj(path_to_lib+'D1_71bestFit.pkl')
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    cell_index  =    2
    
    tstop           =   1000
    cvode           =   h.CVode()
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    
    # load parameters and random set
    parameters      =   model_sets[cell_index]['variables'] 
    rheobase        =   model_sets[cell_index]['rheobase']
    
    fig, ax = plt.subplots(figsize=[5, 4])
    #axins = plt.axes(   [.2, .6,  .2, .2])
    #axins.set_xticks([]) 
    
    Diff = {}
    color = ['k', 'r', 'g']
    ls    = ['-', '--', '-.',]
    for i,sim in enumerate(['control', 'split', 'spines']):
        
        # initiate cell ------------------------------------------------------------------
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=parameters         )
        
        name2sec = use.create_name2sec_dict()
        if i > 0:
            # split section
            
            print('-- splitting section 33')
            SEC = use.split_section(cell, name2sec['33'], n_sec_per_um=1)
            
            if i > 1:
                # insert spines
                SPINES = []
                for id,sec in enumerate(SEC):
                    SPINES.append( build.Spine(id, parent=sec) )
            
            '''
            # without split: connected to center of section
            mixActPat       =   use.load_obj(path_to_lib+'mixActPatRestricted16.pkl')
            index_map       =   mixActPat['map' ][19]
            pattern         =   mixActPat['patterns' ][index_map]
            
            SPINES = []
            for j in range(len(pattern)):
                # build spine without attaching
                SPINES.append( build.Spine(j) )
                
            # attache and set stimuli
            STIM, spike_list = use.set_mixed_stimuli_inSpine(   cell,
                                                                SPINES,
                                                                pattern,
                                                                name2sec,
                                                                ISI=1,     
                                                                start_time=3000)'''
                
                
                
        
        # set cascade 
        #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        use.set_pointers(cell, pointer, mod_list)
        
        # set current injection
        if i < 1:   sec = name2sec['33']
        else:       sec = SEC[int(np.floor(len(SEC)/2))+1]
        Istim = h.IClamp(0.5, sec=sec)
        Istim.delay  =   100
        Istim.dur    =   1000
        Istim.amp    =   rheobase *1e-3
        
            
        # record vectors
        tm  = h.Vector()
        tm.record(h._ref_t)
        vm  = h.Vector()
        vm.record(sec(0.5)._ref_v)
        
        print( 'running sim %d' % (i) )
        # run simulation
        h.finitialize(-80)
        
        # run simulation
        while h.t < tstop:
            h.fadvance()
        
        ax.plot(tm, vm, ls=ls[i], c=color[i], label=sim)
        #axins.plot(tm,vm, ls=ls[i], c=color[i])
        #axins.set_xlim([950,970]) 
        
        
        # clean up (delete sections not to cause errors next iteration
        for sec in h.allsec():
            sec = None
        
        del cell
        
        if i > 0: 
            del SEC
            #del SPINES
    
    ax.legend(loc=4)
    plt.show()

    
'''
# INSERT before placing spines (replace call to use.split_sec())
# disconnect
sec = name2sec['33']
parent = sec.parentseg().sec
children = []
for child_sec in sec.children():
    print( child_sec.name() )
    children.append(child_sec)
for child_sec in children:  
    h.disconnect(sec=child_sec)
L       = sec.L
N = int(np.floor(L))
if N%2 == 0:
    N += 1  # make sure odd number of sections (and therefore segments)

parent_sec = parent 
SEC = []
for i in range(N):
    sub_sec_name = 'spineSec%s[%d]' % (sec.name(), i)
    ssec = h.Section(name=sub_sec_name)
    SEC.append( ssec )

for i in range(1,N):
    SEC[i].connect(SEC[i-1](1),0)
for child_sec in children:
    print( SEC[-1].name() )
    child_sec.connect(SEC[-1](1),0)

SEC[0].connect(parent(1),0)
'''
