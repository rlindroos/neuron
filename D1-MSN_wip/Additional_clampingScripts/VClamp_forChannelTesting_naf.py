'''
Script for running channel testing in V-clamp 

single compartment cell with inserted mechanisms of choice
'''

from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt


# create cell and general setup ------
cell = h.Section(name='cell')


# insert and specify mechanisms 
for mech in ["naf", 'na15a', "naf_ms"]:
    cell.insert(mech)

cell(0.5).gbar_naf    = 1e-7
cell(0.5).gbar_na15a  = 1e-7
cell(0.5).gbar_naf_ms = 1e-7

casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell) 
pointer =   casc._ref_Target1p

h.setpointer( pointer, 'pka',  cell(0.5).naf)


# Protocol 1 =============================================================================
#            fig 2 in Ogata and Tatebayashi, 1990

clampBase   = -100
clamp       = h.SEClamp(cell(0.5))
clamp.rs    = 0.001
clamp.dur1  = 30
clamp.amp1  = clampBase
clamp.dur2  = 30
clamp.dur3  = 50
clamp.amp3  = clampBase



# Create plot window  
fig,ax      = plt.subplots( 3,2, figsize=(12,8) )

# Define result structures etc
start_index = int((25)/h.dt)
end___index = int((55)/h.dt)
voltage     = np.arange(-70,61,10)

channels    = ["naf", "na15a", "naf_ms"]
res         = {"na15a":{}, "naf":{}, "naf_ms":{}}
Max         = {"na15a":[], "naf":[], "naf_ms":[]}
    
color       = ['r', 'b', 'm']
    


# run clamp protocol ---------
for v,V in enumerate(voltage):
    
    clamp.amp2  = V
    
    # define record vectors 
    
    # naf
    res["naf"][V] = h.Vector()
    res["naf"][V].record(cell(0.5).naf._ref_ina)
    
    # na15a
    res["na15a"][V] = h.Vector()
    res["na15a"][V].record(cell(0.5).na15a._ref_ina)
    
    # naf
    res["naf_ms"][V] = h.Vector()
    res["naf_ms"][V].record(cell(0.5).naf_ms._ref_ina)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)


    # finilize and run
    h.finitialize(clampBase)
    while h.t < 120:
        h.fadvance()
    
    
    # plotting ***
    
    # potential
    ax[0,0].plot(tm, vm, 'k')
    ax[0,0].plot(tm[start_index], vm[start_index], 'og', ms=10)
    ax[0,0].plot(tm[end___index], vm[end___index], 'og', ms=10)
    
    # current
    for i, chan in enumerate(channels):
        
        # plot current
        ax[i,1].plot(tm, res[chan][V], color=color[i], lw=3)
    
        # get peak values of current 
        extract_peak = res[chan][V].to_python()
        Max[chan].append( min( extract_peak[start_index:end___index] ) )
    


# plotting IV 


for i, chan in enumerate(channels):
    
    ax[1,0].plot(voltage, 
                 np.divide(Max[chan],-min(Max[chan])), 
                 '-o',
                 color=color[i], 
                 ms=10,
                 alpha=0.5,
                 label=chan
                )
    
    ax[i,1].set_xlim([29, 35])

ax[0,0].set_xlim([29, 35])
    
ax[1,0].spines['right'].set_color('none')
ax[1,0].spines['top'  ].set_color('none')
ax[1,0].xaxis.set_ticks_position('bottom')
ax[1,0].spines['bottom'].set_position(('data',0))
ax[1,0].yaxis.set_ticks_position('left')
ax[1,0].spines['left'].set_position(('data',0))

ax[1,0].legend(loc='best')

fig.savefig('../../../Desktop/fig1_naf.png', transparent=True)

# Protocol 2 =============================================================================
#            fig 6 in Ogata and Tatebayashi, 1990

clamp.dur1  =  1000
clamp.dur2  =  30
clamp.amp2  = -10
clamp.dur3  =  20
clamp.amp3  = -80



# Create plot window  

fig,ax      = plt.subplots( 3,2, figsize=(12,8) )

# Define result structures etc
start_index = int((995)/h.dt)
end___index = int((1025)/h.dt)
voltage     = np.arange(-130,-21,10)

channels    = ["naf", "na15a", "naf_ms"]
res         = {"na15a":{}, "naf":{}, "naf_ms":{}}
Max         = {"na15a":[], "naf":[], "naf_ms":[]} 


# run clamp protocol ------
for v,V in enumerate(voltage):

    clamp.amp1  = V
    
    # define record vectors 
    
    # naf
    res["naf"][V] = h.Vector()
    res["naf"][V].record(cell(0.5).naf._ref_ina)
    
    # na15a
    res["na15a"][V] = h.Vector()
    res["na15a"][V].record(cell(0.5).na15a._ref_ina)
    
    # naf
    res["naf_ms"][V] = h.Vector()
    res["naf_ms"][V].record(cell(0.5).naf_ms._ref_ina)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)


    # finilize and run
    h.finitialize(-80)
    while h.t < 1100:
        h.fadvance()
    
    
    # plotting ***
    
    # potential
    ax[0,0].plot(tm, vm, 'k')
    ax[0,0].plot(tm[start_index], vm[start_index], 'og', ms=10)
    ax[0,0].plot(tm[end___index], vm[end___index], 'og', ms=10)

    
    # current
    for i, chan in enumerate(channels):
        
        # plot current
        ax[i,1].plot(tm, res[chan][V], color=color[i], lw=3)
    
        # get peak values of current 
        extract_peak = res[chan][V].to_python()
        Max[chan].append( min( extract_peak[start_index:end___index] ) )
        
    
for i, chan in enumerate(channels):
    
    ax[1,0].plot(voltage, 
                 np.divide(Max[chan],min(Max[chan])), 
                 '-o',
                 color=color[i], 
                 ms=10,
                 alpha=0.5,
                 label=chan
                )
    
    ax[i,1].set_xlim([999, 1005])

ax[0,0].set_xlim([999, 1005])
    
ax[1,0].spines['right'].set_color('none')
ax[1,0].spines['top'  ].set_color('none')
ax[1,0].xaxis.set_ticks_position('bottom')
ax[1,0].spines['bottom'].set_position(('data',0))
ax[1,0].yaxis.set_ticks_position('left')
ax[1,0].spines['left'].set_position(('data',0))

ax[1,0].legend(loc='best')

fig.savefig('../../../Desktop/fig2_naf.png', transparent=True)    

H = Max
H['V'] = voltage

# Protocol 3 =============================================================================
#            fig 3 in Ogata and Tatebayashi, 1990


clamp.amp1  = -80
clamp.dur1  =  30
clamp.amp2  = -20
clamp.amp3  = -80
clamp.dur3  =  30



# Define result structures etc
start_index = int((995)/h.dt)
end___index = int((1025)/h.dt)
voltage     = np.arange(0.2, 1.01, 0.1)

channels    = ["naf", "na15a", "naf_ms"]
res         = {"na15a":{}, "naf":{}, "naf_ms":{}}
Max         = {"na15a":[], "naf":[], "naf_ms":[]} 

# Create plot window  

fig,ax      = plt.subplots( len(voltage), 1, figsize=(4,18) )

# run clamp protocol -------
for v,V in enumerate(voltage):

    clamp.dur2  = V
    
    # define record vectors 
    
    # naf
    res["naf"][V] = h.Vector()
    res["naf"][V].record(cell(0.5).naf._ref_ina)
    
    # na15a
    res["na15a"][V] = h.Vector()
    res["na15a"][V].record(cell(0.5).na15a._ref_ina)
    
    # naf
    res["naf_ms"][V] = h.Vector()
    res["naf_ms"][V].record(cell(0.5).naf_ms._ref_ina)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)


    # finilize and run
    h.finitialize(-80)
    while h.t < 70:
        h.fadvance()
        
    # extract plot index
    start_index = next(x[0] for x in enumerate(tm) if x[1] > 30+V) 
    end___index = next(x[0] for x in enumerate(tm) if x[1] > 30+V+1)
    
    # plotting ***
    
    # potential
    ax[v].plot(tm, np.add(np.multiply(vm,1e-3),0.3), 'k')
    ax[v].plot(tm[start_index], 0.15, '|k', ms=10)
    ax[v].plot(tm[end___index], 0.15, '|k', ms=10)
    
    # current
    for i, chan in enumerate(channels):
        
        # plot current
        ax[v].plot(tm, np.divide(res[chan][V],-1*min(res[chan][V])), color=color[i], lw=2)
        
        # get peak values of current 
        extract_peak = res[chan][V].to_python()
        Max[chan].append( min( extract_peak[start_index:end___index] ) )
        
    ax[v].set_xlim([29.5, 32.5])

fig,ax      = plt.subplots( 1, 1, figsize=(4,4) )

for i, chan in enumerate(channels):
    
    
    
    ax.plot( voltage, 
             np.divide(Max[chan],-1*min(Max[chan])), 
             '-o',
             color=color[i], 
             ms=10,
             alpha=0.5,
             label=chan
           )


ax.spines['right'].set_color('none')
ax.spines['top'  ].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data',0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))

ax.legend(loc='best')
    
plt.show()


'''
# Johannas code    
stim=neuron.h.SEClamp(0.5)
stim.rs=resistance
neuron.h.tstop=TB
neuron.h.v_init=V0
neuron.h.init()
stim.dur1 = TB
v_clamp = neuron.h.Vector()
v_clamp.from_python(v)
v_clamp.play(stim._ref_amp1, self.dt)'''
        
                                              
                                                    
                                                    
                                                    
    
    
    
          
    
        

