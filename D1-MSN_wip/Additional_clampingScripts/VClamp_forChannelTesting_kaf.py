'''
Script for running channel testing in V-clamp 

single compartment cell with inserted mechanisms of choice
'''

from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt


# create cell and general setup ----------------------------------------------------------
cell = h.Section(name='cell')


# insert and specify mechanisms 
for mech in ["kv4"]:
    cell.insert(mech)

cell(0.5).gbar_kv4    = 1e-7

casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell) 
pointer =   casc._ref_Target1p

h.setpointer( pointer, 'pka',  cell(0.5).kv4)

'''
cell(0.5).qfact_kv4    = 2
cell(0.5).mvhalf_kv4   = -85
cell(0.5).mslope_kv4   = -5'''

# Protocol 1 =============================================================================
#            fig 2 in Tkatch et al., 2000

clampBase   = -40
clamp       = h.SEClamp(cell(0.5))
clamp.rs    = 0.01
clamp.dur1  = 100
clamp.amp1  = clampBase
clamp.dur2  = 100
clamp.amp2  = -95
clamp.dur3  = 500



# Create plot window  
fig,ax      = plt.subplots( 3,1, figsize=(4,12) )

# Define result structures etc
start_index = int((190)/h.dt)
end___index = int((690)/h.dt)
voltage     = np.arange(-70,51,10)
N           = len(voltage)
gradient    = [(1.0*x/(N-1), 0, 1-1.0*x/(N-1)) for x in range(N)]
res         = {}
Max         = []
    


# run clamp protocol ---------------------------------------------------------------------
for v,V in enumerate(voltage):
    
    clamp.amp3  = V
    
    # define record vectors 
    res[V] = h.Vector()
    res[V].record(cell(0.5).kv4._ref_ik)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)


    # finilize and run
    h.finitialize(clampBase)
    while h.t < 750:
        h.fadvance()
    
    
    # plotting ***
    
    # potential
    ax[0].plot(tm, vm, 'k')
    ax[0].plot(tm[start_index], vm[start_index], 'og', ms=10)
    ax[0].plot(tm[end___index], vm[end___index], 'og', ms=10)
    
    # current
    ax[1].plot(tm, res[V], color=gradient[v], lw=3)
    
    # get peak values of current 
    extract_peak = res[V].to_python()
    Max.append( max( extract_peak[start_index:end___index] ) )


# plotting IV 

ax[2].plot(*np.loadtxt('Channel_data/Kaf-Tkatch2000/kaf_m_curve.csv', unpack=True), color='k', ls='--')

ax[2].plot(voltage, np.divide(Max,max(Max)), color='g', lw=3)

ax[2].spines['right'].set_color('none')
ax[2].spines['top'  ].set_color('none')
ax[2].xaxis.set_ticks_position('bottom')
ax[2].spines['bottom'].set_position(('data',0))
ax[2].yaxis.set_ticks_position('left')
ax[2].spines['left'].set_position(('data',0))


# configure   
ax[1].set_xlim([190, 310])


# Protocol 2 =============================================================================
#            fig 3 in Tkatch et al., 2000


clamp       = h.SEClamp(cell(0.5))
clamp.rs    = 0.01
clamp.dur1  = 1500
clamp.dur2  = 500
clamp.amp2  = 0
clamp.dur3  = 50
clamp.amp3  = -60



# Create plot window  
f2,a2      = plt.subplots( 3,1, figsize=(4,12) )

# Define result structures etc
start_index = int((1490)/h.dt)
end___index = int((1990)/h.dt)
voltage     = np.arange(-120,-44,15)
N           = len(voltage)
gradient    = [(1.0*x/(N-1), 0, 1-1.0*x/(N-1)) for x in range(N)]
res         = {}
Max         = []  


# run clamp protocol ---------------------------------------------------------------------
for v,V in enumerate(voltage):

    
    clamp.amp1  = V
    
    # define record vectors 
    res[V] = h.Vector()
    res[V].record(cell(0.5).kv4._ref_ik)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)


    # finilize and run
    h.finitialize(V)
    while h.t < 2100:
        h.fadvance()
    
    
    # plotting ***
    
    # potential
    a2[0].plot(tm, vm, 'k')
    a2[0].plot(tm[start_index], vm[start_index], 'og', ms=10)
    a2[0].plot(tm[end___index], vm[end___index], 'og', ms=10)
    
    # current
    a2[1].plot(tm, res[V], color=gradient[v], lw=3)
    
    # get peak values of current 
    extract_peak = res[V].to_python()
    Max.append( max( extract_peak[start_index:end___index] ) )
    

a2[1].set_xlim(1490, 2010)

a2[2].plot(voltage, np.divide(Max,max(Max)), 'o', color='g', ms=10)
a2[2].plot(*np.loadtxt('Channel_data/Kaf-Tkatch2000/kaf_h_curve.csv', unpack=True), color='k', ls='--') 

a2[2].spines['right'].set_color('none')
a2[2].spines['top'  ].set_color('none')




# Protocol 3 =============================================================================
#            fig 4 in Tkatch et al., 2000



clamp       = h.SEClamp(cell(0.5))
clamp.rs    =  0.01
clamp.dur1  =  200
clamp.amp1  =  0

clamp.amp2  = -95
clamp.dur3  =  200
clamp.amp3  =  0



# Create plot window  
f3,a3      = plt.subplots( 3,1, figsize=(4,12) )

# Define result structures etc
time        = np.arange(2,61,2)
N           = len(time)
gradient    = [(1.0*x/(N-1), 0, 1-1.0*x/(N-1)) for x in range(N)]
res         = {}
Max         = []  


# run clamp protocol ---------------------------------------------------------------------
for v,T in enumerate(time):
    
    clamp.dur2  = T
    
    # define record vectors 
    res[T] = h.Vector()
    res[T].record(cell(0.5).kv4._ref_ik)

    tm = h.Vector()
    tm.record(h._ref_t)

    vm = h.Vector()
    vm.record(cell(0.5)._ref_v)


    # finilize and run
    h.finitialize(-95)
    while h.t < 500:
        h.fadvance()
    
    # extract plot index
    start_index = next(x[0] for x in enumerate(tm) if x[1] > 200+T-1) 
    end___index = next(x[0] for x in enumerate(tm) if x[1] > 200+T+200-1)
    
    
    # plotting ***
    
    # potential
    a3[0].plot(tm, vm, 'k')
    a3[0].plot(tm[start_index], vm[start_index], 'og', ms=10)
    a3[0].plot(tm[end___index], vm[end___index], 'og', ms=10)
    
    # current
    a3[1].plot(tm, res[T], color=gradient[v], lw=3)
    
    # get peak values of current 
    extract_peak = res[T].to_python()
    Max.append( max( extract_peak[start_index:end___index] ) )
    

#a3[1].set_xlim(1490, 2010)

a3[2].plot(time, np.divide(Max,max(Max)), 'o', color='g', ms=10)
a3[2].plot([time[0],time[-1]],[0.63, 0.63], '--k')
#a3[2].set_xlim(0,1000) 

a3[2].spines['right'].set_color('none')
a3[2].spines['top'  ].set_color('none')


plt.show()


'''
# Johannas code    
stim=neuron.h.SEClamp(0.5)
stim.rs=resistance
neuron.h.tstop=TB
neuron.h.v_init=V0
neuron.h.init()
stim.dur1 = TB
v_clamp = neuron.h.Vector()
v_clamp.from_python(v)
v_clamp.play(stim._ref_amp1, self.dt)'''
        
                                              
                                                    
                                                    
                                                    
    
    
    
          
    
        

