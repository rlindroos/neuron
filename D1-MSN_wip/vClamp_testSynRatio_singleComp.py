from    neuron  import          h
import  numpy                as np
import  matplotlib.pyplot    as plt

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

import neuron as nrn
nrn.load_mechanisms('Mech/')

# simulation length
tstop = 2000

# create cell
dend = h.Section(name='dend')

# add new receptors

# create synapse
x = 0.5
syn         = h.glutamate(x, sec=dend)
#syn.use_stp = 0

syn.ratio = 2

stim = h.VecStim()
vec = h.Vector(np.array([0.1,1.1])*1e3)

stim.play(vec)


# create NetCon object
ncon             = h.NetCon(stim, syn)
ncon.delay       = 1
ncon.weight[0]   = 1.5e-3 # (uS) = 1.5 nS
#ncon.threshold   = 0.1

# record vectors
s = h.Vector()
s.record(syn._ref_i)
tm  = h.Vector()
tm.record(h._ref_t)
vm = h.Vector()
vm.record(dend(0.5)._ref_v)


# set V-clamp
clamp       = h.SEClamp(dend(0.5))
clamp.rs    = 0.01
clamp.dur1  = 1000
clamp.dur2  = 1000
clamp.amp1  = -70
clamp.amp2  = 40

h.finitialize(-70)
while h.t < tstop:
    h.fadvance()

f,a = plt.subplots(2,1)
a[0].plot(tm,vm)
a[0].set_ylim([-80,50])
a[1].plot(tm,s)

ampa = np.abs(min(s))
nmda = next(s[i] for i,t in enumerate(tm) if t >1150)

print ampa, nmda, ampa/nmda

plt.show()
    
    
    
    
    

        

