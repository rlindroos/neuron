
'''
Jitters geometetry of a neuronal morphology while keeping its electrical properties.

Run from terminal, e.g.
    
    python morphology_functions.py 1 10 30
    
    arguments give 
        -version (if running multiple copies with same rotation and maximal angle
        -number of rotations
        -maximal angle

Can also be used to create multiple jittered morphologies by using 
    
    morphology_jobstarter.sh 



For unknown reason, working for 

    WT-dMSN_P270-20_1.02_SGA1-m24.swc

but not for 

    WT-P270-20-15ak-cor.swc



The jitter is achieved by rotating random dendritical subtrees.

Two functions using different coordinate systems can be used in the jittering:
-F1. Spherical coordinates centered in the random point with random change of angle theta 
    and phi.
-F2. cartesian coordinates is used together with a random 3D rotation matrix:
    https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions 
    
'''

# TODO
# Find why not working for second morphology?


import sys
import numpy                as np
import matplotlib.pyplot    as plt
from mpl_toolkits.mplot3d import Axes3D

morphology      = {'stem':{}, 'sec':{}, 'points':{}, 'sortlist':[]}
x_morph_none    = []
y_morph_none    = []
z_morph_none    = []
r_morph_none    = []
prev_point      = '0'
sec_coordinates = {}
bp              = {'all':{}, 'branching':{}}
axon            = {'points':{}, 'sortlist':[]}

morphology_file_name    = 'WT-dMSN_P270-20_1.02_SGA1-m24.swc'   # 'WT-P270-20-15ak-cor.swc' # 
out_file_base           = 'Rotated_morphologies/WT-dMSN_P270-20_1.02_offspring'



def check_if_branching(file_name):
    # check if branching points (parent to more than one point)
    with open(file_name) as f:
        
        for line in f.readlines(): # for line in file...  
            if line[0] in ['#', ' ']: continue
            l = line.split() 
            if l[6] in bp['all']:
                bp['branching'][l[6]] = True
            else:
                bp['all'][l[6]] = True
    return bp



def read_morphology(file_name):
    # extract data from morphology file
    with open(file_name) as f:    
        for line in f.readlines(): 
            
            if line[0] in ['#', ' ']: continue
                
            l = line.split()
            
            if l[1] == '2': 
                axon['points'][l[0]] = {  
                                        'type'      :   l[1],
                                        'x'         :   l[2],
                                        'y'         :   l[3],
                                        'z'         :   l[4],
                                        'radius'    :   l[5],
                                        'parent'    :   l[6]   
                                        }
                axon['sortlist'].append(l[0])
                continue
            
            morphology['points'][ l[0] ] = {  
                                    'type'      :   l[1],
                                    'x'         :   l[2],
                                    'y'         :   l[3],
                                    'z'         :   l[4],
                                    'radius'    :   l[5],
                                    'parent'    :   l[6]   
                                            }
            
            morphology['sortlist'].append(l[0])
            
            if l[6] == '-1':
                sec =0
                stem=0
                morphology['sec'][ sec ] = []
                morphology['stem'][ stem ] = [sec]
            elif l[6] != prev_point:
                
                # add None to not connect end point with next start point
                x_morph_none.append( np.nan )
                y_morph_none.append( np.nan )
                z_morph_none.append( np.nan )
                r_morph_none.append( np.nan )
                # add coordinates of parent as start point of new branch
                x_morph_none.append( float(morphology['points'][l[6]]['x']) )
                y_morph_none.append( float(morphology['points'][l[6]]['y']) )
                z_morph_none.append( float(morphology['points'][l[6]]['z']) )
                r_morph_none.append( float(morphology['points'][l[6]]['radius']) )
                
                # update sec
                sec += 1
                if l[6] == '1':
                    stem += 1
                    morphology['stem'][ stem ] = [sec]
                else:
                    morphology['stem'][ stem ].append( sec )
                morphology['sec'][ sec ] = [ l[0] ]
            else:
                morphology['sec'][ sec ].append( l[0] )
                if l[0] in bp['branching']:
                    # update sec
                    sec += 1
                    morphology['sec'][ sec ] = []
                    morphology['stem'][ stem ].append( sec )
            
            # add point
            x_morph_none.append( float(l[2]) )
            y_morph_none.append( float(l[3]) )
            z_morph_none.append( float(l[4]) )
            r_morph_none.append( float(l[5]) )
                
            prev_point = l[0]  


def write_to_swc(morph, out_file_name, morphology_file_name, i,n,a):
    # writes morphology to file by looping all points in dict
    
    file = open(out_file_name+'.swc', 'w')
    
    file.write('# morphology created by file morphology_function.py base on file:')
    file.write('\n# \t%s\n' %morphology_file_name)
    file.write('# number of rotation points: %d\n' % (n) )
    file.write('# maximum angle: %d\n' % (a) )
    file.write('# version: %d\n' % (i) )
    
    for key in morphology['sortlist']:
        # key = name
        t  = morphology['points'][ key ]['type'  ]
        r  = morphology['points'][ key ]['radius']
        p  = morphology['points'][ key ]['parent']
        
        x  = morph[ key ]['x']
        y  = morph[ key ]['y']
        z  = morph[ key ]['z']
        
        file.write('%s %s %0.6f %0.6f %0.6f %s %s\n' % (key, t, x, y, z, r, p) )
    
    for key in axon['sortlist']:
        # key = name
        t  = axon['points'][ key ]['type'  ]
        r  = axon['points'][ key ]['radius']
        p  = axon['points'][ key ]['parent']
        x  = axon['points'][ key ]['x']
        y  = axon['points'][ key ]['y']
        z  = axon['points'][ key ]['z']
        
        file.write('%s %s %s %s %s %s %s\n' % (key, t, x, y, z, r, p) )
    
    file.close()



def center_morph_in_origo():
    # make sure soma is in origo (extract somatic coordinates from all points; absolute coordinates)
    
    for point in morphology['points']:
        for ax in ['x', 'y', 'z']:
        
            morphology['points'][point][ax] =   float(morphology['points'][point][ax]) - \
                                                float(morphology['points']['1'][ax])


def add_relative_and_spherical_coord():
    # adds spherical coordinates to morphology
    
    morphology['relative' ] = {}
    morphology['spherical'] = {}
    
    for point in morphology['sortlist'][1:]:
        
        # get parent coord
        parent  = morphology['points'][ point ]['parent']
        
        x1 = morphology['points'][parent]['x']
        y1 = morphology['points'][parent]['y']
        z1 = morphology['points'][parent]['z']
        
        # get point coord
        x2 = morphology['points'][point ]['x']
        y2 = morphology['points'][point ]['y']
        z2 = morphology['points'][point ]['z']
        
        # calc diff (relative coord) and add to relative
        x  = x1-x2
        y  = y1-y2
        z  = z1-z2
        
        morphology['relative'][point] = {'x':x, 'y':y, 'z':z}
        
        # calc spherical coords and add to spherical
        r       = np.sqrt(x**2 + y**2 + z**2)
        theta   = np.arccos( z/r ) 
        phi     = np.arctan2( y, x )
        
        morphology['spherical'][point] = {'r':r, 'theta':theta, 'phi':phi}



def spherical_to_cartesian(morph):
    # recalculate cartesian coords from rotated spherical coord
    
    morph['points'] = { '1': {'x':0, 'y':0, 'z':0} }
    
    for point in morphology['sortlist'][1:]:
        
        r       = morph[point]['r'    ]
        theta   = morph[point]['theta']
        phi     = morph[point]['phi'  ]
        
        x = r * np.sin(theta) * np.cos(phi)
        y = r * np.sin(theta) * np.sin(phi)
        z = r * np.cos(theta)
        
        parent = morphology['points'][point]['parent']
        
        x0 = morph['points'][parent]['x']
        y0 = morph['points'][parent]['y']
        z0 = morph['points'][parent]['z']
        
        morph['points'][point] = {'x':x0+x, 'y':y0+y, 'z':z0+z}
    
    return morph


        

def mirror_stem(morph, stem_number, m=[]):
    #randomly change direction of stem in morph
    
    if len(m) != 3:
        # draw direction of x, y and z (mirrored or not, -1/1)
        m = np.random.choice([-1,1],3)
    
    #loop over stem and mirror coordinates 
    for sec in morphology['stem'][stem_number]:
        for point_name in morphology['sec'][sec]:
            
            morph[point_name]['x'] = m[0] * float(morph[point_name]['x'])    
            morph[point_name]['y'] = m[1] * float(morph[point_name]['y'])
            morph[point_name]['z'] = m[2] * float(morph[point_name]['z'])




def center_subtree_at_node(subtree):
    # calculate coordinates in subtree space (centered at parent node)
    
    centered_subtree = {}
    
    for p,point in enumerate(subtree):
        
        if p == 0:
            # don't need parent
            x = morphology['relative'][point]['x']
            y = morphology['relative'][point]['y']
            z = morphology['relative'][point]['z']
        else:
            # get parent
            parent = morphology['points'][point]['parent']
            
            x = centered_subtree[parent]['x'] + morphology['relative'][point]['x']
            y = centered_subtree[parent]['y'] + morphology['relative'][point]['y']
            z = centered_subtree[parent]['z'] + morphology['relative'][point]['z']
        
        # calc centered coordinates based on relative coordinates
        centered_subtree[point] = {
                        'x': x,
                        'y': y,
                        'z': z    }
        
    return centered_subtree
    
    
    
    
def get_subtree(P):
    # return subtree of P
    
    subtree = [P]
    
    # get index of P in sortlist
    index = morphology['sortlist'].index(P)
    
    # loop over points downstream of index and check if parents in subtree. else brake
    for point in morphology['sortlist'][index+1:]:
        parent = morphology['points'][point]['parent']
        if parent in subtree:
            subtree.append(point)
        else: return subtree[1:]
    
    return subtree[1:]
        
        

def random_rotation(morph, n_rotation_points=50, max_angle=30):
    # rotate n points by random angle around theta and phi
    
    # draw random rotation points
    rotation_points = np.random.choice( morphology['sortlist'], 
                                        size=n_rotation_points,
                                        replace=False   )
    
    for P in rotation_points:
        # draw rotation angles
        m       = max_angle * np.pi / 180
        angles  = np.random.uniform(low=-m, high=m, size=2)
        
        # create subtree
        subtree = get_subtree(P)
        
        #print P
        #print subtree
        
        # rotate subtree in point
        for p in subtree:
            morph[p]['theta'] = morph[p]['theta'] + angles[0]
            morph[p]['phi'  ] = morph[p]['phi'  ] + angles[1]
            
    return morph
    


def rotate_using_rotation_matrix(morph, n_rotation_points=50, max_angle=30):
    # rotate n points by random angle around axis using rotation matrix
    
    # draw random rotation points
    rotation_points = np.random.choice( morphology['sortlist'], 
                                        size=n_rotation_points,
                                        replace=False   )
    
    for P in rotation_points:
        # draw rotation angles
        m       = max_angle * np.pi / 180
        angles  = np.random.uniform(low=-m, high=m, size=3)
        
        # create subtree
        subtree = get_subtree(P)
        
        # get parent node coordinates
        x = morphology['points'][P]['x']
        y = morphology['points'][P]['y']
        z = morphology['points'][P]['z']
        
        # create rotation matrixes
        rx = [  [1, 0,                 0                ], 
                [0, np.cos(angles[0]),-np.sin(angles[0])],
                [0, np.sin(angles[0]), np.cos(angles[0])] ]
                        
        ry = [  [ np.cos(angles[1]), 0, np.sin(angles[1])], 
                [0,                  1, 0                ],
                [-np.sin(angles[1]), 0, np.cos(angles[1])] ]
                        
        rz = [  [np.cos(angles[0]),-np.sin(angles[0]),0],
                [np.sin(angles[0]), np.cos(angles[0]),0],
                [0,                 0,                1] ]
        
        R  = np.matmul( np.matmul(rx,ry),rz )
        
        for point in subtree:
            # center subtree in P and apply roatation
            v  = [  x - morphology['points'][point]['x'],
                    y - morphology['points'][point]['y'],
                    z - morphology['points'][point]['z']    ]
            
            new_coords = np.matmul(R,v)
            
            # map coordinates back to original space
            morph[point]['x'] = x - new_coords[0]
            morph[point]['y'] = y - new_coords[1]
            morph[point]['z'] = z - new_coords[2] 
    
    return morph




def plot_morphologies(morph_list, out_file_name, plot_org=True, save_fig=True):
    # creates x,y,z lists and plots a 3D plot
    
    fig     = plt.figure()
    ax      = fig.add_subplot(111, projection='3d')
    
    color   = ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ffff33']
    c       = 0
    
    if plot_org:
        # plot original morph
        ax.plot(x_morph_none, y_morph_none, z_morph_none, lw=2, color=color[c], alpha=0.8)
        c += 1
        
    for morph in morph_list:
        X = []
        Y = []
        Z = []
        for sec in morphology['sec']:
            
            # add coordinates of parent as start point of new branch
            parent = morphology['points'][ morphology['sec'][sec][0] ]['parent']
            
            X.append( float(morph[parent]['x']) )
            Y.append( float(morph[parent]['y']) )
            Z.append( float(morph[parent]['z']) )
            
            for point in morphology['sec'][sec]:
            
                X.append( morph[point]['x'] )
                Y.append( morph[point]['y'] )
                Z.append( morph[point]['z'] )
        
            # add None to not connect end point with next start point
            X.append( np.nan )
            Y.append( np.nan )
            Z.append( np.nan )
            
        ax.plot(X,Y,Z, lw=2, color=color[c], alpha=0.8)
        c += 1
        
    if save_fig:
        plt.savefig(out_file_name+'.png')
        plt.close('all')
    else:
        plt.show()
    
                
  
check_if_branching(morphology_file_name)    # find branching points; needed to split morphology in sections
read_morphology(morphology_file_name)
center_morph_in_origo()                     # make sure morphology is centered at origo
add_relative_and_spherical_coord()

# rotate and save family of morphologies
i = int(sys.argv[1])
n = int(sys.argv[2])
a = int(sys.argv[3])

morph = rotate_using_rotation_matrix(morphology['points'],n_rotation_points=n, max_angle=a)
out_file_name = '%s_nrot%d_maxang%d_v%d' % (out_file_base, n, a, i)
plot_morphologies( [morph], out_file_name, save_fig=False )
# write_to_swc(morph, out_file_name, morphology_file_name, i=i,n=n,a=a)

# using spherical coords----------------------------------------------
'''


# rotate and save family of morphologies
i = int(sys.argv[1])
n = int(sys.argv[2])
a = int(sys.argv[3])
        
morph = random_rotation(morphology['spherical'],n_rotation_points=n, max_angle=a)
morph = spherical_to_cartesian(morph)

for stem in morphology['stem']:
    mirror_stem(morph['points'], stem, m=[-1,-1,-1])
    
out_file_name = '%s_nrot%d_maxang%d_v%d' % (out_file_base, n, a, i)
write_to_swc(morph['points'], out_file_name, morphology_file_name, i, n, a)    

plot_morphologies( [morph['points']], out_file_name )  
'''
