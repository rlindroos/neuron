#
'''
Master script used for random tuning of dMSN and iMSN. 

-The tuning is done by randomly chosing dendritic distribution parameters for the 
following channels:

    ['sk', 'can' 'cat32', 'cat33', 'kir', 'kas', 'kaf', 'naf']

from a sigmoidal distribution (except for kir and sk that are uniform):

    y = a4 + a5/(1 + np.exp((x-a6)/a7) )

kaf:
a4  1
a5  np.random.uniform(0.1,0.9)
a6  np.random.uniform(1,130)
a7  np.random.uniform(-3,-70)

naf:
a4  1-a5
a5  np.random.uniform(0.0,1.0)
a6  np.random.uniform(20,50)
a7  np.random.uniform(1,50)

and then validating the somatic excitability to Planert et al 2013 (FI curve)
and dendritic excitability to Day et al 2008 (bAP induced Ca change).

-Cell type is randomly chosen. 
The differences between the cell type protocoles are:

* Kaf channel "base value" (0.11 vs 0.06 in dMSN and iMSN, respectively)
* morphology   

Robert Lindroos (RL) <robert.lindroos at ki.se>
 
Based on original Neuron implementation by  
Alexander Kozlov <akozlov at kth.se>

Implemented in colaboration with Kai Du <kai.du at ki.se>
'''



from __future__ import print_function, division
from neuron import h
import numpy                as np
import MSN_builder          as build
import common_functions     as use

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


# global result dicts
RES             = {}
VARIABLES       = {}
CA              = {}
    

        
# main ==================================================================================

def main(       par='./params_iMSN.json',               \
                morphology='WT-iMSN_P270-09_1.01_SGA2-m1.swc',  \
                sim='vm',                               \
                amp=400e-12,                            \
                run=None,                               \
                cell_type='D2',                         \
                simDur=1.0,                             \
                stimDur=0.9         ): 
    
    print('-iter:', run, '-amp:', amp, '[A]', '-sim', sim)
    
    
    # draw/load random variables for first stage only (Ca). Else load old values
    if sim == 'ca':
        
        # gbar*10^r0*( 1-r1 + r1/(1 + exp{(dist-r2)/r3}) )
        naf = [ np.random.uniform(-0.5,0.5),    \
                np.random.uniform(0.8,1.0),     \
                np.random.uniform(10.0,60.0),   \
                np.random.uniform(1.0,30.0)     ]
        
        # gbar*10^r0*( 1 + r1/(1 + exp{(dist-r2)/r3}) )        
        kaf = [ np.random.uniform(-0.5,0.5),    \
                np.random.uniform(0.0,0.9),     \
                np.random.uniform(1.0,130.0),   \
                np.random.uniform(-3.0,-70.0)   ]
        
        # gbar*10^r0*( 0.1 + 0.9/(1 + exp{(dist-r2)/r3}) )
        kas = [ np.random.uniform(-0.5,0.5),    \
                np.random.uniform(-5.0,60.0),   \
                np.random.uniform(1.0,70.0)     ]
        
        # gbar*10^r0*
        kir = [ np.random.uniform(-0.5,0.5)     ]
        
        # gbar*10^r0*
        sk = [ np.random.uniform(-0.5,0.5)     ]
        
        # 10^r0*( 1-r1 + r1/(1 + exp{(dist-r2)/r3}) )
        can = [ np.random.uniform(-7.0,-5.0),   \
                np.random.uniform(0.8,1.0),     \
                np.random.uniform(10.0,60.0),   \
                np.random.uniform(1.0,30.0)     ]
        
        # 10^r0/(1 + exp{(dist-r1)/r2}) )
        c32 = [ np.random.uniform(-9.0,-6.0),   \
                np.random.uniform(1.0,130.0),   \
                np.random.uniform(-3.0,-70.0)   ]
        
        # 10^r0/(1 + exp{(dist-r1)/r2}) )
        c33 = [ np.random.uniform(-9.0,-6.0),   \
                np.random.uniform(1.0,130.0),   \
                np.random.uniform(-3.0,-70.0)   ]
        
        # draw random variables
        random_variables = {'naf':naf, 'kaf':kaf, 'kas':kas, 'kir':kir, 'sk':sk, 'can':can, 'c32':c32, 'c33':c33}
        
        VARIABLES[run] = random_variables
    else:
        random_variables = VARIABLES[run]
        
    
    
    # initiate cell
    cell = build.MSN(  params=par,             \
                       morphology=morphology,  \
                       variables=random_variables  )
                       
    
    # set cascade--not connected to channels in this script, 
    # but used for setting pointers needed in the channel mechnisms
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    
    # all channels to modulate
    mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    
    
    # set pointer (for all channel instances in mod_list)
    for sec in h.allsec():
        for seg in sec:
            for mech in seg:
                if mech.name() in mod_list:
                    
                    h.setpointer(pointer, 'pka', mech )
       
    
    # set current injection
    stim        =   h.IClamp(0.5, sec=cell.soma)
    stim.amp    =   amp*1e9  
    stim.delay  =   0.1e3
    stim.dur    =   stimDur*1e3    
     
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(cell.soma(0.5)._ref_v)
    
    tstop       = simDur*1e3
    # dt = default value; 0.025 ms (25 us)
                  
    
    
    
    # configure simulation to record from both calcium pools.
    # the concentration is here summed, instead of averaged. 
    # This doesn't matter for the validation fig, since relative concentration is reported.
    # For Fig 5B, where concentration is reported, this is fixed when plotting
    # (dividing by 2 since the volume of both pools are the same).
    # -> see the plot_Ca_updated function in plot_functions.
    if sim == 'ca':
        
        for i,sec in enumerate(h.allsec()):
            
            if sec.name().find('axon') < 0: # don't record in axon
            
                for j,seg in enumerate(sec):
                    
                    sName = sec.name().split('[')[0]
                    
                    
                    # N, P/Q, R Ca pool
                    cmd = 'ca_%s%s_%s = h.Vector()' % (sName, str(i), str(j))
                    exec(cmd)
                    cmd = 'ca_%s%s_%s.record(seg._ref_cai)' % (sName, str(i), str(j))
                    exec(cmd)   
                    
                    # the L-type Ca
                    cmd = 'cal_%s%s_%s = h.Vector()' % (sName, str(i), str(j))
                    exec(cmd)
                    cmd = 'cal_%s%s_%s.record(seg._ref_cali)' % (sName, str(i), str(j))
                    exec(cmd)   
                    
                    
                    # uncomment here if testing kaf blocking effect on bAP
                    #block_fraction = 0.2
                    #gbar           = seg.kaf.gbar
                    #seg.kaf.gbar   = (1 - block_fraction) * gbar
    
    
              
    # solver------------------------------------------------------------------------------            
    cvode = h.CVode()
    
    h.finitialize(cell.v_init)
    
    # run simulation
    while h.t < tstop:
                
        h.fadvance()
        
    
    # save output ------------------------------------------------------------------------
    
    if sim == 'ca':
        
        res         = {}
        if cell_type == 'D1':
            distances   = np.arange(40,200, 10)
        else:
            distances   = np.arange(49,200, 10)
               
        # ca
        for i,sec in enumerate(h.allsec()):
            if sec.name().find('axon') < 0:
                for j,seg in enumerate(sec):
                    
                    dist    =    int( np.round( h.distance(seg.x) ) ) 
                    
                    for k,d in enumerate(distances):
                
                        if dist > d-5 and dist < d+5:
                            
                            if d not in res:
                    
                                res[d] = []
                    
                            vName       =   'ca_%s%s_%s'  %  ( sName, str(i), str(j)  )
                            v2Name      =   'cal_%s%s_%s' %  ( sName, str(i), str(j)  )
                            cmd         =   'V = np.add(%s, %s)' % (vName, v2Name) # this is were concentrations are summed (see above ~line 175)
                            exec(cmd)
                            dCa         = max(V[3300:-1]) - V[3300]
                            res[d].append(dCa)
                    
        y       = []
        norm    = np.mean(res[distances[0]])
        for d in distances:
            
            y.append( np.divide(np.mean(res[d]), norm) )
        
        
        # add vector + variables to result dict             
        CA[run] = y  
        
        
        # only saved for first iteration of Ca sim
        if run == 0:
            RES['dist'] = distances  
        
        
        # check if fit is better than best fit
        if par == './params_dMSN.json':
            fit     = use.check_fit(y, cell='D1')   
        
        else:
            fit     = use.check_fit(y, cell='D2')
            
        if fit < 0.02:
            print('bAP->dCa good. How about rheobase?')
            return False
        else:
            return True
            
        
    elif sim == 'vm':
        
        # extract and save spike list
        spikes = use.getSpikedata_x_y(tm,vm)
        
        return spikes
        
        
    elif sim == 'rheobase':
        
        spikes = use.getSpikedata_x_y(tm,vm)
        return len( spikes ), tm, vm  
        
                       


# Start the simulation and save results
# Function needed for HBP compability  ===================================================
if __name__ == "__main__":
    
    
    coin_toss   = np.random.randint(1,3)
    ID          = False
    
    print('- creating cell of type D', coin_toss)
     
    if coin_toss == 1:
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        currents    =   np.ndarray.tolist( np.arange(240,485,20) )
    elif coin_toss == 2:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        currents    =   np.ndarray.tolist( np.arange(140,385,20) )
        
    
    print('starting sim')
    
    RES['current']  =   currents
    
    # numer of iterations
    for n in range(100000):
        
        # initiate
        freq        =   np.ndarray.tolist( np.zeros(len(currents)) )
        stop        =   False
        SPIKES      =   np.ndarray.tolist( np.zeros(len(currents)) )
        
        # dendritic validation: change in [Ca] following a bAP (validated against Day et al., 2008)
        current = 2000
        new_values = main(  par=par,                    \
                            morphology=morphology,      \
                            run=n,                      \
                            amp=current*1e-12,          \
                            simDur=200e-3,              \
                            stimDur=2e-3,               \
                            cell_type=cell_type,        \
                            sim='ca'            )   
        
        if new_values:
            print('bAP->dCa not close enough; draw new values')
            continue                                            
        
        # somatic validation: FI curve, validated against Planert et al., 2013 -----------
        
        # start with first current--if spikes -> break
        # followed by rest of currents in reversed order-- if don't spikes -> break
        # --- i.e. removes sets that does not spike, or spikes for all currents
        for index in [0]+range(len(currents)-1,0,-1):
            
            current = currents[index]
        
            spikes = main(  par=par,                    \
                            morphology=morphology,      \
                            amp=current*1e-12,          \
                            run=n,                      \
                            simDur=1.0,                 \
                            stimDur=0.9                 )
            
            SPIKES[index]   = spikes
            N               = len(spikes)
            
            print( '\t-index', index, '-spikes', N)
            
            
            if N == 0:
                if current == currents[-1]:
                    # set does not spike for maximal current
                    print('-iter:', n, 'dont spike for Imax -> break')
                    stop = True
                    break
                elif current != currents[0]:
                    if SPIKES[index+1][0] < 500:
                        print('-iter:', n, 'first spike rheobase too early -> break')
                        stop = True
                    break
            
            elif current == currents[0] and N > 0:
                # spikes for all currents
                print('-iter:', n, 'spikes for all currents -> break')
                stop = True
                break
            
            elif N > 70:
                print('-iter:', n, 'spike count too high -> break')
                stop = True
                break
                        
            else:
                if N == 1:
                    freq[index] = 1     # low arbitrary number
                else:
                    freq[index] = (N -1) * ( 1000 / (spikes[-1] -spikes[0]) )
                prev_curr   = current
                prev_spikes = N
                
        if stop: 
            continue
        
        
        # refine rheobase current (down to 1 pA) -------------------------------------
        rheobase_found  = False
        if prev_spikes < 3:
            diff = 5    
        else:
            diff = 10        
        
        # first current to try (base - diff pA)
            
        while not rheobase_found:
            
            # calc current amp
            current = prev_curr - diff
            
            print( '-starting rheobase sim:', current, '-last:', prev_curr )
            
            # run simulation
            results   = main(   par=par,                    \
                                morphology=morphology,      \
                                amp=current*1e-12,          \
                                sim='rheobase',             \
                                run=n,                      \
                                simDur=1.0,                 \
                                stimDur=0.9                 )
            
            print('\t---spikes for this current amp =', results[0])
        
            # recalculate diff (and update rheobase_found?)
            if diff == 10 and results[0] in [1, 2]:
                # +/- 5
                diff        = -5 + 10*np.ceil(results[0]/100.0)
                prev_curr   = current
                prev_spikes = results[0]
            
            else:
                
                if diff < 3:
                    # rheobase found?
                    if prev_spikes > 0 and results[0] == 0:
                        rheobase        = prev_curr
                        rheobase_found  = True
                        
                    elif prev_spikes == 0 and results[0] > 0:
                        rheobase        = current
                        rheobase_found  = True
                        tm              = results[1]
                        vm              = results[2]
                        
                    else:
                        tm  = results[1]
                        vm  = results[2]
                else:
                    tm   = results[1]
                    vm   = results[2]
                        
                
                # +/- 1
                diff        = -1 + 2*np.ceil(results[0]/100.0)
                prev_curr   = current
                prev_spikes = results[0]
    
        print( '\t\trheobase found (pA):', rheobase, '-iter:', n)
        
        # save run -----------------------------------------------------------------------
        RES[n]              = {}
        RES[n]['variables'] = VARIABLES[n]
        RES[n]['spikes']    = SPIKES 
        RES[n]['FI']        = freq
        RES[n]['Ca']        = CA[n]   
        RES[n]['RB']        = rheobase
        RES[n]['vm']        = vm
        
        if not 'tm' in RES:
            RES['tm']       = tm
        
        if not ID:
            ID = 'rand'  + str(np.random.randint(1,100000))
            
        use.save_obj(RES, ''.join(['random_brutalScan_', cell_type, '_', ID]) )  
    
                                                    
    
                                                    
                                                    
                                                    
                                                    
    
    
    
          
    
        

