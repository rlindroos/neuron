
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')


import matplotlib.pyplot as plt
import plot_functions    as plot
from   matplotlib    import animation
from   matplotlib    import cm
from   subprocess    import call  

plt.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'

# initialize animation writer frames per second (fps) = 2
FFMpegWriter = animation.writers['ffmpeg']
metadata = dict(title='Animation of synaptic input', artist='Matplotlib',
        comment='Movie support!')
writer = FFMpegWriter(fps=2, metadata=metadata)



def get_xy_coordinates(segList):
    '''
    approximates cordinates of segments in a list and returnes them as lists of x,y and z.
    '''
    
    X = []
    Y = []
    Z = []
    
    S = []
    Sx = []
    
    shift = 2
    for s,seg in enumerate(activationPattern):
        section     = seg.sec
        # get point number of segment
        for ss,seg_iterator in enumerate(section):
            if seg_iterator == seg:
                x           = h.x3d(seg.x, sec=section)
                y           = h.y3d(seg.x, sec=section)
                z           = h.z3d(seg.x, sec=section)
                break
        
        # if the 3d point is already plotted (it should not be) shift a few um
        
        if x in X and y in Y:
            if X.index(x) == Y.index(y):
                i = X.index(x)
                while x in X:
                    x = x + shift
                        
        X.append(x)
        Y.append(y)
        Z.append(z)
        S.append(section.name())
        Sx.append(seg.x)
    
    return X, Y, Z
                  


def create_segment_list(cell, dist_groups):
    
    # create segment lists
    segments    = {} 
    
    for d in dist_groups:
        segments[d] = []
          
    # sort segments into list            
    for sec in cell.dendlist:
             
        for seg in sec:
            
            dist = h.distance(seg.x, sec=sec)
            
            if dist < 60:
                segments[0].append(seg)
            elif dist < 120:
                segments[1].append(seg)
            elif dist < 180:
                segments[2].append(seg)
            else:
                segments[3].append(seg)
    
    return segments
    



def set_pointers(cell, pointer, mod_list, modulate_axon=False):
    
    maxCond = {}    
    for sec in cell.allseclist:
        for seg in sec:
            for mech in seg:
                
                # if mech in mod_list (skipping car since not used dynamically)
                if mech.name() in mod_list:
                    
                    if sec.name().find('axon') >= 0:
                        # why only set pointer if not modulating axon???
                        if not modulate_axon:
                            h.setpointer(pointer, 'pka', mech )
                            continue
                    
                    # save maximal value to list
                    
                    # Ca channels
                    if mech.name()[0] == 'c':
                        maxCond[mech]   =   mech.pbar
                        
                    else:
                        # non Ca channels
                        maxCond[mech]   =   mech.gbar
                    
                    if not mech.name() == 'car':
                        h.setpointer(pointer, 'pka', mech )
    return maxCond
    


    

def set_modulation( cell,           \
                    maxCond,        \
                    factors,        \
                    mod_list        ):
    
    # use factors stored externally
    mod_fact = factors[0:len(mod_list)]
    
    for mech in maxCond:
        
        # get factor from list
        factor = mod_fact[mod_list.index(mech.name() )]
        
        if mech.name()[0] == 'c':
            
            # Ca channels
            mech.pbar   = maxCond[mech] * factor
            
        else:
        
            # non Ca channels
            mech.gbar   = maxCond[mech] * factor
    
    


       

def set_bg_noise(cell,               \
                 syn_fact=False,     \
                 gabaMod=False       ):
    
    ns      = {}
    nc      = {}
    Syn     = {}
    for sec in cell.allseclist:
        
        # set bg noise----------------------------------
        
        if cell_type == 'D1':
            gbase = 0.7e-3
        else:
            gbase = 0.2e-3
            
        # create a glut synapse (glutamate)
        use.random_synapse(ns, nc, Syn, sec, 0.5,           \
                                NS_interval=1000.0/20.0,    \
                                NC_conductance=gbase )
        # create a gaba synapse (Exp2Syn)
        use.random_synapse(ns, nc, Syn, sec, 0.1,           \
                                Type='gaba',                \
                                NS_interval=1000.0/5.0,     \
                                NC_conductance=gbase*3      )
        
        Syn[sec.name()+'_glut'].ratio = 1.0/3
        
        if syn_fact:
            Syn[sec.name()+'_glut'].ampa_scale_factor = syn_fact[0]
            Syn[sec.name()+'_glut'].nmda_scale_factor = syn_fact[1]
            
        
        if gabaMod:
            # scale gaba
            nc[sec.name()+'_gaba'].weight[0] = gbase * 3 * gabaMod
        
    
    return Syn, nc, ns
            
            


def set_stimuli(cell,               \
                activationPattern,  \
                syn_fact=False,     \
                gabaMod=False       ):
    
    rand    = {}
    stim    = {}
    ncon    = {}
    vmL     = {}
    cur     = {}
    detect  = {}
    delay   = np.arange(100, 110, 1)
    
    # set random stimuli
    for sec in cell.dendlist:
        
        for seg in sec:  
        
            if seg in activationPattern:
                
                key = sec.name() + '_' + str(seg.x)
                
                # get index of activation
                index = activationPattern.index(seg)
                
                # create synapse
                rand[key]        =   h.glutamate(seg.x, sec=sec) # changed from x -> seg.x
                rand[key].ratio  =   1.0/3.0
                
                if syn_fact:
                    rand[key].ampa_scale_factor = syn_fact[0]
                    rand[key].nmda_scale_factor = syn_fact[1]


                # create NetStim object
                stim[key]            = h.NetStim()
                stim[key].number     = 2
                stim[key].start      = delay[index]
                stim[key].interval   = 10 # interval between two spikes in ms (1000 / 50 Hz = 20 ms)

                # create NetCon object
                ncon[key]            = h.NetCon(stim[key], rand[key])
                ncon[key].delay      = 1
                ncon[key].weight[0]  = 1.5e-3 # (uS) = 1.5 nS
                
                #vmL[key] = h.Vector()
                #vmL[key].record(sec(seg.x)._ref_v)
                cur[key] = h.Vector()
                cur[key].record(rand[key]._ref_I)
                
                # set pointer to right target...
                pointer             = rand[key]._ref_i_ampa
    
    return cur, rand, ncon, stim        

    
    
    
    
    
    
def allUnique(x):
    seen = set()
    return not any(i in seen or seen.add(i) for i in x)    









def anim(x, y, color, cell, filename):
    
    fanim = plt.figure(figsize=(12,12), facecolor='g')
    animax = plt.subplot(111, frameon=False)
    animax.axis('off')
    lim = 150
    animax.set_xlim([-lim,lim])
    animax.set_ylim([-lim,lim])
    
    for stem in cell.soma.children(): 
        if 'axon' in stem.name():
            continue 
        plot.plot_morphology(   stem,                   \
                                color=colors[0],        \
                                ax=animax               )
        animax.plot(0,0, 'o', color=colors[0], ms=20, mew=0, mec=colors[-1])
    
    scat = animax.scatter(x,y, c=color[0], s=80, animated=True)
    
    anim = animation.FuncAnimation(fanim, update_plot, frames=range(len(color)),
                                  fargs=(color, scat))
        
    mywriter = animation.FFMpegWriter()
    anim.save(filename,writer=mywriter, fps=2, dpi=300, savefig_kwargs={'facecolor':'g'})


def update_plot(i, color, scat):
    scat.set_color(color[i])
    return scat,
    
    
    
# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    N = 40
    if id > N:                                          
        cell_type   =   'D1'
        par         =   './params_dMSN.json'
        morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
        norm        =   id
        col         =   '#4d97ff'
                    
    else:
        cell_type   =   'D2'
        par         =   './params_iMSN.json'
        morphology  =   'WT-iMSN_P270-09_1.01_SGA2-m1.swc'
        norm        =   id - N
        col         =   '#4337b5'
    
    
    
    
    # load parameters and random set
    parameters      =   use.load_obj(cell_type+'_30bestFit.pkl')[0]['variables'] # parameter set 0 (cell index)
    randActPat      =   use.load_obj('randomActivationPattern.pkl')[cell_type]   # preselected indexes to use with lists of segments (see create_segment_list below)
    
    
    # update colormap
    N = 7
    
    cmap    =   cm.get_cmap('inferno').colors
    parts   =   int(len(cmap) / (N-1))
    
    colors  = []    
    for i in reversed(range(N)):
        j = parts*i
        colors.append(cmap[j])
    
    for index in range(64):
        
        model_version = int(np.floor(index/64))
        norm          = index - model_version*64
        
        # group index 0-3
        group   =   int(index/10)
        
        if norm < 20:
            group   =   0
            C       =   0
        elif norm < 37: 
            group   =   1
            C       =   20
        elif norm < 54: 
            group   =   2
            C       =   37
        else:
            group   =   3
            C       =   54
        
    
        # pattern index 0-n[group]
        p       =   norm-C
        
        pattern =   randActPat[group][p]
        
        dir_name = 'Viz/Group'+str(group)+'Pattern'+str(p)
        call(["mkdir", dir_name])
        
        # initiate cell
        cell = build.MSN(  params=par,                  \
                           morphology=morphology,       \
                           variables=parameters         )
        
        
        # create segment lists
        dist_groups = range(4)
        segments    = create_segment_list(cell, dist_groups)
        
        # set cascade 
        #   used for setting pointers needed in the channel mechnisms (and dynamic DA mod)
        casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
        pointer =   casc._ref_Target1p    
        
        
        # set pointers (for all channel instances in mod_list); 
        # needed since same mechanisms are used for dynamic modulation of channels.
        # Modulation of channels is not used in this script
        if cell_type=='D1':
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
        else:
            mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
        
        maxCond = set_pointers(cell, pointer, mod_list)
                         
        
        # solver------------------------------------------------------------------------------            
        cvode   = h.CVode()
        
        
        # set background noise
        Syn, nc, ns         = set_bg_noise(cell )
            
        # set stimuli
        activationPattern   = [ segments[group][s] for s in pattern ]  
        
        
        ### -------------------- plot and animate -------------
            
        
        if index > 0:
            # close open plot windows
            plt.close('all')
            
        
        movieName = dir_name+'/animation_dispersedPatterns.mp4'
        
        # set up the figures and the plot element we want to animate
        fig,ax          = plt.subplots(1,1, figsize=(10,10) )
        
        ax.axis('off')
        for stem in cell.soma.children(): 
            if 'axon' in stem.name():
                continue 
            plot.plot_morphology(   stem,            \
                                    color=col, \
                                    ax=ax            )
        ax.plot(0,0, 'o', color=col, ms=15, mew=0, mec=colors[-1])
        
        fig.savefig('Figures/morphology_'+cell_type, transparent=True)
        
        kkkk
        
        # find coordinates of segments in the activation pattern 
        X, Y, Z = get_xy_coordinates(activationPattern)
        
        
        COLOR   = {}
        
        cur,rand,ncon,stim  = set_stimuli(cell, activationPattern )
        
        # run simulation
        h.finitialize(-70)
        
        # run simulation
        tstop   = 200
        count   = 0
        while h.t < tstop:
        
            h.fadvance()
            
            if h.t >= 101  and h.t <= 141 and h.t%1 < 0.025:
                    
                    # get current at pattern and map color of marker to color
                    
                    cl = [[], [], [], [], [], [], [], [], [], []]
                    
                    for s,seg in enumerate(activationPattern):
                        
                        section     = seg.sec
                        
                        # get current value
                        I           = rand[section.name() + '_' + str(seg.x)].I
                        color_index = use.get_color_index(  I,                      \
                                                            min_max  = [-0.05, 0],  \
                                                            N_colors = N            )
                        
                        
                        cl[s] = colors[color_index]
                        
                        ax.plot(X[s],Y[s], 'o', color=colors[color_index], ms=10)
                    
                    COLOR[count] = cl
                    
                    fig.savefig(dir_name+'/frame'+str(count)+'.png', facecolor='g', edgecolor='none')
                    
                    count += 1
                   
        #animate(X, Y, COLOR, cell, dir_name+'/animation.mp4')
        anim(X, Y, COLOR, cell, dir_name+'/animation.mp4')
        
        

    

                
            
        
    
    
        
    
    
