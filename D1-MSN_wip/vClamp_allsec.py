
# script used for clamping the cell at differnt voltage-loc pair and plot the 
# attenuation towards soma.

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys


# Load model mechanisms
#import neuron               as nrn
#nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

    


def create_connection_lists():
    
    somatic_connections = {}
    
    for sec in h.allsec():
        s2 = h.SectionRef(sec=sec)
        somatic_connections[sec] = [sec]
        while s2.has_parent():
            parent_seg = s2.parent()
            somatic_connections[sec].append(parent_seg.sec)
            s2 = h.SectionRef(sec=parent_seg.sec)
    
    return somatic_connections
    
    

# if run from terminal...   ===============================================================
if __name__ == "__main__":
    
    # TODO
    # -create jobscript
    
    tstop           =   250
    cvode           =   h.CVode()
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    N           =   71
    
    model_version           = id - int(np.floor(id/N))*N
    modulation_percentage   = int(np.floor(id/N))*50
        
    
    print(model_version, modulation_percentage)
    
    
    # load random sets
    path_to_lib         = './Libraries/'
    random_variables    = use.load_obj(path_to_lib+cell_type+'_71bestFit.pkl')[model_version]['variables']
    
    
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=random_variables   )
    
    # get sec_list connecting sec with soma
    somatic_con = create_connection_lists()
    
    
    # set cascade 
    #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
    
    
    # modulation of channels
    if modulation_percentage > 0:
        # modulation_percentage = [0, 50, 100]    
        modulation_factors = use.get_fixed_modulation_factors(  cell_type,              \
                                                                mod_list,               \
                                                                modulation_percentage   )
        org_chan_gbar = use.make_list_of_gbar(cell, mod_list)
        use.set_channel_modulation( org_chan_gbar,          \
                                    mod_list,               \
                                    modulation_factors,     \
                                    modulate_axon=False     )

    # loop over all sections in morphology
    result = {}
    for sec in h.allsec():
        
        # check if dist larger than 50 um (and that sec is not part of the axon)
        dist = h.distance(0.5, sec=sec)
        
        # only use dendritic channels with a distance larger than 50 um-----
        if sec.name().find('dend') < 0 or dist < 50: continue
        
        print(sec.name())
            
        # set V-clamp
        clamp       = h.SEClamp(sec(0.5))
        clamp.rs    = 0.01
        clamp.dur1  = 100
        clamp.dur2  = 200
        clamp.amp1  = -85
        
        # record vm of all sec connecting current sec to soma
        VM = {}
        for s2 in somatic_con[sec]:
            VM[s2] = h.Vector()
            VM[s2].record(s2(0.5)._ref_v)
            
        tm = h.Vector()
        tm.record(h._ref_t)
            
        # loop over clamp voltages
        result[sec.name()] = {}
        for clamp_voltage in range(-70,-9,15):
            
            clamp.amp2  = clamp_voltage
            
            # spike recorder
            nc = h.NetCon(cell.soma(0.5)._ref_v, None, sec=cell.soma)
            nc.threshold = 0
            vec = h.Vector()
            nc.record(vec)
        
            # run
            h.finitialize(-70)
        
            # run simulation
            while h.t < tstop:
                h.fadvance()
            
            if vec.size() > 0.0:
                result[sec.name()][clamp_voltage] = {'decay':None, 'vm':VM[s2], 'spikes':vec.to_python()}
                continue
            
            # extract voltage value vm = 250ms for all sec in sec_list
            decay = []
            distList = []
            for s2 in somatic_con[sec]:
                decay.append( VM[s2][-1] )
                distList.append( h.distance(0.5, sec=s2) )
                
            # plot/save
            result[sec.name()][clamp_voltage] = {'decay':decay, 'dist':distList}
    
    print('cell index: %d, done!' % (model_version) )
    
    use.save_obj(result, 'V-Clamp_'+cell_type+'_V'+str(model_version)+'_DAmod'+str(modulation_percentage) )
    
             
        

    
    
    
    

        

