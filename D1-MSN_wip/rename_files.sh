
# removes .0 from file name (if echo is deleted)
for file in Results/InVivo_sim_res/Oct2018/Weak/InVivoWeak1ms_*_D1_V*
do
  mv "$file" "${file/.0/}"
done
