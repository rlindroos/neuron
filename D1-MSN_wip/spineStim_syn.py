
# script used for running in vivo simultion of dispersed synaptic activation
# This script is based on plateaus_in_vivo_random.py
#
# Results are stored under Results/InVivo_sim_res/InVivo_dispersed_D<>_run<>.pkl

from __future__ import print_function, division
from neuron import h
pc = h.ParallelContext()
id = int(pc.id())
import numpy                as np
import MSN_builder          as build
import common_functions     as use
import glob
import pickle
import sys

from joblib import Parallel, delayed
import multiprocessing

# Load model mechanisms
import neuron               as nrn
nrn.load_mechanisms('Mech/')

h.load_file('stdlib.hoc')
h.load_file('import3d.hoc')

        
import matplotlib.pyplot as plt
    


if __name__ == "__main__":
    
    # load parameters and random set
    path_to_lib     = './Libraries/'
    model_sets      =   use.load_obj(path_to_lib+'D1_71bestFit.pkl')
    
    cell_type   =   'D1'
    par         =   './params_dMSN.json'
    morphology  =   'WT-dMSN_P270-20_1.02_SGA1-m24.swc'
    cell_index  =    2
    
    tstop           =   500
    cvode           =   h.CVode()
    
    # modulation factors
    if cell_type == 'D1':
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can' ]
    else:
        mod_list = ['naf', 'kas', 'kaf', 'kir', 'cal12', 'cal13', 'can', 'car']
        
    
    # load parameters and random set
    parameters      =   model_sets[cell_index]['variables'] 
    rheobase        =   model_sets[cell_index]['rheobase']
    mixActPat       =   use.load_obj(path_to_lib+'mixActPatRestricted16.pkl')
    index_map       =   mixActPat['map' ][19]
    pattern         =   mixActPat['patterns' ][index_map]
    
    fig, ax = plt.subplots(figsize=[5, 4])
    #axins = plt.axes([.3, .6, .2, .2])
    #axins.set_xticks([790,810]) 
    #axins.set_yticks([])
    
    color = ['k', 'r']
    ls    = ['-', '--']
        
    # initiate cell ------------------------------------------------------------------
    cell = build.MSN(  params=par,                  \
                       morphology=morphology,       \
                       variables=parameters         )
    
    name2sec = use.create_name2sec_dict()
    
    # split section
    #print('-- splitting section 33')
    #SEC = use.split_section(cell, name2sec['33'], n_sec_per_um=1)
    
    # insert spines
    SPINES = []
    for i in range(len(pattern)):
        # build spine without attaching
        SPINES.append( build.Spine(i) )
        
    # attache and set stimuli (including modulation)
    
    STIM, spike_list = use.set_mixed_stimuli_inSpine(   cell,
                                                        SPINES,
                                                        pattern,
                                                        name2sec,
                                                        ISI=1,     
                                                        start_time=300)
                
    #print(h.topology())
    
    # set cascade 
    #   used for setting pointers needed in the channel mechanisms (and dynamic DA mod)
    casc    =   h.D1_reduced_cascade2_0(0.5, sec=cell.soma) 
    pointer =   casc._ref_Target1p    
    
    # set pointers (for all channel instances in mod_list); 
    # needed since same mechanisms are used for dynamic modulation of channels.
    use.set_pointers(cell, pointer, mod_list)
    
    # set current injection
    '''
    Istim        =   h.IClamp(0.5, sec=cell.soma)
    Istim.delay  =   100
    Istim.dur    =   1000
    Istim.amp    =   rheobase *1e-3
    '''
    
    #TODO: Weird behavior fixed! Investigate and if not weired don't do below; else: 
    #- splitting sections in two
    #- add thin passive section to scale diameter
    
    # record vectors
    tm  = h.Vector()
    tm.record(h._ref_t)
    vm  = h.Vector()
    vm.record(SPINES[0].head(0.5)._ref_v)   #STIM[pattern[0]]['rand']._ref_G
    
    # run simulation
    h.finitialize(-80)
    
    # run simulation
    while h.t < tstop:
        h.fadvance()
    
    ax.plot(tm, vm, ls=ls[1], c=color[1])
    #axins.plot(tm,vm, ls=ls[1], c=color[1])
    #axins.set_xlim([780,820])  
    
    plt.show()

    

