
clc
clear all
close all

% allFiles = dir(fullfile('Test/inject_*_100_saveVsoma*.dat'));
% 
% % remove somatic files
% files(length(allFiles)/2)=struct('name', []);
% for i = 0:length(files)/2-1
%     files(1+i*2).name = allFiles(1+i*4).name;
%     files(2+i*2).name = allFiles(2+i*4).name;
% end


%  initialize structure for storing frames for movie
% M(length(files)/2) = struct('cdata',[], 'colormap', []);

% initialize vectors for storing resistances.
Rg=zeros(1,133);
Rn=Rg;
Rn2=Rn;
Rn3=Rg;
Rn4 = Rg;
Rn5 = Rg;
Rn6 = Rg;
Rn7 = Rg;
diff = Rn;

comp = 'den';
for file = 0:132
    
    if strcmp(comp, 'dend') 
        files = dir(fullfile(strcat('Test/*_', int2str(file),...
            '_100_saveV', int2str(file), '_*.dat')));
            
    else
        files = dir(fullfile(strcat('Test/*_', int2str(file),...
            '_100_saveVsoma_*.dat')));

    end
        
        
    [Rg(file+1) Rn(file+1) Rn2(file+1) Rn3(file+1) Rn4(file+1) ...
        Rn5(file+1) Rn6(file+1) Rn7(file+1)] = compRin(files);
    
    diff(file +1) = abs(Rg(file+1) - Rn4(file+1)) / ...
        max(Rg(file+1), Rn3(file+1))*100;
    
end

Rg=Rg*10^-6;
Rn=Rn*10^-6;
Rn2=Rn2*10^-6;
Rn3=Rn3*10^-6;
Rn4=Rn4*10^-6;
Rn5=Rn5*10^-6;
Rn6=Rn6*10^-6;
Rn7=Rn7*10^-6;
[A,indx] = sort(Rn);


h3 = figure();
bar(diff(indx))
set(gca, 'FontSize', 20)
title('Bar plot of Rin diff between G and N')
xlabel('trail (sorted nodes)')
ylabel('Rin diff (%)')
% saveTightFigure(h3, '../Figures/diff-Rin-sorted.pdf');

x=1:length(Rg);
h5 = figure();
plot(x, Rg(indx), 'r', x, Rn(indx), 'b', x, Rn3(indx), 'g',...
    x, Rn5(indx), 'k', x, Rn7(indx), 'y')
set(gca, 'FontSize', 20)
title('dend deflection vs trial for G and N')
legend('Gen', 'Neu', 'Neu3', 'Neu5', 'Neu7', 'Location', 'Best')
xlabel('trial (sorted nodes)')
ylabel('Rin M\Omega')
% saveTightFigure(h5, '../Figures/F-comp.pdf');
    