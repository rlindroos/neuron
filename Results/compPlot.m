function h= compPlot(files)
% function h = compPlot(files)
%
% function for comparing Neuron and Genesis responses.
% returns a figure handle that can be saved to file


h = figure('Visible', 'off');
axis tight
hold on
for file = 1:length(files)
%     disp(files(file).name)
    data = importdata(strcat('Test/',files(file).name));
    
    splitStr = regexp(files(file).name, '_','split');
    vComp = splitStr{4}(6:end);
    injComp = splitStr{2};

    len = length(splitStr{5}) -4;
    
    
    
    if strcmp(splitStr{5}(1:len), 'genesis')
        plot(data(:,1),data(:,2), 'b');
    elseif strcmp(splitStr{5}(1:len), 'neuron')
        plot(data.data(2:end,1)*10^-3, data.data(2:end,2)*10^-3,'r');
    end
    
    title(strcat('injected comp :',injComp, ' meassured in :', vComp))
    xlabel('time (s)')
    ylabel('potential (V)')
    legend('Genesis', 'NEURON', 'Location','Northeast')
    axis([0 1.2 -0.08 0.02])
    set(gca, 'FontSize', 20)
%     set(gca,'YTick',[-0.095 -0.090 -0.085])
%     set(gca,'XTick',[.20 .25 .30])
     
end

hold off