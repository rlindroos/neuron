clc
clear all
close all

nrInfiles =133;

% initialize vectors for storing resistances.
Rg=zeros(1,nrInfiles);
Rn=Rg;
diff=Rg;

for i = 1:133
    
    files = dir(fullfile(strcat('Test3/inject_', int2str(i-1),...
        '_100_saveVsoma*.dat')));
        % '_100_saveV', int2str(i-1), '*.dat')));
       
     
%     figure()
%     hold on
    for file = 1:length(files)

        data = importdata(strcat('Test3/',files(file).name));

        splitStr = regexp(files(file).name, '_','split');

        len = length(splitStr{5}) -4;
        
        
        
        if strcmp(splitStr{5}(1:len), 'genesis')
            maxG = max(data(:,2)*10^3); % mV
            Rg(i) = abs(-73 - maxG)*(10^-3)/(100*10^-12)*10^-6; % mV/pA
%             plot(data(:,1),data(:,2), 'b');

        elseif strcmp(splitStr{5}, 'neuron')
            maxN = max(data.data(2:end,2));
            Rn(i) = abs(-73 - maxN)*(10^-3)/(100*10^-12)*10^-6;
%             plot(data.data(2:end,1)*10^-3, data.data(2:end,2)*10^-3,'r');
           

        end
        


    end
%     hold off

    
%     str = sprintf('%g\t%g', maxG, maxN);
%     disp(str)
    
    diff(i) = abs(Rg(i) -Rn(i)) / max(Rg(i), Rn(i))*100; 
    
        
end

[A,indx] = sort(Rn);

x=1:length(Rg);
h5 = figure();
plot(x, Rg(indx), 'b', x, Rn(indx), 'r')
set(gca, 'FontSize', 20)
title('Contour of local Rin vs trial for G and N')
legend('Gen', 'Neu', 'Location', 'Best')
xlabel('trial (sorted nodes)')
ylabel('Rin M\Omega')
saveTightFigure(h5, '../Figures/plotNoF-soma.pdf');

h3 = figure();
bar(diff(indx))
set(gca, 'FontSize', 20)
title('Bar plot of Rin diff between G and N')
xlabel('trail (sorted nodes)')
ylabel('Rin diff (%)')
% saveTightFigure(h3, '../Figures/diff-Rin-sorted.pdf');
    