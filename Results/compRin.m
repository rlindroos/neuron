function [Rg Rn Rn2 Rn3 Rn4 Rn5 Rn6 Rn7] = compRin(files)
% function [Rg Rn diff] = compRin(files)
%
% function for comparing Neuron and Genesis responses.


for file = 1:length(files)

    data = importdata(strcat('Test/',files(file).name));
    
    splitStr = regexp(files(file).name, '_','split');

    len = length(splitStr{5}) -4;
    
    if strcmp(splitStr{5}(1:len), 'genesis')
        maxG = max(data(:,2)*10^3); % mV
        
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, 'inject')
        maxN = max(data.data(10:end,2)); % mV 
        
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, '2inject')
        maxN2 = max(data.data(10:end,2));
    
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, '3inject')
        maxN3 = max(data.data(10:end,2));
     
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, '4inject')
        maxN4 = max(data.data(10:end,2));
    
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, '5inject')
        maxN5 = max(data.data(10:end,2));
     
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, '6inject')
        maxN6 = max(data.data(10:end,2));
        
    elseif strcmp(splitStr{5}(1:len), 'neuron') &&...
            strcmp(splitStr{1}, '7inject')
        maxN7 = max(data.data(10:end,2));
    end
    
     
end
Rg = abs(-73 - maxG)*(10^-3)/(100*10^-12); % mV/pA
Rn = abs(-73 - maxN)*(10^-3)/(100*10^-12); 
Rn2 = abs(-73 - maxN2)*(10^-3)/(100*10^-12);
Rn3 = abs(-73 - maxN3)*(10^-3)/(100*10^-12);
Rn4 = abs(-73 - maxN4)*(10^-3)/(100*10^-12);
Rn5 = abs(-73 - maxN5)*(10^-3)/(100*10^-12);
Rn6 = abs(-73 - maxN6)*(10^-3)/(100*10^-12);
Rn7 = abs(-73 - maxN7)*(10^-3)/(100*10^-12);

% diff = abs(Rg -Rn) / max(Rg, Rn)*100; 


