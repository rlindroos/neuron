
import numpy as np
import matplotlib.pyplot as plt

def scale_synaptic_weight(T, tau, tauR, tauF, U2, ax=None):
    
    
    #T = np.arange(0,100,0.1)
    X = []
    Y = []
    Z = []
    
    U = []
    W = []
    
    y = 0.0
    z = 0.0
    u = 0.0
    tsyn = 0.0
    weight = 1.0
    
    for t in T:
        z = z*np.exp(-(t-tsyn)/float(tauR))
        z = z + (y*(np.exp(-(t-tsyn)/float(tau)) - np.exp(-(t-tsyn)/float(tauR))) / (tau/float(tauR) - 1) )
        y = y*np.exp(-(t-tsyn)/float(tau))
        x = 1-(y+z)
        if tauF > 0:
            u = u*np.exp(-(t-tsyn)/float(tauF))
            u = u + U2*(1-u)
        else:
            u = U2
        
        
        X.append( x )
        Y.append( y )
        Z.append( z )
        
        U.append( u )
        W.append( weight *x*u )
        
        y = y + x*u
        
        
        '''
        weight_nmda = weight_ampa*nmda_ratio
        
        A_ampa = A_ampa + weight_ampa*factor_ampa 
        B_ampa = B_ampa + weight_ampa*factor_ampa 
        A_nmda = A_nmda + weight_nmda*factor_nmda 
        B_nmda = B_nmda + weight_nmda*factor_nmda 
        '''
    if not any(ax): fig, ax = plt.subplots(3,1, figsize=(6,12) )
    
    ax[0].plot(T,X, 'k', label='x')
    ax[0].plot(T,Y, 'r', label='y')
    ax[0].plot(T,Z, 'g', label='z')
    ax[0].legend()
    
    
    ax[1].plot(T,U, 'k', label='u')
    ax[1].set_ylabel('u')
    ax[2].plot(T,W, 'k', label='w')
    ax[2].set_ylabel('w')
    for a in ax:
        a.set_ylim([0,1])
    
    
        

