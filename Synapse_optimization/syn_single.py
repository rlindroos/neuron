"""Expsyn synapse parameter fitting"""

# pylint: disable=R0914


import os

import bluepyopt as bpopt
import bluepyopt.ephys as ephys

import json
import pickle
import numpy as np
import cell_model, cell_evaluator
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from utils import *


def save_obj(obj, name ):
    '''
    functions used to save data in the pickle format. 
    Used to pickle dictionaries
    
    obj     = dictionary with data
    name    = name to be used, without file ending (.pkl will be added)
    '''
    
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def get_distance():
    ''' return distance of sections '''
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
        
    #protocols  = cell_evaluator.define_protocols('protocols_dSPN.json')
    #calculator = cell_evaluator.define_fitness_calculator(protocols, 'features.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    #parameters = cell_model.define_parameters('parameters.json')
    
    # def cell
    cell = ephys.models.CellModel(  name='dspn', 
                                    morph=morphology,
                                    mechs=mechanisms )
    
    cell.instantiate(sim=nrn_sim)
    
    h = nrn_sim.neuron.h
    
    secId2dist = {}
    count = 0
    for sec in h.allsec():
        if sec.name().find('dend') >= 0:
            secId2dist[ int(sec.name().split('[')[2].split(']')[0]) ] = h.distance(0.5, sec=sec)
            
            for seg in sec:
                count += 1
    
    return secId2dist

def main(id2dist):
    """Main"""
    
    load_data = False
    
    if not load_data:
        # setup
        morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
        parameters = cell_model.define_parameters('parameters.json')
        ca_params  = cell_model.define_parameters('parameters_ca.json')
        mechanisms = cell_model.define_mechanisms('mechanisms.json')
        ca_mech    = cell_model.define_mechanisms('mechanisms_ca.json')
        parameters = parameters + ca_params
        mechanisms = mechanisms + ca_mech
        protocol_definitions  = json.load(open('config/protocols-val.json'))
        
        # simulator (neuron)
        nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
        
        # def morph
        morphology = ephys.morphologies.NrnFileMorphology(
            morphofile, do_replace_axon=True)
        
        # def soma location   
        somatic_loc = ephys.locations.NrnSeclistLocation(
            'somatic',
            seclist_name='somatic')

        somacenter_loc = ephys.locations.NrnSeclistCompLocation(
            name='somacenter',
            seclist_name='somatic',
            sec_index=0,
            comp_x=0.5)
        
        fig,ax = plt.subplots(3,1, figsize=(6,12))
        N_sec2loop = len( id2dist )
        id2res = {}
        for secID in range(N_sec2loop):
            
            sec_loc  =  ephys.locations.NrnSeclistCompLocation(
                            name='secCenter%d'%(secID),
                            seclist_name='basal',
                            sec_index=secID,
                            comp_x=0.5)

            expsyn_mech = ephys.mechanisms.NrnMODPointProcessMechanism(                     
                name='tmglut%d'%(secID),                                                              
                suffix='tmGlut',                                                            
                locations=[sec_loc])

            expsyn_loc = [ephys.locations.NrnPointProcessLocation(
                'expsyn_loc%d'%(secID),
                pprocess_mech=expsyn_mech)]
            
            tau_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_tau',                                                          
                param_name='tau',                                                           
                value=3,                                                                       
                locations=expsyn_loc)
            tauR_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_tauR',                                                          
                param_name='tauR',                                                           
                value=200,                                                                    
                bounds=[1,1000],                                                             
                locations=expsyn_loc)
            tauF_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_tauF',                                                          
                param_name='tauF',                                                           
                value=200,                                                                    
                bounds=[0,1000],                                                             
                locations=expsyn_loc)
            U_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_U',                                                          
                param_name='U',                                                           
                value=0.3,                                                                    
                bounds=[0,1],                                                             
                locations=expsyn_loc)
            q_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_q',                                                          
                param_name='q',                                                           
                value=3,                                                                   
                locations=expsyn_loc)
            
            # def synapse
            ratio  = 5.0
            weight = 1880e-6*10.0/(3*ratio)
            ratio_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_ratio',                                                          
                param_name='nmda_ratio',                                                           
                value=ratio,                                                                  
                locations=expsyn_loc)
            t1ampa_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_t1ampa',                                                          
                param_name='tau1_ampa',                                                           
                value=1.9,                                                                  
                locations=expsyn_loc)
            t2ampa_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_t2ampa',                                                          
                param_name='tau2_ampa',                                                           
                value=4.8,                                                                  
                locations=expsyn_loc)
            t1nmda_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_t1nmda',                                                          
                param_name='tau1_nmda',                                                           
                value=5.52,                                                                  
                locations=expsyn_loc)
            t2nmda_param = ephys.parameters.NrnPointProcessParameter(                   
                name='tmglut_t2nmda',                                                          
                param_name='tau2_nmda',                                                           
                value=231,                                                                  
                locations=expsyn_loc)

            parameters.append(tau_param)
            parameters.append(tauR_param)
            parameters.append(tauF_param)
            parameters.append(U_param)
            parameters.append(q_param)
            
            parameters.append(ratio_param)
            parameters.append(t1ampa_param)
            parameters.append(t2ampa_param)
            parameters.append(t1nmda_param)
            parameters.append(t2nmda_param)

            #We first create a stimulus that injects the presynaptic events:
            stim_start = 200
            number     = 1
            interval   = 1000.0/50    # Hz -> ISI (ms)

            netstim = ephys.stimuli.NrnNetStimStimulus(                                  
                total_duration=600,                                                      
                number=number,                                                                
                interval=interval,                                                              
                start=stim_start,                                                        
                weight=weight,                                                             
                locations=expsyn_loc)

            stim_end = stim_start*2 + interval*number

            cell = ephys.models.CellModel(
                'dspn', 
                morph=morphology, 
                mechs=mechanisms+[expsyn_mech], 
                params=parameters)

            rec = [ephys.recordings.CompRecording(
                name='soma.v',
                location=somacenter_loc,
                variable='v')]
            rec.append( ephys.recordings.CompRecording(
                name='sec%d.cai'%(secID),
                location=sec_loc,
                variable='cai'))
            rec.append( ephys.recordings.CompRecording(
                name='sec%d.cali'%(secID),
                location=sec_loc,
                variable='cali'))
            rec.append( ephys.recordings.CompRecording(
                name='sec.v',
                location=sec_loc,
                variable='v'))

            # set constant istim
            protocol_definition = protocol_definitions.items()[0] # select first prot (same in all)
            stimuli = [] 
            stimulus_definition = protocol_definition[1]['stimuli'][1]
            stimuli.append(ephys.stimuli.NrnSquarePulse(
                step_amplitude=stimulus_definition['amp'],
                step_delay=stimulus_definition['delay'],
                step_duration=stimulus_definition['duration'],
                location=somacenter_loc,
                total_duration=400))

            # append netstim
            stimuli.append(netstim)

            protocol = ephys.protocols.SweepProtocol(
                'netstim_protocol',
                stimuli,
                rec)

            max_volt_feature = ephys.efeatures.eFELFeature(
                'maximum_voltage',
                efel_feature_name='maximum_voltage',
                recording_names={'': 'soma.v'},
                stim_start=stim_start,
                stim_end=stim_end,
                exp_mean=-50,
                exp_std=.1)
            max_volt_objective = ephys.objectives.SingletonObjective(
                max_volt_feature.name,
                max_volt_feature)

            score_calc = ephys.objectivescalculators.ObjectivesCalculator(
                [max_volt_objective])
            
            opt_params = [p.name for p in cell.params.values() if not p.frozen]
            
            cell_evaluator = ephys.evaluators.CellEvaluator(
                cell_model=cell,
                param_names=opt_params,
                fitness_protocols={protocol.name: protocol},
                fitness_calculator=score_calc,
                sim=nrn_sim)

            best_models = json.load(open('best_models.json'))
            default_param_values = best_models[3]
            
            default_param_values['tmglut_tau'] = 3
            default_param_values['tmglut_tauR'] = 40
            default_param_values['tmglut_tauF'] = 50
            default_param_values['tmglut_U'] = 0.32
            default_param_values['tmglut_q'] = 2.5
            #U = 0.3 (1) <0, 1>
            #u0 = 0 (1) <0, 1>

            #print cell_evaluator.evaluate_with_dicts(default_param_values)
            
            responses = protocol.run(                                                    
                cell_model=cell,                                                         
                param_values=default_param_values,                                              
                sim=nrn_sim)   
            time =    [ responses['soma.v']['time'][index]    for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]          
            voltage = [ responses['soma.v']['voltage'][index] for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
            Vl =      [ responses['sec.v' ]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
            
            if secID == 0:
                VM = {'soma':np.zeros((N_sec2loop, len(voltage))), 'local':np.zeros((N_sec2loop, len(voltage)))}
            VM['soma'][secID,:]  = voltage     
            VM['local'][secID,:] = Vl                    
            
            # local [Ca]
            cai     = [ responses['sec%d.cai' %(secID)]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
            cali    = [ responses['sec%d.cali'%(secID)]['voltage'][index]  for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
                                                                          
            ax[0].plot(time, voltage, 'k', alpha=0.3)   
            ax[1].plot(time, Vl, 'r', alpha=0.3)  
            
            # get peak indexes
            c = (np.diff(np.sign(np.diff(voltage))) < 0).nonzero()[0] +1
            
            #if secID == 0:
             #   C = np.zeros((N_sec2loop, len(c)))
            #
            #C[secID,:] = np.array(voltage)[c]
            
            ax[0].plot(np.array(time)[c], np.array(voltage)[c], '*', ms=10 )
            
            id2res[secID] = {'dist':id2dist[secID], 'cai':cai, 'cali':cali, 'vm':voltage}
            
            print(secID)
        
        id2res['time']   = time
        
        save_obj(id2res, 'ca_single_epsp_pickled' )
                                                      
        ax[0].set_xlabel('Time (ms)')                                                      
        ax[0].set_ylabel('Voltage (ms)')                                                   
        
        vms_mean = np.mean( VM['soma'], axis=0 )
        vml_mean = np.mean( VM['local'], axis=0 )
        
        # normalize to amp of first epsp
        #C = C - voltage[0]
        #CT = C.transpose()
        #CN = CT / C[:,0]
        #cnorm = CN.transpose()*100
        # calc mean and std
        #cmean = np.mean(cnorm, axis=0)
        #cstd  = np.std(cnorm, axis=0)
        
        ax[0].plot(time, vms_mean, 'k', lw=3)
        ax[1].plot(time, vml_mean, 'r', lw=3)
        #ax[2].errorbar(np.arange(len(cmean)), cmean, cstd, marker='.', ms=25, lw=2, c='r', markerfacecolor='w', markeredgecolor='r', markeredgewidth=2) 
        #ax[2].set_ylim([20,180])
    else:
        
        with open('ca_single_epsp_pickled.pkl', 'rb') as f:
            id2res = pickle.load(f)
        time = id2res['time']
         
    
    # plot Ca as func of dist
    f_ca, a_ca = plt.subplots(5,1, figsize=(5,10))
    dist = []
    PEAK = {'cai':[], 'cali':[]}
    for secID in range(58):
    
        dist.append(id2res[secID]['dist'])
        cai  = [ca*1e6 for ca in id2res[secID]['cai'] ]  
        cali = [ca*1e6 for ca in id2res[secID]['cali']]
        
        # plot raw traces
        a_ca[3].plot(time, cai,  'b')
        a_ca[4].plot(time, cali, 'r')
        
        # get peak ca
        PEAK['cai' ].append( max(cai)  )
        PEAK['cali'].append( max(cali) )
         
    # plot peak amplitude as function of dist   
    added   = np.add(PEAK['cali'],PEAK['cai'])
    average = np.divide(added, 2)
    a_ca[0].plot(dist, average,      'o', ms=15, mew=1, c='m', mec='w' ) 
    a_ca[1].plot(dist, PEAK['cai' ], 'o', ms=15, mew=1, c='b', mec='w' )
    a_ca[2].plot(dist, PEAK['cali'], 'o', ms=15, mew=1, c='r', mec='w' )   
    a_ca[0].set_ylabel('Average Ca [nM]') 
    a_ca[1].set_ylabel('NPQR Ca    [nM]')
    a_ca[2].set_ylabel('LT NMDA, AMPA Ca [nM]')
    formatter = mticker.ScalarFormatter(useMathText=True)
    for a in a_ca:
        a.yaxis.set_major_formatter(formatter)
    f_ca.savefig('../../../Dropbox/Manuscripts/network/Figures_raw/Ca_vs_dist_single_epsp.png', transparent=True, bbox_inches='tight')
    plt.show()    
    
    
    '''
    optimisation = bpopt.optimisations.DEAPOptimisation(
        evaluator=cell_evaluator,
        offspring_size=10)

    _, hall_of_fame, _, _ = optimisation.run(max_ngen=5)

    best_ind = hall_of_fame[0]

    print 'Best individual: ', best_ind
    print 'Fitness values: ', best_ind.fitness.values

    best_ind_dict = cell_evaluator.param_dict(best_ind)
    responses = protocol.run(
        cell_model=cell,
        param_values=best_ind_dict,
        sim=nrn_sim)

    time = responses['soma.v']['time']
    voltage = responses['soma.v']['voltage']

    import matplotlib.pyplot as plt
    plt.style.use('ggplot')
    plt.plot(time, voltage)
    plt.xlabel('Time (ms)')
    plt.ylabel('Voltage (ms)')
    plt.show()'''

if __name__ == '__main__':
    id2dist = get_distance()
    main(id2dist)
