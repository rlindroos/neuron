"""Expsyn synapse parameter fitting"""

# pylint: disable=R0914


import json, glob, pickle, os

import bluepyopt as bpopt
import bluepyopt.ephys as ephys

import numpy as np
import cell_model, cell_evaluator
import matplotlib.pyplot as plt
from utils import *
import local_stim as lstim


def save_vector(x, y, outfile):
    '''
    save vectors to file.
    
    x       = x-vector
    y       = y-vector
    outfile = file name to be used
    '''
    
    with open(outfile, "w") as out:
        for time, y in zip(x, y):
            out.write("%g %g\n" % (time, y))



def main():
    """Main"""
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    parameters = cell_model.define_parameters('parameters.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    protocol_definitions  = json.load(open('config/protocols.json'))
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # def morph
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
    

    somacenter_loc = ephys.locations.NrnSeclistCompLocation(
        name='somacenter',
        seclist_name='somatic',
        sec_index=0,
        comp_x=0.5)
    
    N_sec2loop = 10
    N_sec = 57
    sec2loop = np.random.choice(N_sec, N_sec2loop, replace=False)
    with open('syn_stim_loc_lists.pkl', 'rb') as fp:
        sec_loc = pickle.load(fp)[0]    # <- chose stim list here! 0 by default
    sec_loc  = []
    for secID in sec2loop:
        
        sec_loc.append( ephys.locations.NrnSeclistCompLocation(
            name='secCenter%d'%(secID),
            seclist_name='basal',
            sec_index=secID,
            comp_x=0.5))

    expsyn_mech = ephys.mechanisms.NrnMODPointProcessMechanism(                     
        name='tmglut',                                                              
        suffix='tmGlut',                                                            
        locations=sec_loc)

    expsyn_loc = ephys.locations.NrnPointProcessLocation(
        'expsyn_loc',
        pprocess_mech=expsyn_mech)
    
    tau_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_tau',                                                          
        param_name='tau',                                                           
        value=3,                                                                    
        bounds=[1, 1000],                                                             
        locations=[expsyn_loc])
    tauR_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_tauR',                                                          
        param_name='tauR',                                                           
        value=200,                                                                    
        bounds=[1,1000],                                                             
        locations=[expsyn_loc])
    tauF_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_tauF',                                                          
        param_name='tauF',                                                           
        value=200,                                                                    
        bounds=[0,3000],                                                             
        locations=[expsyn_loc])
    U_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_U',                                                          
        param_name='U',                                                           
        value=0.3,                                                                    
        bounds=[0,1],                                                             
        locations=[expsyn_loc])
    q_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_q',                                                          
        param_name='q',                                                           
        value=3,                                                                    
        bounds=[1,5],                                                             
        locations=[expsyn_loc])

    parameters.append(tau_param)
    parameters.append(tauR_param)
    parameters.append(tauF_param)
    parameters.append(U_param)
    parameters.append(q_param)

    #We first create a stimulus that injects the presynaptic events:
    stim_start = 100
    number     = 8
    Hz         = 20
    interval   = 1000.0/Hz    # Hz -> ISI (ms)
    
    spike_times = [x for x in range(100,101+int(interval)*(number-1),int(interval))] + [1000]
    
    '''
    stimuli = [ephys.stimuli.NrnNetStimStimulus(                                  
        total_duration=600,                                                      
        number=number,                                                                
        interval=interval,                                                              
        start=stim_start,                                                        
        weight=0.3e-3,                                                             
        locations=[expsyn_loc])]'''
    stimuli = [lstim.NrnNetStimStimulus(                                  
        use_vecstim=True,
        spike_vec=spike_times,                                                       
        weight=0.3e-3,                                                             
        locations=[expsyn_loc])]

    stim_end = stim_start*2 + interval*number

    cell = ephys.models.CellModel(
        'dspn', 
        morph=morphology, 
        mechs=mechanisms+[expsyn_mech], 
        params=parameters)
    
    # recordings -------------------------------------------------------
    rec = []
    rec.append( ephys.recordings.CompRecording(
        name='soma.v',
        location=somacenter_loc,
        variable='v'))
    for i,secID in enumerate(sec2loop): 
        rec.append( ephys.recordings.CompRecording(
            name='sec%d.v'%(secID),
            location=sec_loc[i],
            variable='v'))

    
    # set constant istim ------------------------------------------------
    protocol_definition = protocol_definitions.items()[0] # select first prot (same in all)
    stimulus_definition = protocol_definition[1]['stimuli'][1]
    stimuli.append(ephys.stimuli.NrnSquarePulse(
        step_amplitude=stimulus_definition['amp']+0.15,
        step_delay=stimulus_definition['delay'],
        step_duration=stimulus_definition['duration'],
        location=somacenter_loc,
        total_duration=1100))

    protocol = ephys.protocols.SweepProtocol(
        'netstim_protocol',
        stimuli,
        rec)

    max_volt_feature = ephys.efeatures.eFELFeature(
        'maximum_voltage',
        efel_feature_name='maximum_voltage',
        recording_names={'': 'soma.v'},
        stim_start=stim_start,
        stim_end=stim_end,
        exp_mean=-50,
        exp_std=.1)
    max_volt_objective = ephys.objectives.SingletonObjective(
        max_volt_feature.name,
        max_volt_feature)

    score_calc = ephys.objectivescalculators.ObjectivesCalculator(
        [max_volt_objective])
    
    opt_params = [p.name for p in cell.params.values() if not p.frozen]
    
    cell_evaluator = ephys.evaluators.CellEvaluator(
        cell_model=cell,
        param_names=opt_params,
        fitness_protocols={protocol.name: protocol},
        fitness_calculator=score_calc,
        sim=nrn_sim)

    best_models = json.load(open('best_models.json'))
    default_param_values = best_models[2]
    
    # values from file! ----------------------------------
    area = 'M1'
    brain_half = 'LH'
    cell_type = 'MSND1'
    f = glob.glob('models/%s_Analysis*%s_*-%s-require-H20-model-parameters-with-traces.json' % (area, brain_half, cell_type) )[0]
    with open(f) as file:
        data = json.load(file)
    
    for i,key in enumerate(data):
        params = data[key]
        
        fig,ax = plt.subplots(3,1, figsize=(6,12))
        
        default_param_values['tmglut_tau'] = 3
        default_param_values['tmglut_tauR'] = params['tauD']*1000
        default_param_values['tmglut_tauF'] = params['tauF']*1000
        default_param_values['tmglut_U'] = params['U']
        default_param_values['tmglut_q'] = 2.5
        
        responses = protocol.run(                                                    
            cell_model=cell,                                                         
            param_values=default_param_values,                                              
            sim=nrn_sim)  
             
        time    = [ responses['soma.v']['time'][index]    for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]          
        voltage = [ responses['soma.v']['voltage'][index] for index, tt in enumerate(responses['soma.v']['time']) if tt > 100]
        ax[0].plot(time, voltage, 'k', alpha=0.3)
        
        
        # get peak indexes
        peak_and_dipp_index = np.diff(np.sign(np.diff(voltage)))
        c = (peak_and_dipp_index < 0).nonzero()[0] +1
        c_min = (peak_and_dipp_index > 0).nonzero()[0] +1
        
        ax[0].plot(np.array(time)[c], np.array(voltage)[c], '*', ms=10, color='k' )
        ax[0].plot(np.array(time)[c_min], np.array(voltage)[c_min], 'o', ms=10, color='k' )
                                                              
        ax[2].set_xlabel('Time (ms)')                                                      
        ax[0].set_ylabel('Voltage (mV)')    
        ax[1].set_ylabel('normalized dynamics (mV)')  
        ax[2].set_ylabel('peak vm defl from baseline (mV)')                                             
        
        
        
        peaks = np.array(voltage)[c]
        dipps = np.array(voltage)[c_min]
        dipps = np.insert(dipps, 0, voltage[0])
        amp = peaks - dipps
        rel_amp = amp / amp[0]
        
        ta = params['expdata']
        ax[0].plot(np.multiply(ta['tTrace'],1000), np.multiply(ta['vTrace'],1000), color='r')
        t = np.multiply(ta['time'], 1000)
        ax[1].plot(t, np.divide(ta['amplitude'],ta['amplitude'][0]), 'o-', color='r', ms=5, mec='w', alpha=0.5)
        ax[1].plot(np.array(time)[c], rel_amp, '*', ms=10, color='k' )
        #ax[1].plot(np.array(time)[c], 
        ax[2].plot(t, np.multiply(ta['amplitude'], 1000), 'o-', color='r', ms=5, mec='w', alpha=0.5)
        ax[2].plot(np.array(time)[c], amp, '*', ms=10, color='k' )
        for a in ax:
            a.set_xlim([0,1100])
            a.set_xticks(range(100,601,200)+[1000])
        
        plt.savefig('synaptic_dynamics_%s_%s_%s_run%d.png' % (area, brain_half, cell_type,i) )   
        plt.close('all')
           
    


def plot_expdata(tag):
    
    plt.figure()
    
    files = glob.glob('expdata/%s*MSND1*' % (tag))
    
    colors = {}
    
    for f in files:
        
        with open(f) as file:
            data = json.load(file)
        
        if 'TH  ' in f:
            c='r'
        else: c='k'
        
        for entry in data:
            
            if c not in colors:
                if c == 'k':
                    l='crtx'
                else:
                    l='thal'
                colors[c] = 1
            else:
                l=''
                    
            
            ta = data[entry]['expdata']
            
            plt.plot(ta['time'], ta['amplitude'], 'o-', color=c, ms=5, mec='w', alpha=0.5, label=l) # np.divide(ta['amplitude'],ta['amplitude'][0])
    
    plt.legend(loc=1)
    plt.title(tag, fontsize=30)
    plt.ylabel('amplitude')
    plt.xlabel('time')
    plt.savefig('synaptic_dynamics_%s.png' % (tag) )
    plt.close('all')
    #plt.show()
    

if __name__ == '__main__':
    for tag in ['M1']:  #, 'S1', 'TH', 'trace']:
        #for tag in ['MSN', 'LTS', 'FS']:
        #plot_expdata(tag)
        pass
    main()
    #plt.show() 
