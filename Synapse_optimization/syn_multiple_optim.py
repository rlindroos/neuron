"""Expsyn synapse parameter fitting"""

# pylint: disable=R0914


import os

import bluepyopt as bpopt
import bluepyopt.ephys as ephys

import json
import numpy as np
import pickle
import cell_model, cell_evaluator
import matplotlib.pyplot as plt
from utils import *
import efel



def extract_features(t,y):
    # extracts electrical features from trace using bbp efel library
    
    # set interpolation frequency
    #efel.setDoubleSetting('interp_step', 0.05)
    efel.setDoubleSetting('DerivativeThreshold', 30.0)
    
    # code based on example example code from: https://github.com/BlueBrain/eFEL --------------------
    
    traces = []
    
    # loop over the 10 peaks and extract max vm for each.
    
    for i in range(10):
        # A 'trace' is a dictionary
        trace = {}

        # Set the 'T' (=time) key of the trace
        trace['T'] = t

        # Set the 'V' (=voltage) key of the trace
        trace['V'] = y
        
        # Set the 'stim_start' and end (in ms)
        # Warning: each need to be a list (with one element)
        start   = 200 + 20*(i)      # 50 hz stimulation -> 20 ms intervals
        end     = 200 + 20*(i+1)
        trace['stim_start'] = [start]
        trace['stim_end']   = [end]
        
        traces.append( trace )
    
    # Now we pass 'traces' to the efel and ask it to calculate the feature value (max vm)
    max_vm_peaks = efel.getFeatureValues( traces, ['maximum_voltage'] )
    
    return max_vm_peaks



def save_vector(x, y, outfile):
    '''
    save vectors to file.
    
    x       = x-vector
    y       = y-vector
    outfile = file name to be used
    '''
    
    with open(outfile, "w") as out:
        for time, y in zip(x, y):
            out.write("%g %g\n" % (time, y))



def main():
    """Main"""
    
    
    # sim parameters ------------------------------------------------------
    
    weight      = 650             # synaptic weight (*1e-4; uS)
    offspring   = 10            # offspring size (number of combinations run in each generation)
    ngen        = 1             # number of generations (max value)
    stimN       = 0             # synaptic activation pattern (optimized on 0, validated on 1-9)
    origin      = 'cortex'      # input origin: cortex or thalamus
    
    # ---------------------------------------------------------------------
    
    print '\norigin %s; weight %d; offspring %d; ngen %d; stim# %d\n' % (origin,weight,offspring,ngen,stimN)
    
    
    # setup
    morphofile = 'morphology/WT-P270-20-15ak-cor.swc'
    parameters = cell_model.define_parameters('parameters_freeze.json')
    mechanisms = cell_model.define_mechanisms('mechanisms.json')
    protocol_definitions  = json.load(open('config/protocols-val.json'))
    
    # simulator (neuron)
    nrn_sim = ephys.simulators.NrnSimulator(cvode_active=False)
    
    # def morph
    morphology = ephys.morphologies.NrnFileMorphology(
        morphofile, do_replace_axon=True)
    
    # def soma location   
    somatic_loc = ephys.locations.NrnSeclistLocation(
        'somatic',
        seclist_name='somatic')

    somacenter_loc = ephys.locations.NrnSeclistCompLocation(
        name='somacenter',
        seclist_name='somatic',
        sec_index=0,
        comp_x=0.5)
    
    
    with open('syn_stim_loc_lists.pkl', 'rb') as fp:
        sec2loop = pickle.load(fp)[stimN]    # <- chose stim list here! 0 by default
    
    sec_loc  = []
    for secID in sec2loop:
        
        sec_loc.append( ephys.locations.NrnSeclistCompLocation(
            name='secCenter%d'%(secID),
            seclist_name='basal',
            sec_index=secID,
            comp_x=0.5))

    expsyn_mech = ephys.mechanisms.NrnMODPointProcessMechanism(                     
        name='tmglut',                                                              
        suffix='tmGlut',                                                            
        locations=sec_loc)

    expsyn_loc = ephys.locations.NrnPointProcessLocation(
        'expsyn_loc',
        pprocess_mech=expsyn_mech)
    
    # optim parameters
    tau_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_tau',                                                          
        param_name='tau',                                                                
        bounds=[1,15],                                                             
        locations=[expsyn_loc])
    tauR_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_tauR',                                                          
        param_name='tauR',                                                                
        bounds=[15,45],                                                             
        locations=[expsyn_loc])
    tauF_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_tauF',                                                          
        param_name='tauF',                                                                 
        bounds=[30,90],                                                        
        locations=[expsyn_loc])
    U_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_U',                                                          
        param_name='U',                                                                 
        bounds=[0.85,0.95],                                                             
        locations=[expsyn_loc])
    q_param = ephys.parameters.NrnPointProcessParameter(                   
        name='tmglut_q',                                                          
        param_name='q',                                                                   
        bounds=[3,4.5],                                                             
        locations=[expsyn_loc])
    
    if origin == 'cortex':
        first_peak_vm = -82
    elif origin == 'thalamus':
        first_peak_vm = -84
        tauF_param.value=0,
        tauF_param.frozen=True,
        U_param.value=0.5,
        U_param.frozen=True,
    else:
        print 'error: origin must be set to "cortex" or "thalamus"'
        raise 
    
    parameters.append(tauR_param)
    parameters.append(tauF_param)
    parameters.append(U_param)
    parameters.append(q_param)

    #We first create a stimulus that injects the presynaptic events:
    stim_start  = 200
    number      = 10
    interval    = 1000.0/50    # Hz -> ISI (ms)
    
    stimuli = [ephys.stimuli.NrnNetStimStimulus(                                  
        total_duration=600,                                                      
        number=number,                                                                
        interval=interval,                                                              
        start=stim_start,                                                        
        weight=weight*1e-4,                                                             
        locations=[expsyn_loc])]
    
    # metaparam for setting weight of synapse
    
    metaparam = ephys.parameters.MetaParameter(
        name='meta',
        obj=stimuli[0],
        attr_name='weight',
        bounds=[0.5e-3, 1.2e-3])

    stim_end = stim_start*2 + interval*number

    cell = ephys.models.CellModel(
        'dspn', 
        morph=morphology, 
        mechs=mechanisms+[expsyn_mech], 
        params=parameters+[metaparam])
    
    # recordings -------------------------------------------------------
    rec = [ ephys.recordings.CompRecording(
        name='soma.v',
        location=somacenter_loc,
        variable='v') ]

    
    # set constant istim ------------------------------------------------
    protocol_definition = protocol_definitions.items()[0] # select first prot (same in all)
    stimulus_definition = protocol_definition[1]['stimuli'][1]
    stimuli.append(ephys.stimuli.NrnSquarePulse(
        step_amplitude=stimulus_definition['amp'],
        step_delay=stimulus_definition['delay'],
        step_duration=stimulus_definition['duration'],
        location=somacenter_loc,
        total_duration=600))

    protocol = ephys.protocols.SweepProtocol(
        'netstim_protocol',
        stimuli,
        rec)
    
    # TODO update taraget values and std's
    max_volt_features = []
    max_volt_objectives = []
    #means = [-82,-81.5,-81,-80.5,-81,-81.5,-80.5,-81,-81.2]
    means = [first_peak_vm]
    first_peak_amp = first_peak_vm - (-86.8)
    for i in range(2,11):
        if origin == 'cortex':
            percent_1st = 28 + 100*(0.7/(1+np.exp((i-2.0)/-1.5))+0.55/(1+np.exp((i-7)/3.0)))
            means.append( first_peak_vm + first_peak_amp*(percent_1st/100.0-1) )
        elif origin == 'thalamus':
            percent_1st = 46 + 82.0/(1+np.exp((i-2)/1.5))
            means.append( first_peak_vm + first_peak_amp*(percent_1st/100.0-1) ) # TODO: check
    stds  = [0.001, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]       # [0.1]*9     #
    print stds
    print means
    for i in range(number):
        # calc start and end time for interval
        start   = stim_start + interval*i
        end     = stim_start + interval*(i+1)
        # calc target mean and std
        mean = means[i-1]
        std  = stds[i-1] 
        # create feature and objective
        max_volt_features.append( ephys.efeatures.eFELFeature(
            '%dpeak' % i,
            efel_feature_name='maximum_voltage',
            recording_names={'': 'soma.v'},
            stim_start=start,
            stim_end=end,
            exp_mean=mean,
            exp_std=std))
        max_volt_objectives.append( ephys.objectives.SingletonObjective(
            max_volt_features[i-1].name,
            max_volt_features[i-1]))

    score_calc = ephys.objectivescalculators.ObjectivesCalculator(max_volt_objectives)
    
    # TODO plot opt params and freeze all but synaptic values!
    opt_params = [p.name for p in cell.params.values() if not p.frozen]
    cell_evaluator = ephys.evaluators.CellEvaluator(
        cell_model=cell,
        param_names=opt_params,
        fitness_protocols={protocol.name: protocol},
        fitness_calculator=score_calc,
        sim=nrn_sim)
    
    
    # Optimize
    optimisation = bpopt.optimisations.DEAPOptimisation(                         
    evaluator=cell_evaluator,                                                
    offspring_size=offspring)                                                       

    _, hall_of_fame, _, _ = optimisation.run(max_ngen=ngen)
    
    # plot best individuals
    plt.style.use('ggplot')
    fig,ax = plt.subplots(2,1, figsize=(10,8) )
    best = {}
    for i in range(5):   
        best_ind = hall_of_fame[i]                                                   
        
        print
        print 'Best individual: ', best_ind                                          
        print 'Fitness values: ', best_ind.fitness.values 
        
        best_ind_dict = cell_evaluator.param_dict(best_ind)                          
        responses = protocol.run(                                                    
            cell_model=cell,                                                         
            param_values=best_ind_dict,                                              
            sim=nrn_sim)                                                             

        time    = responses['soma.v']['time']                                           
        voltage = responses['soma.v']['voltage']    
        
        # get peak values and plot vs experimental curve
        max_vm_peaks = extract_features(time,voltage)
        
        first_peak = max_vm_peaks[0]['maximum_voltage'][0]
        normalized = []
        for j in range(10):
            normalized.append( 100.0*(max_vm_peaks[j]['maximum_voltage'][0]+86.8)/(first_peak+86.8) )
        
        # add features to dict
        best[i] = {'params':best_ind, 'vm':voltage, 'time':time, 'peaks':{'abs':max_vm_peaks, 'rel':normalized} }                                 
                                                   
        ax[0].plot(time, voltage)    
        ax[1].plot(range(1,11), normalized, '-o', mec='w', label=str(i)) 
                                                         
    ax[0].set_xlabel('Time (ms)')                                                      
    ax[0].set_ylabel('Voltage (ms)')   
    x = np.arange(1,11)
    if origin == 'cortex':
        y = 28 + 100*(0.7/(1+np.exp((x-2.0)/-1.5))+0.55/(1+np.exp((x-7)/3.0)))
        ax[1].plot(x,y, color='brown', label='fit', ls='--')
        ax[1].plot(*np.loadtxt('ding2010_crtx_relative.csv', unpack=True), color='k', ls='--', label='relative')
    elif origin == 'thalamus':
        y = 46 + 82.0/(1+np.exp((x-2)/1.5))
        ax[1].plot(x,y, color='brown', label='fit', ls='--')
        ax[1].plot(*np.loadtxt('ding2010_thalamic_relative.csv', unpack=True), color='k', ls='--', label='relative')
    ax[1].set_ylim([0,180])
    ax[1].legend(loc=3, ncol=4)
    name_base = 'results/optim_syn_%s_g%de-4_stim%d_ngen%d_offspring%d' % (origin,weight,stimN,ngen,offspring)
    fig.savefig('%s.png' % name_base)                                                
    plt.show()  
    
    print dir(best_ind)
    print best_ind
    
    with open('%s.pkl' % name_base, 'wb') as fp:
        pickle.dump(best, fp, pickle.HIGHEST_PROTOCOL)
        
    best_ofThe_best = []
    for record in hall_of_fame:
        params = cell_evaluator.param_dict(record)
        best_ofThe_best.append(params)
    with open('%s.json' % name_base, 'w') as fp:
        json.dump(best_ofThe_best, fp, indent=4, sort_keys=True)
    

if __name__ == '__main__':
    main()
