#! /usr/bin/python

'''
read trace and quantify certain aspects of the trace 
such as spike amp, width and ahp deapt.

'''

import numpy as np
import matplotlib.pyplot as plt
import glob

#from scipy.signal import find_peaks_cwt

import peakutils

#import scipy.fftpack as fft



def quantify(fileNames):
    
    '''
    load file, one at a time.
    find spike threshold by second derivative.
    quantify certain aspects of spike trace (amp, width, ahp, ISI etc)
    return as file name lists (of lists)
    '''
    
    AHP = []
    s   = []
    
    for f in fileNames:
        
        # load spike train data
        x, y    = np.loadtxt(f, unpack=True)
        
        # find spike threshold
        '''
        dy      = np.gradient( y)
        ddy     = np.gradient(dy)
        dddy    = np.gradient(ddy)
        ddddy   = np.gradient(dddy)
        '''
        
        
        # get peak index
        indexes     = peakutils.indexes(y)
        
        # subtract x positions to get threshold--hard coded...
        subtract    = 25
        
        # split on threshold points
        splitTrace  = np.split(y, indexes-subtract)
        
        # EXTRACT VALUES:
        
        if indexes.size != 0:
            # depth of first AHP 
            
            ahpDepth    = np.abs(splitTrace[1][0] - min(splitTrace[1]))
            
            # store results
            AHP.append(ahpDepth)
            s.append(indexes[0])
            
        else:
            AHP.append(None)
            s.append(None)
        
    # return values
    return [AHP, s]
    
    
    

    
    #interpolatedIndexes = peakutils.interpolate(range(0, len(ddddy)), ddddy, ind=indexes)
    
    #indexes = find_peaks_cwt(ddy, np.arange(1, 550))
    '''
    plt.figure()
    plt.plot(x, y )
    #plt.plot(x, ddy-55, lw=2, color='k' )
    plt.plot(x[indexes-25], y[indexes-25], 'k|', ms=100)
    plt.show()
    '''
    
    
def show():
    
    #tauh = [0.14, 0.19, 0.24, 0.29, 0.34, 0.39, 0.44, 0.49, 0.54]
    taum = [2.5, 3.5, 4.5, 6.5, 7.5] #[52, 54, 56, 58, 60, 62, 64] #0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.10, 0.11, 0.12, 0.13, 0.14, 0.15
    
    for tau in taum:
        
        fs      = 'naf_tau3/vm_control_0.4_([1,8]*,' + str(tau) + ',-33).out'
        files = sorted(glob.glob(fs))
        
        N = len(files)
        
        gradient  = [(1-1.0*x/(N-1), 0, 1.0*x/(N-1)) for x in range(N)]
        
        plt.figure(figsize=(10,8))
        for i,f in enumerate(files):
            print f
            split = f.split(',')
            label = split[0][-2:] + '_' + split[1][:5] + '_' + split[2][:5]
            plt.plot(*np.loadtxt(f, unpack=True),label=label, color=gradient[i], lw=3)
                
        plt.legend(loc=2)
    
    plt.show()
            
def plot_gates():
    
    '''
    plots gate values
    '''
    
    files = glob.glob('naf_t*_[t,s]*.txt')
    labels = {}
    for f in files:
        
        x,y = np.loadtxt(f, unpack=True)
        
        y = np.power(y,3)
        
        split = f.split('_')
        
        if split[1] == 'th':
            c = 'r'
            lw = 1
            if split[1] in labels:
                l = ''
            else:
                l = split[1]
                labels[split[1]] = []
        elif split[2][0] == 's':
            c = 'b'
            lw = 3
            l = 'fig 4C'
            plt.plot(x, np.log(np.power(10,y)) , color='k', lw=5, label='10->e')
        else:
            c = 'g'
            lw = 1
            if split[1] in labels:
                l = ''
            else:
                l = split[1]
                labels[split[1]] = []
        
        
        plt.plot(x, np.exp(y) , color=c, lw=lw, label=l)  
    
    v = np.arange(-90, 45)   
    v_h = np.arange(-90,-60)
    v_d = np.arange(-61, 45) 
    plt.plot(v_h, (0.015*v_h+3.4), color='k')
    plt.plot(v_d, (0.56+1.1/(1+np.exp((v_d-(-48))/15.0))+\
                      1.2/(1+np.exp((v_d-(-48))/4.0))),\
        color='k', label='fit th')
    plt.plot(v, (0.38+1/(0.6*np.exp((v-(-58.0))/8.0)+\
                         1.8*np.exp((v-(-58.0))/(-35.0)))), '--',\
        color='k', label='fit tm') 
    
    plt.plot(v, (0.09+1/(0.6*np.exp((v-(-58.0))/8.0)+1.8*np.exp((v-(-58.0))/(-33.0)))), '-.',\
        color='k', label='old tm') 
    plt.plot(v, (0.34+1.2/(1+np.exp((v-(-32))/4.5))), '-.',\
        color='k', label='old th')
    #plt.ylim([0.2,1])
    #v_w = np.arange(-40,45)
    #plt.plot(v_w, (0.02 + 0.145 * np.exp( (-v_w + 40.0 ) / 10.0)), color='k', lw=3)
    plt.plot(v, ( 0.4+1.0/(np.exp((v-(-41.0))/4.9)+np.exp((v-(-150.0))/(-42.0))) ),
        '-.', color='m', label='alex th', lw=2)
    ratio = (0.56+1.1/(1+np.exp((v-(-48))/15.0))+\
                      1.2/(1+np.exp((v-(-48))/4.0))) / \
                      (0.38+1/(0.6*np.exp((v-(-58.0))/8.0)+\
                         1.8*np.exp((v-(-58.0))/(-35.0))))
    plt.plot(v, ratio, 'm', lw=3)
    
    # wolf
    #v = [0.06, 0.06, 0.07,0.09,0.11,0.13,0.20,0.32,0.16,0.15,0.12,0.08,0.06,0.06,0.06,0.06]
    
    
    plt.legend()  
    plt.show() 
    
    
    
def fit2trace():
    
    plt.plot(*np.loadtxt('naf_th_0.txt', unpack=True))
    t = np.arange(0.5,6, 0.1)
    plt.plot(t, 8*np.exp(-(t-0.2)/0.43) )
    plt.show()
    
          
            
