TITLE T-type calcium channel (Cav3.3)

UNITS {
    (mV) = (millivolt)
    (mA) = (milliamp)
    (S) = (siemens)
    (molar) = (1/liter)
    (mM) = (millimolar)
    FARADAY = (faraday) (coulomb)
    R = (k-mole) (joule/degC)
}

NEURON {
    SUFFIX cav33
    USEION cal READ cali, calo WRITE ical VALENCE 2
    RANGE pbar, ical, mshift, hshift
}

PARAMETER {
    pbar = 6.7e-6 (cm/s)
    mvhalf = -78.01 (mV)
    mslope = -5.472 (mV)
    mshift = -5 (mV)
    hvhalf = -78.3 (mV)
    hslope = 6.5 (mV)
    hshift = -5 (mV)
    q = 1

}

ASSIGNED { 
    v (mV)
    ical (mA/cm2)
    ecal (mV)
    celsius (degC)
    cali (mM)
    calo (mM)
    minf
    hinf
    mtau (ms)
    htau (ms)
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    ical = ghk(v, cali, calo)*pbar*m*m*h
}

INITIAL {
    rates(v)
    m = minf
    h = hinf
}

DERIVATIVE states { 
    rates(v)
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}

PROCEDURE rates(v (mV)) {
    minf = 1/(1+exp((v-mvhalf-mshift)/mslope))
    hinf = 1/(1+exp((v-hvhalf-hshift)/hslope))
    mtau = 6.1+35.3/(1+exp((v-(-58.7))/8.1))
    htau = 120+1/(0.003*exp((v-(-85.8))/19.3)+0.003*exp(-(v-(-85.8))/19.3))
}

FUNCTION ghk(v (mV), ci (mM), co (mM)) (.001 coul/cm3) {
    LOCAL z, eci, eco
    z = (1e-3)*2*FARADAY*v/(R*(celsius+273.15))
    if(z == 0) {
        z = z+1e-6
    }
    eco = co*(z)/(exp(z)-1)
    eci = ci*(-z)/(exp(-z)-1)
    ghk = (1e-3)*2*FARADAY*(eci-eco)
}

COMMENT 

Original data by Iftinca (2006), rat, 21 C, q_10~=1.0.

Genesis implementation by Kai Du <kaidu828@gmail.com>, m*h, also corrected
for m^2*h.

Revised Genesis model by Robert Lindroos <robert.lindroos@ki.se>, m^2*h.

NEURON implementation by Alexander Kozlov <akozlov@nada.kth.se>, smooth
fit of mtau and htau.

ENDCOMMENT
