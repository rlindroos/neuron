TITLE T-type calcium channel (Cav3.2)

UNITS {
    (mV) = (millivolt)
    (mA) = (milliamp)
    (S) = (siemens)
    (molar) = (1/liter)
    (mM) = (millimolar)
    FARADAY = (faraday) (coulomb)
    R = (k-mole) (joule/degC)
}

NEURON {
    SUFFIX cav32
    USEION cal READ cali, calo WRITE ical VALENCE 2
    RANGE pbar, ical, mshift, hshift
}

PARAMETER {
    pbar = 6.7e-6 (cm/s)
    mvhalf = -42.9 (mV)
    mslope = -6.2 (mV)
    mshift = -5 (mV)
    hvhalf = -64.2 (mV)
    hslope = 8.8 (mV)
    hshift = -5 (mV)
    q = 3
}

ASSIGNED { 
    v (mV)
    ical (mA/cm2)
    ecal (mV)
    celsius (degC)
    cali (mM)
    calo (mM)
    minf
    hinf
    mtau (ms)
    htau (ms)
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    ical = ghk(v, cali, calo)*pbar*m*h
}

INITIAL {
    rates(v)
    m = minf
    h = hinf
}

DERIVATIVE states { 
    rates(v)
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}

PROCEDURE rates(v (mV)) {
    minf = 1/(1+exp((v-mvhalf-mshift)/mslope))
    hinf = 1/(1+exp((v-hvhalf-hshift)/hslope))
    mtau = 2.2+22.6/(1+exp((v-(-58.8))/6.3))
    htau = 32.7+1/(0.002*exp((v-(-62.4))/10.7)+0.002*exp(-(v-(-62.4))/(5*10.7)))
}

FUNCTION ghk(v (mV), ci (mM), co (mM)) (.001 coul/cm3) {
    LOCAL z, eci, eco
    z = (1e-3)*2*FARADAY*v/(R*(celsius+273.15))
    if(z == 0) {
        z = z+1e-6
    }
    eco = co*(z)/(exp(z)-1)
    eci = ci*(-z)/(exp(-z)-1)
    ghk = (1e-3)*2*FARADAY*(eci-eco)
}

COMMENT 

Original data by Iftinca (2006), rat, 21 C.

Genesis implementation by Kai Du <kaidu828@gmail.com>, m*h and m^2*h.

Revised Genesis model by Robert Lindroos <robert.lindroos@ki.se>, m*h.

NEURON implementation by Alexander Kozlov <akozlov@nada.kth.se>, smooth
fit of mtau and htau.

ENDCOMMENT
