TITLE LVA L-type calcium channel (Cav1.3)

UNITS {
    (mV) = (millivolt)
    (mA) = (milliamp)
    (S) = (siemens)
    (molar) = (1/liter)
    (mM) = (millimolar)
    FARADAY = (faraday) (coulomb)
    R = (k-mole) (joule/degC)
}

NEURON {
    SUFFIX cal13
    USEION cal READ cali, calo WRITE ical VALENCE 2
    RANGE pbar, ical
}

PARAMETER {
    pbar = 1.7e-6 (cm/s)
    mvhalf = -33 (mV)
    mslope = -6.7 (mV)
    vm = -8.124 (mV)
    k = 9.005 (mV)
    kpr = 31.4 (mV)
    c = 0.0398 (/ms-mV)
    cpr = 0.99 (/ms)
    hvhalf = -13.4 (mV)
    hslope = 11.9 (mV)
    :htau = 44.3 (ms) : Wolf (2005)
    htau = 44.3 (ms) : Evans (2012)
    :q = 3 : Wolf (2005)
    q = 2 : Evans (2012)
}

ASSIGNED { 
    v (mV)
    ical (mA/cm2)
    ecal (mV)
    celsius (degC)
    cali (mM)
    calo (mM)
    minf
    mtau (ms)
    hinf
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    ical  = ghk(v, cali, calo)*pbar*m*m*h 
}

INITIAL {
    rates(v)
    m = minf
    h = hinf
}
DERIVATIVE states {  
    rates(v)
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}


PROCEDURE rates(v (mV)) {
    LOCAL malpha, mbeta
     minf = 1/(1+exp((v-mvhalf)/mslope))
     hinf = 1/(1+exp((v-hvhalf)/hslope))
     malpha = c*(v-vm)/(exp((v-vm)/k)-1)
     mbeta = cpr*exp(v/kpr)
     mtau = 1/(malpha+mbeta)
}

FUNCTION ghk(v (mV), ci (mM), co (mM)) (.001 coul/cm3) {
    LOCAL z, eci, eco
    z = (1e-3)*2*FARADAY*v/(R*(celsius+273.15))
    if(z == 0) {
        z = z+1e-6
    }
    eco = co*(z)/(exp(z)-1)
    eci = ci*(-z)/(exp(-z)-1)
    ghk = (1e-3)*2*FARADAY*(eci-eco)
}

COMMENT 

Original NEURON model by Wolf (2005), rat nucleus accumbens, 22 C.

Genesis implementation by Evans (2012), htau and q-factor adjusted.

NEURON implementation by Alexander Kozlov <akozlov@nada.kth.se>.

ENDCOMMENT
