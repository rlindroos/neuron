TITLE Calcium dynamics for N, P/Q, R calcium pool

NEURON {
    SUFFIX cadyn
    USEION ca READ ica, cai WRITE cai
    RANGE pump, cainf, taur, drive, kb
}

UNITS {
    (molar) = (1/liter) 
    (mM) = (millimolar)
    (um) = (micron)
    (mA) = (milliamp)
    (msM) = (ms mM)
    FARADAY = (faraday) (coulomb)
}

PARAMETER {
    drive = 10000 (1)
    depth = 0.1 (um)
    cainf = 50e-6 (mM)
    taur = 25 (ms)
    kb = 96 : 200 in soma
    kt = 1e-4 (mM/ms)
    kd = 1e-4 (mM)
    pump = 0.02
}

STATE { cai (mM) }

INITIAL { cai = cainf }

ASSIGNED {
    ica (mA/cm2)
    drive_channel (mM/ms)
    drive_pump (mM/ms)
}
    
BREAKPOINT {
    SOLVE state METHOD cnexp
}

DERIVATIVE state { 
    drive_channel = -drive*ica/(2*FARADAY*depth*(1+kb))
    if (drive_channel <= 0.) { drive_channel = 0. }
    drive_pump = -kt*cai/(cai+kd)
    cai' = (drive_channel+pump*drive_pump+(cainf-cai)/taur)
}

COMMENT

Original model by Wolf (2005) and Destexhe (1992).

Ca shell parameters by Evans (2012), with kb but without pump.

NEURON implementation by Alexander Kozlov <akozlov@nada.kth.se>.

ENDCOMMENT
