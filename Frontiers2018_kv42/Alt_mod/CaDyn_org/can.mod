TITLE N-type calcium channel

UNITS {
    (mV) = (millivolt)
    (mA) = (milliamp)
    (S) = (siemens)
    (molar) = (1/liter)
    (mM) = (millimolar)
    FARADAY = (faraday) (coulomb)
    R = (k-mole) (joule/degC)
}

NEURON {
    SUFFIX can
    USEION ca READ cai, cao WRITE ica
    RANGE pbar, ica
}

PARAMETER {
    pbar = 0.0 (cm/s)
    a = 0.21
    :q = 3 : Wolf (2005)
    q = 2 : Evans (2012)
}

ASSIGNED {
    v (mV)
    ica (mA/cm2)
    celsius (degC)
    cai (mM)
    cao (mM)
    minf
    mtau (ms)
    hinf
    htau (ms)
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    :ica = pbar*m*m*(a*h+(1-a))*ghk(v, cai, cao) : Wolf (2005)
    ica =  pbar*m*(a*h+(1-a))*ghk(v, cai, cao) : Evans (2012)
}

INITIAL {
    rates(v)
    m = minf
    h = hinf
}

DERIVATIVE states {
    rates(v)
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}

PROCEDURE rates(v (mV)) {
    LOCAL alpha, beta
    minf = 1/(1+exp((v-(-8.7))/(-7.4)))
    alpha = 0.03856*(v-(-17.19))/(exp((v-(-17.19))/15.22)-1)
    beta = 0.3842*exp(v/23.82)
    mtau = 1/(alpha+beta)
    hinf = 1/(1+exp((v-(-74.8))/6.5))
    htau = 70
}

FUNCTION ghk(v (mV), ci (mM), co (mM)) (.001 coul/cm3) {
    LOCAL z, eci, eco
    z = (1e-3)*2*FARADAY*v/(R*(celsius+273.15))
    if(z == 0) {
        z = z+1e-6
    }
    eco = co*(z)/(exp(z)-1)
    eci = ci*(-z)/(exp(-z)-1)
    ghk = (1e-3)*2*FARADAY*(eci-eco)
}

COMMENT

Original data by Shen (2004), diss MSN, rat, room temp.

Original NEURON model by Wolf (2005).

Genesis implementation by Evans (2012) and Kai Du <kai.du@ki.se>,
MScell v9.5.

NEURON implementation by Alexander Kozlov <akozlov@csc.kth.se>.

ENDCOMMENT
