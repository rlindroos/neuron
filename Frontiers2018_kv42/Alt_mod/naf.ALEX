TITLE Fast transient sodium current

NEURON {
    SUFFIX naf
    USEION na READ ena WRITE ina
    RANGE gbar, gna, ina, q, shift
    POINTER pka
}

UNITS {
    (S) = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

PARAMETER {
    gbar = 0.0 (S/cm2) 
    :q = 1	: room temperature 22 C
    q = 1.2	: body temperature 35 C (Evans 2012)
    shift = 0
}

ASSIGNED {
    v (mV)
    ena (mV)
    ina (mA/cm2)
    gna (S/cm2)
    minf
    mtau (ms)
    hinf
    htau (ms)
    pka (1)
}

STATE { m h }

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = (1-0.001*(pka-11.5))*gbar*m*m*m*h
    ina = gna*(v-ena)
}

DERIVATIVE states {
    rates()
    m' = (minf-m)/mtau*q
    h' = (hinf-h)/htau*q
}

INITIAL {
    rates()
    m = minf
    h = hinf
}

PROCEDURE rates() {
    UNITSOFF
    :minf = 1/(1+exp((v-(-25))/(-10)))
    minf = 1/(1+exp((v-(-25)-0)/(-10)))
    :mtau = 0.09+1/(1.1*exp((v-(-57.6))/8)+0.9*exp((v-(-58))/(-13)))
    mtau = 0.09+1/(exp((v-(-58))/8)+exp((v-(-59))/(-13)))
    :hinf = 1/(1+exp((v-(-62))/6))
    hinf = 1/(1+exp((v-(-62)-0)/6))
    :htau = 0.4+1/(1.6*exp((v-(-39))/4.9)+0.1*exp((v-(-53))/(-42)))
    htau = 0.4+1/(exp((v-(-41))/4.9)+exp((v-(-150))/(-42)))
    UNITSON
}

COMMENT

-shift parameters removed; no shift applied.

Original data by Ogata and Tatebayashi (1990) [1]. Neostriatal neurons
of medium size (putative medium spiny neurons) freshly isolated from
the adult guinea pig brain (either sex, 200 g). Data compensated for
the liquid junction potential (-13 mV). Experiments carried out at room
temperature (22 C). Conductance fitted by m3h kinetics.

Smooth fit of mtau and htau data [1] by Alexander Kozlov <akozlov@kth.se>.
Time constants were adjusted to body temperature with factor q=1.2 [2].

[1] Ogata N, Tatebayashi H (1990) Sodium current kinetics in freshly
isolated neostriatal neurones of the adult guinea pig. Pflugers Arch
416(5):594-603.

[2]  Evans RC, Morera-Herreras T, Cui Y, Du K, Sheehan T, Kotaleski JH,
Venance L, Blackwell KT (2012) The effects of NMDA subunit composition on
calcium influx and spike timing-dependent plasticity in striatal medium
spiny neurons. PLoS Comput Biol 8(4):e1002493.

ENDCOMMENT
